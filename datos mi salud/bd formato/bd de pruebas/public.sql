/*
Navicat PGSQL Data Transfer

Source Server         : postgres
Source Server Version : 90501
Source Host           : localhost:5432
Source Database       : misalud-pruebas
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90501
File Encoding         : 65001

Date: 2017-11-13 14:04:11
*/


-- ----------------------------
-- Sequence structure for afacturar_historial_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."afacturar_historial_id_seq";
CREATE SEQUENCE "public"."afacturar_historial_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for afacturar_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."afacturar_id_seq";
CREATE SEQUENCE "public"."afacturar_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for app_perfil_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."app_perfil_id_seq";
CREATE SEQUENCE "public"."app_perfil_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."app_perfil_id_seq"', 8, true);

-- ----------------------------
-- Sequence structure for app_usuario_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."app_usuario_id_seq";
CREATE SEQUENCE "public"."app_usuario_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."app_usuario_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for audits_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."audits_id_seq";
CREATE SEQUENCE "public"."audits_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 109
 CACHE 1;
SELECT setval('"public"."audits_id_seq"', 109, true);

-- ----------------------------
-- Sequence structure for banco_tipo_cuenta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."banco_tipo_cuenta_id_seq";
CREATE SEQUENCE "public"."banco_tipo_cuenta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."banco_tipo_cuenta_id_seq"', 3, true);

-- ----------------------------
-- Sequence structure for bancos_empresa_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."bancos_empresa_id_seq";
CREATE SEQUENCE "public"."bancos_empresa_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."bancos_empresa_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for bancos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."bancos_id_seq";
CREATE SEQUENCE "public"."bancos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 13
 CACHE 1;
SELECT setval('"public"."bancos_id_seq"', 13, true);

-- ----------------------------
-- Sequence structure for beneficiarios_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."beneficiarios_id_seq";
CREATE SEQUENCE "public"."beneficiarios_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."beneficiarios_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for beneficiarios_temp_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."beneficiarios_temp_id_seq";
CREATE SEQUENCE "public"."beneficiarios_temp_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ciudades_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ciudades_id_seq";
CREATE SEQUENCE "public"."ciudades_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 498
 CACHE 1;
SELECT setval('"public"."ciudades_id_seq"', 498, true);

-- ----------------------------
-- Sequence structure for cobros_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."cobros_id_seq";
CREATE SEQUENCE "public"."cobros_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 13
 CACHE 1;
SELECT setval('"public"."cobros_id_seq"', 13, true);

-- ----------------------------
-- Sequence structure for configuracion_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."configuracion_id_seq";
CREATE SEQUENCE "public"."configuracion_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."configuracion_id_seq"', 8, true);

-- ----------------------------
-- Sequence structure for contrato_tipo_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contrato_tipo_id_seq";
CREATE SEQUENCE "public"."contrato_tipo_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."contrato_tipo_id_seq"', 3, true);

-- ----------------------------
-- Sequence structure for contratos_detalles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contratos_detalles_id_seq";
CREATE SEQUENCE "public"."contratos_detalles_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 14
 CACHE 1;
SELECT setval('"public"."contratos_detalles_id_seq"', 14, true);

-- ----------------------------
-- Sequence structure for contratos_facturar_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contratos_facturar_id_seq";
CREATE SEQUENCE "public"."contratos_facturar_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for contratos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contratos_id_seq";
CREATE SEQUENCE "public"."contratos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."contratos_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for contratos_temp_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."contratos_temp_id_seq";
CREATE SEQUENCE "public"."contratos_temp_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for controlfacturacion_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."controlfacturacion_id_seq";
CREATE SEQUENCE "public"."controlfacturacion_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for empresa_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."empresa_id_seq";
CREATE SEQUENCE "public"."empresa_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."empresa_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for estados_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."estados_id_seq";
CREATE SEQUENCE "public"."estados_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 25
 CACHE 1;
SELECT setval('"public"."estados_id_seq"', 25, true);

-- ----------------------------
-- Sequence structure for estatus_contrato_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."estatus_contrato_id_seq";
CREATE SEQUENCE "public"."estatus_contrato_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 6
 CACHE 1;
SELECT setval('"public"."estatus_contrato_id_seq"', 6, true);

-- ----------------------------
-- Sequence structure for frecuencia_pagos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."frecuencia_pagos_id_seq";
CREATE SEQUENCE "public"."frecuencia_pagos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 13
 CACHE 1;
SELECT setval('"public"."frecuencia_pagos_id_seq"', 13, true);

-- ----------------------------
-- Sequence structure for libro_historial_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."libro_historial_id_seq";
CREATE SEQUENCE "public"."libro_historial_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for libro_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."libro_id_seq";
CREATE SEQUENCE "public"."libro_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for lotes_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."lotes_id_seq";
CREATE SEQUENCE "public"."lotes_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 60
 CACHE 1;
SELECT setval('"public"."lotes_id_seq"', 60, true);

-- ----------------------------
-- Sequence structure for migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
CREATE SEQUENCE "public"."migrations_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 52
 CACHE 1;
SELECT setval('"public"."migrations_id_seq"', 52, true);

-- ----------------------------
-- Sequence structure for movimientos_historial_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."movimientos_historial_id_seq";
CREATE SEQUENCE "public"."movimientos_historial_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for movimientos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."movimientos_id_seq";
CREATE SEQUENCE "public"."movimientos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for municipios_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."municipios_id_seq";
CREATE SEQUENCE "public"."municipios_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 335
 CACHE 1;
SELECT setval('"public"."municipios_id_seq"', 335, true);

-- ----------------------------
-- Sequence structure for parentesco_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."parentesco_id_seq";
CREATE SEQUENCE "public"."parentesco_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 35
 CACHE 1;
SELECT setval('"public"."parentesco_id_seq"', 35, true);

-- ----------------------------
-- Sequence structure for parroquias_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."parroquias_id_seq";
CREATE SEQUENCE "public"."parroquias_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1139
 CACHE 1;
SELECT setval('"public"."parroquias_id_seq"', 1139, true);

-- ----------------------------
-- Sequence structure for personas_bancos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."personas_bancos_id_seq";
CREATE SEQUENCE "public"."personas_bancos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."personas_bancos_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for personas_correo_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."personas_correo_id_seq";
CREATE SEQUENCE "public"."personas_correo_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 17
 CACHE 1;
SELECT setval('"public"."personas_correo_id_seq"', 17, true);

-- ----------------------------
-- Sequence structure for personas_detalles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."personas_detalles_id_seq";
CREATE SEQUENCE "public"."personas_detalles_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."personas_detalles_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for personas_direccion_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."personas_direccion_id_seq";
CREATE SEQUENCE "public"."personas_direccion_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."personas_direccion_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for personas_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."personas_id_seq";
CREATE SEQUENCE "public"."personas_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 18
 CACHE 1;
SELECT setval('"public"."personas_id_seq"', 18, true);

-- ----------------------------
-- Sequence structure for personas_telefono_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."personas_telefono_id_seq";
CREATE SEQUENCE "public"."personas_telefono_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 17
 CACHE 1;
SELECT setval('"public"."personas_telefono_id_seq"', 17, true);

-- ----------------------------
-- Sequence structure for plan_detalles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."plan_detalles_id_seq";
CREATE SEQUENCE "public"."plan_detalles_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 10
 CACHE 1;
SELECT setval('"public"."plan_detalles_id_seq"', 10, true);

-- ----------------------------
-- Sequence structure for plan_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."plan_id_seq";
CREATE SEQUENCE "public"."plan_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."plan_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for profesion_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."profesion_id_seq";
CREATE SEQUENCE "public"."profesion_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."profesion_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for sectores_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sectores_id_seq";
CREATE SEQUENCE "public"."sectores_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."sectores_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for solicitud_beneficiarios_temp_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."solicitud_beneficiarios_temp_id_seq";
CREATE SEQUENCE "public"."solicitud_beneficiarios_temp_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for solicitudes_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."solicitudes_id_seq";
CREATE SEQUENCE "public"."solicitudes_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for sucursal_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sucursal_id_seq";
CREATE SEQUENCE "public"."sucursal_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."sucursal_id_seq"', 4, true);

-- ----------------------------
-- Sequence structure for tipo_persona_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tipo_persona_id_seq";
CREATE SEQUENCE "public"."tipo_persona_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 4
 CACHE 1;
SELECT setval('"public"."tipo_persona_id_seq"', 4, true);

-- ----------------------------
-- Sequence structure for tipo_telefono_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tipo_telefono_id_seq";
CREATE SEQUENCE "public"."tipo_telefono_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."tipo_telefono_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for vendedores_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."vendedores_id_seq";
CREATE SEQUENCE "public"."vendedores_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 16
 CACHE 1;
SELECT setval('"public"."vendedores_id_seq"', 16, true);

-- ----------------------------
-- Table structure for afacturar
-- ----------------------------
DROP TABLE IF EXISTS "public"."afacturar";
CREATE TABLE "public"."afacturar" (
"id" int4 DEFAULT nextval('afacturar_id_seq'::regclass) NOT NULL,
"ci" varchar(191) COLLATE "default" NOT NULL,
"fecha" date NOT NULL,
"total" numeric(10,2) NOT NULL,
"correlativo" int4,
"sucursal_id" int4 NOT NULL,
"controlfacturacion_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of afacturar
-- ----------------------------

-- ----------------------------
-- Table structure for afacturar_historial
-- ----------------------------
DROP TABLE IF EXISTS "public"."afacturar_historial";
CREATE TABLE "public"."afacturar_historial" (
"id" int4 DEFAULT nextval('afacturar_historial_id_seq'::regclass) NOT NULL,
"ci" varchar(191) COLLATE "default" NOT NULL,
"fecha" date NOT NULL,
"total" numeric(10,2) NOT NULL,
"correlativo" int4,
"sucursal_id" int4 NOT NULL,
"controlfacturacion_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of afacturar_historial
-- ----------------------------

-- ----------------------------
-- Table structure for app_perfil
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_perfil";
CREATE TABLE "public"."app_perfil" (
"id" int4 DEFAULT nextval('app_perfil_id_seq'::regclass) NOT NULL,
"nombre" varchar(50) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_perfil
-- ----------------------------
INSERT INTO "public"."app_perfil" VALUES ('1', 'Vendedor', '2017-11-09 22:26:32', '2017-11-09 22:37:54', '2017-11-09 22:38:23');
INSERT INTO "public"."app_perfil" VALUES ('2', 'Administrador', '2017-11-09 22:26:32', '2017-11-09 22:36:39', null);
INSERT INTO "public"."app_perfil" VALUES ('3', 'Tecnico', '2017-11-09 22:26:32', '2017-11-09 22:26:32', '2017-11-09 22:38:18');
INSERT INTO "public"."app_perfil" VALUES ('4', 'Supervisor', '2017-11-09 22:26:32', '2017-11-09 22:26:32', '2017-11-09 22:38:14');
INSERT INTO "public"."app_perfil" VALUES ('5', 'Analista', '2017-11-09 22:26:32', '2017-11-09 22:36:05', null);
INSERT INTO "public"."app_perfil" VALUES ('6', 'Secretaria', '2017-11-09 22:26:32', '2017-11-09 22:26:32', '2017-11-09 22:38:08');
INSERT INTO "public"."app_perfil" VALUES ('7', 'Administracion', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."app_perfil" VALUES ('8', 'Consulta', '2017-11-09 22:26:32', '2017-11-09 22:36:57', null);

-- ----------------------------
-- Table structure for app_perfiles_permisos
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_perfiles_permisos";
CREATE TABLE "public"."app_perfiles_permisos" (
"perfil_id" int4 NOT NULL,
"ruta" varchar(200) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_perfiles_permisos
-- ----------------------------
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', '#Configuracion', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', '#Personas', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', '#Ubicaciones', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', '#Ventas', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/buscar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/cambiar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/cambio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/datatable', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/destruir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/eliminar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/nuevo', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ciudades/restaurar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/asisplan', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/beneficiario', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/beneficiarios', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/beneficiariosguardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/buscar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/cobrostable', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/consulta', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/infoplan', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/planes', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/renovacion', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'consulta/renovacion/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'escritorio/planesactivos/imprimir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/buscar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/cambiar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/cambio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/datatable', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/destruir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/eliminar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/nuevo', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'estados/restaurar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/buscar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/cambiar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/cambio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/datatable', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/destruir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/eliminar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/nuevo', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'municipio/restaurar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/buscar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/cambiar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/cambio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/datatable', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/destruir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/eliminar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/nuevo', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'parroquia/restaurar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/bancos', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/buscar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/cambiar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/cambio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/ciudades', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/correo', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/datatable', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/destruir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/eliminar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/municipios', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/nuevo', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/parroquias', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/restaurar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/sectores', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/telefonos', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/persona/validar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/buscar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/cambiar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/cambio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/datatable', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/destruir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/eliminar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/nuevo', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'personas/profesion/restaurar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/buscar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/cambiar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/cambio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/datatable', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/destruir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/eliminar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/nuevo', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'sector/restaurar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/asistente', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/asistente/guardar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/asistente/infoplan', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/asistente/planes', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/asistente/validar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/asistente/vendedores', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/beneficiariosconfirmacion', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/confirmacion', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/consulta', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/contratorechazdos', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/contratos', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/datainformacion', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/planesactivos', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/planesactivos/imprimir', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/rechazados', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/rechazadosrenovacion', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/reenviarcontrato', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/reenviarrenovacion', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/renovacionesrechazdos', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('1', 'ventas/escritorio/validar', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Administracion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Bancos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Configuracion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#contratos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Definiciones', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#DefinicionesPersonas', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Empresa', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Facturacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#lotes', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Personas', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Ubicaciones', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', '#Ventas', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ciudades/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'cobranza/analista/escritorio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'cobranza/analista/escritorio/confirmacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'cobranza/analista/escritorio/validar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'cobranza/export', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'cobranza/export/generar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'cobranza/export/lotes', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'cobranza/export/prueba', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/asisplan', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/beneficiario', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/beneficiarios', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/beneficiariosguardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/cobrostable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/consulta', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/infoplan', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/planes', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/renovacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'consulta/renovacion/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/activar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/anular', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/asisplan', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/beneficiario', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/beneficiarios', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/buscarpersona', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/cobros', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/cobrostable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/frecuencias', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/guardar/beneficiarios', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/historial', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/infoplan', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/planes', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/refinaciamientoguardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/refinanciamiento', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/renovacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/renovacion/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratos/vendedores', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/contratotipo/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/bene', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/beneficiario', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/beneficiariosguardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/buscarpersona', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/frecuencia', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/corporativo/vendedores', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/estatuscontrato/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/frecuenciapagos/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco/frecuencias', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'contratos/parentesco/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/bancos_empresa/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/empresa/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/ciudades', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/municipios', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/parroquias', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'empresa/sucursal/sectores', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'escritorio/planesactivos/imprimir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'estados/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/afacturar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/afacturar/agrupar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/afacturar/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/afacturar/correlativo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/afacturar/facturas', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/cierre', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/cierre/cierre', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/libro', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/libro/carga', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/libro/export', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'facturacion/resulcarga', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'municipio/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'parroquia/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancos/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/bancotipo/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/bancos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/ciudades', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/correo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/municipios', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/parroquias', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/sectores', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/telefonos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/persona/validar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/profesion/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/telefonotipo/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'personas/tipopersona/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'sector/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/arbol', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/cambio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'usuarios/validar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/asistente', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/asistente/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/asistente/infoplan', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/asistente/planes', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/asistente/validar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/asistente/vendedores', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/beneficiariosconfirmacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/confirmacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/consulta', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/contratorechazdos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/contratos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/datainformacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/planesactivos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/planesactivos/imprimir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/rechazados', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/rechazadosrenovacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/reenviarcontrato', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/reenviarrenovacion', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/renovacionesrechazdos', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/escritorio/validar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/planes/validar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/buscar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/cambiar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/datatable', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/destruir', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/eliminar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/guardar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/nuevo', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/restaurar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('2', 'ventas/vendedores/validar', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', '#Bancos', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', '#Configuracion', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', '#contratos', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', '#Definiciones', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', '#DefinicionesPersonas', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', '#lotes', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', '#Personas', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', '#Ubicaciones', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'ciudades/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'cobranza/analista/escritorio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'cobranza/analista/escritorio/confirmacion', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'cobranza/analista/escritorio/validar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'cobranza/export', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'cobranza/export/generar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'cobranza/export/lotes', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'cobranza/export/prueba', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/asisplan', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/beneficiario', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/beneficiarios', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/beneficiariosguardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/cobrostable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/consulta', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/infoplan', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/planes', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/renovacion', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'consulta/renovacion/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/activar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/anular', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/asisplan', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/beneficiario', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/beneficiarios', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/buscarpersona', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/cobros', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/cobrostable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/frecuencias', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/guardar/beneficiarios', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/historial', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/infoplan', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/planes', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/refinaciamientoguardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/refinanciamiento', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/renovacion', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/renovacion/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratos/vendedores', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/contratotipo/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/bene', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/beneficiario', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/beneficiariosguardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/buscarpersona', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/frecuencia', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/corporativo/vendedores', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/estatuscontrato/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/frecuenciapagos/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco/frecuencias', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'contratos/parentesco/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'estados/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'municipio/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'parroquia/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancos/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/bancotipo/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/bancos', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/ciudades', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/correo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/municipios', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/parroquias', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/sectores', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/telefonos', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/persona/validar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/profesion/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/telefonotipo/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'personas/tipopersona/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/buscar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/cambiar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/cambio', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/datatable', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/destruir', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/eliminar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/guardar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/nuevo', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('5', 'sector/restaurar', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/asisplan', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/beneficiario', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/beneficiarios', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/beneficiariosguardar', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/buscar', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/cobrostable', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/consulta', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/infoplan', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/planes', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/renovacion', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."app_perfiles_permisos" VALUES ('8', 'consulta/renovacion/guardar', '2017-11-09 22:36:57', '2017-11-09 22:36:57');

-- ----------------------------
-- Table structure for app_usuario
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_usuario";
CREATE TABLE "public"."app_usuario" (
"id" int4 DEFAULT nextval('app_usuario_id_seq'::regclass) NOT NULL,
"personas_id" int4 NOT NULL,
"usuario" varchar(50) COLLATE "default" NOT NULL,
"password" varchar(60) COLLATE "default" NOT NULL,
"perfil_id" int4,
"super" char(1) COLLATE "default" DEFAULT 'n'::bpchar NOT NULL,
"vendedor" char(1) COLLATE "default" DEFAULT 'n'::bpchar NOT NULL,
"remember_token" varchar(100) COLLATE "default",
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_usuario
-- ----------------------------
INSERT INTO "public"."app_usuario" VALUES ('1', '1', 'admin', '$2y$10$ytcSocFCDBgD7B4sgTlq9Oy/pfdzLn9B6FtNWWeidm9XOWHjkdDmy', '2', 's', 'n', null, '2017-11-09 22:26:42', '2017-11-10 14:53:00', null);

-- ----------------------------
-- Table structure for app_usuario_empresa
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_usuario_empresa";
CREATE TABLE "public"."app_usuario_empresa" (
"usuario_id" int4 NOT NULL,
"empresa_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_usuario_empresa
-- ----------------------------
INSERT INTO "public"."app_usuario_empresa" VALUES ('1', '1');

-- ----------------------------
-- Table structure for app_usuario_permisos
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_usuario_permisos";
CREATE TABLE "public"."app_usuario_permisos" (
"usuario_id" int4 NOT NULL,
"ruta" varchar(200) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_usuario_permisos
-- ----------------------------
INSERT INTO "public"."app_usuario_permisos" VALUES ('1', '', '2017-11-10 14:53:00', '2017-11-10 14:53:00');

-- ----------------------------
-- Table structure for audits
-- ----------------------------
DROP TABLE IF EXISTS "public"."audits";
CREATE TABLE "public"."audits" (
"id" int4 DEFAULT nextval('audits_id_seq'::regclass) NOT NULL,
"user_id" int4,
"event" varchar(191) COLLATE "default" NOT NULL,
"auditable_id" int4 NOT NULL,
"auditable_type" varchar(191) COLLATE "default" NOT NULL,
"old_values" text COLLATE "default",
"new_values" text COLLATE "default",
"url" varchar(191) COLLATE "default",
"ip_address" inet,
"user_agent" varchar(191) COLLATE "default",
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of audits
-- ----------------------------
INSERT INTO "public"."audits" VALUES ('1', '1', 'created', '2', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"3","nombres":"INACTIVO","tipo_persona_id":1,"id":2}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('2', '1', 'created', '2', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":2,"principal":1,"correo":"correo1@corroeo.com","id":2}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('3', '1', 'created', '2', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":2,"tipo_telefono_id":1,"principal":1,"numero":"04167857172","id":2}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('4', '1', 'created', '1', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":2,"estatus":1,"codigo":0,"id":1}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('5', '1', 'created', '3', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"4","nombres":"CONVENIO DE INTERCAMBIO","tipo_persona_id":1,"id":3}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('6', '1', 'created', '3', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":3,"principal":1,"correo":"correo@corroeo.com","id":3}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('7', '1', 'created', '3', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":3,"tipo_telefono_id":1,"principal":1,"numero":"04167857173","id":3}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('8', '1', 'created', '2', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":3,"estatus":1,"codigo":2,"id":2}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('9', '1', 'created', '4', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"8867067","nombres":"FRANCIA CARVAJAL","tipo_persona_id":1,"id":4}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('10', '1', 'created', '4', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":4,"principal":1,"correo":"arturorafa2el.medina@gmail.com ","id":4}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('11', '1', 'created', '4', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":4,"tipo_telefono_id":1,"principal":1,"numero":"04167857175","id":4}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('12', '1', 'created', '3', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":4,"estatus":1,"codigo":80,"id":3}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('13', '1', 'created', '5', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"18012306","nombres":"VINICIO GRILLET","tipo_persona_id":1,"id":5}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('14', '1', 'created', '5', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":5,"principal":1,"correo":"arturorafa4el.medina@gmail.com ","id":5}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('15', '1', 'created', '5', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":5,"tipo_telefono_id":1,"principal":1,"numero":"04167857176","id":5}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('16', '1', 'created', '4', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":5,"estatus":1,"codigo":88,"id":4}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('17', '1', 'created', '6', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"12194357","nombres":"ZULEIMA NU\u00d1EZ","tipo_persona_id":1,"id":6}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('18', '1', 'created', '6', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":6,"principal":1,"correo":"zuleima18034543@gmail.com","id":6}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('19', '1', 'created', '6', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":6,"tipo_telefono_id":1,"principal":1,"numero":"04167857177","id":6}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('20', '1', 'created', '5', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":6,"estatus":1,"codigo":100,"id":5}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('21', '1', 'created', '7', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"16500783","nombres":"MIURYCA RENDON","tipo_persona_id":1,"id":7}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('22', '1', 'created', '7', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":7,"principal":1,"correo":"arturorafa5el.medina@gmail.com ","id":7}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('23', '1', 'created', '7', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":7,"tipo_telefono_id":1,"principal":1,"numero":"04167857178","id":7}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('24', '1', 'created', '6', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":7,"estatus":1,"codigo":120,"id":6}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('25', '1', 'created', '8', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"5706650","nombres":"AGUILERA RAMOS ROSA OFELIA","tipo_persona_id":1,"id":8}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('26', '1', 'created', '8', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":8,"principal":1,"correo":"arturorafeael.medina@gmail.com ","id":8}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('27', '1', 'created', '8', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":8,"tipo_telefono_id":1,"principal":1,"numero":"04167857179","id":8}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('28', '1', 'created', '7', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":8,"estatus":1,"codigo":122,"id":7}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('29', '1', 'created', '9', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"11228418","nombres":"MARIA FRANCIA ORTIZ NIETO","tipo_persona_id":1,"id":9}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('30', '1', 'created', '9', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":9,"principal":1,"correo":"arturorafaefel.medina@gmail.com ","id":9}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('31', '1', 'created', '9', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":9,"tipo_telefono_id":1,"principal":1,"numero":"04167857180","id":9}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('32', '1', 'created', '8', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":9,"estatus":1,"codigo":123,"id":8}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('33', '1', 'created', '10', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"15617290","nombres":"EUKARYS CAROLINA VECCHIONACCE CEDE\u00d1O","tipo_persona_id":1,"id":10}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('34', '1', 'created', '10', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":10,"principal":1,"correo":"arturorafavvvel.medina@gmail.com ","id":10}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('35', '1', 'created', '10', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":10,"tipo_telefono_id":1,"principal":1,"numero":"04167857181","id":10}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('36', '1', 'created', '9', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":10,"estatus":1,"codigo":124,"id":9}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('37', '1', 'created', '11', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"18947721","nombres":"YOLIMAR DESIREE HERRERA RUIZ","tipo_persona_id":1,"id":11}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('38', '1', 'created', '11', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":11,"principal":1,"correo":"lachicayolimar@gmail.com ","id":11}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('39', '1', 'created', '11', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":11,"tipo_telefono_id":1,"principal":1,"numero":"04167857182","id":11}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('40', '1', 'created', '10', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":11,"estatus":1,"codigo":125,"id":10}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('41', '1', 'created', '12', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"19369434","nombres":"CINDY LA BELLE MONTERO RIVAS","tipo_persona_id":1,"id":12}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('42', '1', 'created', '12', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":12,"principal":1,"correo":"arturorafaasdadel.medina@gmail.com ","id":12}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('43', '1', 'created', '12', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":12,"tipo_telefono_id":1,"principal":1,"numero":"04167857183","id":12}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('44', '1', 'created', '11', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":12,"estatus":1,"codigo":127,"id":11}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('45', '1', 'created', '13', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"20774473","nombres":"NAZARETH MEJIAS","tipo_persona_id":1,"id":13}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('46', '1', 'created', '13', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":13,"principal":1,"correo":"arturorafaasdasdel.medina@gmail.com ","id":13}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('47', '1', 'created', '13', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":13,"tipo_telefono_id":1,"principal":1,"numero":"04167857184","id":13}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('48', '1', 'created', '12', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":13,"estatus":1,"codigo":128,"id":12}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('49', '1', 'created', '14', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"18828478","nombres":"IRIANA CEDE\u00d1O","tipo_persona_id":1,"id":14}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('50', '1', 'created', '14', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":14,"principal":1,"correo":"arturorassssfael.medina@gmail.com ","id":14}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('51', '1', 'created', '14', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":14,"tipo_telefono_id":1,"principal":1,"numero":"04167857185","id":14}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('52', '1', 'created', '13', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":14,"estatus":1,"codigo":129,"id":13}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('53', '1', 'created', '15', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"10810986","nombres":"CONNIE SOTO LANZA ","tipo_persona_id":1,"id":15}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('54', '1', 'created', '15', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":15,"principal":1,"correo":"isajossoto@gmail.com","id":15}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('55', '1', 'created', '15', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":15,"tipo_telefono_id":1,"principal":1,"numero":"04167857186","id":15}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('56', '1', 'created', '14', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":15,"estatus":1,"codigo":130,"id":14}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('57', '1', 'created', '16', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"18947098","nombres":"ARTURO MEDINA","tipo_persona_id":1,"id":16}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('58', '1', 'created', '16', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":16,"principal":1,"correo":"arturorawwwfael.medina@gmail.com ","id":16}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('59', '1', 'created', '16', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":16,"tipo_telefono_id":1,"principal":1,"numero":"04167857187","id":16}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('60', '1', 'created', '15', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":16,"estatus":1,"codigo":131,"id":15}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('61', '1', 'created', '17', 'App\Modules\Base\Models\Personas', '[]', '{"dni":"19730697","nombres":"CARMERY SOTO","tipo_persona_id":1,"id":17}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('62', '1', 'created', '17', 'App\Modules\Base\Models\PersonasCorreo', '[]', '{"personas_id":17,"principal":1,"correo":"arturorafaaaael.medina@gmail.com ","id":17}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('63', '1', 'created', '17', 'App\Modules\Base\Models\PersonasTelefono', '[]', '{"personas_id":17,"tipo_telefono_id":1,"principal":1,"numero":"04167857188","id":17}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('64', '1', 'created', '16', 'App\Modules\Ventas\Models\Vendedores', '[]', '{"personas_id":17,"estatus":1,"codigo":132,"id":16}', 'http://localhost/migracion/vendedores', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:32:45', '2017-11-09 22:32:45');
INSERT INTO "public"."audits" VALUES ('65', '1', 'updated', '5', 'App\Modules\Base\Models\Perfil', '{"nombre":"Asistente"}', '{"nombre":"Analista"}', 'http://localhost/perfiles/guardar/5', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:36:05', '2017-11-09 22:36:05');
INSERT INTO "public"."audits" VALUES ('66', '1', 'updated', '2', 'App\Modules\Base\Models\Perfil', '[]', '[]', 'http://localhost/perfiles/guardar/2', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:36:39', '2017-11-09 22:36:39');
INSERT INTO "public"."audits" VALUES ('67', '1', 'updated', '8', 'App\Modules\Base\Models\Perfil', '{"nombre":"Contador"}', '{"nombre":"Consulta"}', 'http://localhost/perfiles/guardar/8', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:36:57', '2017-11-09 22:36:57');
INSERT INTO "public"."audits" VALUES ('68', '1', 'updated', '1', 'App\Modules\Base\Models\Perfil', '{"nombre":"Desarrollador"}', '{"nombre":"Vendedor"}', 'http://localhost/perfiles/guardar/1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:37:54', '2017-11-09 22:37:54');
INSERT INTO "public"."audits" VALUES ('69', '1', 'deleted', '6', 'App\Modules\Base\Models\Perfil', '{"id":6,"nombre":"Secretaria","deleted_at":"2017-11-09 22:38:08"}', '[]', 'http://localhost/perfiles/eliminar/6', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:38:08', '2017-11-09 22:38:08');
INSERT INTO "public"."audits" VALUES ('70', '1', 'deleted', '4', 'App\Modules\Base\Models\Perfil', '{"id":4,"nombre":"Supervisor","deleted_at":"2017-11-09 22:38:14"}', '[]', 'http://localhost/perfiles/eliminar/4', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:38:14', '2017-11-09 22:38:14');
INSERT INTO "public"."audits" VALUES ('71', '1', 'deleted', '3', 'App\Modules\Base\Models\Perfil', '{"id":3,"nombre":"Tecnico","deleted_at":"2017-11-09 22:38:18"}', '[]', 'http://localhost/perfiles/eliminar/3', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:38:18', '2017-11-09 22:38:18');
INSERT INTO "public"."audits" VALUES ('72', '1', 'deleted', '1', 'App\Modules\Base\Models\Perfil', '{"id":1,"nombre":"Vendedor","deleted_at":"2017-11-09 22:38:23"}', '[]', 'http://localhost/perfiles/eliminar/1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-09 22:38:23', '2017-11-09 22:38:23');
INSERT INTO "public"."audits" VALUES ('75', '1', 'updated', '1', 'App\Modules\Base\Models\Usuario', '{"perfil_id":1}', '{"perfil_id":"2"}', 'http://192.168.0.50/usuarios/guardar/1', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:00', '2017-11-10 14:53:00');
INSERT INTO "public"."audits" VALUES ('76', '1', 'updated', '1', 'App\Modules\Base\Models\Personas', '[]', '[]', 'http://192.168.0.50/usuarios/guardar/1', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:00', '2017-11-10 14:53:00');
INSERT INTO "public"."audits" VALUES ('77', '1', 'created', '1', 'App\Modules\Ventas\Models\Plan', '[]', '{"nombre":"plan prueba","desde":{"date":"2017-11-01 14:53:45.000000","timezone_type":3,"timezone":"UTC"},"hasta":{"date":"2017-11-30 14:53:45.000000","timezone_type":3,"timezone":"UTC"},"meses_pagar":"3","empresa_id":"1","id":1}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('78', '1', 'created', '1', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":1,"porsertaje_descuento":0,"contado":100,"inicial":10,"total":100,"id":1}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('79', '1', 'created', '2', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":2,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":2}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('80', '1', 'created', '3', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":3,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":3}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('81', '1', 'created', '4', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":4,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":4}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('82', '1', 'created', '5', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":5,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":5}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('83', '1', 'created', '6', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":6,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":6}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('84', '1', 'created', '7', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":7,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":7}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('85', '1', 'created', '8', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":8,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":8}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('86', '1', 'created', '9', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":9,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":9}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('87', '1', 'created', '10', 'App\Modules\Ventas\Models\PlanDetalles', '[]', '{"plan_id":1,"beneficiarios":10,"porsertaje_descuento":0,"contado":0,"inicial":0,"total":0,"id":10}', 'http://192.168.0.50/ventas/planes/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:53:45', '2017-11-10 14:53:45');
INSERT INTO "public"."audits" VALUES ('88', '1', 'created', '1', 'App\Modules\Empresa\Models\BancosEmpresa', '[]', '{"nombre":"Mercantil","empresa_id":"1","bancos_id":"3","cuenta":"01050134271134036906","id":1}', 'http://192.168.0.50/empresa/bancos_empresa/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 14:58:13', '2017-11-10 14:58:13');
INSERT INTO "public"."audits" VALUES ('89', '1', 'created', '2', 'App\Modules\Empresa\Models\BancosEmpresa', '[]', '{"nombre":"caroni","empresa_id":"1","bancos_id":"7","cuenta":"01280041014101070102","id":2}', 'http://192.168.0.50/empresa/bancos_empresa/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:00:37', '2017-11-10 15:00:37');
INSERT INTO "public"."audits" VALUES ('90', '1', 'created', '1', 'App\Modules\Base\Models\Sector', '[]', '{"nombre":"asdaeq","parroquias_id":"462","slug":"asdaeq","id":1}', 'http://192.168.0.50/sector/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:15:10', '2017-11-10 15:15:10');
INSERT INTO "public"."audits" VALUES ('91', '1', 'created', '18', 'App\Modules\Base\Models\Personas', '[]', '{"tipo_persona_id":"1","dni":"15252476","nombres":"Iliana Cede\u00f1o","id":18}', 'http://192.168.0.50/personas/persona/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:15:14', '2017-11-10 15:15:14');
INSERT INTO "public"."audits" VALUES ('92', '1', 'created', '2', 'App\Modules\Base\Models\PersonasDetalles', '[]', '{"personas_id":18,"profesion_id":"1","sexo":"f","fecha_nacimiento":{"date":"2017-11-10 15:15:14.000000","timezone_type":3,"timezone":"UTC"},"id":2}', 'http://192.168.0.50/personas/persona/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:15:14', '2017-11-10 15:15:14');
INSERT INTO "public"."audits" VALUES ('93', '1', 'created', '2', 'App\Modules\Base\Models\PersonasDireccion', '[]', '{"personas_id":18,"estados_id":"12","ciudades_id":"200","municipios_id":"145","parroquias_id":"462","sectores_id":"1","direccion":"asdqweasd","id":2}', 'http://192.168.0.50/personas/persona/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:15:14', '2017-11-10 15:15:14');
INSERT INTO "public"."audits" VALUES ('94', '1', 'created', '1', 'App\Modules\Base\Models\PersonasBancos', '[]', '{"personas_id":18,"bancos_id":"3","tipo_cuenta_id":"2","cuenta":"01050134261134057571","digitos":null,"id":1}', 'http://192.168.0.50/personas/persona/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:15:14', '2017-11-10 15:15:14');
INSERT INTO "public"."audits" VALUES ('95', '1', 'created', '1', 'App\Modules\Contratos\Models\Contratos', '[]', '{"inicio":"10\/11\/2017","sucursal_id":"1","vendedor_id":"2","planilla":"001","tipo_pago":"2","personas_bancos_id":"1","fecha_incial":"10\/11\/2017","frecuencia_pagos_id":"1","titular":18,"empresa_id":1,"cargado":"2017-11-10","vencimiento":{"date":"2018-11-10 15:16:18.000000","timezone_type":3,"timezone":"UTC"},"plan_detalles_id":1,"total_contrato":"100.00","inicial":"10.00","beneficiarios":"1","cuotas":3,"fecha_pago":"10\/11\/2017","total_pagado":0,"estatus_contrato_id":1,"id":1}', 'http://192.168.0.50/contratos/contratos/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:16:18', '2017-11-10 15:16:18');
INSERT INTO "public"."audits" VALUES ('96', '1', 'created', '1', 'App\Modules\Contratos\Models\ContratosDetalles', '[]', '{"contratos_id":1,"personas_id":1,"operacion":"Carga de Nuevo contrato Por Analista: Administrador","planilla":"001","id":1}', 'http://192.168.0.50/contratos/contratos/guardar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:16:18', '2017-11-10 15:16:18');
INSERT INTO "public"."audits" VALUES ('97', '1', 'created', '2', 'App\Modules\Contratos\Models\ContratosDetalles', '[]', '{"contratos_id":1,"personas_id":1,"operacion":"Generacion de Cobro de inicial por sistema","planilla":"s\/n","id":2}', 'http://192.168.0.50/cobranza/export/generar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:16:36', '2017-11-10 15:16:36');
INSERT INTO "public"."audits" VALUES ('98', '1', 'created', '3', 'App\Modules\Contratos\Models\ContratosDetalles', '[]', '{"contratos_id":1,"personas_id":1,"operacion":"Generacion de Cobro de inicial por sistema","planilla":"s\/n","id":3}', 'http://192.168.0.50/cobranza/export/generar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:25:32', '2017-11-10 15:25:32');
INSERT INTO "public"."audits" VALUES ('99', '1', 'created', '4', 'App\Modules\Contratos\Models\ContratosDetalles', '[]', '{"contratos_id":1,"personas_id":1,"operacion":"Generacion de Cobro de inicial por sistema","planilla":"s\/n","id":4}', 'http://192.168.0.50/cobranza/export/generar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 15:42:44', '2017-11-10 15:42:44');
INSERT INTO "public"."audits" VALUES ('100', '1', 'created', '5', 'App\Modules\Contratos\Models\ContratosDetalles', '[]', '{"contratos_id":1,"personas_id":1,"operacion":"Generacion de Cobro de inicial por sistema","planilla":"s\/n","id":5}', 'http://192.168.0.50/cobranza/export/generar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 19:06:45', '2017-11-10 19:06:45');
INSERT INTO "public"."audits" VALUES ('109', '1', 'created', '14', 'App\Modules\Contratos\Models\ContratosDetalles', '[]', '{"contratos_id":1,"personas_id":1,"operacion":"Generacion de Cobro de inicial por sistema","planilla":"s\/n","id":14}', 'http://192.168.0.50/cobranza/export/generar', '192.168.0.103', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2017-11-10 19:47:52', '2017-11-10 19:47:52');

-- ----------------------------
-- Table structure for banco_tipo_cuenta
-- ----------------------------
DROP TABLE IF EXISTS "public"."banco_tipo_cuenta";
CREATE TABLE "public"."banco_tipo_cuenta" (
"id" int4 DEFAULT nextval('banco_tipo_cuenta_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of banco_tipo_cuenta
-- ----------------------------
INSERT INTO "public"."banco_tipo_cuenta" VALUES ('1', 'Ahorro', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."banco_tipo_cuenta" VALUES ('2', 'Corriente', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."banco_tipo_cuenta" VALUES ('3', 'Targeta de Credito', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);

-- ----------------------------
-- Table structure for bancos
-- ----------------------------
DROP TABLE IF EXISTS "public"."bancos";
CREATE TABLE "public"."bancos" (
"id" int4 DEFAULT nextval('bancos_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"codigo" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of bancos
-- ----------------------------
INSERT INTO "public"."bancos" VALUES ('1', 'Cacrete', '00', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('2', 'Banco de Venezuela S.A.C.A. Banco Universal', '0102', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('3', 'Banco Mercantil, C.A S.A.C.A. Banco Universal', '0105', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('4', 'Banco Provincial, S.A. Banco Universal', '0108', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('5', 'Bancaribe C.A. Banco Universal', '0114', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('6', 'Banco Exterior C.A. Banco Universal', '0115', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('7', 'Banco Caroni C.A. Banco Universal', '0128', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('8', 'Banesco Banco Universal S.A.C.A.', '0134', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('9', 'DelSur Banco Universal, C.A.', '0157', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('10', 'Banco Bicentenario Banco Universal C.A.', '0175', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('11', 'Banco Nacional de Crédito, C.A. Banco Universal', '0191', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('12', 'Banco Occidental de Descuento, Banco Universal C.A.', '0116', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."bancos" VALUES ('13', 'corpbanca', '1234', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);

-- ----------------------------
-- Table structure for bancos_empresa
-- ----------------------------
DROP TABLE IF EXISTS "public"."bancos_empresa";
CREATE TABLE "public"."bancos_empresa" (
"id" int4 DEFAULT nextval('bancos_empresa_id_seq'::regclass) NOT NULL,
"nombre" varchar(255) COLLATE "default" NOT NULL,
"empresa_id" int4 NOT NULL,
"bancos_id" int4 NOT NULL,
"cuenta" char(20) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of bancos_empresa
-- ----------------------------
INSERT INTO "public"."bancos_empresa" VALUES ('1', 'Mercantil', '1', '3', '01050134271134036906', '2017-11-10 14:58:13', '2017-11-10 14:58:13', null);
INSERT INTO "public"."bancos_empresa" VALUES ('2', 'caroni', '1', '7', '01280041014101070102', '2017-11-10 15:00:37', '2017-11-10 15:00:37', null);

-- ----------------------------
-- Table structure for beneficiarios
-- ----------------------------
DROP TABLE IF EXISTS "public"."beneficiarios";
CREATE TABLE "public"."beneficiarios" (
"id" int4 DEFAULT nextval('beneficiarios_id_seq'::regclass) NOT NULL,
"contratos_id" int4 NOT NULL,
"personas_id" int4 NOT NULL,
"parentesco_id" int4 NOT NULL,
"estatus" bool DEFAULT false NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of beneficiarios
-- ----------------------------
INSERT INTO "public"."beneficiarios" VALUES ('1', '1', '18', '33', 'f', '2017-11-10 15:16:18', '2017-11-10 15:16:18');

-- ----------------------------
-- Table structure for beneficiarios_temp
-- ----------------------------
DROP TABLE IF EXISTS "public"."beneficiarios_temp";
CREATE TABLE "public"."beneficiarios_temp" (
"id" int4 DEFAULT nextval('beneficiarios_temp_id_seq'::regclass) NOT NULL,
"dni" varchar(20) COLLATE "default" NOT NULL,
"nombre" varchar(60) COLLATE "default",
"sexo" varchar(10) COLLATE "default",
"solicitud_beneficiarios_temp_id" int4 NOT NULL,
"parentesco_id" int4 NOT NULL,
"fecha_nacimiento" date NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of beneficiarios_temp
-- ----------------------------

-- ----------------------------
-- Table structure for ciudades
-- ----------------------------
DROP TABLE IF EXISTS "public"."ciudades";
CREATE TABLE "public"."ciudades" (
"id" int4 DEFAULT nextval('ciudades_id_seq'::regclass) NOT NULL,
"estados_id" int4,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"capital" int2 DEFAULT '0'::smallint NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ciudades
-- ----------------------------
INSERT INTO "public"."ciudades" VALUES ('1', '1', 'Maroa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('2', '1', 'Puerto Ayacucho', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('3', '1', 'San Fernando de Atabapo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('4', '2', 'Anaco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('5', '2', 'Aragua de Barcelona', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('6', '2', 'Barcelona', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('7', '2', 'Boca de Uchire', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('8', '2', 'Cantaura', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('9', '2', 'Clarines', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('10', '2', 'El Chaparro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('11', '2', 'El Pao Anzoátegui', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('12', '2', 'El Tigre', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('13', '2', 'El Tigrito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('14', '2', 'Guanape', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('15', '2', 'Guanta', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('16', '2', 'Lechería', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('17', '2', 'Onoto', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('18', '2', 'Pariaguán', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('19', '2', 'Píritu', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('20', '2', 'Puerto La Cruz', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('21', '2', 'Puerto Píritu', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('22', '2', 'Sabana de Uchire', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('23', '2', 'San Mateo Anzoátegui', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('24', '2', 'San Pablo Anzoátegui', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('25', '2', 'San Tomé', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('26', '2', 'Santa Ana de Anzoátegui', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('27', '2', 'Santa Fe Anzoátegui', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('28', '2', 'Santa Rosa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('29', '2', 'Soledad', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('30', '2', 'Urica', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('31', '2', 'Valle de Guanape', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('32', '3', 'Achaguas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('33', '3', 'Biruaca', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('34', '3', 'Bruzual', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('35', '3', 'El Amparo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('36', '3', 'El Nula', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('37', '3', 'Elorza', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('38', '3', 'Guasdualito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('39', '3', 'Mantecal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('40', '3', 'Puerto Páez', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('41', '3', 'San Fernando de Apure', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('42', '3', 'San Juan de Payara', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('43', '4', 'Barbacoas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('44', '4', 'Cagua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('45', '4', 'Camatagua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('46', '4', 'Choroní', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('47', '4', 'Colonia Tovar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('48', '4', 'El Consejo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('49', '4', 'La Victoria', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('50', '4', 'Las Tejerías', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('51', '4', 'Magdaleno', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('52', '4', 'Maracay', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('53', '4', 'Ocumare de La Costa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('54', '4', 'Palo Negro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('55', '4', 'San Casimiro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('56', '4', 'San Mateo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('57', '4', 'San Sebastián', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('58', '4', 'Santa Cruz de Aragua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('59', '4', 'Tocorón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('60', '4', 'Turmero', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('61', '4', 'Villa de Cura', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('62', '4', 'Zuata', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('63', '5', 'Barinas', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('64', '5', 'Barinitas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('65', '5', 'Barrancas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('66', '5', 'Calderas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('67', '5', 'Capitanejo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('68', '5', 'Ciudad Bolivia', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('69', '5', 'El Cantón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('70', '5', 'Las Veguitas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('71', '5', 'Libertad de Barinas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('72', '5', 'Sabaneta', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('73', '5', 'Santa Bárbara de Barinas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('74', '5', 'Socopó', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('75', '6', 'Caicara del Orinoco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('76', '6', 'Canaima', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('77', '6', 'Ciudad Bolívar', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('78', '6', 'Ciudad Piar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('79', '6', 'El Callao', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('80', '6', 'El Dorado', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('81', '6', 'El Manteco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('82', '6', 'El Palmar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('83', '6', 'El Pao', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('84', '6', 'Guasipati', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('85', '6', 'Guri', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('86', '6', 'La Paragua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('87', '6', 'Matanzas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('88', '6', 'Puerto Ordaz', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('89', '6', 'San Félix', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('90', '6', 'Santa Elena de Uairén', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('91', '6', 'Tumeremo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('92', '6', 'Unare', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('93', '6', 'Upata', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('94', '7', 'Bejuma', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('95', '7', 'Belén', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('96', '7', 'Campo de Carabobo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('97', '7', 'Canoabo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('98', '7', 'Central Tacarigua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('99', '7', 'Chirgua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('100', '7', 'Ciudad Alianza', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('101', '7', 'El Palito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('102', '7', 'Guacara', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('103', '7', 'Guigue', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('104', '7', 'Las Trincheras', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('105', '7', 'Los Guayos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('106', '7', 'Mariara', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('107', '7', 'Miranda', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('108', '7', 'Montalbán', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('109', '7', 'Morón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('110', '7', 'Naguanagua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('111', '7', 'Puerto Cabello', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('112', '7', 'San Joaquín', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('113', '7', 'Tocuyito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('114', '7', 'Urama', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('115', '7', 'Valencia', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('116', '7', 'Vigirimita', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('117', '8', 'Aguirre', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('118', '8', 'Apartaderos Cojedes', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('119', '8', 'Arismendi', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('120', '8', 'Camuriquito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('121', '8', 'El Baúl', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('122', '8', 'El Limón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('123', '8', 'El Pao Cojedes', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('124', '8', 'El Socorro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('125', '8', 'La Aguadita', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('126', '8', 'Las Vegas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('127', '8', 'Libertad de Cojedes', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('128', '8', 'Mapuey', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('129', '8', 'Piñedo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('130', '8', 'Samancito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('131', '8', 'San Carlos', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('132', '8', 'Sucre', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('133', '8', 'Tinaco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('134', '8', 'Tinaquillo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('135', '8', 'Vallecito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('136', '9', 'Tucupita', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('137', '24', 'Caracas', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('138', '24', 'El Junquito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('139', '10', 'Adícora', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('140', '10', 'Boca de Aroa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('141', '10', 'Cabure', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('142', '10', 'Capadare', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('143', '10', 'Capatárida', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('144', '10', 'Chichiriviche', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('145', '10', 'Churuguara', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('146', '10', 'Coro', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('147', '10', 'Cumarebo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('148', '10', 'Dabajuro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('149', '10', 'Judibana', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('150', '10', 'La Cruz de Taratara', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('151', '10', 'La Vela de Coro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('152', '10', 'Los Taques', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('153', '10', 'Maparari', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('154', '10', 'Mene de Mauroa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('155', '10', 'Mirimire', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('156', '10', 'Pedregal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('157', '10', 'Píritu Falcón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('158', '10', 'Pueblo Nuevo Falcón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('159', '10', 'Puerto Cumarebo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('160', '10', 'Punta Cardón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('161', '10', 'Punto Fijo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('162', '10', 'San Juan de Los Cayos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('163', '10', 'San Luis', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('164', '10', 'Santa Ana Falcón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('165', '10', 'Santa Cruz De Bucaral', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('166', '10', 'Tocopero', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('167', '10', 'Tocuyo de La Costa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('168', '10', 'Tucacas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('169', '10', 'Yaracal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('170', '11', 'Altagracia de Orituco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('171', '11', 'Cabruta', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('172', '11', 'Calabozo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('173', '11', 'Camaguán', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('174', '11', 'Chaguaramas Guárico', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('175', '11', 'El Socorro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('176', '11', 'El Sombrero', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('177', '11', 'Las Mercedes de Los Llanos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('178', '11', 'Lezama', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('179', '11', 'Onoto', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('180', '11', 'Ortíz', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('181', '11', 'San José de Guaribe', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('182', '11', 'San Juan de Los Morros', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('183', '11', 'San Rafael de Laya', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('184', '11', 'Santa María de Ipire', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('185', '11', 'Tucupido', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('186', '11', 'Valle de La Pascua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('187', '11', 'Zaraza', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('188', '12', 'Aguada Grande', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('189', '12', 'Atarigua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('190', '12', 'Barquisimeto', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('191', '12', 'Bobare', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('192', '12', 'Cabudare', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('193', '12', 'Carora', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('194', '12', 'Cubiro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('195', '12', 'Cují', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('196', '12', 'Duaca', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('197', '12', 'El Manzano', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('198', '12', 'El Tocuyo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('199', '12', 'Guaríco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('200', '12', 'Humocaro Alto', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('201', '12', 'Humocaro Bajo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('202', '12', 'La Miel', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('203', '12', 'Moroturo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('204', '12', 'Quíbor', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('205', '12', 'Río Claro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('206', '12', 'Sanare', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('207', '12', 'Santa Inés', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('208', '12', 'Sarare', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('209', '12', 'Siquisique', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('210', '12', 'Tintorero', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('211', '13', 'Apartaderos Mérida', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('212', '13', 'Arapuey', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('213', '13', 'Bailadores', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('214', '13', 'Caja Seca', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('215', '13', 'Canaguá', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('216', '13', 'Chachopo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('217', '13', 'Chiguara', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('218', '13', 'Ejido', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('219', '13', 'El Vigía', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('220', '13', 'La Azulita', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('221', '13', 'La Playa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('222', '13', 'Lagunillas Mérida', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('223', '13', 'Mérida', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('224', '13', 'Mesa de Bolívar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('225', '13', 'Mucuchíes', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('226', '13', 'Mucujepe', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('227', '13', 'Mucuruba', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('228', '13', 'Nueva Bolivia', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('229', '13', 'Palmarito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('230', '13', 'Pueblo Llano', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('231', '13', 'Santa Cruz de Mora', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('232', '13', 'Santa Elena de Arenales', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('233', '13', 'Santo Domingo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('234', '13', 'Tabáy', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('235', '13', 'Timotes', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('236', '13', 'Torondoy', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('237', '13', 'Tovar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('238', '13', 'Tucani', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('239', '13', 'Zea', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('240', '14', 'Araguita', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('241', '14', 'Carrizal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('242', '14', 'Caucagua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('243', '14', 'Chaguaramas Miranda', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('244', '14', 'Charallave', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('245', '14', 'Chirimena', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('246', '14', 'Chuspa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('247', '14', 'Cúa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('248', '14', 'Cupira', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('249', '14', 'Curiepe', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('250', '14', 'El Guapo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('251', '14', 'El Jarillo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('252', '14', 'Filas de Mariche', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('253', '14', 'Guarenas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('254', '14', 'Guatire', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('255', '14', 'Higuerote', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('256', '14', 'Los Anaucos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('257', '14', 'Los Teques', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('258', '14', 'Ocumare del Tuy', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('259', '14', 'Panaquire', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('260', '14', 'Paracotos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('261', '14', 'Río Chico', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('262', '14', 'San Antonio de Los Altos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('263', '14', 'San Diego de Los Altos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('264', '14', 'San Fernando del Guapo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('265', '14', 'San Francisco de Yare', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('266', '14', 'San José de Los Altos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('267', '14', 'San José de Río Chico', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('268', '14', 'San Pedro de Los Altos', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('269', '14', 'Santa Lucía', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('270', '14', 'Santa Teresa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('271', '14', 'Tacarigua de La Laguna', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('272', '14', 'Tacarigua de Mamporal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('273', '14', 'Tácata', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('274', '14', 'Turumo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('275', '15', 'Aguasay', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('276', '15', 'Aragua de Maturín', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('277', '15', 'Barrancas del Orinoco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('278', '15', 'Caicara de Maturín', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('279', '15', 'Caripe', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('280', '15', 'Caripito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('281', '15', 'Chaguaramal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('282', '15', 'Chaguaramas Monagas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('283', '15', 'El Furrial', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('284', '15', 'El Tejero', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('285', '15', 'Jusepín', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('286', '15', 'La Toscana', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('287', '15', 'Maturín', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('288', '15', 'Miraflores', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('289', '15', 'Punta de Mata', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('290', '15', 'Quiriquire', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('291', '15', 'San Antonio de Maturín', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('292', '15', 'San Vicente Monagas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('293', '15', 'Santa Bárbara', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('294', '15', 'Temblador', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('295', '15', 'Teresen', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('296', '15', 'Uracoa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('297', '16', 'Altagracia', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('298', '16', 'Boca de Pozo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('299', '16', 'Boca de Río', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('300', '16', 'El Espinal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('301', '16', 'El Valle del Espíritu Santo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('302', '16', 'El Yaque', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('303', '16', 'Juangriego', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('304', '16', 'La Asunción', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('305', '16', 'La Guardia', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('306', '16', 'Pampatar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('307', '16', 'Porlamar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('308', '16', 'Puerto Fermín', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('309', '16', 'Punta de Piedras', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('310', '16', 'San Francisco de Macanao', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('311', '16', 'San Juan Bautista', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('312', '16', 'San Pedro de Coche', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('313', '16', 'Santa Ana de Nueva Esparta', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('314', '16', 'Villa Rosa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('315', '17', 'Acarigua', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('316', '17', 'Agua Blanca', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('317', '17', 'Araure', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('318', '17', 'Biscucuy', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('319', '17', 'Boconoito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('320', '17', 'Campo Elías', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('321', '17', 'Chabasquén', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('322', '17', 'Guanare', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('323', '17', 'Guanarito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('324', '17', 'La Aparición', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('325', '17', 'La Misión', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('326', '17', 'Mesa de Cavacas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('327', '17', 'Ospino', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('328', '17', 'Papelón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('329', '17', 'Payara', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('330', '17', 'Pimpinela', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('331', '17', 'Píritu de Portuguesa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('332', '17', 'San Rafael de Onoto', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('333', '17', 'Santa Rosalía', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('334', '17', 'Turén', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('335', '18', 'Altos de Sucre', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('336', '18', 'Araya', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('337', '18', 'Cariaco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('338', '18', 'Carúpano', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('339', '18', 'Casanay', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('340', '18', 'Cumaná', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('341', '18', 'Cumanacoa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('342', '18', 'El Morro Puerto Santo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('343', '18', 'El Pilar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('344', '18', 'El Poblado', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('345', '18', 'Guaca', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('346', '18', 'Guiria', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('347', '18', 'Irapa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('348', '18', 'Manicuare', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('349', '18', 'Mariguitar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('350', '18', 'Río Caribe', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('351', '18', 'San Antonio del Golfo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('352', '18', 'San José de Aerocuar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('353', '18', 'San Vicente de Sucre', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('354', '18', 'Santa Fe de Sucre', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('355', '18', 'Tunapuy', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('356', '18', 'Yaguaraparo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('357', '18', 'Yoco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('358', '19', 'Abejales', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('359', '19', 'Borota', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('360', '19', 'Bramon', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('361', '19', 'Capacho', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('362', '19', 'Colón', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('363', '19', 'Coloncito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('364', '19', 'Cordero', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('365', '19', 'El Cobre', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('366', '19', 'El Pinal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('367', '19', 'Independencia', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('368', '19', 'La Fría', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('369', '19', 'La Grita', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('370', '19', 'La Pedrera', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('371', '19', 'La Tendida', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('372', '19', 'Las Delicias', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('373', '19', 'Las Hernández', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('374', '19', 'Lobatera', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('375', '19', 'Michelena', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('376', '19', 'Palmira', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('377', '19', 'Pregonero', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('378', '19', 'Queniquea', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('379', '19', 'Rubio', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('380', '19', 'San Antonio del Tachira', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('381', '19', 'San Cristobal', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('382', '19', 'San José de Bolívar', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('383', '19', 'San Josecito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('384', '19', 'San Pedro del Río', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('385', '19', 'Santa Ana Táchira', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('386', '19', 'Seboruco', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('387', '19', 'Táriba', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('388', '19', 'Umuquena', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('389', '19', 'Ureña', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('390', '20', 'Batatal', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('391', '20', 'Betijoque', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('392', '20', 'Boconó', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('393', '20', 'Carache', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('394', '20', 'Chejende', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('395', '20', 'Cuicas', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('396', '20', 'El Dividive', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('397', '20', 'El Jaguito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('398', '20', 'Escuque', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('399', '20', 'Isnotú', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('400', '20', 'Jajó', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('401', '20', 'La Ceiba', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('402', '20', 'La Concepción de Trujllo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('403', '20', 'La Mesa de Esnujaque', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('404', '20', 'La Puerta', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('405', '20', 'La Quebrada', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('406', '20', 'Mendoza Fría', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('407', '20', 'Meseta de Chimpire', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('408', '20', 'Monay', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('409', '20', 'Motatán', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('410', '20', 'Pampán', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('411', '20', 'Pampanito', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('412', '20', 'Sabana de Mendoza', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('413', '20', 'San Lázaro', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('414', '20', 'Santa Ana de Trujillo', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('415', '20', 'Tostós', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('416', '20', 'Trujillo', '1', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('417', '20', 'Valera', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('418', '21', 'Carayaca', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('419', '21', 'Litoral', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('420', '25', 'Archipiélago Los Roques', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('421', '22', 'Aroa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('422', '22', 'Boraure', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('423', '22', 'Campo Elías de Yaracuy', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('424', '22', 'Chivacoa', '0', '2017-11-09 22:26:33', '2017-11-09 22:26:33', null);
INSERT INTO "public"."ciudades" VALUES ('425', '22', 'Cocorote', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('426', '22', 'Farriar', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('427', '22', 'Guama', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('428', '22', 'Marín', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('429', '22', 'Nirgua', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('430', '22', 'Sabana de Parra', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('431', '22', 'Salom', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('432', '22', 'San Felipe', '1', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('433', '22', 'San Pablo de Yaracuy', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('434', '22', 'Urachiche', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('435', '22', 'Yaritagua', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('436', '22', 'Yumare', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('437', '23', 'Bachaquero', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('438', '23', 'Bobures', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('439', '23', 'Cabimas', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('440', '23', 'Campo Concepción', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('441', '23', 'Campo Mara', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('442', '23', 'Campo Rojo', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('443', '23', 'Carrasquero', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('444', '23', 'Casigua', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('445', '23', 'Chiquinquirá', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('446', '23', 'Ciudad Ojeda', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('447', '23', 'El Batey', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('448', '23', 'El Carmelo', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('449', '23', 'El Chivo', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('450', '23', 'El Guayabo', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('451', '23', 'El Mene', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('452', '23', 'El Venado', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('453', '23', 'Encontrados', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('454', '23', 'Gibraltar', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('455', '23', 'Isla de Toas', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('456', '23', 'La Concepción del Zulia', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('457', '23', 'La Paz', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('458', '23', 'La Sierrita', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('459', '23', 'Lagunillas del Zulia', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('460', '23', 'Las Piedras de Perijá', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('461', '23', 'Los Cortijos', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('462', '23', 'Machiques', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('463', '23', 'Maracaibo', '1', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('464', '23', 'Mene Grande', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('465', '23', 'Palmarejo', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('466', '23', 'Paraguaipoa', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('467', '23', 'Potrerito', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('468', '23', 'Pueblo Nuevo del Zulia', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('469', '23', 'Puertos de Altagracia', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('470', '23', 'Punta Gorda', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('471', '23', 'Sabaneta de Palma', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('472', '23', 'San Francisco', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('473', '23', 'San José de Perijá', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('474', '23', 'San Rafael del Moján', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('475', '23', 'San Timoteo', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('476', '23', 'Santa Bárbara Del Zulia', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('477', '23', 'Santa Cruz de Mara', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('478', '23', 'Santa Cruz del Zulia', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('479', '23', 'Santa Rita', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('480', '23', 'Sinamaica', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('481', '23', 'Tamare', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('482', '23', 'Tía Juana', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('483', '23', 'Villa del Rosario', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('484', '21', 'La Guaira', '1', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('485', '21', 'Catia La Mar', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('486', '21', 'Macuto', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('487', '21', 'Naiguatá', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('488', '25', 'Archipiélago Los Monjes', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('489', '25', 'Isla La Tortuga y Cayos adyacentes', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('490', '25', 'Isla La Sola', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('491', '25', 'Islas Los Testigos', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('492', '25', 'Islas Los Frailes', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('493', '25', 'Isla La Orchila', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('494', '25', 'Archipiélago Las Aves', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('495', '25', 'Isla de Aves', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('496', '25', 'Isla La Blanquilla', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('497', '25', 'Isla de Patos', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."ciudades" VALUES ('498', '25', 'Islas Los Hermanos', '0', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);

-- ----------------------------
-- Table structure for cobros
-- ----------------------------
DROP TABLE IF EXISTS "public"."cobros";
CREATE TABLE "public"."cobros" (
"id" int4 DEFAULT nextval('cobros_id_seq'::regclass) NOT NULL,
"contratos_id" int4 NOT NULL,
"sucursal_id" int4 NOT NULL,
"personas_id" int4 NOT NULL,
"personas_bancos_id" int4,
"bancos_id" int4,
"total_cobrar" numeric(15,2) NOT NULL,
"concepto" text COLLATE "default" NOT NULL,
"fecha_pagado" date,
"total_pagado" numeric(15,2),
"num_recibo" varchar(20) COLLATE "default",
"tipo_pago" varchar(20) COLLATE "default",
"completo" bool DEFAULT false NOT NULL,
"giro" bool DEFAULT false NOT NULL,
"lotes_id" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cobros
-- ----------------------------

-- ----------------------------
-- Table structure for configuracion
-- ----------------------------
DROP TABLE IF EXISTS "public"."configuracion";
CREATE TABLE "public"."configuracion" (
"id" int4 DEFAULT nextval('configuracion_id_seq'::regclass) NOT NULL,
"propiedad" varchar(100) COLLATE "default" NOT NULL,
"valor" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of configuracion
-- ----------------------------
INSERT INTO "public"."configuracion" VALUES ('1', 'logo', 'logo.png', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."configuracion" VALUES ('2', 'login_logo', 'login_logo.png', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."configuracion" VALUES ('3', 'nombre', 'Base', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."configuracion" VALUES ('4', 'formato_fecha', 'd/m/Y', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."configuracion" VALUES ('5', 'miles', '.', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."configuracion" VALUES ('6', 'email', 'Base@base.com', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."configuracion" VALUES ('7', 'email_name', 'Base', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."configuracion" VALUES ('8', 'nombre_empresa', 'empresa', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);

-- ----------------------------
-- Table structure for contrato_tipo
-- ----------------------------
DROP TABLE IF EXISTS "public"."contrato_tipo";
CREATE TABLE "public"."contrato_tipo" (
"id" int4 DEFAULT nextval('contrato_tipo_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of contrato_tipo
-- ----------------------------
INSERT INTO "public"."contrato_tipo" VALUES ('1', 'Normal', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."contrato_tipo" VALUES ('2', 'Corporativo', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."contrato_tipo" VALUES ('3', 'Combenio', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);

-- ----------------------------
-- Table structure for contratos
-- ----------------------------
DROP TABLE IF EXISTS "public"."contratos";
CREATE TABLE "public"."contratos" (
"id" int4 DEFAULT nextval('contratos_id_seq'::regclass) NOT NULL,
"planilla" varchar(10) COLLATE "default",
"titular" int4 NOT NULL,
"vendedor_id" int4 NOT NULL,
"sucursal_id" int4 NOT NULL,
"empresa_id" int4 NOT NULL,
"cargado" date NOT NULL,
"primer_cobro" date,
"vencimiento" date NOT NULL,
"inicio" date NOT NULL,
"plan_detalles_id" int4,
"frecuencia_pagos_id" int4,
"tipo_pago" int4 NOT NULL,
"total_contrato" numeric(15,2) NOT NULL,
"total_pagado" numeric(15,2) NOT NULL,
"cuotas" int4,
"cuotas_pagadas" int4,
"cuota_valor" numeric(15,2),
"giros_pagados" int4,
"giro_valor" numeric(15,2),
"giros" numeric(15,2),
"personas_bancos_id" int4,
"beneficiarios" int4 NOT NULL,
"meses" int4,
"inicial" numeric(15,2),
"fecha_incial" date,
"fecha_pago" date,
"anulado" bool DEFAULT false NOT NULL,
"bookie" bool DEFAULT false NOT NULL,
"cobrando" bool DEFAULT false NOT NULL,
"renovaciones" int4 DEFAULT 0 NOT NULL,
"tipo_contrato" int4 DEFAULT 0 NOT NULL,
"estatus_contrato_id" int4 NOT NULL,
"observaciones" text COLLATE "default",
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of contratos
-- ----------------------------
INSERT INTO "public"."contratos" VALUES ('1', '001', '18', '2', '1', '1', '2017-11-10', null, '2018-11-10', '2017-11-10', '1', '1', '2', '100.00', '0.00', '3', null, null, null, null, null, '1', '1', null, '10.00', '2017-11-10', '2017-11-10', 'f', 'f', 'f', '0', '0', '1', null, '2017-11-10 15:16:18', '2017-11-10 15:16:18', null);

-- ----------------------------
-- Table structure for contratos_detalles
-- ----------------------------
DROP TABLE IF EXISTS "public"."contratos_detalles";
CREATE TABLE "public"."contratos_detalles" (
"id" int4 DEFAULT nextval('contratos_detalles_id_seq'::regclass) NOT NULL,
"contratos_id" int4 NOT NULL,
"personas_id" int4,
"operacion" text COLLATE "default" NOT NULL,
"planilla" varchar(10) COLLATE "default",
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of contratos_detalles
-- ----------------------------
INSERT INTO "public"."contratos_detalles" VALUES ('1', '1', '1', 'Carga de Nuevo contrato Por Analista: Administrador', '001', '2017-11-10 15:16:18', '2017-11-10 15:16:18', null);
INSERT INTO "public"."contratos_detalles" VALUES ('2', '1', '1', 'Generacion de Cobro de inicial por sistema', 's/n', '2017-11-10 15:16:36', '2017-11-10 15:16:36', null);
INSERT INTO "public"."contratos_detalles" VALUES ('3', '1', '1', 'Generacion de Cobro de inicial por sistema', 's/n', '2017-11-10 15:25:32', '2017-11-10 15:25:32', null);
INSERT INTO "public"."contratos_detalles" VALUES ('4', '1', '1', 'Generacion de Cobro de inicial por sistema', 's/n', '2017-11-10 15:42:44', '2017-11-10 15:42:44', null);
INSERT INTO "public"."contratos_detalles" VALUES ('5', '1', '1', 'Generacion de Cobro de inicial por sistema', 's/n', '2017-11-10 19:06:45', '2017-11-10 19:06:45', null);
INSERT INTO "public"."contratos_detalles" VALUES ('14', '1', '1', 'Generacion de Cobro de inicial por sistema', 's/n', '2017-11-10 19:47:52', '2017-11-10 19:47:52', null);

-- ----------------------------
-- Table structure for contratos_facturar
-- ----------------------------
DROP TABLE IF EXISTS "public"."contratos_facturar";
CREATE TABLE "public"."contratos_facturar" (
"id" int4 DEFAULT nextval('contratos_facturar_id_seq'::regclass) NOT NULL,
"fecha_facturar" date NOT NULL,
"contratos_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of contratos_facturar
-- ----------------------------

-- ----------------------------
-- Table structure for contratos_temp
-- ----------------------------
DROP TABLE IF EXISTS "public"."contratos_temp";
CREATE TABLE "public"."contratos_temp" (
"id" int4 DEFAULT nextval('contratos_temp_id_seq'::regclass) NOT NULL,
"planilla" varchar(10) COLLATE "default",
"contrato_id" int4 NOT NULL,
"solicitud_id" int4 NOT NULL,
"primer_cobro" date,
"vencimiento" date NOT NULL,
"inicio" date NOT NULL,
"plan_detalles_id" int4,
"frecuencia_pagos_id" int4,
"tipo_pago" int4 NOT NULL,
"total_contrato" numeric(15,2) NOT NULL,
"total_pagado" numeric(15,2) NOT NULL,
"cuotas" int4 NOT NULL,
"cuotas_pagadas" int4,
"personas_bancos_id" int4,
"beneficiarios" int4 NOT NULL,
"rechazado" bool DEFAULT false NOT NULL,
"inicial" numeric(15,2),
"fecha_incial" date,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of contratos_temp
-- ----------------------------

-- ----------------------------
-- Table structure for controlfacturacion
-- ----------------------------
DROP TABLE IF EXISTS "public"."controlfacturacion";
CREATE TABLE "public"."controlfacturacion" (
"id" int4 DEFAULT nextval('controlfacturacion_id_seq'::regclass) NOT NULL,
"fecha_inicio" date NOT NULL,
"fecha_final" date,
"total_registros" int4,
"total_bolivares" numeric(10,2),
"total_facturado" numeric(10,2),
"mes" int4 NOT NULL,
"ano" int4 NOT NULL,
"estatus" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of controlfacturacion
-- ----------------------------

-- ----------------------------
-- Table structure for empresa
-- ----------------------------
DROP TABLE IF EXISTS "public"."empresa";
CREATE TABLE "public"."empresa" (
"id" int4 DEFAULT nextval('empresa_id_seq'::regclass) NOT NULL,
"rif" varchar(80) COLLATE "default" NOT NULL,
"nombre" varchar(80) COLLATE "default" NOT NULL,
"abreviatura" varchar(80) COLLATE "default" NOT NULL,
"direccion" varchar(200) COLLATE "default" NOT NULL,
"tlf" varchar(200) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of empresa
-- ----------------------------
INSERT INTO "public"."empresa" VALUES ('1', 'J-30687366-0', 'MI SALUD MEDICINA PREPAGADA C.A.', 'Mi salud', 'direccion', '0', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);

-- ----------------------------
-- Table structure for estados
-- ----------------------------
DROP TABLE IF EXISTS "public"."estados";
CREATE TABLE "public"."estados" (
"id" int4 DEFAULT nextval('estados_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"iso_3166-2" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of estados
-- ----------------------------
INSERT INTO "public"."estados" VALUES ('1', 'Amazonas', 'VE-X', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('2', 'Anzoátegui', 'VE-B', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('3', 'Apure', 'VE-C', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('4', 'Aragua', 'VE-D', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('5', 'Barinas', 'VE-E', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('6', 'Bolívar', 'VE-F', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('7', 'Carabobo', 'VE-G', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('8', 'Cojedes', 'VE-H', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('9', 'Delta Amacuro', 'VE-Y', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('10', 'Falcón', 'VE-I', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('11', 'Guárico', 'VE-J', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('12', 'Lara', 'VE-K', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('13', 'Mérida', 'VE-L', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('14', 'Miranda', 'VE-M', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('15', 'Monagas', 'VE-N', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('16', 'Nueva Esparta', 'VE-O', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('17', 'Portuguesa', 'VE-P', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('18', 'Sucre', 'VE-R', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('19', 'Táchira', 'VE-S', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('20', 'Trujillo', 'VE-T', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('21', 'Vargas', 'VE-W', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('22', 'Yaracuy', 'VE-U', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('23', 'Zulia', 'VE-V', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('24', 'Distrito Capital', 'VE-A', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);
INSERT INTO "public"."estados" VALUES ('25', 'Dependencias Federales', 'VE-Z', '2017-11-09 22:26:32', '2017-11-09 22:26:32', null);

-- ----------------------------
-- Table structure for estatus_contrato
-- ----------------------------
DROP TABLE IF EXISTS "public"."estatus_contrato";
CREATE TABLE "public"."estatus_contrato" (
"id" int4 DEFAULT nextval('estatus_contrato_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of estatus_contrato
-- ----------------------------
INSERT INTO "public"."estatus_contrato" VALUES ('1', 'Activo', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."estatus_contrato" VALUES ('2', 'En espera de Confirmacion', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."estatus_contrato" VALUES ('3', 'En Revision', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."estatus_contrato" VALUES ('4', 'Rechazado', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."estatus_contrato" VALUES ('5', 'Vencido', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."estatus_contrato" VALUES ('6', 'Anulado Bookie', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);

-- ----------------------------
-- Table structure for frecuencia_pagos
-- ----------------------------
DROP TABLE IF EXISTS "public"."frecuencia_pagos";
CREATE TABLE "public"."frecuencia_pagos" (
"id" int4 DEFAULT nextval('frecuencia_pagos_id_seq'::regclass) NOT NULL,
"frecuencia" varchar(1) COLLATE "default" NOT NULL,
"dias" varchar(10) COLLATE "default" NOT NULL,
"dias_str" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of frecuencia_pagos
-- ----------------------------
INSERT INTO "public"."frecuencia_pagos" VALUES ('1', 'm', '05', 'Mensual los 05', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('2', 'm', '10', 'Mensual los 10', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('3', 'm', '15', 'Mensual los 15', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('4', 'm', '17', 'MENSUAL LOS 17', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('5', 'm', '20', 'Mensual los 20', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('6', 'm', '25', 'Mensual los 25', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('7', 'm', '30', 'Mensual los 30', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('8', 'm', '11', 'Mensual los 11', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('9', 'q', '01,16', 'QUINCENAL 16 y 01', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('10', 'q', '15,30', 'DOCENTE 15 Y 30', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('11', 'q', '05,20', 'FERROMINERA 05 Y 20', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('12', 'q', '07,22', 'QUINCENALES 07 Y 22', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."frecuencia_pagos" VALUES ('13', 'q', '10,25', 'OBREROS DE EDUCACION 10 Y 25', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);

-- ----------------------------
-- Table structure for libro
-- ----------------------------
DROP TABLE IF EXISTS "public"."libro";
CREATE TABLE "public"."libro" (
"id" int4 DEFAULT nextval('libro_id_seq'::regclass) NOT NULL,
"fecha" date NOT NULL,
"desde" int4 NOT NULL,
"hasta" int4 NOT NULL,
"total" numeric(10,2) NOT NULL,
"sucursal_id" int4,
"controlfacturacion_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of libro
-- ----------------------------

-- ----------------------------
-- Table structure for libro_historial
-- ----------------------------
DROP TABLE IF EXISTS "public"."libro_historial";
CREATE TABLE "public"."libro_historial" (
"id" int4 DEFAULT nextval('libro_historial_id_seq'::regclass) NOT NULL,
"fecha" date NOT NULL,
"desde" int4 NOT NULL,
"hasta" int4 NOT NULL,
"total" numeric(10,2) NOT NULL,
"sucursal_id" int4,
"controlfacturacion_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of libro_historial
-- ----------------------------

-- ----------------------------
-- Table structure for lotes
-- ----------------------------
DROP TABLE IF EXISTS "public"."lotes";
CREATE TABLE "public"."lotes" (
"id" int4 DEFAULT nextval('lotes_id_seq'::regclass) NOT NULL,
"fecha" date,
"banco_empresa_id" int4,
"total_registros" int4,
"recobro" bool DEFAULT false NOT NULL,
"total_cobrar" numeric(15,2),
"archivo" varchar(100) COLLATE "default",
"cargado" bool DEFAULT false NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of lotes
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."migrations";
CREATE TABLE "public"."migrations" (
"id" int4 DEFAULT nextval('migrations_id_seq'::regclass) NOT NULL,
"migration" varchar(191) COLLATE "default" NOT NULL,
"batch" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO "public"."migrations" VALUES ('1', '2016_10_04_092235_sessions', '1');
INSERT INTO "public"."migrations" VALUES ('2', '2016_10_04_092243_password_resets', '1');
INSERT INTO "public"."migrations" VALUES ('3', '2016_10_04_092257_app_perfil', '1');
INSERT INTO "public"."migrations" VALUES ('4', '2016_10_04_092315_app_perfiles_permisos', '1');
INSERT INTO "public"."migrations" VALUES ('5', '2016_11_11_092235_configuracion', '1');
INSERT INTO "public"."migrations" VALUES ('6', '2017_02_12_171815_tipo_persona', '1');
INSERT INTO "public"."migrations" VALUES ('7', '2017_03_03_125108_estados', '1');
INSERT INTO "public"."migrations" VALUES ('8', '2017_03_03_125109_ciudades', '1');
INSERT INTO "public"."migrations" VALUES ('9', '2017_03_03_125110_municipios', '1');
INSERT INTO "public"."migrations" VALUES ('10', '2017_03_03_125111_parroquias', '1');
INSERT INTO "public"."migrations" VALUES ('11', '2017_03_03_125112_sectores', '1');
INSERT INTO "public"."migrations" VALUES ('12', '2017_03_04_141241_bancos', '1');
INSERT INTO "public"."migrations" VALUES ('13', '2017_03_04_151316_banco_tipo_cuenta', '1');
INSERT INTO "public"."migrations" VALUES ('14', '2017_03_04_152926_tipo_telefono', '1');
INSERT INTO "public"."migrations" VALUES ('15', '2017_03_06_091217_profesion', '1');
INSERT INTO "public"."migrations" VALUES ('16', '2017_03_06_143056_personas', '1');
INSERT INTO "public"."migrations" VALUES ('17', '2017_03_06_143543_personas_detalles', '1');
INSERT INTO "public"."migrations" VALUES ('18', '2017_03_06_144342_personas_telefonos', '1');
INSERT INTO "public"."migrations" VALUES ('19', '2017_03_06_145308_personas_correo', '1');
INSERT INTO "public"."migrations" VALUES ('20', '2017_03_06_145912_personas_direccion', '1');
INSERT INTO "public"."migrations" VALUES ('21', '2017_03_06_150621_personas_bancos', '1');
INSERT INTO "public"."migrations" VALUES ('22', '2017_03_06_150623_app_usuario', '1');
INSERT INTO "public"."migrations" VALUES ('23', '2017_03_22_114150_app_usuario_empresa', '1');
INSERT INTO "public"."migrations" VALUES ('24', '2017_04_18_094006_app_usuario_app_perfil', '1');
INSERT INTO "public"."migrations" VALUES ('25', '2017_05_01_172938_solicitudes', '1');
INSERT INTO "public"."migrations" VALUES ('26', '2017_05_21_005027_create_audits_table', '1');
INSERT INTO "public"."migrations" VALUES ('27', '2017_10_04_092321_app_usuario_permisos', '1');
INSERT INTO "public"."migrations" VALUES ('28', '2017_04_11_212633_empresa', '2');
INSERT INTO "public"."migrations" VALUES ('29', '2017_05_06_112649_sucursal', '2');
INSERT INTO "public"."migrations" VALUES ('30', '2017_08_30_090521_bancos_empresa', '2');
INSERT INTO "public"."migrations" VALUES ('31', '2017_03_31_160715_plan', '3');
INSERT INTO "public"."migrations" VALUES ('32', '2017_03_31_161332_plan_detalles', '3');
INSERT INTO "public"."migrations" VALUES ('33', '2017_04_07_214815_vendedores', '3');
INSERT INTO "public"."migrations" VALUES ('34', '2017_04_07_220834_vendedores_sucusal', '3');
INSERT INTO "public"."migrations" VALUES ('35', '2017_03_30_192602_contrato_tipo', '4');
INSERT INTO "public"."migrations" VALUES ('36', '2017_03_30_193317_parentesco', '4');
INSERT INTO "public"."migrations" VALUES ('37', '2017_03_30_202615_estatus_contrato', '4');
INSERT INTO "public"."migrations" VALUES ('38', '2017_04_23_195947_frecuencia_pagos', '4');
INSERT INTO "public"."migrations" VALUES ('39', '2017_04_24_111732_contratos', '4');
INSERT INTO "public"."migrations" VALUES ('40', '2017_04_24_111732_contratos_temp', '4');
INSERT INTO "public"."migrations" VALUES ('41', '2017_04_26_111619_contratos_detalles', '4');
INSERT INTO "public"."migrations" VALUES ('42', '2017_04_26_180459_beneficiarios', '4');
INSERT INTO "public"."migrations" VALUES ('43', '2017_05_30_200448_solicitud_beneficiarios_temp', '4');
INSERT INTO "public"."migrations" VALUES ('44', '2017_05_30_200507_beneficiarios_temp', '4');
INSERT INTO "public"."migrations" VALUES ('45', '2017_08_02_152943_contratos_facturar', '5');
INSERT INTO "public"."migrations" VALUES ('46', '2017_08_05_154231_controlfacturacion', '5');
INSERT INTO "public"."migrations" VALUES ('47', '2017_08_05_154633_movimientos', '5');
INSERT INTO "public"."migrations" VALUES ('48', '2017_08_05_160040_afacturar', '5');
INSERT INTO "public"."migrations" VALUES ('49', '2017_08_05_160314_libro', '5');
INSERT INTO "public"."migrations" VALUES ('50', '2017_08_09_150301_historial', '5');
INSERT INTO "public"."migrations" VALUES ('51', '2017_06_02_200116_lotes', '6');
INSERT INTO "public"."migrations" VALUES ('52', '2017_06_03_224231_cobros', '6');

-- ----------------------------
-- Table structure for movimientos
-- ----------------------------
DROP TABLE IF EXISTS "public"."movimientos";
CREATE TABLE "public"."movimientos" (
"id" int4 DEFAULT nextval('movimientos_id_seq'::regclass) NOT NULL,
"ci" varchar(191) COLLATE "default" NOT NULL,
"fecha" date NOT NULL,
"monto" numeric(10,2) NOT NULL,
"sucursal_id" int4 NOT NULL,
"controlfacturacion_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of movimientos
-- ----------------------------

-- ----------------------------
-- Table structure for movimientos_historial
-- ----------------------------
DROP TABLE IF EXISTS "public"."movimientos_historial";
CREATE TABLE "public"."movimientos_historial" (
"id" int4 DEFAULT nextval('movimientos_historial_id_seq'::regclass) NOT NULL,
"ci" varchar(191) COLLATE "default" NOT NULL,
"fecha" date NOT NULL,
"monto" numeric(10,2) NOT NULL,
"sucursal_id" int4 NOT NULL,
"controlfacturacion_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of movimientos_historial
-- ----------------------------

-- ----------------------------
-- Table structure for municipios
-- ----------------------------
DROP TABLE IF EXISTS "public"."municipios";
CREATE TABLE "public"."municipios" (
"id" int4 DEFAULT nextval('municipios_id_seq'::regclass) NOT NULL,
"estados_id" int4 NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of municipios
-- ----------------------------
INSERT INTO "public"."municipios" VALUES ('1', '1', 'Alto Orinoco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('2', '1', 'Atabapo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('3', '1', 'Atures', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('4', '1', 'Autana', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('5', '1', 'Manapiare', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('6', '1', 'Maroa', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('7', '1', 'Río Negro', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('8', '2', 'Anaco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('9', '2', 'Aragua', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('10', '2', 'Manuel Ezequiel Bruzual', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('11', '2', 'Diego Bautista Urbaneja', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('12', '2', 'Fernando Peñalver', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('13', '2', 'Francisco Del Carmen Carvajal', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('14', '2', 'General Sir Arthur McGregor', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('15', '2', 'Guanta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('16', '2', 'Independencia', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('17', '2', 'José Gregorio Monagas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('18', '2', 'Juan Antonio Sotillo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('19', '2', 'Juan Manuel Cajigal', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('20', '2', 'Libertad', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('21', '2', 'Francisco de Miranda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('22', '2', 'Pedro María Freites', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('23', '2', 'Píritu', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('24', '2', 'San José de Guanipa', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('25', '2', 'San Juan de Capistrano', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('26', '2', 'Santa Ana', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('27', '2', 'Simón Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('28', '2', 'Simón Rodríguez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('29', '3', 'Achaguas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('30', '3', 'Biruaca', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('31', '3', 'Muñóz', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('32', '3', 'Páez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('33', '3', 'Pedro Camejo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('34', '3', 'Rómulo Gallegos', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('35', '3', 'San Fernando', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('36', '4', 'Atanasio Girardot', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('37', '4', 'Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('38', '4', 'Camatagua', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('39', '4', 'Francisco Linares Alcántara', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('40', '4', 'José Ángel Lamas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('41', '4', 'José Félix Ribas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('42', '4', 'José Rafael Revenga', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('43', '4', 'Libertador', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('44', '4', 'Mario Briceño Iragorry', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('45', '4', 'Ocumare de la Costa de Oro', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('46', '4', 'San Casimiro', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('47', '4', 'San Sebastián', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('48', '4', 'Santiago Mariño', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('49', '4', 'Santos Michelena', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('50', '4', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('51', '4', 'Tovar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('52', '4', 'Urdaneta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('53', '4', 'Zamora', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('54', '5', 'Alberto Arvelo Torrealba', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('55', '5', 'Andrés Eloy Blanco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('56', '5', 'Antonio José de Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('57', '5', 'Arismendi', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('58', '5', 'Barinas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('59', '5', 'Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('60', '5', 'Cruz Paredes', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('61', '5', 'Ezequiel Zamora', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('62', '5', 'Obispos', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('63', '5', 'Pedraza', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('64', '5', 'Rojas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('65', '5', 'Sosa', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('66', '6', 'Caroní', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('67', '6', 'Cedeño', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('68', '6', 'El Callao', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('69', '6', 'Gran Sabana', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('70', '6', 'Heres', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('71', '6', 'Piar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('72', '6', 'Angostura (Raúl Leoni)', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('73', '6', 'Roscio', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('74', '6', 'Sifontes', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('75', '6', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('76', '6', 'Padre Pedro Chien', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('77', '7', 'Bejuma', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('78', '7', 'Carlos Arvelo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('79', '7', 'Diego Ibarra', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('80', '7', 'Guacara', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('81', '7', 'Juan José Mora', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('82', '7', 'Libertador', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('83', '7', 'Los Guayos', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('84', '7', 'Miranda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('85', '7', 'Montalbán', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('86', '7', 'Naguanagua', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('87', '7', 'Puerto Cabello', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('88', '7', 'San Diego', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('89', '7', 'San Joaquín', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('90', '7', 'Valencia', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('91', '8', 'Anzoátegui', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('92', '8', 'Tinaquillo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('93', '8', 'Girardot', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('94', '8', 'Lima Blanco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('95', '8', 'Pao de San Juan Bautista', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('96', '8', 'Ricaurte', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('97', '8', 'Rómulo Gallegos', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('98', '8', 'San Carlos', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('99', '8', 'Tinaco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('100', '9', 'Antonio Díaz', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('101', '9', 'Casacoima', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('102', '9', 'Pedernales', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('103', '9', 'Tucupita', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('104', '10', 'Acosta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('105', '10', 'Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('106', '10', 'Buchivacoa', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('107', '10', 'Cacique Manaure', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('108', '10', 'Carirubana', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('109', '10', 'Colina', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('110', '10', 'Dabajuro', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('111', '10', 'Democracia', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('112', '10', 'Falcón', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('113', '10', 'Federación', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('114', '10', 'Jacura', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('115', '10', 'José Laurencio Silva', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('116', '10', 'Los Taques', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('117', '10', 'Mauroa', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('118', '10', 'Miranda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('119', '10', 'Monseñor Iturriza', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('120', '10', 'Palmasola', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('121', '10', 'Petit', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('122', '10', 'Píritu', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('123', '10', 'San Francisco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('124', '10', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('125', '10', 'Tocópero', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('126', '10', 'Unión', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('127', '10', 'Urumaco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('128', '10', 'Zamora', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('129', '11', 'Camaguán', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('130', '11', 'Chaguaramas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('131', '11', 'El Socorro', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('132', '11', 'José Félix Ribas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('133', '11', 'José Tadeo Monagas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('134', '11', 'Juan Germán Roscio', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('135', '11', 'Julián Mellado', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('136', '11', 'Las Mercedes', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('137', '11', 'Leonardo Infante', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('138', '11', 'Pedro Zaraza', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('139', '11', 'Ortíz', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('140', '11', 'San Gerónimo de Guayabal', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('141', '11', 'San José de Guaribe', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('142', '11', 'Santa María de Ipire', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('143', '11', 'Sebastián Francisco de Miranda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('144', '12', 'Andrés Eloy Blanco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('145', '12', 'Crespo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('146', '12', 'Iribarren', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('147', '12', 'Jiménez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('148', '12', 'Morán', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('149', '12', 'Palavecino', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('150', '12', 'Simón Planas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('151', '12', 'Torres', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('152', '12', 'Urdaneta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('153', '13', 'Alberto Adriani', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('154', '13', 'Andrés Bello', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('155', '13', 'Antonio Pinto Salinas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('156', '13', 'Aricagua', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('157', '13', 'Arzobispo Chacón', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('158', '13', 'Campo Elías', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('159', '13', 'Caracciolo Parra Olmedo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('160', '13', 'Cardenal Quintero', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('161', '13', 'Guaraque', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('162', '13', 'Julio César Salas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('163', '13', 'Justo Briceño', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('164', '13', 'Libertador', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('165', '13', 'Miranda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('166', '13', 'Obispo Ramos de Lora', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('167', '13', 'Padre Noguera', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('168', '13', 'Pueblo Llano', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('169', '13', 'Rangel', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('170', '13', 'Rivas Dávila', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('171', '13', 'Santos Marquina', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('172', '13', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('173', '13', 'Tovar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('174', '13', 'Tulio Febres Cordero', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('175', '13', 'Zea', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('176', '14', 'Acevedo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('177', '14', 'Andrés Bello', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('178', '14', 'Baruta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('179', '14', 'Brión', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('180', '14', 'Buroz', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('181', '14', 'Carrizal', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('182', '14', 'Chacao', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('183', '14', 'Cristóbal Rojas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('184', '14', 'El Hatillo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('185', '14', 'Guaicaipuro', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('186', '14', 'Independencia', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('187', '14', 'Lander', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('188', '14', 'Los Salias', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('189', '14', 'Páez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('190', '14', 'Paz Castillo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('191', '14', 'Pedro Gual', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('192', '14', 'Plaza', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('193', '14', 'Simón Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('194', '14', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('195', '14', 'Urdaneta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('196', '14', 'Zamora', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('197', '15', 'Acosta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('198', '15', 'Aguasay', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('199', '15', 'Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('200', '15', 'Caripe', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('201', '15', 'Cedeño', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('202', '15', 'Ezequiel Zamora', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('203', '15', 'Libertador', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('204', '15', 'Maturín', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('205', '15', 'Piar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('206', '15', 'Punceres', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('207', '15', 'Santa Bárbara', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('208', '15', 'Sotillo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('209', '15', 'Uracoa', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('210', '16', 'Antolín del Campo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('211', '16', 'Arismendi', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('212', '16', 'García', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('213', '16', 'Gómez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('214', '16', 'Maneiro', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('215', '16', 'Marcano', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('216', '16', 'Mariño', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('217', '16', 'Península de Macanao', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('218', '16', 'Tubores', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('219', '16', 'Villalba', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('220', '16', 'Díaz', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('221', '17', 'Agua Blanca', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('222', '17', 'Araure', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('223', '17', 'Esteller', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('224', '17', 'Guanare', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('225', '17', 'Guanarito', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('226', '17', 'Monseñor José Vicente de Unda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('227', '17', 'Ospino', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('228', '17', 'Páez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('229', '17', 'Papelón', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('230', '17', 'San Genaro de Boconoíto', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('231', '17', 'San Rafael de Onoto', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('232', '17', 'Santa Rosalía', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('233', '17', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('234', '17', 'Turén', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('235', '18', 'Andrés Eloy Blanco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('236', '18', 'Andrés Mata', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('237', '18', 'Arismendi', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('238', '18', 'Benítez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('239', '18', 'Bermúdez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('240', '18', 'Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('241', '18', 'Cajigal', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('242', '18', 'Cruz Salmerón Acosta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('243', '18', 'Libertador', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('244', '18', 'Mariño', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('245', '18', 'Mejía', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('246', '18', 'Montes', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('247', '18', 'Ribero', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('248', '18', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('249', '18', 'Valdéz', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('250', '19', 'Andrés Bello', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('251', '19', 'Antonio Rómulo Costa', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('252', '19', 'Ayacucho', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('253', '19', 'Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('254', '19', 'Cárdenas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('255', '19', 'Córdoba', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('256', '19', 'Fernández Feo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('257', '19', 'Francisco de Miranda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('258', '19', 'García de Hevia', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('259', '19', 'Guásimos', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('260', '19', 'Independencia', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('261', '19', 'Jáuregui', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('262', '19', 'José María Vargas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('263', '19', 'Junín', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('264', '19', 'Libertad', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('265', '19', 'Libertador', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('266', '19', 'Lobatera', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('267', '19', 'Michelena', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('268', '19', 'Panamericano', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('269', '19', 'Pedro María Ureña', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('270', '19', 'Rafael Urdaneta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('271', '19', 'Samuel Darío Maldonado', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('272', '19', 'San Cristóbal', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('273', '19', 'Seboruco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('274', '19', 'Simón Rodríguez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('275', '19', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('276', '19', 'Torbes', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('277', '19', 'Uribante', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('278', '19', 'San Judas Tadeo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('279', '20', 'Andrés Bello', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('280', '20', 'Boconó', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('281', '20', 'Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('282', '20', 'Candelaria', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('283', '20', 'Carache', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('284', '20', 'Escuque', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('285', '20', 'José Felipe Márquez Cañizalez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('286', '20', 'Juan Vicente Campos Elías', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('287', '20', 'La Ceiba', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('288', '20', 'Miranda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('289', '20', 'Monte Carmelo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('290', '20', 'Motatán', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('291', '20', 'Pampán', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('292', '20', 'Pampanito', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('293', '20', 'Rafael Rangel', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('294', '20', 'San Rafael de Carvajal', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('295', '20', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('296', '20', 'Trujillo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('297', '20', 'Urdaneta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('298', '20', 'Valera', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('299', '21', 'Vargas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('300', '22', 'Arístides Bastidas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('301', '22', 'Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('302', '22', 'Bruzual', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('303', '22', 'Cocorote', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('304', '22', 'Independencia', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('305', '22', 'José Antonio Páez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('306', '22', 'La Trinidad', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('307', '22', 'Manuel Monge', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('308', '22', 'Nirgua', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('309', '22', 'Peña', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('310', '22', 'San Felipe', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('311', '22', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('312', '22', 'Urachiche', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('313', '22', 'José Joaquín Veroes', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('314', '23', 'Almirante Padilla', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('315', '23', 'Baralt', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('316', '23', 'Cabimas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('317', '23', 'Catatumbo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('318', '23', 'Colón', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('319', '23', 'Francisco Javier Pulgar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('320', '23', 'Páez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('321', '23', 'Jesús Enrique Losada', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('322', '23', 'Jesús María Semprún', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('323', '23', 'La Cañada de Urdaneta', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('324', '23', 'Lagunillas', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('325', '23', 'Machiques de Perijá', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('326', '23', 'Mara', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('327', '23', 'Maracaibo', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('328', '23', 'Miranda', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('329', '23', 'Rosario de Perijá', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('330', '23', 'San Francisco', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('331', '23', 'Santa Rita', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('332', '23', 'Simón Bolívar', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('333', '23', 'Sucre', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('334', '23', 'Valmore Rodríguez', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."municipios" VALUES ('335', '24', 'Libertador', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);

-- ----------------------------
-- Table structure for parentesco
-- ----------------------------
DROP TABLE IF EXISTS "public"."parentesco";
CREATE TABLE "public"."parentesco" (
"id" int4 DEFAULT nextval('parentesco_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of parentesco
-- ----------------------------
INSERT INTO "public"."parentesco" VALUES ('1', 'ABUELA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('2', 'ABUELO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('3', 'CUÑADO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('4', 'CUÑADA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('5', 'DESCONOCIDO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('6', 'SIN ESPECIFICAR', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('7', 'ESPOSO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('8', 'ESPOSA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('9', 'FAMILIAR LEJANO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('10', 'HERMANO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('11', 'HERMANA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('12', 'HIJO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('13', 'HIJA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('14', 'NIETO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('15', 'NIETA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('16', 'NINGUNO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('17', 'NUERA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('18', 'YERNO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('19', 'PAPA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('20', 'MAMA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('21', 'PRIMO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('22', 'PRIMA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('23', 'SOBRINO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('24', 'SOBRINA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('25', 'SUEGRO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('26', 'SUEGRA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('27', 'TIO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('28', 'TIA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('29', 'VECINO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('30', 'VECINA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('31', 'CONOCIDO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('32', 'CONOCIDA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('33', 'TITULAR', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('34', 'AMIGO', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."parentesco" VALUES ('35', 'AMIGA', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);

-- ----------------------------
-- Table structure for parroquias
-- ----------------------------
DROP TABLE IF EXISTS "public"."parroquias";
CREATE TABLE "public"."parroquias" (
"id" int4 DEFAULT nextval('parroquias_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"municipios_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of parroquias
-- ----------------------------
INSERT INTO "public"."parroquias" VALUES ('1', 'Alto Orinoco', '1', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."parroquias" VALUES ('2', 'Huachamacare Acanaña', '1', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."parroquias" VALUES ('3', 'Marawaka Toky Shamanaña', '1', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."parroquias" VALUES ('4', 'Mavaka Mavaka', '1', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."parroquias" VALUES ('5', 'Sierra Parima Parimabé', '1', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."parroquias" VALUES ('6', 'Ucata Laja Lisa', '2', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."parroquias" VALUES ('7', 'Yapacana Macuruco', '2', '2017-11-09 22:26:34', '2017-11-09 22:26:34', null);
INSERT INTO "public"."parroquias" VALUES ('8', 'Caname Guarinuma', '2', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('9', 'Fernando Girón Tovar', '3', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('10', 'Luis Alberto Gómez', '3', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('11', 'Pahueña Limón de Parhueña', '3', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('12', 'Platanillal Platanillal', '3', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('13', 'Samariapo', '4', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('14', 'Sipapo', '4', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('15', 'Munduapo', '4', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('16', 'Guayapo', '4', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('17', 'Alto Ventuari', '5', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('18', 'Medio Ventuari', '5', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('19', 'Bajo Ventuari', '5', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('20', 'Victorino', '6', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('21', 'Comunidad', '6', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('22', 'Casiquiare', '7', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('23', 'Cocuy', '7', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('24', 'San Carlos de Río Negro', '7', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('25', 'Solano', '7', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('26', 'Anaco', '8', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('27', 'San Joaquín', '8', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('28', 'Cachipo', '9', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('29', 'Aragua de Barcelona', '9', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('30', 'Lechería', '11', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('31', 'El Morro', '11', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('32', 'Puerto Píritu', '12', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('33', 'San Miguel', '12', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('34', 'Sucre', '12', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('35', 'Valle de Guanape', '13', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('36', 'Santa Bárbara', '13', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('37', 'El Chaparro', '14', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('38', 'Tomás Alfaro', '14', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('39', 'Calatrava', '14', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('40', 'Guanta', '15', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('41', 'Chorrerón', '15', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('42', 'Mamo', '16', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('43', 'Soledad', '16', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('44', 'Mapire', '17', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('45', 'Piar', '17', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('46', 'Santa Clara', '17', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('47', 'San Diego de Cabrutica', '17', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('48', 'Uverito', '17', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('49', 'Zuata', '17', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('50', 'Puerto La Cruz', '18', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('51', 'Pozuelos', '18', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('52', 'Onoto', '19', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('53', 'San Pablo', '19', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('54', 'San Mateo', '20', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('55', 'El Carito', '20', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('56', 'Santa Inés', '20', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('57', 'La Romereña', '20', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('58', 'Atapirire', '21', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('59', 'Boca del Pao', '21', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('60', 'El Pao', '21', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('61', 'Pariaguán', '21', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('62', 'Cantaura', '22', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('63', 'Libertador', '22', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('64', 'Santa Rosa', '22', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('65', 'Urica', '22', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('66', 'Píritu', '23', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('67', 'San Francisco', '23', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('68', 'San José de Guanipa', '24', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('69', 'Boca de Uchire', '25', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('70', 'Boca de Chávez', '25', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('71', 'Pueblo Nuevo', '26', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('72', 'Santa Ana', '26', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('73', 'Bergantín', '27', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('74', 'Caigua', '27', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('75', 'El Carmen', '27', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('76', 'El Pilar', '27', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('77', 'Naricual', '27', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('78', 'San Crsitóbal', '27', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('79', 'Edmundo Barrios', '28', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('80', 'Miguel Otero Silva', '28', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('81', 'Achaguas', '29', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('82', 'Apurito', '29', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('83', 'El Yagual', '29', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('84', 'Guachara', '29', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('85', 'Mucuritas', '29', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('86', 'Queseras del medio', '29', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('87', 'Biruaca', '30', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('88', 'Bruzual', '31', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('89', 'Mantecal', '31', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('90', 'Quintero', '31', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('91', 'Rincón Hondo', '31', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('92', 'San Vicente', '31', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('93', 'Guasdualito', '32', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('94', 'Aramendi', '32', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('95', 'El Amparo', '32', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('96', 'San Camilo', '32', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('97', 'Urdaneta', '32', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('98', 'San Juan de Payara', '33', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('99', 'Codazzi', '33', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('100', 'Cunaviche', '33', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('101', 'Elorza', '34', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('102', 'La Trinidad', '34', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('103', 'San Fernando', '35', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('104', 'El Recreo', '35', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('105', 'Peñalver', '35', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('106', 'San Rafael de Atamaica', '35', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('107', 'Pedro José Ovalles', '36', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('108', 'Joaquín Crespo', '36', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('109', 'José Casanova Godoy', '36', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('110', 'Madre María de San José', '36', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('111', 'Andrés Eloy Blanco', '36', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('112', 'Los Tacarigua', '36', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('113', 'Las Delicias', '36', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('114', 'Choroní', '36', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('115', 'Bolívar', '37', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('116', 'Camatagua', '38', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('117', 'Carmen de Cura', '38', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('118', 'Santa Rita', '39', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('119', 'Francisco de Miranda', '39', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('120', 'Moseñor Feliciano González', '39', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('121', 'Santa Cruz', '40', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('122', 'José Félix Ribas', '41', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('123', 'Castor Nieves Ríos', '41', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('124', 'Las Guacamayas', '41', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('125', 'Pao de Zárate', '41', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('126', 'Zuata', '41', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('127', 'José Rafael Revenga', '42', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('128', 'Palo Negro', '43', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('129', 'San Martín de Porres', '43', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('130', 'El Limón', '44', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('131', 'Caña de Azúcar', '44', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('132', 'Ocumare de la Costa', '45', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('133', 'San Casimiro', '46', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('134', 'Güiripa', '46', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('135', 'Ollas de Caramacate', '46', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('136', 'Valle Morín', '46', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('137', 'San Sebastían', '47', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('138', 'Turmero', '48', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('139', 'Arevalo Aponte', '48', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('140', 'Chuao', '48', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('141', 'Samán de Güere', '48', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('142', 'Alfredo Pacheco Miranda', '48', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('143', 'Santos Michelena', '49', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('144', 'Tiara', '49', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('145', 'Cagua', '50', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('146', 'Bella Vista', '50', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('147', 'Tovar', '51', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('148', 'Urdaneta', '52', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('149', 'Las Peñitas', '52', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('150', 'San Francisco de Cara', '52', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('151', 'Taguay', '52', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('152', 'Zamora', '53', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('153', 'Magdaleno', '53', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('154', 'San Francisco de Asís', '53', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('155', 'Valles de Tucutunemo', '53', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('156', 'Augusto Mijares', '53', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('157', 'Sabaneta', '54', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('158', 'Juan Antonio Rodríguez Domínguez', '54', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('159', 'El Cantón', '55', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('160', 'Santa Cruz de Guacas', '55', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('161', 'Puerto Vivas', '55', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('162', 'Ticoporo', '56', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('163', 'Nicolás Pulido', '56', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('164', 'Andrés Bello', '56', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('165', 'Arismendi', '57', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('166', 'Guadarrama', '57', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('167', 'La Unión', '57', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('168', 'San Antonio', '57', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('169', 'Barinas', '58', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('170', 'Alberto Arvelo Larriva', '58', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('171', 'San Silvestre', '58', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('172', 'Santa Inés', '58', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('173', 'Santa Lucía', '58', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('174', 'Torumos', '58', '2017-11-09 22:26:35', '2017-11-09 22:26:35', null);
INSERT INTO "public"."parroquias" VALUES ('175', 'El Carmen', '58', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('176', 'Rómulo Betancourt', '58', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('177', 'Corazón de Jesús', '58', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('178', 'Ramón Ignacio Méndez', '58', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('179', 'Alto Barinas', '58', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('180', 'Manuel Palacio Fajardo', '58', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('181', 'Juan Antonio Rodríguez Domínguez', '58', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('182', 'Dominga Ortiz de Páez', '58', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('183', 'Barinitas', '59', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('184', 'Altamira de Cáceres', '59', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('185', 'Calderas', '59', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('186', 'Barrancas', '60', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('187', 'El Socorro', '60', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('188', 'Mazparrito', '60', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('189', 'Santa Bárbara', '61', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('190', 'Pedro Briceño Méndez', '61', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('191', 'Ramón Ignacio Méndez', '61', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('192', 'José Ignacio del Pumar', '61', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('193', 'Obispos', '62', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('194', 'Guasimitos', '62', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('195', 'El Real', '62', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('196', 'La Luz', '62', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('197', 'Ciudad Bolívia', '63', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('198', 'José Ignacio Briceño', '63', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('199', 'José Félix Ribas', '63', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('200', 'Páez', '63', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('201', 'Libertad', '64', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('202', 'Dolores', '64', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('203', 'Santa Rosa', '64', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('204', 'Palacio Fajardo', '64', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('205', 'Ciudad de Nutrias', '65', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('206', 'El Regalo', '65', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('207', 'Puerto Nutrias', '65', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('208', 'Santa Catalina', '65', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('209', 'Cachamay', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('210', 'Chirica', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('211', 'Dalla Costa', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('212', 'Once de Abril', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('213', 'Simón Bolívar', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('214', 'Unare', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('215', 'Universidad', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('216', 'Vista al Sol', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('217', 'Pozo Verde', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('218', 'Yocoima', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('219', '5 de Julio', '66', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('220', 'Cedeño', '67', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('221', 'Altagracia', '67', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('222', 'Ascensión Farreras', '67', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('223', 'Guaniamo', '67', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('224', 'La Urbana', '67', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('225', 'Pijiguaos', '67', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('226', 'El Callao', '68', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('227', 'Gran Sabana', '69', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('228', 'Ikabarú', '69', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('229', 'Catedral', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('230', 'Zea', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('231', 'Orinoco', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('232', 'José Antonio Páez', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('233', 'Marhuanta', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('234', 'Agua Salada', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('235', 'Vista Hermosa', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('236', 'La Sabanita', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('237', 'Panapana', '70', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('238', 'Andrés Eloy Blanco', '71', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('239', 'Pedro Cova', '71', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('240', 'Raúl Leoni', '72', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('241', 'Barceloneta', '72', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('242', 'Santa Bárbara', '72', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('243', 'San Francisco', '72', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('244', 'Roscio', '73', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('245', 'Salóm', '73', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('246', 'Tumeremo', '74', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('247', 'El Dorado', '74', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('248', 'Dalla Costa', '74', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('249', 'San Isidro', '74', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('250', 'Sucre', '75', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('251', 'Aripao', '75', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('252', 'Guarataro', '75', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('253', 'Las Majadas', '75', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('254', 'Moitaco', '75', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('255', 'Padre Pedro Chien', '76', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('256', 'Río Grande', '76', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('257', 'Bejuma', '77', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('258', 'Canoabo', '77', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('259', 'Simón Bolívar', '77', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('260', 'Güigüe', '78', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('261', 'Carabobo', '78', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('262', 'Tacarigua', '78', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('263', 'Mariara', '79', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('264', 'Aguas Calientes', '79', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('265', 'Ciudad Alianza', '80', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('266', 'Guacara', '80', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('267', 'Yagua', '80', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('268', 'Morón', '81', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('269', 'Yagua', '81', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('270', 'Tocuyito', '82', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('271', 'Independencia', '82', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('272', 'Los Guayos', '83', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('273', 'Miranda', '84', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('274', 'Montalbán', '85', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('275', 'Naguanagua', '86', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('276', 'Bartolomé Salóm', '87', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('277', 'Democracia', '87', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('278', 'Fraternidad', '87', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('279', 'Goaigoaza', '87', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('280', 'Juan José Flores', '87', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('281', 'Unión', '87', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('282', 'Borburata', '87', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('283', 'Patanemo', '87', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('284', 'San Diego', '88', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('285', 'San Joaquín', '89', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('286', 'Candelaria', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('287', 'Catedral', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('288', 'El Socorro', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('289', 'Miguel Peña', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('290', 'Rafael Urdaneta', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('291', 'San Blas', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('292', 'San José', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('293', 'Santa Rosa', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('294', 'Negro Primero', '90', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('295', 'Cojedes', '91', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('296', 'Juan de Mata Suárez', '91', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('297', 'Tinaquillo', '92', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('298', 'El Baúl', '93', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('299', 'Sucre', '93', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('300', 'La Aguadita', '94', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('301', 'Macapo', '94', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('302', 'El Pao', '95', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('303', 'El Amparo', '96', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('304', 'Libertad de Cojedes', '96', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('305', 'Rómulo Gallegos', '97', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('306', 'San Carlos de Austria', '98', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('307', 'Juan Ángel Bravo', '98', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('308', 'Manuel Manrique', '98', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('309', 'General en Jefe José Laurencio Silva', '99', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('310', 'Curiapo', '100', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('311', 'Almirante Luis Brión', '100', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('312', 'Francisco Aniceto Lugo', '100', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('313', 'Manuel Renaud', '100', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('314', 'Padre Barral', '100', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('315', 'Santos de Abelgas', '100', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('316', 'Imataca', '101', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('317', 'Cinco de Julio', '101', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('318', 'Juan Bautista Arismendi', '101', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('319', 'Manuel Piar', '101', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('320', 'Rómulo Gallegos', '101', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('321', 'Pedernales', '102', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('322', 'Luis Beltrán Prieto Figueroa', '102', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('323', 'San José (Delta Amacuro)', '103', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('324', 'José Vidal Marcano', '103', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('325', 'Juan Millán', '103', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('326', 'Leonardo Ruíz Pineda', '103', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('327', 'Mariscal Antonio José de Sucre', '103', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('328', 'Monseñor Argimiro García', '103', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('329', 'San Rafael (Delta Amacuro)', '103', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('330', 'Virgen del Valle', '103', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('331', 'Clarines', '10', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('332', 'Guanape', '10', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('333', 'Sabana de Uchire', '10', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('334', 'Capadare', '104', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('335', 'La Pastora', '104', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('336', 'Libertador', '104', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('337', 'San Juan de los Cayos', '104', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('338', 'Aracua', '105', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('339', 'La Peña', '105', '2017-11-09 22:26:36', '2017-11-09 22:26:36', null);
INSERT INTO "public"."parroquias" VALUES ('340', 'San Luis', '105', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('341', 'Bariro', '106', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('342', 'Borojó', '106', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('343', 'Capatárida', '106', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('344', 'Guajiro', '106', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('345', 'Seque', '106', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('346', 'Zazárida', '106', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('347', 'Valle de Eroa', '106', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('348', 'Cacique Manaure', '107', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('349', 'Norte', '108', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('350', 'Carirubana', '108', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('351', 'Santa Ana', '108', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('352', 'Urbana Punta Cardón', '108', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('353', 'La Vela de Coro', '109', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('354', 'Acurigua', '109', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('355', 'Guaibacoa', '109', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('356', 'Las Calderas', '109', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('357', 'Macoruca', '109', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('358', 'Dabajuro', '110', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('359', 'Agua Clara', '111', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('360', 'Avaria', '111', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('361', 'Pedregal', '111', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('362', 'Piedra Grande', '111', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('363', 'Purureche', '111', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('364', 'Adaure', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('365', 'Adícora', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('366', 'Baraived', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('367', 'Buena Vista', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('368', 'Jadacaquiva', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('369', 'El Vínculo', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('370', 'El Hato', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('371', 'Moruy', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('372', 'Pueblo Nuevo', '112', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('373', 'Agua Larga', '113', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('374', 'El Paují', '113', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('375', 'Independencia', '113', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('376', 'Mapararí', '113', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('377', 'Agua Linda', '114', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('378', 'Araurima', '114', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('379', 'Jacura', '114', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('380', 'Tucacas', '115', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('381', 'Boca de Aroa', '115', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('382', 'Los Taques', '116', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('383', 'Judibana', '116', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('384', 'Mene de Mauroa', '117', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('385', 'San Félix', '117', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('386', 'Casigua', '117', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('387', 'Guzmán Guillermo', '118', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('388', 'Mitare', '118', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('389', 'Río Seco', '118', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('390', 'Sabaneta', '118', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('391', 'San Antonio', '118', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('392', 'San Gabriel', '118', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('393', 'Santa Ana', '118', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('394', 'Boca del Tocuyo', '119', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('395', 'Chichiriviche', '119', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('396', 'Tocuyo de la Costa', '119', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('397', 'Palmasola', '120', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('398', 'Cabure', '121', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('399', 'Colina', '121', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('400', 'Curimagua', '121', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('401', 'San José de la Costa', '122', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('402', 'Píritu', '122', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('403', 'San Francisco', '123', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('404', 'Sucre', '124', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('405', 'Pecaya', '124', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('406', 'Tocópero', '125', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('407', 'El Charal', '126', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('408', 'Las Vegas del Tuy', '126', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('409', 'Santa Cruz de Bucaral', '126', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('410', 'Bruzual', '127', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('411', 'Urumaco', '127', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('412', 'Puerto Cumarebo', '128', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('413', 'La Ciénaga', '128', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('414', 'La Soledad', '128', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('415', 'Pueblo Cumarebo', '128', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('416', 'Zazárida', '128', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('417', 'Churuguara', '113', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('418', 'Camaguán', '129', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('419', 'Puerto Miranda', '129', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('420', 'Uverito', '129', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('421', 'Chaguaramas', '130', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('422', 'El Socorro', '131', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('423', 'Tucupido', '132', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('424', 'San Rafael de Laya', '132', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('425', 'Altagracia de Orituco', '133', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('426', 'San Rafael de Orituco', '133', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('427', 'San Francisco Javier de Lezama', '133', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('428', 'Paso Real de Macaira', '133', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('429', 'Carlos Soublette', '133', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('430', 'San Francisco de Macaira', '133', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('431', 'Libertad de Orituco', '133', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('432', 'Cantaclaro', '134', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('433', 'San Juan de los Morros', '134', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('434', 'Parapara', '134', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('435', 'El Sombrero', '135', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('436', 'Sosa', '135', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('437', 'Las Mercedes', '136', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('438', 'Cabruta', '136', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('439', 'Santa Rita de Manapire', '136', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('440', 'Valle de la Pascua', '137', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('441', 'Espino', '137', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('442', 'San José de Unare', '138', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('443', 'Zaraza', '138', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('444', 'San José de Tiznados', '139', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('445', 'San Francisco de Tiznados', '139', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('446', 'San Lorenzo de Tiznados', '139', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('447', 'Ortiz', '139', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('448', 'Guayabal', '140', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('449', 'Cazorla', '140', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('450', 'San José de Guaribe', '141', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('451', 'Uveral', '141', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('452', 'Santa María de Ipire', '142', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('453', 'Altamira', '142', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('454', 'El Calvario', '143', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('455', 'El Rastro', '143', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('456', 'Guardatinajas', '143', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('457', 'Capital Urbana Calabozo', '143', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('458', 'Quebrada Honda de Guache', '144', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('459', 'Pío Tamayo', '144', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('460', 'Yacambú', '144', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('461', 'Fréitez', '145', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('462', 'José María Blanco', '145', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('463', 'Catedral', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('464', 'Concepción', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('465', 'El Cují', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('466', 'Juan de Villegas', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('467', 'Santa Rosa', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('468', 'Tamaca', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('469', 'Unión', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('470', 'Aguedo Felipe Alvarado', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('471', 'Buena Vista', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('472', 'Juárez', '146', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('473', 'Juan Bautista Rodríguez', '147', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('474', 'Cuara', '147', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('475', 'Diego de Lozada', '147', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('476', 'Paraíso de San José', '147', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('477', 'San Miguel', '147', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('478', 'Tintorero', '147', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('479', 'José Bernardo Dorante', '147', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('480', 'Coronel Mariano Peraza ', '147', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('481', 'Bolívar', '148', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('482', 'Anzoátegui', '148', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('483', 'Guarico', '148', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('484', 'Hilario Luna y Luna', '148', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('485', 'Humocaro Alto', '148', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('486', 'Humocaro Bajo', '148', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('487', 'La Candelaria', '148', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('488', 'Morán', '148', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('489', 'Cabudare', '149', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('490', 'José Gregorio Bastidas', '149', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('491', 'Agua Viva', '149', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('492', 'Sarare', '150', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('493', 'Buría', '150', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('494', 'Gustavo Vegas León', '150', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('495', 'Trinidad Samuel', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('496', 'Antonio Díaz', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('497', 'Camacaro', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('498', 'Castañeda', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('499', 'Cecilio Zubillaga', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('500', 'Chiquinquirá', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('501', 'El Blanco', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('502', 'Espinoza de los Monteros', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('503', 'Lara', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('504', 'Las Mercedes', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('505', 'Manuel Morillo', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('506', 'Montaña Verde', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('507', 'Montes de Oca', '151', '2017-11-09 22:26:37', '2017-11-09 22:26:37', null);
INSERT INTO "public"."parroquias" VALUES ('508', 'Torres', '151', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('509', 'Heriberto Arroyo', '151', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('510', 'Reyes Vargas', '151', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('511', 'Altagracia', '151', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('512', 'Siquisique', '152', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('513', 'Moroturo', '152', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('514', 'San Miguel', '152', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('515', 'Xaguas', '152', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('516', 'Presidente Betancourt', '153', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('517', 'Presidente Páez', '153', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('518', 'Presidente Rómulo Gallegos', '153', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('519', 'Gabriel Picón González', '153', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('520', 'Héctor Amable Mora', '153', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('521', 'José Nucete Sardi', '153', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('522', 'Pulido Méndez', '153', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('523', 'La Azulita', '154', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('524', 'Santa Cruz de Mora', '155', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('525', 'Mesa Bolívar', '155', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('526', 'Mesa de Las Palmas', '155', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('527', 'Aricagua', '156', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('528', 'San Antonio', '156', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('529', 'Canagua', '157', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('530', 'Capurí', '157', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('531', 'Chacantá', '157', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('532', 'El Molino', '157', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('533', 'Guaimaral', '157', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('534', 'Mucutuy', '157', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('535', 'Mucuchachí', '157', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('536', 'Fernández Peña', '158', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('537', 'Matriz', '158', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('538', 'Montalbán', '158', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('539', 'Acequias', '158', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('540', 'Jají', '158', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('541', 'La Mesa', '158', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('542', 'San José del Sur', '158', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('543', 'Tucaní', '159', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('544', 'Florencio Ramírez', '159', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('545', 'Santo Domingo', '160', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('546', 'Las Piedras', '160', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('547', 'Guaraque', '161', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('548', 'Mesa de Quintero', '161', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('549', 'Río Negro', '161', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('550', 'Arapuey', '162', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('551', 'Palmira', '162', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('552', 'San Cristóbal de Torondoy', '163', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('553', 'Torondoy', '163', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('554', 'Antonio Spinetti Dini', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('555', 'Arias', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('556', 'Caracciolo Parra Pérez', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('557', 'Domingo Peña', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('558', 'El Llano', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('559', 'Gonzalo Picón Febres', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('560', 'Jacinto Plaza', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('561', 'Juan Rodríguez Suárez', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('562', 'Lasso de la Vega', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('563', 'Mariano Picón Salas', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('564', 'Milla', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('565', 'Osuna Rodríguez', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('566', 'Sagrario', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('567', 'El Morro', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('568', 'Los Nevados', '164', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('569', 'Andrés Eloy Blanco', '165', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('570', 'La Venta', '165', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('571', 'Piñango', '165', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('572', 'Timotes', '165', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('573', 'Eloy Paredes', '166', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('574', 'San Rafael de Alcázar', '166', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('575', 'Santa Elena de Arenales', '166', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('576', 'Santa María de Caparo', '167', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('577', 'Pueblo Llano', '168', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('578', 'Cacute', '169', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('579', 'La Toma', '169', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('580', 'Mucuchíes', '169', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('581', 'Mucurubá', '169', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('582', 'San Rafael', '169', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('583', 'Gerónimo Maldonado', '170', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('584', 'Bailadores', '170', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('585', 'Tabay', '171', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('586', 'Chiguará', '172', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('587', 'Estánquez', '172', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('588', 'Lagunillas', '172', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('589', 'La Trampa', '172', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('590', 'Pueblo Nuevo del Sur', '172', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('591', 'San Juan', '172', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('592', 'El Amparo', '173', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('593', 'El Llano', '173', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('594', 'San Francisco', '173', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('595', 'Tovar', '173', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('596', 'Independencia', '174', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('597', 'María de la Concepción Palacios Blanco', '174', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('598', 'Nueva Bolivia', '174', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('599', 'Santa Apolonia', '174', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('600', 'Caño El Tigre', '175', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('601', 'Zea', '175', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('602', 'Aragüita', '176', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('603', 'Arévalo González', '176', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('604', 'Capaya', '176', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('605', 'Caucagua', '176', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('606', 'Panaquire', '176', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('607', 'Ribas', '176', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('608', 'El Café', '176', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('609', 'Marizapa', '176', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('610', 'Cumbo', '177', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('611', 'San José de Barlovento', '177', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('612', 'El Cafetal', '178', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('613', 'Las Minas', '178', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('614', 'Nuestra Señora del Rosario', '178', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('615', 'Higuerote', '179', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('616', 'Curiepe', '179', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('617', 'Tacarigua de Brión', '179', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('618', 'Mamporal', '180', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('619', 'Carrizal', '181', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('620', 'Chacao', '182', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('621', 'Charallave', '183', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('622', 'Las Brisas', '183', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('623', 'El Hatillo', '184', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('624', 'Altagracia de la Montaña', '185', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('625', 'Cecilio Acosta', '185', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('626', 'Los Teques', '185', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('627', 'El Jarillo', '185', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('628', 'San Pedro', '185', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('629', 'Tácata', '185', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('630', 'Paracotos', '185', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('631', 'Cartanal', '186', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('632', 'Santa Teresa del Tuy', '186', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('633', 'La Democracia', '187', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('634', 'Ocumare del Tuy', '187', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('635', 'Santa Bárbara', '187', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('636', 'San Antonio de los Altos', '188', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('637', 'Río Chico', '189', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('638', 'El Guapo', '189', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('639', 'Tacarigua de la Laguna', '189', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('640', 'Paparo', '189', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('641', 'San Fernando del Guapo', '189', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('642', 'Santa Lucía del Tuy', '190', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('643', 'Cúpira', '191', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('644', 'Machurucuto', '191', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('645', 'Guarenas', '192', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('646', 'San Antonio de Yare', '193', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('647', 'San Francisco de Yare', '193', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('648', 'Leoncio Martínez', '194', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('649', 'Petare', '194', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('650', 'Caucagüita', '194', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('651', 'Filas de Mariche', '194', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('652', 'La Dolorita', '194', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('653', 'Cúa', '195', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('654', 'Nueva Cúa', '195', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('655', 'Guatire', '196', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('656', 'Bolívar', '196', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('657', 'San Antonio de Maturín', '197', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('658', 'San Francisco de Maturín', '197', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('659', 'Aguasay', '198', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('660', 'Caripito', '199', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('661', 'El Guácharo', '200', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('662', 'La Guanota', '200', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('663', 'Sabana de Piedra', '200', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('664', 'San Agustín', '200', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('665', 'Teresen', '200', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('666', 'Caripe', '200', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('667', 'Areo', '201', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('668', 'Capital Cedeño', '201', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('669', 'San Félix de Cantalicio', '201', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('670', 'Viento Fresco', '201', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('671', 'El Tejero', '202', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('672', 'Punta de Mata', '202', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('673', 'Chaguaramas', '203', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('674', 'Las Alhuacas', '203', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('675', 'Tabasca', '203', '2017-11-09 22:26:38', '2017-11-09 22:26:38', null);
INSERT INTO "public"."parroquias" VALUES ('676', 'Temblador', '203', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('677', 'Alto de los Godos', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('678', 'Boquerón', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('679', 'Las Cocuizas', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('680', 'La Cruz', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('681', 'San Simón', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('682', 'El Corozo', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('683', 'El Furrial', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('684', 'Jusepín', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('685', 'La Pica', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('686', 'San Vicente', '204', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('687', 'Aparicio', '205', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('688', 'Aragua de Maturín', '205', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('689', 'Chaguamal', '205', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('690', 'El Pinto', '205', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('691', 'Guanaguana', '205', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('692', 'La Toscana', '205', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('693', 'Taguaya', '205', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('694', 'Cachipo', '206', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('695', 'Quiriquire', '206', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('696', 'Santa Bárbara', '207', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('697', 'Barrancas', '208', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('698', 'Los Barrancos de Fajardo', '208', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('699', 'Uracoa', '209', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('700', 'Antolín del Campo', '210', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('701', 'Arismendi', '211', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('702', 'García', '212', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('703', 'Francisco Fajardo', '212', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('704', 'Bolívar', '213', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('705', 'Guevara', '213', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('706', 'Matasiete', '213', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('707', 'Santa Ana', '213', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('708', 'Sucre', '213', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('709', 'Aguirre', '214', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('710', 'Maneiro', '214', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('711', 'Adrián', '215', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('712', 'Juan Griego', '215', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('713', 'Yaguaraparo', '215', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('714', 'Porlamar', '216', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('715', 'San Francisco de Macanao', '217', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('716', 'Boca de Río', '217', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('717', 'Tubores', '218', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('718', 'Los Baleales', '218', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('719', 'Vicente Fuentes', '219', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('720', 'Villalba', '219', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('721', 'San Juan Bautista', '220', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('722', 'Zabala', '220', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('723', 'Capital Araure', '222', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('724', 'Río Acarigua', '222', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('725', 'Capital Esteller', '223', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('726', 'Uveral', '223', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('727', 'Guanare', '224', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('728', 'Córdoba', '224', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('729', 'San José de la Montaña', '224', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('730', 'San Juan de Guanaguanare', '224', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('731', 'Virgen de la Coromoto', '224', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('732', 'Guanarito', '225', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('733', 'Trinidad de la Capilla', '225', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('734', 'Divina Pastora', '225', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('735', 'Monseñor José Vicente de Unda', '226', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('736', 'Peña Blanca', '226', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('737', 'Capital Ospino', '227', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('738', 'Aparición', '227', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('739', 'La Estación', '227', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('740', 'Páez', '228', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('741', 'Payara', '228', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('742', 'Pimpinela', '228', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('743', 'Ramón Peraza', '228', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('744', 'Papelón', '229', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('745', 'Caño Delgadito', '229', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('746', 'San Genaro de Boconoito', '230', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('747', 'Antolín Tovar', '230', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('748', 'San Rafael de Onoto', '231', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('749', 'Santa Fe', '231', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('750', 'Thermo Morles', '231', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('751', 'Santa Rosalía', '232', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('752', 'Florida', '232', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('753', 'Sucre', '233', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('754', 'Concepción', '233', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('755', 'San Rafael de Palo Alzado', '233', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('756', 'Uvencio Antonio Velásquez', '233', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('757', 'San José de Saguaz', '233', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('758', 'Villa Rosa', '233', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('759', 'Turén', '234', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('760', 'Canelones', '234', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('761', 'Santa Cruz', '234', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('762', 'San Isidro Labrador', '234', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('763', 'Mariño', '235', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('764', 'Rómulo Gallegos', '235', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('765', 'San José de Aerocuar', '236', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('766', 'Tavera Acosta', '236', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('767', 'Río Caribe', '237', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('768', 'Antonio José de Sucre', '237', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('769', 'El Morro de Puerto Santo', '237', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('770', 'Puerto Santo', '237', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('771', 'San Juan de las Galdonas', '237', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('772', 'El Pilar', '238', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('773', 'El Rincón', '238', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('774', 'General Francisco Antonio Váquez', '238', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('775', 'Guaraúnos', '238', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('776', 'Tunapuicito', '238', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('777', 'Unión', '238', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('778', 'Santa Catalina', '239', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('779', 'Santa Rosa', '239', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('780', 'Santa Teresa', '239', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('781', 'Bolívar', '239', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('782', 'Maracapana', '239', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('783', 'Libertad', '241', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('784', 'El Paujil', '241', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('785', 'Yaguaraparo', '241', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('786', 'Cruz Salmerón Acosta', '242', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('787', 'Chacopata', '242', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('788', 'Manicuare', '242', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('789', 'Tunapuy', '243', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('790', 'Campo Elías', '243', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('791', 'Irapa', '244', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('792', 'Campo Claro', '244', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('793', 'Maraval', '244', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('794', 'San Antonio de Irapa', '244', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('795', 'Soro', '244', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('796', 'Mejía', '245', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('797', 'Cumanacoa', '246', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('798', 'Arenas', '246', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('799', 'Aricagua', '246', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('800', 'Cogollar', '246', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('801', 'San Fernando', '246', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('802', 'San Lorenzo', '246', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('803', 'Villa Frontado (Muelle de Cariaco)', '247', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('804', 'Catuaro', '247', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('805', 'Rendón', '247', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('806', 'San Cruz', '247', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('807', 'Santa María', '247', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('808', 'Altagracia', '248', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('809', 'Santa Inés', '248', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('810', 'Valentín Valiente', '248', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('811', 'Ayacucho', '248', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('812', 'San Juan', '248', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('813', 'Raúl Leoni', '248', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('814', 'Gran Mariscal', '248', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('815', 'Cristóbal Colón', '249', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('816', 'Bideau', '249', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('817', 'Punta de Piedras', '249', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('818', 'Güiria', '249', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('819', 'Andrés Bello', '250', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('820', 'Antonio Rómulo Costa', '251', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('821', 'Ayacucho', '252', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('822', 'Rivas Berti', '252', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('823', 'San Pedro del Río', '252', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('824', 'Bolívar', '253', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('825', 'Palotal', '253', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('826', 'General Juan Vicente Gómez', '253', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('827', 'Isaías Medina Angarita', '253', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('828', 'Cárdenas', '254', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('829', 'Amenodoro Ángel Lamus', '254', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('830', 'La Florida', '254', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('831', 'Córdoba', '255', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('832', 'Fernández Feo', '256', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('833', 'Alberto Adriani', '256', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('834', 'Santo Domingo', '256', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('835', 'Francisco de Miranda', '257', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('836', 'García de Hevia', '258', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('837', 'Boca de Grita', '258', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('838', 'José Antonio Páez', '258', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('839', 'Guásimos', '259', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('840', 'Independencia', '260', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('841', 'Juan Germán Roscio', '260', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('842', 'Román Cárdenas', '260', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('843', 'Jáuregui', '261', '2017-11-09 22:26:39', '2017-11-09 22:26:39', null);
INSERT INTO "public"."parroquias" VALUES ('844', 'Emilio Constantino Guerrero', '261', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('845', 'Monseñor Miguel Antonio Salas', '261', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('846', 'José María Vargas', '262', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('847', 'Junín', '263', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('848', 'La Petrólea', '263', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('849', 'Quinimarí', '263', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('850', 'Bramón', '263', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('851', 'Libertad', '264', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('852', 'Cipriano Castro', '264', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('853', 'Manuel Felipe Rugeles', '264', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('854', 'Libertador', '265', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('855', 'Doradas', '265', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('856', 'Emeterio Ochoa', '265', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('857', 'San Joaquín de Navay', '265', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('858', 'Lobatera', '266', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('859', 'Constitución', '266', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('860', 'Michelena', '267', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('861', 'Panamericano', '268', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('862', 'La Palmita', '268', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('863', 'Pedro María Ureña', '269', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('864', 'Nueva Arcadia', '269', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('865', 'Delicias', '270', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('866', 'Pecaya', '270', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('867', 'Samuel Darío Maldonado', '271', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('868', 'Boconó', '271', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('869', 'Hernández', '271', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('870', 'La Concordia', '272', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('871', 'San Juan Bautista', '272', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('872', 'Pedro María Morantes', '272', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('873', 'San Sebastián', '272', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('874', 'Dr. Francisco Romero Lobo', '272', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('875', 'Seboruco', '273', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('876', 'Simón Rodríguez', '274', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('877', 'Sucre', '275', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('878', 'Eleazar López Contreras', '275', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('879', 'San Pablo', '275', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('880', 'Torbes', '276', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('881', 'Uribante', '277', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('882', 'Cárdenas', '277', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('883', 'Juan Pablo Peñalosa', '277', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('884', 'Potosí', '277', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('885', 'San Judas Tadeo', '278', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('886', 'Araguaney', '279', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('887', 'El Jaguito', '279', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('888', 'La Esperanza', '279', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('889', 'Santa Isabel', '279', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('890', 'Boconó', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('891', 'El Carmen', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('892', 'Mosquey', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('893', 'Ayacucho', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('894', 'Burbusay', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('895', 'General Ribas', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('896', 'Guaramacal', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('897', 'Vega de Guaramacal', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('898', 'Monseñor Jáuregui', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('899', 'Rafael Rangel', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('900', 'San Miguel', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('901', 'San José', '280', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('902', 'Sabana Grande', '281', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('903', 'Cheregüé', '281', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('904', 'Granados', '281', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('905', 'Arnoldo Gabaldón', '282', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('906', 'Bolivia', '282', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('907', 'Carrillo', '282', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('908', 'Cegarra', '282', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('909', 'Chejendé', '282', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('910', 'Manuel Salvador Ulloa', '282', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('911', 'San José', '282', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('912', 'Carache', '283', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('913', 'La Concepción', '283', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('914', 'Cuicas', '283', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('915', 'Panamericana', '283', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('916', 'Santa Cruz', '283', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('917', 'Escuque', '284', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('918', 'La Unión', '284', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('919', 'Santa Rita', '284', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('920', 'Sabana Libre', '284', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('921', 'El Socorro', '285', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('922', 'Los Caprichos', '285', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('923', 'Antonio José de Sucre', '285', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('924', 'Campo Elías', '286', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('925', 'Arnoldo Gabaldón', '286', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('926', 'Santa Apolonia', '287', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('927', 'El Progreso', '287', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('928', 'La Ceiba', '287', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('929', 'Tres de Febrero', '287', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('930', 'El Dividive', '288', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('931', 'Agua Santa', '288', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('932', 'Agua Caliente', '288', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('933', 'El Cenizo', '288', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('934', 'Valerita', '288', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('935', 'Monte Carmelo', '289', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('936', 'Buena Vista', '289', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('937', 'Santa María del Horcón', '289', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('938', 'Motatán', '290', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('939', 'El Baño', '290', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('940', 'Jalisco', '290', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('941', 'Pampán', '291', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('942', 'Flor de Patria', '291', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('943', 'La Paz', '291', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('944', 'Santa Ana', '291', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('945', 'Pampanito', '292', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('946', 'La Concepción', '292', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('947', 'Pampanito II', '292', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('948', 'Betijoque', '293', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('949', 'José Gregorio Hernández', '293', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('950', 'La Pueblita', '293', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('951', 'Los Cedros', '293', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('952', 'Carvajal', '294', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('953', 'Campo Alegre', '294', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('954', 'Antonio Nicolás Briceño', '294', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('955', 'José Leonardo Suárez', '294', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('956', 'Sabana de Mendoza', '295', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('957', 'Junín', '295', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('958', 'Valmore Rodríguez', '295', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('959', 'El Paraíso', '295', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('960', 'Andrés Linares', '296', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('961', 'Chiquinquirá', '296', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('962', 'Cristóbal Mendoza', '296', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('963', 'Cruz Carrillo', '296', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('964', 'Matriz', '296', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('965', 'Monseñor Carrillo', '296', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('966', 'Tres Esquinas', '296', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('967', 'Cabimbú', '297', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('968', 'Jajó', '297', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('969', 'La Mesa de Esnujaque', '297', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('970', 'Santiago', '297', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('971', 'Tuñame', '297', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('972', 'La Quebrada', '297', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('973', 'Juan Ignacio Montilla', '298', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('974', 'La Beatriz', '298', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('975', 'La Puerta', '298', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('976', 'Mendoza del Valle de Momboy', '298', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('977', 'Mercedes Díaz', '298', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('978', 'San Luis', '298', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('979', 'Caraballeda', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('980', 'Carayaca', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('981', 'Carlos Soublette', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('982', 'Caruao Chuspa', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('983', 'Catia La Mar', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('984', 'El Junko', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('985', 'La Guaira', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('986', 'Macuto', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('987', 'Maiquetía', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('988', 'Naiguatá', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('989', 'Urimare', '299', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('990', 'Arístides Bastidas', '300', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('991', 'Bolívar', '301', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('992', 'Chivacoa', '302', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('993', 'Campo Elías', '302', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('994', 'Cocorote', '303', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('995', 'Independencia', '304', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('996', 'José Antonio Páez', '305', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('997', 'La Trinidad', '306', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('998', 'Manuel Monge', '307', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('999', 'Salóm', '308', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1000', 'Temerla', '308', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1001', 'Nirgua', '308', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1002', 'San Andrés', '309', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1003', 'Yaritagua', '309', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1004', 'San Javier', '310', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1005', 'Albarico', '310', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1006', 'San Felipe', '310', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1007', 'Sucre', '311', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1008', 'Urachiche', '312', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1009', 'El Guayabo', '313', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1010', 'Farriar', '313', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1011', 'Isla de Toas', '314', '2017-11-09 22:26:40', '2017-11-09 22:26:40', null);
INSERT INTO "public"."parroquias" VALUES ('1012', 'Monagas', '314', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1013', 'San Timoteo', '315', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1014', 'General Urdaneta', '315', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1015', 'Libertador', '315', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1016', 'Marcelino Briceño', '315', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1017', 'Pueblo Nuevo', '315', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1018', 'Manuel Guanipa Matos', '315', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1019', 'Ambrosio', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1020', 'Carmen Herrera', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1021', 'La Rosa', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1022', 'Germán Ríos Linares', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1023', 'San Benito', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1024', 'Rómulo Betancourt', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1025', 'Jorge Hernández', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1026', 'Punta Gorda', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1027', 'Arístides Calvani', '316', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1028', 'Encontrados', '317', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1029', 'Udón Pérez', '317', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1030', 'Moralito', '318', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1031', 'San Carlos del Zulia', '318', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1032', 'Santa Cruz del Zulia', '318', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1033', 'Santa Bárbara', '318', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1034', 'Urribarrí', '318', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1035', 'Carlos Quevedo', '319', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1036', 'Francisco Javier Pulgar', '319', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1037', 'Simón Rodríguez', '319', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1038', 'Guamo-Gavilanes', '319', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1039', 'La Concepción', '321', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1040', 'San José', '321', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1041', 'Mariano Parra León', '321', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1042', 'José Ramón Yépez', '321', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1043', 'Jesús María Semprún', '322', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1044', 'Barí', '322', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1045', 'Concepción', '323', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1046', 'Andrés Bello', '323', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1047', 'Chiquinquirá', '323', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1048', 'El Carmelo', '323', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1049', 'Potreritos', '323', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1050', 'Libertad', '324', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1051', 'Alonso de Ojeda', '324', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1052', 'Venezuela', '324', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1053', 'Eleazar López Contreras', '324', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1054', 'Campo Lara', '324', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1055', 'Bartolomé de las Casas', '325', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1056', 'Libertad', '325', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1057', 'Río Negro', '325', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1058', 'San José de Perijá', '325', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1059', 'San Rafael', '326', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1060', 'La Sierrita', '326', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1061', 'Las Parcelas', '326', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1062', 'Luis de Vicente', '326', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1063', 'Monseñor Marcos Sergio Godoy', '326', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1064', 'Ricaurte', '326', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1065', 'Tamare', '326', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1066', 'Antonio Borjas Romero', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1067', 'Bolívar', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1068', 'Cacique Mara', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1069', 'Carracciolo Parra Pérez', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1070', 'Cecilio Acosta', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1071', 'Cristo de Aranza', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1072', 'Coquivacoa', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1073', 'Chiquinquirá', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1074', 'Francisco Eugenio Bustamante', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1075', 'Idelfonzo Vásquez', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1076', 'Juana de Ávila', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1077', 'Luis Hurtado Higuera', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1078', 'Manuel Dagnino', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1079', 'Olegario Villalobos', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1080', 'Raúl Leoni', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1081', 'Santa Lucía', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1082', 'Venancio Pulgar', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1083', 'San Isidro', '327', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1084', 'Altagracia', '328', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1085', 'Faría', '328', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1086', 'Ana María Campos', '328', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1087', 'San Antonio', '328', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1088', 'San José', '328', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1089', 'Donaldo García', '329', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1090', 'El Rosario', '329', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1091', 'Sixto Zambrano', '329', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1092', 'San Francisco', '330', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1093', 'El Bajo', '330', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1094', 'Domitila Flores', '330', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1095', 'Francisco Ochoa', '330', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1096', 'Los Cortijos', '330', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1097', 'Marcial Hernández', '330', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1098', 'Santa Rita', '331', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1099', 'El Mene', '331', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1100', 'Pedro Lucas Urribarrí', '331', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1101', 'José Cenobio Urribarrí', '331', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1102', 'Rafael Maria Baralt', '332', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1103', 'Manuel Manrique', '332', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1104', 'Rafael Urdaneta', '332', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1105', 'Bobures', '333', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1106', 'Gibraltar', '333', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1107', 'Heras', '333', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1108', 'Monseñor Arturo Álvarez', '333', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1109', 'Rómulo Gallegos', '333', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1110', 'El Batey', '333', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1111', 'Rafael Urdaneta', '334', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1112', 'La Victoria', '334', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1113', 'Raúl Cuenca', '334', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1114', 'Sinamaica', '320', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1115', 'Alta Guajira', '320', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1116', 'Elías Sánchez Rubio', '320', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1117', 'Guajira', '320', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1118', 'Altagracia', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1119', 'Antímano', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1120', 'Caricuao', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1121', 'Catedral', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1122', 'Coche', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1123', 'El Junquito', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1124', 'El Paraíso', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1125', 'El Recreo', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1126', 'El Valle', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1127', 'La Candelaria', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1128', 'La Pastora', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1129', 'La Vega', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1130', 'Macarao', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1131', 'San Agustín', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1132', 'San Bernardino', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1133', 'San José', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1134', 'San Juan', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1135', 'San Pedro', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1136', 'Santa Rosalía', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1137', 'Santa Teresa', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1138', 'Sucre (Catia)', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."parroquias" VALUES ('1139', '23 de enero', '335', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS "public"."password_resets";
CREATE TABLE "public"."password_resets" (
"email" varchar(191) COLLATE "default" NOT NULL,
"token" varchar(191) COLLATE "default" NOT NULL,
"created_at" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personas
-- ----------------------------
DROP TABLE IF EXISTS "public"."personas";
CREATE TABLE "public"."personas" (
"id" int4 DEFAULT nextval('personas_id_seq'::regclass) NOT NULL,
"tipo_persona_id" int4 NOT NULL,
"dni" varchar(20) COLLATE "default" NOT NULL,
"nombres" varchar(200) COLLATE "default" NOT NULL,
"foto" varchar(191) COLLATE "default" DEFAULT 'user.png'::character varying NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of personas
-- ----------------------------
INSERT INTO "public"."personas" VALUES ('1', '1', '1', 'Administrador', 'user.png', '2017-11-09 22:26:41', '2017-11-10 14:53:00', null);
INSERT INTO "public"."personas" VALUES ('2', '1', '3', 'INACTIVO', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('3', '1', '4', 'CONVENIO DE INTERCAMBIO', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('4', '1', '8867067', 'FRANCIA CARVAJAL', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('5', '1', '18012306', 'VINICIO GRILLET', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('6', '1', '12194357', 'ZULEIMA NUÑEZ', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('7', '1', '16500783', 'MIURYCA RENDON', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('8', '1', '5706650', 'AGUILERA RAMOS ROSA OFELIA', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('9', '1', '11228418', 'MARIA FRANCIA ORTIZ NIETO', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('10', '1', '15617290', 'EUKARYS CAROLINA VECCHIONACCE CEDEÑO', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('11', '1', '18947721', 'YOLIMAR DESIREE HERRERA RUIZ', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('12', '1', '19369434', 'CINDY LA BELLE MONTERO RIVAS', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('13', '1', '20774473', 'NAZARETH MEJIAS', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('14', '1', '18828478', 'IRIANA CEDEÑO', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('15', '1', '10810986', 'CONNIE SOTO LANZA ', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('16', '1', '18947098', 'ARTURO MEDINA', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('17', '1', '19730697', 'CARMERY SOTO', 'user.png', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas" VALUES ('18', '1', '15252476', 'Iliana Cedeño', 'user.png', '2017-11-10 15:15:14', '2017-11-10 15:15:14', null);

-- ----------------------------
-- Table structure for personas_bancos
-- ----------------------------
DROP TABLE IF EXISTS "public"."personas_bancos";
CREATE TABLE "public"."personas_bancos" (
"id" int4 DEFAULT nextval('personas_bancos_id_seq'::regclass) NOT NULL,
"personas_id" int4 NOT NULL,
"bancos_id" int4 NOT NULL,
"tipo_cuenta_id" int4 NOT NULL,
"digitos" char(4) COLLATE "default",
"cuenta" char(20) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of personas_bancos
-- ----------------------------
INSERT INTO "public"."personas_bancos" VALUES ('1', '18', '3', '2', null, '01050134261134057571', '2017-11-10 15:15:14', '2017-11-10 15:15:14', null);

-- ----------------------------
-- Table structure for personas_correo
-- ----------------------------
DROP TABLE IF EXISTS "public"."personas_correo";
CREATE TABLE "public"."personas_correo" (
"id" int4 DEFAULT nextval('personas_correo_id_seq'::regclass) NOT NULL,
"personas_id" int4 NOT NULL,
"principal" bool NOT NULL,
"correo" varchar(200) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of personas_correo
-- ----------------------------
INSERT INTO "public"."personas_correo" VALUES ('1', '1', 't', 'admin@dominio.com', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."personas_correo" VALUES ('2', '2', 't', 'correo1@corroeo.com', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('3', '3', 't', 'correo@corroeo.com', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('4', '4', 't', 'arturorafa2el.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('5', '5', 't', 'arturorafa4el.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('6', '6', 't', 'zuleima18034543@gmail.com', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('7', '7', 't', 'arturorafa5el.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('8', '8', 't', 'arturorafeael.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('9', '9', 't', 'arturorafaefel.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('10', '10', 't', 'arturorafavvvel.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('11', '11', 't', 'lachicayolimar@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('12', '12', 't', 'arturorafaasdadel.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('13', '13', 't', 'arturorafaasdasdel.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('14', '14', 't', 'arturorassssfael.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('15', '15', 't', 'isajossoto@gmail.com', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('16', '16', 't', 'arturorawwwfael.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_correo" VALUES ('17', '17', 't', 'arturorafaaaael.medina@gmail.com ', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);

-- ----------------------------
-- Table structure for personas_detalles
-- ----------------------------
DROP TABLE IF EXISTS "public"."personas_detalles";
CREATE TABLE "public"."personas_detalles" (
"id" int4 DEFAULT nextval('personas_detalles_id_seq'::regclass) NOT NULL,
"personas_id" int4 NOT NULL,
"profesion_id" int4,
"sexo" varchar(1) COLLATE "default",
"fecha_nacimiento" date,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of personas_detalles
-- ----------------------------
INSERT INTO "public"."personas_detalles" VALUES ('1', '1', '1', 'm', '1987-08-25', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."personas_detalles" VALUES ('2', '18', '1', 'f', '2017-11-10', '2017-11-10 15:15:14', '2017-11-10 15:15:14', null);

-- ----------------------------
-- Table structure for personas_direccion
-- ----------------------------
DROP TABLE IF EXISTS "public"."personas_direccion";
CREATE TABLE "public"."personas_direccion" (
"id" int4 DEFAULT nextval('personas_direccion_id_seq'::regclass) NOT NULL,
"personas_id" int4 NOT NULL,
"estados_id" int4 NOT NULL,
"ciudades_id" int4,
"municipios_id" int4,
"parroquias_id" int4,
"sectores_id" int4,
"direccion" varchar(200) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of personas_direccion
-- ----------------------------
INSERT INTO "public"."personas_direccion" VALUES ('1', '1', '6', '77', '70', '235', null, '', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."personas_direccion" VALUES ('2', '18', '12', '200', '145', '462', '1', 'asdqweasd', '2017-11-10 15:15:14', '2017-11-10 15:15:14', null);

-- ----------------------------
-- Table structure for personas_telefono
-- ----------------------------
DROP TABLE IF EXISTS "public"."personas_telefono";
CREATE TABLE "public"."personas_telefono" (
"id" int4 DEFAULT nextval('personas_telefono_id_seq'::regclass) NOT NULL,
"personas_id" int4 NOT NULL,
"tipo_telefono_id" int4 NOT NULL,
"principal" bool NOT NULL,
"numero" varchar(20) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of personas_telefono
-- ----------------------------
INSERT INTO "public"."personas_telefono" VALUES ('1', '1', '1', 't', '0414-850-8123', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."personas_telefono" VALUES ('2', '2', '1', 't', '04167857172', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('3', '3', '1', 't', '04167857173', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('4', '4', '1', 't', '04167857175', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('5', '5', '1', 't', '04167857176', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('6', '6', '1', 't', '04167857177', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('7', '7', '1', 't', '04167857178', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('8', '8', '1', 't', '04167857179', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('9', '9', '1', 't', '04167857180', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('10', '10', '1', 't', '04167857181', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('11', '11', '1', 't', '04167857182', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('12', '12', '1', 't', '04167857183', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('13', '13', '1', 't', '04167857184', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('14', '14', '1', 't', '04167857185', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('15', '15', '1', 't', '04167857186', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('16', '16', '1', 't', '04167857187', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."personas_telefono" VALUES ('17', '17', '1', 't', '04167857188', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);

-- ----------------------------
-- Table structure for plan
-- ----------------------------
DROP TABLE IF EXISTS "public"."plan";
CREATE TABLE "public"."plan" (
"id" int4 DEFAULT nextval('plan_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"desde" date NOT NULL,
"hasta" date NOT NULL,
"meses_pagar" int4 NOT NULL,
"empresa_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of plan
-- ----------------------------
INSERT INTO "public"."plan" VALUES ('1', 'plan prueba', '2017-11-01', '2017-11-30', '3', '1', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);

-- ----------------------------
-- Table structure for plan_detalles
-- ----------------------------
DROP TABLE IF EXISTS "public"."plan_detalles";
CREATE TABLE "public"."plan_detalles" (
"id" int4 DEFAULT nextval('plan_detalles_id_seq'::regclass) NOT NULL,
"plan_id" int4 NOT NULL,
"beneficiarios" int4 NOT NULL,
"porsertaje_descuento" numeric(15,2) NOT NULL,
"contado" numeric(15,2) NOT NULL,
"inicial" numeric(15,2) NOT NULL,
"total" numeric(15,2) NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of plan_detalles
-- ----------------------------
INSERT INTO "public"."plan_detalles" VALUES ('1', '1', '1', '0.00', '100.00', '10.00', '100.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('2', '1', '2', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('3', '1', '3', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('4', '1', '4', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('5', '1', '5', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('6', '1', '6', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('7', '1', '7', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('8', '1', '8', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('9', '1', '9', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);
INSERT INTO "public"."plan_detalles" VALUES ('10', '1', '10', '0.00', '0.00', '0.00', '0.00', '2017-11-10 14:53:45', '2017-11-10 14:53:45', null);

-- ----------------------------
-- Table structure for profesion
-- ----------------------------
DROP TABLE IF EXISTS "public"."profesion";
CREATE TABLE "public"."profesion" (
"id" int4 DEFAULT nextval('profesion_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"slug" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of profesion
-- ----------------------------
INSERT INTO "public"."profesion" VALUES ('1', 'Ing en Informatica', 'ing-en-informatica', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);

-- ----------------------------
-- Table structure for sectores
-- ----------------------------
DROP TABLE IF EXISTS "public"."sectores";
CREATE TABLE "public"."sectores" (
"id" int4 DEFAULT nextval('sectores_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"slug" varchar(100) COLLATE "default" NOT NULL,
"parroquias_id" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sectores
-- ----------------------------
INSERT INTO "public"."sectores" VALUES ('1', 'asdaeq', 'asdaeq', '462', '2017-11-10 15:15:10', '2017-11-10 15:15:10', null);

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS "public"."sessions";
CREATE TABLE "public"."sessions" (
"id" varchar(191) COLLATE "default" NOT NULL,
"user_id" int4,
"ip_address" varchar(45) COLLATE "default",
"user_agent" text COLLATE "default",
"payload" text COLLATE "default" NOT NULL,
"last_activity" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sessions
-- ----------------------------

-- ----------------------------
-- Table structure for solicitud_beneficiarios_temp
-- ----------------------------
DROP TABLE IF EXISTS "public"."solicitud_beneficiarios_temp";
CREATE TABLE "public"."solicitud_beneficiarios_temp" (
"id" int4 DEFAULT nextval('solicitud_beneficiarios_temp_id_seq'::regclass) NOT NULL,
"planilla" varchar(10) COLLATE "default",
"contratos_id" int4 NOT NULL,
"solicitud_id" int4 NOT NULL,
"planes_id" int4,
"fecha" date NOT NULL,
"operacion" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of solicitud_beneficiarios_temp
-- ----------------------------

-- ----------------------------
-- Table structure for solicitudes
-- ----------------------------
DROP TABLE IF EXISTS "public"."solicitudes";
CREATE TABLE "public"."solicitudes" (
"id" int4 DEFAULT nextval('solicitudes_id_seq'::regclass) NOT NULL,
"tipo_solicitud" int4 NOT NULL,
"solicitante" int4 NOT NULL,
"aquien" int4 NOT NULL,
"indece" int4 NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of solicitudes
-- ----------------------------

-- ----------------------------
-- Table structure for sucursal
-- ----------------------------
DROP TABLE IF EXISTS "public"."sucursal";
CREATE TABLE "public"."sucursal" (
"id" int4 DEFAULT nextval('sucursal_id_seq'::regclass) NOT NULL,
"empresa_id" int4 NOT NULL,
"nombre" varchar(80) COLLATE "default" NOT NULL,
"abreviatura" varchar(80) COLLATE "default" NOT NULL,
"cod_sucursal" int4 NOT NULL,
"correlativo" int4 NOT NULL,
"estados_id" int4 NOT NULL,
"ciudades_id" int4 NOT NULL,
"municipios_id" int4 NOT NULL,
"parroquias_id" int4 NOT NULL,
"sectores_id" int4,
"direccion" varchar(200) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sucursal
-- ----------------------------
INSERT INTO "public"."sucursal" VALUES ('1', '1', 'Ciudad Bolivar', 'CBO', '1', '0', '6', '88', '17', '14', null, 'direccion', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."sucursal" VALUES ('2', '1', 'Puerto Ordaz', 'POZ', '2', '0', '6', '88', '17', '14', null, 'direccion', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."sucursal" VALUES ('3', '1', 'El tigre', 'TGR', '3', '0', '6', '88', '17', '14', null, 'direccion', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);
INSERT INTO "public"."sucursal" VALUES ('4', '1', 'Puerto la Cruz', 'PLC', '4', '0', '6', '88', '17', '14', null, 'direccion', '2017-11-09 22:26:42', '2017-11-09 22:26:42', null);

-- ----------------------------
-- Table structure for tipo_persona
-- ----------------------------
DROP TABLE IF EXISTS "public"."tipo_persona";
CREATE TABLE "public"."tipo_persona" (
"id" int4 DEFAULT nextval('tipo_persona_id_seq'::regclass) NOT NULL,
"nombre" varchar(40) COLLATE "default" NOT NULL,
"descripcion" varchar(40) COLLATE "default",
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tipo_persona
-- ----------------------------
INSERT INTO "public"."tipo_persona" VALUES ('1', 'V', 'Venezolano', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."tipo_persona" VALUES ('2', 'E', 'Extranjero', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."tipo_persona" VALUES ('3', 'G', 'Gubernamental', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."tipo_persona" VALUES ('4', 'J', 'Juridico', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);

-- ----------------------------
-- Table structure for tipo_telefono
-- ----------------------------
DROP TABLE IF EXISTS "public"."tipo_telefono";
CREATE TABLE "public"."tipo_telefono" (
"id" int4 DEFAULT nextval('tipo_telefono_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default" NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tipo_telefono
-- ----------------------------
INSERT INTO "public"."tipo_telefono" VALUES ('1', 'Movil', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);
INSERT INTO "public"."tipo_telefono" VALUES ('2', 'Casa', '2017-11-09 22:26:41', '2017-11-09 22:26:41', null);

-- ----------------------------
-- Table structure for vendedores
-- ----------------------------
DROP TABLE IF EXISTS "public"."vendedores";
CREATE TABLE "public"."vendedores" (
"id" int4 DEFAULT nextval('vendedores_id_seq'::regclass) NOT NULL,
"personas_id" int4 NOT NULL,
"codigo" varchar(100) COLLATE "default" NOT NULL,
"estatus" bool NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of vendedores
-- ----------------------------
INSERT INTO "public"."vendedores" VALUES ('1', '2', '0', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('2', '3', '2', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('3', '4', '80', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('4', '5', '88', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('5', '6', '100', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('6', '7', '120', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('7', '8', '122', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('8', '9', '123', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('9', '10', '124', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('10', '11', '125', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('11', '12', '127', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('12', '13', '128', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('13', '14', '129', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('14', '15', '130', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('15', '16', '131', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);
INSERT INTO "public"."vendedores" VALUES ('16', '17', '132', 't', '2017-11-09 22:32:45', '2017-11-09 22:32:45', null);

-- ----------------------------
-- Table structure for vendedores_sucusal
-- ----------------------------
DROP TABLE IF EXISTS "public"."vendedores_sucusal";
CREATE TABLE "public"."vendedores_sucusal" (
"vendedor_id" int4 NOT NULL,
"sucursal_id" int4 NOT NULL,
"empresa_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of vendedores_sucusal
-- ----------------------------
INSERT INTO "public"."vendedores_sucusal" VALUES ('1', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('1', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('1', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('1', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('2', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('2', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('2', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('2', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('3', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('3', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('3', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('3', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('4', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('4', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('4', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('4', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('5', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('5', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('5', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('5', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('6', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('6', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('6', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('6', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('7', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('7', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('7', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('7', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('8', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('8', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('8', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('8', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('9', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('9', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('9', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('9', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('10', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('10', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('10', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('10', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('11', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('11', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('11', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('11', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('12', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('12', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('12', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('12', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('13', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('13', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('13', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('13', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('14', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('14', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('14', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('14', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('15', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('15', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('15', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('15', '4', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('16', '1', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('16', '2', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('16', '3', '1');
INSERT INTO "public"."vendedores_sucusal" VALUES ('16', '4', '1');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."afacturar_historial_id_seq" OWNED BY "afacturar_historial"."id";
ALTER SEQUENCE "public"."afacturar_id_seq" OWNED BY "afacturar"."id";
ALTER SEQUENCE "public"."app_perfil_id_seq" OWNED BY "app_perfil"."id";
ALTER SEQUENCE "public"."app_usuario_id_seq" OWNED BY "app_usuario"."id";
ALTER SEQUENCE "public"."audits_id_seq" OWNED BY "audits"."id";
ALTER SEQUENCE "public"."banco_tipo_cuenta_id_seq" OWNED BY "banco_tipo_cuenta"."id";
ALTER SEQUENCE "public"."bancos_empresa_id_seq" OWNED BY "bancos_empresa"."id";
ALTER SEQUENCE "public"."bancos_id_seq" OWNED BY "bancos"."id";
ALTER SEQUENCE "public"."beneficiarios_id_seq" OWNED BY "beneficiarios"."id";
ALTER SEQUENCE "public"."beneficiarios_temp_id_seq" OWNED BY "beneficiarios_temp"."id";
ALTER SEQUENCE "public"."ciudades_id_seq" OWNED BY "ciudades"."id";
ALTER SEQUENCE "public"."cobros_id_seq" OWNED BY "cobros"."id";
ALTER SEQUENCE "public"."configuracion_id_seq" OWNED BY "configuracion"."id";
ALTER SEQUENCE "public"."contrato_tipo_id_seq" OWNED BY "contrato_tipo"."id";
ALTER SEQUENCE "public"."contratos_detalles_id_seq" OWNED BY "contratos_detalles"."id";
ALTER SEQUENCE "public"."contratos_facturar_id_seq" OWNED BY "contratos_facturar"."id";
ALTER SEQUENCE "public"."contratos_id_seq" OWNED BY "contratos"."id";
ALTER SEQUENCE "public"."contratos_temp_id_seq" OWNED BY "contratos_temp"."id";
ALTER SEQUENCE "public"."controlfacturacion_id_seq" OWNED BY "controlfacturacion"."id";
ALTER SEQUENCE "public"."empresa_id_seq" OWNED BY "empresa"."id";
ALTER SEQUENCE "public"."estados_id_seq" OWNED BY "estados"."id";
ALTER SEQUENCE "public"."estatus_contrato_id_seq" OWNED BY "estatus_contrato"."id";
ALTER SEQUENCE "public"."frecuencia_pagos_id_seq" OWNED BY "frecuencia_pagos"."id";
ALTER SEQUENCE "public"."libro_historial_id_seq" OWNED BY "libro_historial"."id";
ALTER SEQUENCE "public"."libro_id_seq" OWNED BY "libro"."id";
ALTER SEQUENCE "public"."lotes_id_seq" OWNED BY "lotes"."id";
ALTER SEQUENCE "public"."migrations_id_seq" OWNED BY "migrations"."id";
ALTER SEQUENCE "public"."movimientos_historial_id_seq" OWNED BY "movimientos_historial"."id";
ALTER SEQUENCE "public"."movimientos_id_seq" OWNED BY "movimientos"."id";
ALTER SEQUENCE "public"."municipios_id_seq" OWNED BY "municipios"."id";
ALTER SEQUENCE "public"."parentesco_id_seq" OWNED BY "parentesco"."id";
ALTER SEQUENCE "public"."parroquias_id_seq" OWNED BY "parroquias"."id";
ALTER SEQUENCE "public"."personas_bancos_id_seq" OWNED BY "personas_bancos"."id";
ALTER SEQUENCE "public"."personas_correo_id_seq" OWNED BY "personas_correo"."id";
ALTER SEQUENCE "public"."personas_detalles_id_seq" OWNED BY "personas_detalles"."id";
ALTER SEQUENCE "public"."personas_direccion_id_seq" OWNED BY "personas_direccion"."id";
ALTER SEQUENCE "public"."personas_id_seq" OWNED BY "personas"."id";
ALTER SEQUENCE "public"."personas_telefono_id_seq" OWNED BY "personas_telefono"."id";
ALTER SEQUENCE "public"."plan_detalles_id_seq" OWNED BY "plan_detalles"."id";
ALTER SEQUENCE "public"."plan_id_seq" OWNED BY "plan"."id";
ALTER SEQUENCE "public"."profesion_id_seq" OWNED BY "profesion"."id";
ALTER SEQUENCE "public"."sectores_id_seq" OWNED BY "sectores"."id";
ALTER SEQUENCE "public"."solicitud_beneficiarios_temp_id_seq" OWNED BY "solicitud_beneficiarios_temp"."id";
ALTER SEQUENCE "public"."solicitudes_id_seq" OWNED BY "solicitudes"."id";
ALTER SEQUENCE "public"."sucursal_id_seq" OWNED BY "sucursal"."id";
ALTER SEQUENCE "public"."tipo_persona_id_seq" OWNED BY "tipo_persona"."id";
ALTER SEQUENCE "public"."tipo_telefono_id_seq" OWNED BY "tipo_telefono"."id";
ALTER SEQUENCE "public"."vendedores_id_seq" OWNED BY "vendedores"."id";

-- ----------------------------
-- Primary Key structure for table afacturar
-- ----------------------------
ALTER TABLE "public"."afacturar" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table afacturar_historial
-- ----------------------------
ALTER TABLE "public"."afacturar_historial" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table app_perfil
-- ----------------------------
ALTER TABLE "public"."app_perfil" ADD UNIQUE ("nombre");

-- ----------------------------
-- Primary Key structure for table app_perfil
-- ----------------------------
ALTER TABLE "public"."app_perfil" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table app_usuario
-- ----------------------------
ALTER TABLE "public"."app_usuario" ADD UNIQUE ("personas_id");
ALTER TABLE "public"."app_usuario" ADD UNIQUE ("usuario");

-- ----------------------------
-- Primary Key structure for table app_usuario
-- ----------------------------
ALTER TABLE "public"."app_usuario" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table audits
-- ----------------------------
CREATE INDEX "audits_auditable_id_auditable_type_index" ON "public"."audits" USING btree ("auditable_id", "auditable_type");

-- ----------------------------
-- Primary Key structure for table audits
-- ----------------------------
ALTER TABLE "public"."audits" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table banco_tipo_cuenta
-- ----------------------------
ALTER TABLE "public"."banco_tipo_cuenta" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table bancos
-- ----------------------------
ALTER TABLE "public"."bancos" ADD UNIQUE ("codigo");

-- ----------------------------
-- Primary Key structure for table bancos
-- ----------------------------
ALTER TABLE "public"."bancos" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table bancos_empresa
-- ----------------------------
ALTER TABLE "public"."bancos_empresa" ADD UNIQUE ("cuenta");

-- ----------------------------
-- Primary Key structure for table bancos_empresa
-- ----------------------------
ALTER TABLE "public"."bancos_empresa" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table beneficiarios
-- ----------------------------
ALTER TABLE "public"."beneficiarios" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table beneficiarios_temp
-- ----------------------------
ALTER TABLE "public"."beneficiarios_temp" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ciudades
-- ----------------------------
ALTER TABLE "public"."ciudades" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table cobros
-- ----------------------------
CREATE INDEX "cobros_index_1" ON "public"."cobros" USING btree ("completo");
CREATE INDEX "cobros_index_2" ON "public"."cobros" USING btree ("completo", "giro");
CREATE INDEX "cobros_index_3" ON "public"."cobros" USING btree ("completo", "bancos_id", "giro");

-- ----------------------------
-- Primary Key structure for table cobros
-- ----------------------------
ALTER TABLE "public"."cobros" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table configuracion
-- ----------------------------
ALTER TABLE "public"."configuracion" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table contrato_tipo
-- ----------------------------
ALTER TABLE "public"."contrato_tipo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table contratos
-- ----------------------------
ALTER TABLE "public"."contratos" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table contratos_detalles
-- ----------------------------
ALTER TABLE "public"."contratos_detalles" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table contratos_facturar
-- ----------------------------
ALTER TABLE "public"."contratos_facturar" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table contratos_temp
-- ----------------------------
ALTER TABLE "public"."contratos_temp" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table controlfacturacion
-- ----------------------------
ALTER TABLE "public"."controlfacturacion" ADD UNIQUE ("mes", "ano");

-- ----------------------------
-- Primary Key structure for table controlfacturacion
-- ----------------------------
ALTER TABLE "public"."controlfacturacion" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table empresa
-- ----------------------------
ALTER TABLE "public"."empresa" ADD UNIQUE ("rif");
ALTER TABLE "public"."empresa" ADD UNIQUE ("nombre");
ALTER TABLE "public"."empresa" ADD UNIQUE ("abreviatura");

-- ----------------------------
-- Primary Key structure for table empresa
-- ----------------------------
ALTER TABLE "public"."empresa" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table estados
-- ----------------------------
ALTER TABLE "public"."estados" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table estatus_contrato
-- ----------------------------
ALTER TABLE "public"."estatus_contrato" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table frecuencia_pagos
-- ----------------------------
ALTER TABLE "public"."frecuencia_pagos" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table libro
-- ----------------------------
ALTER TABLE "public"."libro" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table libro_historial
-- ----------------------------
ALTER TABLE "public"."libro_historial" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table lotes
-- ----------------------------
ALTER TABLE "public"."lotes" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "public"."migrations" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table movimientos
-- ----------------------------
ALTER TABLE "public"."movimientos" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table movimientos_historial
-- ----------------------------
ALTER TABLE "public"."movimientos_historial" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table municipios
-- ----------------------------
ALTER TABLE "public"."municipios" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table parentesco
-- ----------------------------
ALTER TABLE "public"."parentesco" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table parroquias
-- ----------------------------
ALTER TABLE "public"."parroquias" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table password_resets
-- ----------------------------
CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree ("email");
CREATE INDEX "password_resets_token_index" ON "public"."password_resets" USING btree ("token");

-- ----------------------------
-- Uniques structure for table personas
-- ----------------------------
ALTER TABLE "public"."personas" ADD UNIQUE ("dni");

-- ----------------------------
-- Primary Key structure for table personas
-- ----------------------------
ALTER TABLE "public"."personas" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table personas_bancos
-- ----------------------------
ALTER TABLE "public"."personas_bancos" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table personas_correo
-- ----------------------------
ALTER TABLE "public"."personas_correo" ADD UNIQUE ("correo");

-- ----------------------------
-- Primary Key structure for table personas_correo
-- ----------------------------
ALTER TABLE "public"."personas_correo" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table personas_detalles
-- ----------------------------
ALTER TABLE "public"."personas_detalles" ADD UNIQUE ("personas_id");

-- ----------------------------
-- Primary Key structure for table personas_detalles
-- ----------------------------
ALTER TABLE "public"."personas_detalles" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table personas_direccion
-- ----------------------------
ALTER TABLE "public"."personas_direccion" ADD UNIQUE ("personas_id");

-- ----------------------------
-- Primary Key structure for table personas_direccion
-- ----------------------------
ALTER TABLE "public"."personas_direccion" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table personas_telefono
-- ----------------------------
ALTER TABLE "public"."personas_telefono" ADD UNIQUE ("numero");

-- ----------------------------
-- Primary Key structure for table personas_telefono
-- ----------------------------
ALTER TABLE "public"."personas_telefono" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table plan
-- ----------------------------
ALTER TABLE "public"."plan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table plan_detalles
-- ----------------------------
ALTER TABLE "public"."plan_detalles" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table profesion
-- ----------------------------
ALTER TABLE "public"."profesion" ADD UNIQUE ("slug");

-- ----------------------------
-- Primary Key structure for table profesion
-- ----------------------------
ALTER TABLE "public"."profesion" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table sectores
-- ----------------------------
ALTER TABLE "public"."sectores" ADD UNIQUE ("slug", "parroquias_id");

-- ----------------------------
-- Primary Key structure for table sectores
-- ----------------------------
ALTER TABLE "public"."sectores" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table sessions
-- ----------------------------
ALTER TABLE "public"."sessions" ADD UNIQUE ("id");

-- ----------------------------
-- Primary Key structure for table solicitud_beneficiarios_temp
-- ----------------------------
ALTER TABLE "public"."solicitud_beneficiarios_temp" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table solicitudes
-- ----------------------------
ALTER TABLE "public"."solicitudes" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table sucursal
-- ----------------------------
ALTER TABLE "public"."sucursal" ADD UNIQUE ("nombre");
ALTER TABLE "public"."sucursal" ADD UNIQUE ("abreviatura");

-- ----------------------------
-- Primary Key structure for table sucursal
-- ----------------------------
ALTER TABLE "public"."sucursal" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table tipo_persona
-- ----------------------------
ALTER TABLE "public"."tipo_persona" ADD UNIQUE ("nombre");

-- ----------------------------
-- Primary Key structure for table tipo_persona
-- ----------------------------
ALTER TABLE "public"."tipo_persona" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tipo_telefono
-- ----------------------------
ALTER TABLE "public"."tipo_telefono" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table vendedores
-- ----------------------------
ALTER TABLE "public"."vendedores" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."afacturar"
-- ----------------------------
ALTER TABLE "public"."afacturar" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."afacturar" ADD FOREIGN KEY ("controlfacturacion_id") REFERENCES "public"."controlfacturacion" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."afacturar_historial"
-- ----------------------------
ALTER TABLE "public"."afacturar_historial" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."afacturar_historial" ADD FOREIGN KEY ("controlfacturacion_id") REFERENCES "public"."controlfacturacion" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."app_perfiles_permisos"
-- ----------------------------
ALTER TABLE "public"."app_perfiles_permisos" ADD FOREIGN KEY ("perfil_id") REFERENCES "public"."app_perfil" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."app_usuario"
-- ----------------------------
ALTER TABLE "public"."app_usuario" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."app_usuario" ADD FOREIGN KEY ("perfil_id") REFERENCES "public"."app_perfil" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."app_usuario_empresa"
-- ----------------------------
ALTER TABLE "public"."app_usuario_empresa" ADD FOREIGN KEY ("usuario_id") REFERENCES "public"."app_usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."app_usuario_permisos"
-- ----------------------------
ALTER TABLE "public"."app_usuario_permisos" ADD FOREIGN KEY ("usuario_id") REFERENCES "public"."app_usuario" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."bancos_empresa"
-- ----------------------------
ALTER TABLE "public"."bancos_empresa" ADD FOREIGN KEY ("empresa_id") REFERENCES "public"."empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."bancos_empresa" ADD FOREIGN KEY ("bancos_id") REFERENCES "public"."bancos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."beneficiarios"
-- ----------------------------
ALTER TABLE "public"."beneficiarios" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."beneficiarios" ADD FOREIGN KEY ("contratos_id") REFERENCES "public"."contratos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."ciudades"
-- ----------------------------
ALTER TABLE "public"."ciudades" ADD FOREIGN KEY ("estados_id") REFERENCES "public"."estados" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."cobros"
-- ----------------------------
ALTER TABLE "public"."cobros" ADD FOREIGN KEY ("lotes_id") REFERENCES "public"."lotes" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."cobros" ADD FOREIGN KEY ("bancos_id") REFERENCES "public"."bancos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."cobros" ADD FOREIGN KEY ("contratos_id") REFERENCES "public"."contratos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."cobros" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."cobros" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."cobros" ADD FOREIGN KEY ("personas_bancos_id") REFERENCES "public"."personas_bancos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."contratos"
-- ----------------------------
ALTER TABLE "public"."contratos" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."contratos" ADD FOREIGN KEY ("plan_detalles_id") REFERENCES "public"."plan_detalles" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."contratos" ADD FOREIGN KEY ("empresa_id") REFERENCES "public"."empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."contratos" ADD FOREIGN KEY ("vendedor_id") REFERENCES "public"."vendedores" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."contratos" ADD FOREIGN KEY ("estatus_contrato_id") REFERENCES "public"."estatus_contrato" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."contratos" ADD FOREIGN KEY ("personas_bancos_id") REFERENCES "public"."personas_bancos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."contratos" ADD FOREIGN KEY ("frecuencia_pagos_id") REFERENCES "public"."frecuencia_pagos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."contratos_detalles"
-- ----------------------------
ALTER TABLE "public"."contratos_detalles" ADD FOREIGN KEY ("contratos_id") REFERENCES "public"."contratos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."contratos_detalles" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."contratos_facturar"
-- ----------------------------
ALTER TABLE "public"."contratos_facturar" ADD FOREIGN KEY ("contratos_id") REFERENCES "public"."contratos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."libro"
-- ----------------------------
ALTER TABLE "public"."libro" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."libro" ADD FOREIGN KEY ("controlfacturacion_id") REFERENCES "public"."controlfacturacion" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."libro_historial"
-- ----------------------------
ALTER TABLE "public"."libro_historial" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."libro_historial" ADD FOREIGN KEY ("controlfacturacion_id") REFERENCES "public"."controlfacturacion" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."movimientos"
-- ----------------------------
ALTER TABLE "public"."movimientos" ADD FOREIGN KEY ("controlfacturacion_id") REFERENCES "public"."controlfacturacion" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."movimientos" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."movimientos_historial"
-- ----------------------------
ALTER TABLE "public"."movimientos_historial" ADD FOREIGN KEY ("controlfacturacion_id") REFERENCES "public"."controlfacturacion" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."movimientos_historial" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."municipios"
-- ----------------------------
ALTER TABLE "public"."municipios" ADD FOREIGN KEY ("estados_id") REFERENCES "public"."estados" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."parroquias"
-- ----------------------------
ALTER TABLE "public"."parroquias" ADD FOREIGN KEY ("municipios_id") REFERENCES "public"."municipios" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."personas"
-- ----------------------------
ALTER TABLE "public"."personas" ADD FOREIGN KEY ("tipo_persona_id") REFERENCES "public"."tipo_persona" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."personas_bancos"
-- ----------------------------
ALTER TABLE "public"."personas_bancos" ADD FOREIGN KEY ("tipo_cuenta_id") REFERENCES "public"."banco_tipo_cuenta" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_bancos" ADD FOREIGN KEY ("bancos_id") REFERENCES "public"."bancos" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_bancos" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."personas_correo"
-- ----------------------------
ALTER TABLE "public"."personas_correo" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."personas_detalles"
-- ----------------------------
ALTER TABLE "public"."personas_detalles" ADD FOREIGN KEY ("profesion_id") REFERENCES "public"."profesion" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_detalles" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."personas_direccion"
-- ----------------------------
ALTER TABLE "public"."personas_direccion" ADD FOREIGN KEY ("parroquias_id") REFERENCES "public"."parroquias" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_direccion" ADD FOREIGN KEY ("municipios_id") REFERENCES "public"."municipios" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_direccion" ADD FOREIGN KEY ("estados_id") REFERENCES "public"."estados" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_direccion" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_direccion" ADD FOREIGN KEY ("ciudades_id") REFERENCES "public"."ciudades" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_direccion" ADD FOREIGN KEY ("sectores_id") REFERENCES "public"."sectores" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."personas_telefono"
-- ----------------------------
ALTER TABLE "public"."personas_telefono" ADD FOREIGN KEY ("tipo_telefono_id") REFERENCES "public"."tipo_telefono" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."personas_telefono" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."plan"
-- ----------------------------
ALTER TABLE "public"."plan" ADD FOREIGN KEY ("empresa_id") REFERENCES "public"."empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."plan_detalles"
-- ----------------------------
ALTER TABLE "public"."plan_detalles" ADD FOREIGN KEY ("plan_id") REFERENCES "public"."plan" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."sectores"
-- ----------------------------
ALTER TABLE "public"."sectores" ADD FOREIGN KEY ("parroquias_id") REFERENCES "public"."parroquias" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."sucursal"
-- ----------------------------
ALTER TABLE "public"."sucursal" ADD FOREIGN KEY ("estados_id") REFERENCES "public"."estados" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sucursal" ADD FOREIGN KEY ("municipios_id") REFERENCES "public"."municipios" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sucursal" ADD FOREIGN KEY ("ciudades_id") REFERENCES "public"."ciudades" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sucursal" ADD FOREIGN KEY ("empresa_id") REFERENCES "public"."empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sucursal" ADD FOREIGN KEY ("sectores_id") REFERENCES "public"."sectores" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."sucursal" ADD FOREIGN KEY ("parroquias_id") REFERENCES "public"."parroquias" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."vendedores"
-- ----------------------------
ALTER TABLE "public"."vendedores" ADD FOREIGN KEY ("personas_id") REFERENCES "public"."personas" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."vendedores_sucusal"
-- ----------------------------
ALTER TABLE "public"."vendedores_sucusal" ADD FOREIGN KEY ("sucursal_id") REFERENCES "public"."sucursal" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."vendedores_sucusal" ADD FOREIGN KEY ("vendedor_id") REFERENCES "public"."vendedores" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."vendedores_sucusal" ADD FOREIGN KEY ("empresa_id") REFERENCES "public"."empresa" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
