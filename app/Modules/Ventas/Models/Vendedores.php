<?php

namespace App\Modules\Ventas\Models;

use App\Modules\Base\Models\Modelo;



class Vendedores extends Modelo
{
    protected $table = 'vendedores';
    protected $fillable = ["personas_id","estatus","codigo"];
    protected $campos = [
    'personas_id' => [
        'type' => 'number',
        'label' => 'Personas',
        'placeholder' => 'Personas del Vendedores'
    ],
    'estatus' => [
        'type' => 'checkbox',
        'label' => 'Estatus',
        'placeholder' => 'Estatus del Vendedores'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}