<?php

namespace App\Modules\Ventas\Models;

use App\Modules\Base\Models\Modelo;
use Illuminate\Database\Eloquent\Model;


class VendedoresSucusal extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    protected $table = 'vendedores_sucusal';
    protected $fillable = ["vendedor_id","sucursal_id","empresa_id"];
    protected $campos = [
    'vendedor_id' => [
        'type' => 'number',
        'label' => 'Vendedor',
        'placeholder' => 'Vendedor del Vendedores Sucusal'
    ],
    'sucursal_id' => [
        'type' => 'number',
        'label' => 'Sucursal',
        'placeholder' => 'Sucursal del Vendedores Sucusal'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}