<?php

namespace App\Modules\Ventas\Models;

use App\Modules\Base\Models\Modelo;

use Illuminate\Database\Eloquent\Model;

class SolicitudBeneficiariosTemp extends Model
{
    protected $table = 'solicitud_beneficiarios_temp';
    protected $fillable = ["planilla","contratos_id","solicitud_id","planes_id","fecha","operacion"];
    protected $campos = [
    'planilla' => [
        'type' => 'text',
        'label' => 'Planilla',
        'placeholder' => 'Planilla del Solicitud Beneficiarios Temp'
    ],
    'contratos_id' => [
        'type' => 'number',
        'label' => 'Contratos',
        'placeholder' => 'Contratos del Solicitud Beneficiarios Temp'
    ],
    'solicitud_id' => [
        'type' => 'number',
        'label' => 'Solicitud',
        'placeholder' => 'Solicitud del Solicitud Beneficiarios Temp'
    ],
    'planes_id' => [
        'type' => 'number',
        'label' => 'Planes',
        'placeholder' => 'Planes del Solicitud Beneficiarios Temp'
    ],
    'fecha' => [
        'type' => 'date',
        'label' => 'Fecha',
        'placeholder' => 'Fecha del Solicitud Beneficiarios Temp'
    ],
    'operacion' => [
        'type' => 'number',
        'label' => 'Operacion',
        'placeholder' => 'Operacion del Solicitud Beneficiarios Temp'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}