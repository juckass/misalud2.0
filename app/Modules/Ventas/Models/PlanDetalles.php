<?php
namespace App\Modules\Ventas\Models;

use App\Modules\Base\Models\Modelo;

class PlanDetalles extends modelo
{
	protected $table = 'plan_detalles';
    protected $fillable = [
	    'plan_id',
	    'beneficiarios',
	    'porsertaje_descuento',
	    'contado',
	    'inicial',
	    'total'
    ];

    public function plan()
	{
		return $this->belongsTo('App\Modules\Ventas\Models\Plan');
	}
}