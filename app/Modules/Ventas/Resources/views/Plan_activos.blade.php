@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content')
    <div id="botonera">
            <button id="buscar" class="btn green" title="{{ Lang::get('backend.btn_group.search.title') }}">
                <i class="fa fa-search"></i>
                <span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.search.btn') }}</span>
            </button>
            <button id="imprimir" class="btn btn-info" title="{{ Lang::get('backend.btn_group.print.title') }}">
                <i class="fa fa-print"></i>
                <span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.print.btn') }}</span>
            </button>
    </div>
    <div class="row">
        <form id="formReporte" name="formReporte" method="get" action="{{url('ventas/escritorio/planesactivos/imprimir')}}" target="_blank">
            {{ csrf_field() }}
           
            <div class = "col-md-6">
                {{ Form::bsText('fecha_desde', '', [
                    'class'       => 'form-control',
                    'label'       => 'fecha Desde:',
                    'class_cont'  => 'col-md-6',
                    'placeholder' => '',
                    'required' => 'required'
                ]) }}

                {{ Form::bsText('fecha_hasta', '', [
                    'class'       => 'form-control',
                    'label'       => 'fecha Hasta:',
                    'class_cont'  => 'col-md-6',
                    'placeholder' => '',
                    'required' => 'required'
                ]) }}
                   
            </div>
        </form>
    </div>
@endsection