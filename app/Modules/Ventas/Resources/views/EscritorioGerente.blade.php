@extends('base::layouts.default')
@section('content')
	@include('base::partials.ubicacion', ['ubicacion' => ['Escritorio Vendedor']])
	<div class="row">
		<div class="col-md-4">
			<div class="dashboard-stat  green-jungle ">
				<div class="visual">
					<i class="fa fa-check"></i>
				</div>
				<div class="details">
					<div class="number">{{$activos}}</div>
					<div class="desc"> Activos </div>
				</div>
				<a class="more" href="javascript:;" data-tipo="1"> Total Contratos Activos
					<i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="dashboard-stat  yellow ">
				<div class="visual">
					<i class="fa fa-calendar-times-o"></i>
				</div>
				<div class="details">
					<div class="number">{{$vencidos}}</div>
					<div class="desc"> Vencidos </div>
				</div>
				<a class="more" href="javascript:;" data-tipo="2"> Total Contratos Vencidos
					<i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="dashboard-stat  red-thunderbird ">
				<div class="visual">
					<i class="fa fa-times-circle-o"></i>
				</div>
				<div class="details">
					<div class="number">{{$anulados}}</div>
					<div class="desc"> Anulados </div>
				</div>
				<a class="more" href="javascript:;" data-tipo="3"> Total Contratos Anulados
					<i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">	
			<div class="list-group">
				<a href="#" class="list-group-item active"><center>Opciones</center></a>
				<a href="{{ url('contratos/contratos') }}" class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i> Contratos</a>	
				
				<a href="{{ url('contratos/contratos') }}" class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i> Contratos de Intercambios</a>	
	
				<a href="{{ url('consulta/') }}" class="list-group-item "><i class="fa fa-search" aria-hidden="true"></i> Consultar Cliente</a>

				<a href="#" class="list-group-item "><i class="fa fa-clipboard" aria-hidden="true"></i> Planes Actualizados</a>
				
			</div>
		</div>  
		<div class="col-md-9">
		
			
		</div>
	</div>
@endsection