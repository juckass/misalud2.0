@include('ventas::pdf.css')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
    div.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
</style>
<body>
	<div id="reporteHtml">
		<div class="header clearfix">
			<div id="logo">
				<img src="{{ url('public/img/logos/logo.png') }}" style="width: 4cm;">
				<div style="margin-left:650px;"><span>Fecha de impresi&oacute;n</span> {{ date('d/m/Y') }}</div>
			</div>
			<h1>Reporte de Planes</h1>
			<div id="project">
		        <div></div>
			</div>
				@foreach($query as $key)
				<div id="company" class="clearfix page">
					<div class="col-md-12" >
						<label class="labell" >Nombre:</label>
						<input name="nombre[]" style="margin-right:364px;" id="nombre" class="innput"  type="text" value="{{ $key['nombre'] }}"/>

						<label style="margin-bottom:50px; font-size: 12pt;" >Meses por Pagar:</label>
						<input name="meses[]" id="meses" class="innput" type="text" value="{{ $key['meses_pagar'] }}"/>

					</div>
					<div class="col-md-12" style="margin-left:10px;">
							<label class="labell" style="margin-left:-10px;">Desde:</label>
							<input name="desde[]" style="margin-right:372px;" id="desde" class="innput" type="text" value="{{ $key['desde'] }}"/>
						
							<label class="labell" >Hasta:</label>
							<input name="hasta[]" id="hasta" class="innput"  type="text" value="{{ $key['hasta'] }}"/>
						
					</div>
					<div style="margin-top: 10px;">
					</div>
						<div class="table-scrollable">
						<div class="pagebreak">
			                <table id="tabla-plan">
			                        <tr>
			                            <th class="tcentro" rowspan="2">Beneficiarios</th>
			                            <th class="tcentro" colspan="2">Contado</th>
			                            <th class="tcentro" rowspan="2">Inicial Credito</th>
			                            <th class="tcentro" colspan="3">Giros de Credito</th>
			                        </tr>
			                        <tr>
			                            <th class="tcentro">(-) %</th>
				                        <th class="tcentro">Bs</th>
				                        <th class="tcentro">Total (Bs)</th>
				                        <!-- <th class="tcentro">{{ $key['id'] }}</th>   -->

			                        </tr>
									@foreach($key['detalle'] as $detall)
			                     		<div>
						                    <tr class="page">
						                      <input name="plan_detalle_id[]" class="form-control id" type="hidden" value="{{ $detall['plan_id'] }}"/>
						                            <td>
							                            <input name="beneficiarios[]" class="form-control total" type="text" value="{{ $detall['beneficiarios'] }}"/>
						                            </td>
						                            <td>
							                            <input name="porsertaje_descuento[]" id="porsertaje_descuento" placeholder="% de descuento" min="0" max="100" class="form-control porsertaje_descuento" value="{{$detall['porsertaje_descuento']}}" />
						                            </td>
						                            <td>
							                            <input name="contado[]" id="contado" class="form-control contado" type="text" value="{{$detall['contado']}}" readonly="readonly" />
						                            </td>
						                            <td>
							                            <input name="inicial[]" class="form-control inicial" type="text" value="{{$detall['inicial']}}"/>
						                            </td>
						                            <td>
							                            <input name="total[]" class="form-control total" type="text" value="{{$detall['total']}}"/>
						                            </td>
						                            <!-- <td>
							                            <input name="total[]" class="form-control total" type="text" value="{{ $key['id'] }}"/>
						                            </td> -->
						                    </tr>
			                     		</div>
								@endforeach
		               </table>
		            </div>   
				</div>
		</div>
		@endforeach
		<div class="main">  
			<div></div>
		</div>
		<div class="footer">
			
		</div>
	</div>
</body>
</html>