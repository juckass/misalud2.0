@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Vendedores']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Vendedores.',
        'columnas' => [
            'Codigo de Vendedor' => '50',
		    'Cedula de Vendedor' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            
            {{ Form::bsNumber('codigo', '', [
                'label' => 'Codigo del Vendedor',
                'placeholder' => 'Codigo del Vendedor',
                'required' => 'required'
            ]) }}
             {{ Form::bsSelect('estatus',[
                1 => 'Activo',
                0 => 'No Activo'
                ], '', [
                'label' => 'Estatus Vendedor',
                'name'  => 'estatus'
            ]) }}
            {{ Form::bsSelect('sucursal_id',$controller->sucur(), '', [
                'label' => 'Sucursales',
                'class' => 'bs-select',
                'multiple' => 'multiple',
                'name'    => 'sucursales[]'
            ]) }}	
             <div class="col-md-12"></div>
            {!! $Personas->generate() !!}
            <div class="col-md-12"></div>
            {!! $Personas_telefono->generate() !!}
            {!! $Personas_correo->generate() !!}
            <div class="col-md-12"></div>
           
        {!! Form::close() !!}
    </div>
@endsection