var aplicacion, $form, datos_persona = [];
$(function() {
    aplicacion = new app('submit_form', {
        'limpiar': function() {
            $('#meses').val('');
            $('#porsentaje').val('');
            $('#contado').val('');
            $('#inicial').val('');
            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
        }
    });

    $form = aplicacion.form;

    $('#nombres').prop('disabled', true);
    $('#fecha').change(function() {
        validar($(this).val());
    });

    $('#guardar').on('click', function() {
        aplicacion.guardar();
    });

    $('#calcular').on('click', function() {

        info_plan($('#planes_id').val(), $('#Num_benefe').val());
    });

    $('#tipo_pago').change(function() {

        $('#credito').css('display', 'none');
        $('#decontado').css('display', 'none');

        if ($(this).val() == 1) {
            $('#decontado').css('display', 'block');
        } else if ($(this).val() == 2) {
            $('#credito').css('display', 'block');
        } else {

            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
        }

    });

    $('#sucursal_id').change(function() {
        if ($super === 'n') {
            return false;
        }

        aplicacion.selectCascada($(this).val(), 'vendedor_id', 'vendedores');
    });

    $("#btn-buscar-persona").on('click', function() {
        $('#dni', $form).blur();
    });

    $('#dni', $form)
        .numeric({ min: 0 })
        .blur(function() {
            var tipo_persona_id = $('#tipo_persona').val(),
                dni = $(this).val()
            if ($(this).val() == '') {
                $('#tipo_persona_id').prop('disabled', false);
                $('#tipo_telefono_id').prop('disabled', false);
                $('#nombres').prop('disabled', false);
                $('#numero').prop('disabled', false);
                $('#cuenta').prop('disabled', false);
                aplicacion.limpiar();
                return false;
            }

            $.post($url + 'validar', {
                'tipo_persona_id': tipo_persona_id,
                'dni': dni,
            }, function(r) {
                if (r.s == 'n') {
                    $('#nombres').val('');
                    var win = window.open(dire + '/personas/persona/nuevo?tipo_persona_id=' + tipo_persona_id + '&dni=' + dni, 'nuevo', 'height=500,width=1000,resizable=yes,scrollbars=yes');
                    win.focus();
                    return false
                }
                $('#personas_bancos_id').html('').append('<option value="">- Seleccione</option>');
                $('#nombres').val(r.persona.nombres).prop('disabled', true);
                for (var i in r.cuentas) {
                    $('#personas_bancos_id').append('<option value="' + r.cuentas[i].id + '">' + r.cuentas[i].nombre + ' => ' + r.cuentas[i].cuenta + '</option>');

                    //console.log(r.cuentas[i].nombre);
                }

                datos_persona = r.detalles;
            });
        });


    $(".nacimiento_beneficiario").live().datepicker();


    $("#fecha", $form).datepicker({
        maxDate: "+0d"
    });
    $("#fecha2", $form).datepicker({
        maxDate: "+15d"
    });
    $("#fecha_decontado", $form).datepicker({
        maxDate: "+0d"
    });


    $(".dni_beneficiario", $form).numeric({ min: 0 });

    var selectFormGroup = function(event) {
        event.preventDefault();

        var $selectGroup = $(this).closest('.input-group-select'),
            param = $(this).attr("href").replace("#", ""),
            concept = $(this).text();

        $selectGroup.find('.concept').text(concept);
        $selectGroup.find('.input-group-select-val').val(param);
    }

    var countFormGroup = function($form) {
        return $form.find('.form-group').length;
    };

    $('.cont-persona').on('click', '.dropdown-menu a', selectFormGroup);

});

//valida DNI, RIF y CORREO
function validar($dato) {
    /*
    $dato = dato a validar
    $tipo = que campo validar
	
    */
    aplicacion.rellenar({
        planes_id: {}
    });

    $.ajax({
        url: $url + 'planes',
        type: 'POST',
        data: {
            'dato': $dato,
        },
        success: function(r) {
            $('#planes_id').html('').append('<option value="">- Seleccione</option>');
            if (r.s == 'n') {
                $('#planes_id').html('').append('<option value="">- Seleccione</option>');

            }
            if (r.s == 's') {
                for (var i in r.planes_id) {
                    $('#planes_id').append('<option value="' + r.planes_id[i].id + '">' + r.planes_id[i].nombre + '</option>');
                }
            }
        }

    });
}

function info_plan($plan_id, $beneficiarios) {

    $.ajax({
        url: $url + 'infoplan',
        type: 'POST',
        data: {
            'plan_id': $plan_id,
            'beneficiarios': $beneficiarios,
            'frecuencia': $('#frecuencia').val()
        },
        success: function(r) {
            $('#meses').val(r.plan[0].meses_pagar);

            $('#porsentaje').val(r.plan_detalles.porsertaje_descuento + ' %');

            $('#contado').val(r.plan_detalles.contado2);

            $('#inicial').val(r.plan_detalles.inicial2);

            var cuotas = 0;

            switch ($('#frecuencia').val()) {
                case 'm':
                    // mensual
                    cuotas = 1 * r.plan[0].meses_pagar;

                    break;

                case 'q':
                    // mensaul
                    cuotas = 2 * r.plan[0].meses_pagar;
                    break;

                default:
                    // statements_def
                    cuotas = 0
                    break;
            }

            var $result = r.plan_detalles.total - r.plan_detalles.inicial;

            $('#n_giros').val(cuotas);
            $us = parseFloat($result / cuotas).toFixed(2);
            $('#giro_bs').val($us);

            $('#total').val(r.plan_detalles.total2);
            $numero = 1;
            if ($('#goza').val() == 0) {
                $numero = 2;

                $("#beneficiarios").append(tmpl("tmpl-demo2"));
                $('#dni_beneficiario').val(datos_persona.dni);
                $('#nombres_beneficiario').val(datos_persona.nombre);
                $('#sexo_beneficioario').val(datos_persona.sexo);
                $('#nacimiento_beneficiario').val(datos_persona.fecha_nacimiento);
                $('#parentescos_beneficioario').val(datos_persona.parentescos);
            }

            for (var i = $numero; i <= $('#Num_benefe').val(); i++) {
                $("#beneficiarios").append(tmpl("tmpl-demo2"));
            }

            $('#cobros').html('').append('<option value="">- Seleccione</option>');

            aplicacion.rellenar({
                frecuencia_pagos_id: {}
            });

            for (var i in r.frecuencia) {
                $('#frecuencia_pagos_id').append('<option value="' + r.frecuencia[i].id + '">' + r.frecuencia[i].dias_str + '</option>');
            }

        }
    });
}