var aplicacion, $form, tabla;
$(function() {
    aplicacion = new app('formulario', {
        'limpiar': function() {
            tabla.ajax.reload();
            $('#tipo_telefono_id').prop('disabled', false);
            $('#tipo_persona_id').prop('disabled', false);
            $('#nombres').prop('disabled', false);
            $('#numero').prop('disabled', false);
            $('#cuenta').prop('disabled', false);
        },
        'buscar': function(r) {
            $('#dni').val(r.persona.dni);
            validar(r.persona.dni, 'dni', '#dni');
            if (r.estatus) {
                $('#estatus').val(1);
            } else {
                $('#estatus').val(0);
            }

        }
    });

    $form = aplicacion.form;

    tabla = datatable('#tabla', {
        ajax: $url + "datatable",
        columns: [
            { "data": "codigo", "name": "vendedores.codigo" },
            { "data": "dni", "name": "personas.dni" }
        ]
    });

    $('#tabla').on("click", "tbody tr", function() {
        aplicacion.buscar(this.id);
    });

    $('#dni').blur(function() {
        if ($(this).val() == '') {

            $('#tipo_persona_id').prop('disabled', false);
            $('#tipo_telefono_id').prop('disabled', false);
            $('#nombres').prop('disabled', false);
            $('#numero').prop('disabled', false);
            $('#cuenta').prop('disabled', false);
            aplicacion.limpiar();
            return false;
        }
        validar($(this).val(), 'dni', '#dni');
    });
});
//valida DNI, RIF y CORREO
function validar($dato, $campo, $id) {
    /*
    $dato = dato a validar
    $tipo = que campo validar
	
    */
    $.ajax({
        url: $url + 'validar',
        type: 'POST',
        data: {
            'dato': $dato,
        },
        success: function(r) {
            if (r.s == 'n') {

                return false
            }

            $('#tipo_persona_id').val(r.persona.tipo_persona_id).prop('disabled', true);
            $('#nombres').val(r.persona.nombres).prop('disabled', true);;
            if (r.telefono != 'n') {
                $('#tipo_telefono_id').val(r.telefono.tipo_telefono_id).prop('disabled', true);
                $('#numero').val(r.telefono.numero).prop('disabled', true);
            }
            if (r.correo != 'n') {
                $('#cuenta').val(r.correo.correo).prop('disabled', true);
            }
        }
    });
}