   var aplicacion, $form, tabla, $tablaPlan;
   $(function() {
       /*	aplicacion = new app('formulario', {
       		'limpiar' : function(){
       		}
       	});*/
       confimacion();
       rechazados();
       rechazados_renovacion();
       $('.more').on('click', function() {
           var win = window.open(dire + '/ventas/escritorio/contratos/' + this.getAttribute('data-tipo'), 'estatus', 'height=500,width=1000,resizable=yes,scrollbars=yes');
           win.focus();
           return false
       });
   });


   function confimacion() {
       solicitud_asignadas = $('#tabla1')
           .on("click", "tbody tr", function() {
               //procesarsolicitud(this.id);
               contrato(this.id);
           })
           .dataTable({
               processing: true,
               serverSide: true,
               ajax: $url + "confirmacion",
               oLanguage: datatableEspanol,
               columns: [
                   { data: 'planilla', name: 'contratos.planilla' },
                   { data: 'cargado', name: 'contratos.cargado' },
                   { data: 'inicio', name: 'contratos.inicio' },
                   { data: 'nombres', name: 'personas.nombres' },
                   { data: 'nombre', name: 'sucursal.nombre' },
                   { data: 'tipo_solicitud', name: 'solicitudes.tipo_solicitud' }
               ]
           });
   }

   function rechazados() {

       solicitud_rechazadas = $('#tabla2')
           .on("click", "tbody tr", function() {
               //procesarsolicitud(this.id);
               contratorechazdos(this.id, 3);
           })
           .dataTable({
               processing: true,
               serverSide: true,
               ajax: $url + "rechazados",
               oLanguage: datatableEspanol,
               columns: [
                   { data: 'planilla', name: 'contratos.planilla' },
                   { data: 'cargado', name: 'contratos.cargado' },
                   { data: 'inicio', name: 'contratos.inicio' },
                   { data: 'nombres', name: 'personas.nombres' },
                   { data: 'nombre', name: 'sucursal.nombre' }
               ]
           });
   }

   function rechazados_renovacion() {

       $('#tabla4')
           .on("click", "tbody tr", function() {
               //procesarsolicitud(this.id);
               renocavionesorechazdos(this.id);
           })
           .dataTable({
               processing: true,
               serverSide: true,
               ajax: $url + "rechazadosrenovacion",
               oLanguage: datatableEspanol,
               columns: [
                   { data: 'planilla', name: 'contratos_temp.planilla' },
                   { data: 'inicio', name: 'contratos_temp.inicio' },
                   { data: 'nombres', name: 'personas.nombres' },
                   { data: 'nombre', name: 'sucursal.nombre' }
               ]
           });
   }

   function contrato(id) {

       $.ajax({
           url: $url + 'validar',
           type: 'POST',
           data: {
               'id': id,
           },
           success: function(r) {
               if (r.s == 'n') {

                   return false;
               }
               if (r.tipo_solicitud == 3) {
                   var win = window.open(dire + '/ventas/escritorio/beneficiariosconfirmacion/' + id, 'beneficiarios', 'height=500,width=1000,resizable=yes,scrollbars=yes');
                   win.focus();
                   return false
               }
               var win = window.open(dire + '/ventas/escritorio/consulta/' + id + '/' + r.tipo_solicitud + '/0', 'consulta', 'height=500,width=1000,resizable=yes,scrollbars=yes');
               win.focus();
               return false;
           }
       });
   }

   function contratorechazdos(id) {
       var win = window.open(dire + '/ventas/escritorio/contratorechazdos/' + id, 'consulta', 'height=500,width=1000,resizable=yes,scrollbars=yes');
       win.focus();
       return false;
   }

   function renocavionesorechazdos(id) {
       var win = window.open(dire + '/ventas/escritorio/renovacionesrechazdos/' + id, 'consulta', 'height=500,width=1000,resizable=yes,scrollbars=yes');
       win.focus();
       return false
   }