<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Plan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('plan', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre', 100);
            $table->date('desde');
            $table->date('hasta');
            //$table->string('frecuencia_de_pago', 10);
            //$table->integer('cuotas_especiales');
            $table->integer('meses_pagar')->unsigned();
            $table->integer('empresa_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')
                ->references('id')->on('empresa')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan');
    }
}
