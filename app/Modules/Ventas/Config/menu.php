<?php

$menu['Venta'] = [
	[
		'nombre' 	=> 'Ventas',
		'direccion' => '#Ventas',
		'icono' 	=> 'fa fa-clipboard',
		'menu' 		=> [
			[
				'nombre' 	=> 'Asistente Creacion de Contratos',
				'direccion' => 'ventas/asistente',
				'icono' 	=> 'fa fa-clipboard'
			],
			[
				'nombre' 	=> 'Vendedores',
				'direccion' => 'ventas/vendedores',
				'icono' 	=> 'fa fa-users'
			],
			[
				'nombre' 	=> 'Planes',
				'direccion' => 'ventas/planes',
				'icono' 	=> 'fa fa-list-alt'
			],
			[
				'nombre' 	=> 'Reporte',
				'direccion' => 'escritorio/planesactivos/imprimir',
				'icono' 	=> 'fa fa-file-pdf-o'
			]
		]
	]
];
 $menu['escritorios'] = [
	
	[
		'nombre' 	=> 'Vendedor',
		'direccion' => 'ventas/escritorio',
		'icono' 	=> 'fa fa-home'
	],
	[
		'nombre' 	=> 'Gerente',
		'direccion' => 'ventas/gerente',
		'icono' 	=> 'fa fa-home'
	],

]; 
