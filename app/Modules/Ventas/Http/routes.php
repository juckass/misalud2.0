<?php

Route::group(['middleware' => 'web', 'prefix' => 'ventas', 'namespace' => 'App\\Modules\Ventas\Http\Controllers'], function()
{
    Route::group(['prefix' => 'planes'], function () {
        Route::get('/', 				'PlanController@index');
        Route::get('nuevo', 			'PlanController@nuevo');
        Route::get('cambiar/{id}', 		'PlanController@cambiar');
        
        Route::get('buscar/{id}', 		'PlanController@buscar');

        Route::post('validar',			'PlanController@validar');
        Route::post('guardar',			'PlanController@guardar');
        Route::put('guardar/{id}', 		'PlanController@guardar');

        Route::delete('eliminar/{id}', 	'PlanController@eliminar');
        Route::post('restaurar/{id}', 	'PlanController@restaurar');
        Route::delete('destruir/{id}', 	'PlanController@destruir');

        Route::get('datatable', 		'PlanController@datatable');
    });
    Route::group(['prefix' => 'vendedores'], function() {
        Route::get('/',                 'VendedoresController@index');
        Route::get('nuevo',             'VendedoresController@nuevo');
        Route::get('cambiar/{id}',      'VendedoresController@cambiar');
        
        Route::get('buscar/{id}',       'VendedoresController@buscar');

        Route::post('guardar',          'VendedoresController@guardar');
        Route::post('validar',          'VendedoresController@validar');
        Route::put('guardar/{id}',      'VendedoresController@guardar');

        Route::delete('eliminar/{id}',  'VendedoresController@eliminar');
        Route::post('restaurar/{id}',   'VendedoresController@restaurar');
        Route::delete('destruir/{id}',  'VendedoresController@destruir');

        Route::get('datatable',         'VendedoresController@datatable');
    });

    Route::group(['prefix' => 'asistente'], function() {
        Route::get('/',                 'AsistentecontratosController@index');
        Route::post('/planes',          'AsistentecontratosController@planes');
        Route::get('vendedores/{id}',   'AsistentecontratosController@vendedores');
        Route::post('validar',          'AsistentecontratosController@buscarpersona');
        Route::post('infoplan',			'AsistentecontratosController@infoplan');
        Route::post('guardar',			'AsistentecontratosController@guardar');
    });

    Route::group(['prefix' => 'escritorio'], function() {
        Route::get('/',                                     'EscritoriovendedorController@index');
        Route::get('/confirmacion',                         'EscritoriovendedorController@confirmacion');
        Route::get('/rechazados',                           'EscritoriovendedorController@rechazados');
        Route::get('/rechazadosrenovacion',                 'EscritoriovendedorController@rechazadosrenovacion');
        Route::post('/validar',                             'EscritoriovendedorController@validar');
        Route::post('/reenviarcontrato',                    'EscritoriovendedorController@reenviarcontrato');
        Route::post('/reenviarrenovacion',                  'EscritoriovendedorController@reenviarrenovacion');
        Route::get('/consulta/{id}/{opera}/{solicitud_id}', 'EscritoriovendedorController@consulta');
        Route::get('/contratorechazdos/{id}',               'EscritoriovendedorController@contratorechazdos');
        Route::get('/renovacionesrechazdos/{id}',           'EscritoriovendedorController@renocavionesorechazdos');
        Route::get('/beneficiariosconfirmacion/{id}',       'EscritoriovendedorController@beneficiariosconfirmacion');
        Route::get('/contratos/{id}',                       'EscritoriovendedorController@contratos');
        Route::get('/datainformacion',                      'EscritoriovendedorController@datainformacion');
    });
    Route::group(['prefix' => 'gerente'], function() {
        Route::get('/',  'GerenteController@index');
       
    });

    Route::group(['prefix' => 'escritorio/planesactivos'], function() {
        Route::get('/',           'Planes_activosController@index');
        Route::get('imprimir',    'Planes_activosController@imprimir');
       
    });

    //{{route}}
});