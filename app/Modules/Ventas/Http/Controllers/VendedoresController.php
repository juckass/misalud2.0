<?php

namespace App\Modules\Ventas\Http\Controllers;

//Controlador Padre
use App\Modules\Ventas\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Session;
//Request
use App\Modules\Ventas\Http\Requests\VendedoresRequest;

//Modelos
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\VendedoresSucusal;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasTelefono;
use App\Modules\Base\Models\PersonasCorreo;
use App\Modules\Empresa\Models\Sucursal;

class VendedoresController extends Controller
{
    protected $titulo = 'Vendedores';

    public $js = [
        'Vendedores'
    ];
    
    public $css = [
        'Vendedores'
    ];

    public $librerias = [
        'datatables',
        'bootstrap-select'
    ];

    public function index()
    {
        return $this->view('ventas::Vendedores', [
            'Vendedores' => new Vendedores(),
            'Personas' => new Personas(),
			'Personas_telefono' => new PersonasTelefono(),
			'Personas_correo' => new PersonasCorreo()
        ]);
    }

    public function nuevo()
    {
        $Vendedores = new Vendedores();
        return $this->view('ventas::Vendedores', [
            'layouts' => 'base::layouts.popup',
            'Vendedores' => $Vendedores
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Vendedores = Vendedores::find($id);
        return $this->view('ventas::Vendedores', [
            'layouts' => 'base::layouts.popup',
            'Vendedores' => $Vendedores
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Vendedores = Vendedores::withTrashed()->find($id);
        } else {
            $Vendedores = Vendedores::find($id);
        }
        
        if ($Vendedores) {
            $persona = Personas::find($Vendedores->personas_id);
            $sucursales = VendedoresSucusal::where('vendedor_id', $Vendedores->id)->get();
    
            $_sucursales =[];
    
            foreach ($sucursales as $re) {
                $_sucursales[]= $re->sucursal_id;
            }
            return array_merge($Vendedores->toArray(), [
                's' => 's',
                'sucursal_id' => $_sucursales,
                'persona' => $persona,
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(VendedoresRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
        
            $persona = Personas::where('dni',$request->dni)->first();
            if(count($persona) == 0){
                $persona = Personas::create([
                    "tipo_persona_id" => $request->tipo_persona_id,
                    "dni"             => $request->dni,
                    "nombres"         => $request->nombres,
                    "foto"            => 'user.png'
                ]);
            }
            $telefono = PersonasTelefono::where('personas_id',  $persona->id)
            ->where('principal', 1)->get();
            
            if($telefono->count() == 0){
                PersonasTelefono::create([
                    "personas_id" =>  $persona->id,
                    "tipo_telefono_id" => $request->tipo_telefono_id,
                    "numero" => $request->numero,
                    "principal" => true
                ]);
            }

            $correo = PersonasCorreo::where('personas_id',  $persona->id)
            ->where('principal', 1)->get();
            
            if($correo->count() == 0){
                PersonasCorreo::create([
                    "personas_id" =>  $persona->id,
                    "correo" => $request->cuenta,
                    "principal" => true
                ]);
            }
                   
           
            $Vendedores = $id == 0 ? new Vendedores() : Vendedores::find($id);
            
            $Vendedores->fill([
                "codigo"      => $request->codigo,
                "personas_id" => $persona->id,
                "estatus"     => $request->estatus
            ]);
            $Vendedores->save();
            
            VendedoresSucusal::where('vendedor_id', $Vendedores->id)->delete();
            
            foreach ($request->sucursales as $value) {
                VendedoresSucusal::create([
                    'vendedor_id' => $Vendedores->id,
                    'sucursal_id' => $value,
                    'empresa_id' => Session::get('empresa')
                ]);
            } 


        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Vendedores->id,
            'texto' => $Vendedores->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Vendedores::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Vendedores::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Vendedores::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Vendedores::select([
            'vendedores.id','vendedores.codigo', 'personas.dni', 'vendedores.deleted_at'
        ])
        ->leftJoin('personas', 'personas.id','=','vendedores.personas_id');

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function sucur()
    {

        $sql = Sucursal::select('id', 'nombre')
        ->where('empresa_id', Session::get('empresa'))
        ->pluck('nombre', 'id');        
        return $sql;
    }

    public function validar(Request $request){
        
        $persona = Personas::where('dni',$request->dato)->get();
        
        if($persona->count() == 0){

            return [
            's' => 'n'
            ];
        }	
        $_persona = $persona->toArray();
        $telefono = PersonasTelefono::where('personas_id', $_persona[0]['id'])
            ->where('principal', 1)->get();
        
        if($telefono->count() == 0){
            $telefono = 'n';
        }else{
            $telefono =	$telefono->toArray();
        }

        $correo = PersonasCorreo::where('personas_id', $_persona[0]['id'])
            ->where('principal', 1)->get();
        
        if($correo->count() == 0){
            $correo = 'n';
        }else{
            $correo =	$correo->toArray();
        }

        return [
            'persona' => $_persona[0],
            'telefono'=> $telefono[0],
            'correo'  => $correo[0],
            's' => 's', 
            'msj' => trans('controller.incluir')
        ];
    }
}