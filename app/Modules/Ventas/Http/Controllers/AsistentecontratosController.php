<?php
namespace App\Modules\Ventas\Http\Controllers;

//Controlador Padre
use App\Modules\Ventas\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Session;
use Carbon\Carbon;
//Request
use App\Modules\Ventas\Http\Requests\ContratosRequest;

//Modelos
use App\Modules\Contratos\Models\Contratos;
use App\Modules\Contratos\Models\Parentesco;
use App\Modules\Contratos\Models\Beneficiarios;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosDetalles;
use App\Modules\Cobranza\Models\Cobros;
use App\Modules\Facturacion\Models\ContratosFacturar;
use App\Modules\Ventas\Models\Plan;
use App\Modules\Ventas\Models\PlanDetalles;
use App\Modules\Ventas\Models\VendedoresSucusal;
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\Solicitudes;
use App\Modules\base\Models\Personas;
use App\Modules\base\Models\PersonasDetalles;
use App\Modules\base\Models\PersonasBancos;
use App\Modules\Empresa\Models\sucursal;
    

class AsistentecontratosController extends Controller
{
    protected $titulo = "Nuevo Contratos";

    public $js = [
        'moment.min.js',
        'Contratos',
        'jquery.validate.min.js',
        'additional-methods.min.js',
       // 'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
        'select2.full.min.js'
    ];
    
    public $css = [
        'Contratos',
        'select2.min.css',
        'select2-bootstrap.min.css'
    ];

    public $numero = [
        1 =>'1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10'
    ];

    public $_tipo_pago = [
       0 => 'Por Tarjeta',
        'Transferencia',
        'efectivo'

    ];

    public $librerias = [
        'datatables',
        'maskedinput',
        'alphanum',
        'template',
        'jquery-ui',
        'jquery-ui-timepicker',
        'bootstrap-sweetalert',
        'bootstrap-wizard',
        'maskMoney'
    ];

    public function index()
    {
        $tipo = "nuevo";

        return $this->view('ventas::asistentecontrato',[
            'Personas' => new Personas(),
            'tipo'     => $tipo
        ]);
    }

    public function parentescos(){
        return Parentesco::all();
    }
    
    public function tipo_persona(){
        return TipoPersona::pluck('nombre', 'id');
    }
    public function planes(Request $request){
       
        
        $fecha = Carbon::createFromFormat('d/m/Y',$request->dato)->format("Y-m-d");
        $planes = Plan::where('desde','<=',  $fecha)
        ->where('hasta','>=',  $fecha)->get();


        $salida = ['s' => 'n' , 'msj'=> 'NO Hay planes activos'];
        
        if(count($planes) > 0 ){
            $salida = ['s' => 's', 'planes_id'=> $planes];
        }               
        
        return $salida;
    }

    public function sucursales()
    {

        $sqls = VendedoresSucusal::select('vendedores_sucusal.sucursal_id')
            ->leftJoin('vendedores', 'vendedores.id','=', 'vendedores_sucusal.vendedor_id')
            ->where('vendedores.estatus', 1)//estatus 1 = activo 
            ->where('vendedores.personas_id', auth()->user()->personas_id)
            ->get();

            $sucursales= [];

            foreach ($sqls as $key => $sql) {
                 $sucursales[] = $sql->sucursal_id;
            }

        if(auth()->user()->super === "s"){
            return Sucursal::where('empresa_id', Session::get('empresa'))->pluck('nombre', 'id');
        }
        return Sucursal::whereIn('id', $sucursales)->pluck('nombre', 'id');
    }
    public function vendedores(Request $request){
        
        $sql = VendedoresSucusal::join('vendedores', 'vendedores.id','=', 'vendedores_sucusal.vendedor_id')
            ->leftJoin('personas', 'personas.id','=','vendedores.personas_id')
            ->where('vendedores_sucusal.sucursal_id', $request->id)
            ->where('vendedores.estatus', 1)//estatus 1 = activo 
            ->pluck('vendedores.codigo','vendedores.id')
            ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el sucursal no Contiene Vendedores Activos'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Vendedores encontrados', 'vendedor_id'=> $sql];
        }               
        
        return $salida;
    } 
    public function buscarpersona(Request $request){
        
        $persona = Personas::where('dni',$request->dni)->get();
        
        if($persona->count() == 0){
            return [
            's' => 'n'
            ];
        }   
        
        $_persona = $persona->toArray();
        $detalles = PersonasDetalles::where('personas_id',$_persona[0]['id'])->first();

        if(!$detalles){
            return [
                's'   => 'n'
            ];
        }
        
        $fecha_n =Carbon::createFromFormat('d/m/Y', $detalles->fecha_nacimiento);
        $date = Carbon::parse($fecha_n)->age;
        
        $detalles->nombre = $_persona[0]['nombres'];
        $detalles->dni = $_persona[0]['dni'];
        $detalles->edad = $date;
        $detalles->parentescos = 33;
        
        $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
            ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
            ->where('personas_bancos.personas_id', $_persona[0]['id'])->get()->toArray();
            
        return [
            'persona'   => $_persona[0],
            'cuentas'   => $cuentas,
            'detalles'  => $detalles,
            's'         => 's', 
            'msj' => trans('controller.incluir')
        ];
    }
    public function infoplan(Request $request){
        

        $plan = Plan::where('id',$request->plan_id )->where('empresa_id',Session::get('empresa'))->get();

        $plan_detalles = PlanDetalles::where('plan_id',$request->plan_id )->where('beneficiarios', $request->beneficiarios)->first();


        $plan_detalles->contado2 = number_format($plan_detalles->contado, 2, ',', '.');
        $plan_detalles->inicial2 = number_format($plan_detalles->inicial, 2, ',', '.');
        $plan_detalles->total2 = number_format($plan_detalles->total, 2, ',', '.');

        $frecuencia = FrecuenciaPagos::where('frecuencia', $request->frecuencia)->get();

        return['plan' => $plan, 'plan_detalles'=> $plan_detalles, 'frecuencia' => $frecuencia]; 

    }
    public function guardar(ContratosRequest $request, $id = 0)
    {       
        DB::beginTransaction();
        try{

            $Contratos = $id == 0 ? new Contratos() : Contratos::find($id);

            $datos = $request->all();

            $persona = Personas::where('dni', $datos['dni'])->first();

            $datos['titular'] = $persona->id;

            $datos['empresa_id'] = Session::get('empresa');
            $datos['cargado'] = date('Y-m-d');

            $date =Carbon::createFromFormat('d/m/Y', $datos['inicio'])->addYear();

            $datos['vencimiento'] = $date;

            $plan_detalles = PlanDetalles::where('plan_id',$datos['planes_id'])->where('beneficiarios', $request->Num_benefe)->first();

            $datos['plan_detalles_id']  = $plan_detalles->id;
            $datos['total_contrato']    = $plan_detalles->total;
            $datos['inicial']           = $plan_detalles->inicial;
            $datos['beneficiarios']     = $request->Num_benefe;
            $datos['tipo_contrato']     = 0;

            $pago = 0;

            if(auth()->user()->super === 'n'){
                 
                 $vende = Vendedores::select('id')
                ->where('vendedores.personas_id', auth()->user()->personas_id)
                ->first();

                $datos['vendedor_id'] = $vende->id;

                $vendedor = $vende->id;

            }else{
                 $vendedor =  $datos['vendedor_id'];
            }
           
            if($request->tipo_pago == 1 ){
                $pago = $plan_detalles->contado;

                $datos['total_contrato'] = $pago;
               
                $datos['cuotas'] = 1;
                $datos['inicial'] = '0';
                $datos['personas_bancos_id'] = null;
                $datos['frecuencia_pagos_id'] = null;
                $datos['primer_cobro'] = $datos['fecha_decontado'];
                $datos['cobrando']  = 1;
                $datos['fecha_pago'] = null;

            }else{
                $plan = Plan::where('id',$request->planes_id )->first();
                if($request->frecuencia == 'm'){

                    $cuotas = 1 * $plan->meses_pagar ;
                }else{

                    $cuotas = 2 * $plan->meses_pagar; 
                }

                $datos['cuotas'] = $cuotas;
                $datos['fecha_pago'] =  $datos['fecha_incial'];
            }

            $datos['total_pagado']  = $pago; 
            $datos['estatus_contrato_id']  = 2;  
            
            $Contratos->fill($datos);
            $Contratos->save();
            
            $_tipo_pago = [
               0 => 'Por Tarjeta',
                'Transferencia',
                'efectivo'
            ];

            if($request->tipo_pago == 1 ){

                ContratosFacturar::create([
                    "fecha_facturar" => date('Y-m-d'),
                    "contratos_id"   => $Contratos->id
                ]);

                Cobros::create([
                    "contratos_id"  => $Contratos->id,
                    "sucursal_id"   => $datos['sucursal_id'],
                    "personas_id"   => $persona->id,
                    "total_cobrar"  => $pago,
                    "concepto"      => 'Pago de Contrato de Contado por: '.$_tipo_pago[$datos['pago']],
                    "cuota"         => 1,
                    "fecha_pagado"  => $request->fecha_decontado,
                    "total_pagado"  => $pago,
                    "num_recibo"    => $datos['n_recibo'],
                    "tipo_pago"     => 'caja',
                    "completo"      => true
                ]);

            }

            ContratosDetalles::create([
               "contratos_id" => $Contratos->id,
               "personas_id"  => $vendedor,
               "operacion"    => "Carga de Nuevo contrato",
               "planilla"     => $request->planilla
            ]);
            

            //$this->beneficiarios($request->all(), $Contratos->id);

            $_datos = [];
            $res = $request->all();
          
            Beneficiarios::where('contratos_id', $Contratos->id)->delete();
            
            foreach ($res['dni_beneficiario'] as $_id => $beneficiario) {
                
                if($res['dni_beneficiario'][$_id] == ""){
                
                    $persona = Personas::where('dni', 'LIKE',$res['dni'].'%')->count();
                    $result = $persona +  1;
                    $dni = $res['dni'] . "-" . $result;

                }else{
                    $dni = $res['dni_beneficiario'][$_id];
                }

                $persona = Personas::where('dni', $dni)->first();

                if(count($persona) == 0){
                    $persona = Personas::create([
                        "tipo_persona_id" => 1,
                        "dni"             => $dni,
                        "nombres"         => $res['nombres_beneficiario'][$_id]
                    ]);

                    PersonasDetalles::create([
                        "personas_id"      => $persona->id,
                        "sexo"             => $res['sexo_beneficiario'][$_id],
                        "fecha_nacimiento" => $res['nacimiento_beneficiario'][$_id]
                    ]);
                }


                $_datos= [
                    "contratos_id"  => $Contratos->id,
                    "personas_id"   => $persona->id,
                    "parentesco_id" => $res['parentescos_beneficioario'][$_id],
                    "estatus"       => 0
                ];
                
                Beneficiarios::create($_datos);   
            }
              
            /*
                Tipo solicitud 
                    1 = contrato nuevo
                    2 = renovacion contrato
                    3 = agregar beneficiarios
                    4 = modificacion de informacion personal

                Solicitante 
                    id_persona quien solicita.
                aquien 
                    1-contrato
                    2-persona
                
                indice 
                    persona_id o contrato_id
            */

            Solicitudes::create([
               "tipo_solicitud" => 1,
               "solicitante"    => auth()->user()->personas_id,
               "aquien"         => 1,
               "indece"         => $Contratos->id
            ]);


        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Contratos->id,  
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }
}