<?php

namespace App\Modules\Ventas\Http\Controllers;

use App\Modules\Ventas\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use DB;
use App\Modules\Base\Models\TipoPersona;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasBancos;
use App\Modules\Base\Models\PersonasDetalles;

use App\Modules\Contratos\Models\Contratos;

use App\Modules\Contratos\Models\ContratosTemp;
use App\Modules\Contratos\Models\Beneficiarios;
use App\Modules\Contratos\Models\Parentesco;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosDetalles;

use App\Modules\Base\Models\Bancos;
use App\Modules\Base\Models\BancoTipoCuenta;
use App\Modules\Empresa\Models\Sucursal;
use App\Modules\Ventas\Models\VendedoresSucusal;
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\Plan;
use App\Modules\Ventas\Models\PlanDetalles;
use App\Modules\Ventas\Models\Solicitudes;
use App\Modules\Ventas\Models\SolicitudBeneficiariosTemp;
use App\Modules\Contratos\Models\BeneficiariosTemp;

use Auth;


class EscritoriovendedorController extends Controller {

    public $autenticar = false;
    public function __construct() 
	{
        $this->init();
        $this->middleware('auth');
	}
    protected $tipo;
	protected $titulo = 'Escritorio Vendedor';

	public $librerias = [
        'datatables'
    ];
	public $js = [
        'escritorio'
    ];
    
    public $css = [
        'escritorio'
    ];
	public function index() {
        $vende = Vendedores::where('vendedores.personas_id', auth()->user()->personas_id)->first();
            
        $vendedor = 'n';

            $activos = 0;
            $vencidos = 0;
            $anulados = 0;

        if(count($vende) >= 1){
            $vendedor = 's';

            $activos = Contratos::where('contratos.estatus_contrato_id', 1)
            ->where('contratos.anulado', false)
            ->where('contratos.vendedor_id',  $vende->id)->count();

            $vencidos = Contratos::where('contratos.estatus_contrato_id', 5)
            ->where('contratos.anulado', false)
            ->where('contratos.vendedor_id',  $vende->id)->count();

            $anulados = Contratos::where('contratos.anulado', true)
            ->where('contratos.vendedor_id',  $vende->id)->count();
        }
        return $this->view('ventas::Escritorio',[
            'vendedor' =>$vendedor,
            'activos'  =>$activos,
            'vencidos' =>$vencidos,
            'anulados' =>$anulados,
        ]);
	}

    public function contratos(Request $request, $id= 0){
        $this->librerias = [  
            'datatables'
        ];  
        $this->js = [];
       
        switch ( $id) {
            case 1:
                $text =  "Activos";
                break;
            case 2:
                $text =   "Vencidos";
                break;
            case 3:
                $text =   "Anulados";
                break;
            default :
                $text =   "";
                break;
        }
        $this->titulo = "infomarcion de tus contratos " . $text;

        return $this->view('ventas::popup-info-estatus', [
            'layouts' => 'base::layouts.popup-consulta',
            'tipo' => $id
        ]);
    }
	public function confirmacion(Request $request)
    {
    
       
        $sql = Solicitudes::select([
            'solicitudes.id', 
            'solicitudes.tipo_solicitud', 
            'contratos.planilla', 
            'contratos.cargado', 
            'contratos.inicio', 
            'personas.nombres', 
            'sucursal.nombre'      
        ])->join('contratos', 'solicitudes.indece', '=', 'contratos.id')
        ->join('personas', 'personas.id', '=', 'contratos.titular')
        ->join('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id');
              
        if(auth()->user()->super === 'n'){
           
            $vende = Vendedores::where('vendedores.personas_id', auth()->user()->id)->first();
            if( count($vende) != 0  ){
                $sql->where('contratos.vendedor_id',  $vende->id);
            }

        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            })
            ->editColumn('tipo_solicitud', function($sql) {
                
                switch ($sql->tipo_solicitud) {
                    case 1:
                        return "contrato nuevo";
                        break;
                    case 2:
                        return  "renovacion contrato";
                        break;
                    case 3:
                        return  "agregar beneficiarios";
                        break;
                    case 4:
                        return  "modificacion de informacion personal";
                        break;
                }
            })
            ->editColumn('cargado', function($sql) {  
                return Carbon::parse($sql->cargado)->format('d/m/Y');
            }) 
            ->editColumn('inicio', function($sql) {  
                return Carbon::parse($sql->inicio)->format('d/m/Y');
            }) 
            ->make(true);
    }

    public function rechazadosrenovacion(Request $request)
    {
    
        $sql = ContratosTemp::select([
            'contratos.id', 
            'contratos_temp.planilla', 
            'contratos_temp.inicio', 
            'personas.nombres', 
            'sucursal.nombre'
           
        ])->LeftJoin('contratos', 'contratos_temp.contrato_id', '=', 'contratos.id')
          ->LeftJoin('personas', 'personas.id', '=', 'contratos.titular')
          ->LeftJoin('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id')
          ->where('contratos_temp.rechazado', 1);

        if(auth()->user()->super === 'n'){

            $vende = Vendedores::where('vendedores.personas_id', auth()->user()->id)->first();
            if( count($vende) != 0  ){
                $sql->where('contratos.vendedor_id',  $vende->id);
            }
         

         }


        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            }) 
            ->editColumn('inicio', function($sql) {  
                return Carbon::parse($sql->inicio)->format('d/m/Y');
            }) 
            ->make(true);
    }

    public function rechazados(Request $request){
    
        $sql = Contratos::select([
            'contratos.id', 
            'contratos.planilla', 
            'contratos.cargado', 
            'contratos.inicio', 
            'personas.nombres', 
            'sucursal.nombre'
           
        ])//->join('contratos_detalles', 'contratos_detalles.contratos_id', '=', 'contratos.id')
        ->join('personas', 'personas.id', '=', 'contratos.titular')
        ->join('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id')
        ->where('contratos.estatus_contrato_id', 4);

        if(auth()->user()->super === 'n'){

            $vende = Vendedores::where('vendedores.personas_id', auth()->user()->id)->first();
            if( count($vende) != 0  ){
                $sql->where('contratos.vendedor_id',  $vende->id);
            }

         }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            })
            ->editColumn('inicio', function($sql) {  
                return Carbon::parse($sql->inicio)->format('d/m/Y');
            }) 
            ->editColumn('cargado', function($sql) {  
                return Carbon::parse($sql->cargado)->format('d/m/Y');
            }) 
            ->make(true);
    }
    public function datainformacion(Request $request){
    
        $sql = Contratos::select([
            'contratos.id', 
            'contratos.planilla', 
            'contratos.inicio', 
            'contratos.vencimiento', 
            'personas.nombres', 
            'personas.dni', 
            'sucursal.nombre'
           
        ])
        ->join('personas', 'personas.id', '=', 'contratos.titular')
        ->join('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id');
        
            switch ($request->id) {
                case 1:
                    $sql->where('contratos.anulado', false)
                    ->where('contratos.estatus_contrato_id', 1);
                    break;
                case 2:
                    $sql->where('contratos.anulado', false)
                    ->where('contratos.estatus_contrato_id',5);
                    break;
                case 3:
                    $sql->where('contratos.anulado', true);
                    break;
                default :
                    break;
            }
        if(auth()->user()->super === 'n'){

            $vende = Vendedores::where('vendedores.personas_id', auth()->user()->id)->first();
            if( count($vende) != 0  ){
                $sql->where('contratos.vendedor_id',  $vende->id);
            }

         }
        
        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            })
            ->editColumn('inicio', function($sql) {  
                return Carbon::parse($sql->inicio)->format('d/m/Y');
            }) 
            ->editColumn('vencimiento', function($sql) {  
                return Carbon::parse($sql->vencimiento)->format('d/m/Y');
            }) 
            ->make(true);
    } 

    public function validar(Request $request){
        return  Solicitudes::find($request->id);
      
    }
    
    public function consulta(Request $request, $id = 0, $opera = 0, $solicitud_id = 0)
    {      

        $this->librerias = [
            'bootstrap-sweetalert',
            'datatables',
            'maskMoney'
        ];
       
       

        $this->css=[
            ''
        ];

      
        $solicitud = Solicitudes::where('id',intval($id))->first();

        //informacion del contrato actual
        $Contratos = $this->contratoactual($solicitud->indece);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $solicitud->indece )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $solicitud->indece )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id', $solicitud->indece)->get();

        // Solicitud de renovacion de contrato 
        
        switch ($solicitud->tipo_solicitud) {
        case '2':
            $contrato_renovar = ContratosTemp::where('solicitud_id', $solicitud->id)->first();

            $contrato_renovar->vencimiento  =  Carbon::parse($contrato_renovar->vencimiento)->format('d/m/Y');

            $beneficiario2 = 0;
            $plan2 = 'Personalizado';

            if($contrato_renovar->plan_detalles_id  != '')
            {

                $plan_detalles2 = PlanDetalles::where('id',$contrato_renovar->plan_detalles_id )->first();
                $plan2          = Plan::where('id',$plan_detalles2->plan_id )->first();
                $beneficiario   = $plan_detalles->beneficiarios;
                $plan2          = $plan2->nombre;

            } else
            {
                $beneficiario2   =  Beneficiarios::where('contratos_id', $solicitud->indece )->where('estatus',0)->count(); 
            }

            $frecuencia = FrecuenciaPagos::where('id', $contrato_renovar->frecuencia_pagos_id)->first();
            
                $cuenta = "";
                $Bancos = "";

            if($contrato_renovar->personas_bancos_id != ''){
                $cuenta = PersonasBancos::find($contrato_renovar->personas_bancos_id);
                $Bancos = Bancos::where('id', $cuenta->bancos_id)->first();
            }
            

            $this->js=[
                'popup-renovacion',
            ];

            
            return $this->view('ventas::Contratos-renovacion', [
                'layouts'           => 'base::layouts.popup-consulta',
                'Contrato'          => $Contratos,
                'Persona'           => $persona,
                'plan'              => $plan,
                'beneficiario'      => $beneficiario,
                'plan2'             => $plan2,
                'beneficiario2'     => $beneficiario2,
                'contrato_renovar'  => $contrato_renovar,
                'contratos_detalles'=> $contratos_detalles,
                'tipo'              => $opera,
                'solicitud_id'      => $solicitud_id,
                'frecuencia22'      => $frecuencia,
                'per_bancos'        => $cuenta,
                'banco'             => $Bancos
                ]);
            break;
            
        }
        
    
        $this->js=[
            'popup-consulta'
             
        ];

        return $this->view('ventas::Contratos-consulta', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => $opera,
            'solicitud_id'      => $solicitud_id
        ]);
    }
    public function contratoactual($id){
        
        $Contratos = Contratos::select(
            'contratos.id',
            'contratos.planilla',
            'contratos.titular',
            'contratos.vencimiento',
            'contratos.inicio',
            'contratos.cargado',
            'contratos.primer_cobro',
            'contratos.fecha_incial',
            'contratos.inicial',
            'contratos.renovaciones',
            'contratos.tipo_pago',
            'contratos.cuotas',
            'contratos.cuotas_pagadas',
            'contratos.cuota_valor',
            'contratos.plan_detalles_id',
            'contratos.giros',
            'contratos.giros_pagados',
            'contratos.giro_valor',
            'contratos.bookie',
            'frecuencia_pagos.frecuencia',
            'frecuencia_pagos.dias',
            'contratos.total_pagado',
            'contratos.total_contrato',
            'contratos.anulado',
            'personas_bancos.cuenta',
            'bancos.nombre as banco',
            'estatus_contrato.nombre as estatus_contrato',
            'sucursal.nombre as sucursal',
            'personas.nombres as vendedor' 

        )
        ->leftJoin('vendedores', 'vendedores.id','=', 'contratos.vendedor_id')
        ->leftJoin('personas', 'personas.id','=','vendedores.personas_id')
        ->leftJoin('personas_bancos', 'personas_bancos.id','=', 'contratos.personas_bancos_id')
        ->leftJoin('bancos', 'bancos.id','=', 'personas_bancos.bancos_id')
        ->leftJoin('frecuencia_pagos', 'frecuencia_pagos.id','=', 'contratos.frecuencia_pagos_id')
        ->leftJoin('estatus_contrato','estatus_contrato.id', '=', 'contratos.estatus_contrato_id')      
        ->leftJoin('sucursal','sucursal.id', '=', 'contratos.sucursal_id')        
        ->where('contratos.id', $id)->first();

        $Contratos->vencimiento  =  Carbon::parse($Contratos->vencimiento)->format('d/m/Y');
        
        if($Contratos->anulado){
            $Contratos->estatus_contrato = 'Contrato Anulado';
        }


        return $Contratos;
    }
    public function beneficiariosconfirmacion(Request $request, $id = 0)
    {
        
        //adonde: 0 = analista, 1= vendedor, dice de adonde esta accediendo
        
        $this->librerias = [
            'bootstrap-sweetalert'
        ];
        $this->js=[
            'beneficiariosconfirmacion'
             
        ];

        $this->css=[
            ''
        ];

   
        $solicitud = Solicitudes::find($id);

        //informacion del contrato actual
        $Contratos = $this->contratoactual($solicitud->indece);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $solicitud->indece )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $solicitud->indece )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id',$solicitud->indece )->get();

        
        $solicitud_temp = SolicitudBeneficiariosTemp::where('solicitud_id', $solicitud->id)->first();


        $beneficiarios_temp = BeneficiariosTemp::select([
            "beneficiarios_temp.dni",
            "beneficiarios_temp.nombre",
            'parentesco.nombre as parentesco',
            "beneficiarios_temp.sexo",
            "beneficiarios_temp.solicitud_beneficiarios_temp_id",
            "beneficiarios_temp.parentesco_id",
            "beneficiarios_temp.fecha_nacimiento"
            ])
            ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios_temp.parentesco_id')
            ->where('solicitud_beneficiarios_temp_id', $solicitud_temp->id)->get();

        $operacion='';

        $inf_plan= $plan;

        if($Contratos->tipo_pago == 1){
            $t = "De contado";
        }else{
            $t = "Credito";

        }
        $inf_tipo= $t;
        
        $inf_frecuencia     = $Contratos->frecuencia == 'm' ? 'Mensual'.' : '. $Contratos->dias : 'Quincenal' .' : '. $Contratos->dias;
        $inf_cuotas2        = $Contratos->total_contrato / $Contratos->cuotas;
        $inf_cuotas         = $Contratos->cuotas;
        $inf_total          = $Contratos->total_contrato;
        $inf_total_pagado   = $Contratos->total_pagado;
        
        $num_b = $beneficiarios_temp->count();
        
        switch ($solicitud_temp->operacion) {
            case 0:
                //cambio
                $operacion = 'Cambio de Beneficiario';
              
                break;
            case 1:
                //reduccion
                $operacion = 'Reduccion de Beneficiarios';
                $_plan_detalles2 = PlanDetalles::where('id', $Contratos->plan_detalles_id)->first();

                $_plan_detalles = PlanDetalles::where('plan_id',$_plan_detalles2->plan_id)
                    ->where('beneficiarios', $num_b)
                    ->first();

                $inf_cuotas2 = $_plan_detalles->total / $Contratos->cuotas;

                $inf_total   = $_plan_detalles->total;

                break;
            case 2:
                $operacion = 'Inclusion de Beneficiarios';
                $_plan = Plan::where('id',$solicitud_temp->planes_id )->first();
                $inf_plan = $_plan->nombre;

                $_plan_detalles = PlanDetalles::where('plan_id',$solicitud_temp->planes_id)
                ->where('beneficiarios', $num_b)
                ->first();

                $inf_cuotas2 = $_plan_detalles->total / $Contratos->cuotas;

                $inf_total   = $_plan_detalles->total;

                break; 
            default:
                # code...
                break;
        }


        return $this->view('ventas::beneficiariosconfirmacion', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'beneficiarios_temp'=> $beneficiarios_temp,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => $operacion,
            'solicitud_id'      => $solicitud->id,
            'inf_plan'          => $inf_plan,
            'inf_tipo'          => $inf_tipo,
            'inf_frecuencia'    => $inf_frecuencia,
            'inf_cuotas2'       => $inf_cuotas2,
            'inf_cuotas'        => $inf_cuotas,
            'inf_total'         => $inf_total,
            'inf_total_pagado'  => $inf_total_pagado
        ]);
    }

    public function contratorechazdos(Request $request, $id = 0){
        $this->librerias = [
            'bootstrap-sweetalert',
            'datatables',
            'maskMoney'
        ];
       
       

        $this->css=[
            ''
        ];

      

        //informacion del contrato actual
        $Contratos = $this->contratoactual($id);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $id )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $id )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id', $id)->get();

        // Solicitud de renovacion de contrato 
        
    
        $this->js=[
            'popup-consulta'
             
        ];

        return $this->view('ventas::Contratos-rechazados', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => '',
            'solicitud_id'      => ''
        ]);
    }

    public function reenviarcontrato(Request $request){
        DB::beginTransaction();
        try{

            $Contratos = Contratos::find($request->id);

            $datos['estatus_contrato_id']  = 2;  
            
            $Contratos->fill($datos);
            $Contratos->save();
            
            $vende = Vendedores::select('id')
            ->where('vendedores.personas_id', auth()->user()->personas_id)
            ->first();


            $vendedor = $vende->id;

            ContratosDetalles::create([
               "contratos_id" => $Contratos->id,
               "personas_id"  => $vendedor,
               "operacion"    => 'Reenvio de solicitud de contrato',
               "planilla"     => $Contratos->planilla
            ]);
              
              
            /*
                Tipo solicitud 
                    1 = contrato nuevo
                    2 = renovacion contrato
                    3 = agregar beneficiarios
                    4 = modificacion de informacion personal

                Solicitante 
                    id_persona quien solicita.
                aquien 
                    1-contrato
                    2-persona
    
                indice 
                    persona_id o contrato_id
            */

            Solicitudes::create([
               "tipo_solicitud" => 1,
               "solicitante"    => auth()->user()->personas->id,
               "aquien"         => 1,
               "indece"         => $Contratos->id
            ]);


        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Contratos->id,  
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }
    public function reenviarrenovacion(Request $request){
        DB::beginTransaction();
        try{

            $Contratos = Contratos::find($request->id);

          
            
              
            $vende = Vendedores::select('id')
            ->where('vendedores.personas_id', auth()->user()->id)
            ->first();


            $vendedor = $vende->id;

            ContratosDetalles::create([
               "contratos_id" => $Contratos->id,
               "personas_id"  => $vendedor,
               "operacion"    => "Reenvio de solicitud de Renovacion de contrato",
               "planilla"     => $Contratos->planilla
            ]);
              
              
            /*
                Tipo solicitud 
                    1 = contrato nuevo
                    2 = renovacion contrato
                    3 = agregar beneficiarios
                    4 = modificacion de informacion personal

                Solicitante 
                    id_persona quien solicita.
                aquien 
                    1-contrato
                    2-persona
    
                indice 
                    persona_id o contrato_id
            */

           $solicitud =  Solicitudes::create([
               "tipo_solicitud" => 2,
               "solicitante"    => auth()->user()->personas->id,
               "aquien"         => 1,
               "indece"         => $Contratos->id
            ]);
            $contrato_renovar = ContratosTemp::where('contrato_id',$request->id)->update([
                'rechazado' => false,
                'solicitud_id' => $solicitud->id
            ]);


        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Contratos->id,  
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }
    public function renocavionesorechazdos(Request $request, $id = 0){
        $this->librerias = [
            'bootstrap-sweetalert',
            'datatables',
            'maskMoney'
        ];
       

        $this->css=[
            ''
        ];

      
        //$solicitud = Solicitudes::where('id',intval($id))->first();

        //informacion del contrato actual
        $Contratos = $this->contratoactual($id);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $id )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $id )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id', $id)->get();

        // Solicitud de renovacion de contrato 
        
       
        $contrato_renovar = ContratosTemp::where('contrato_id',$id)->first();

        $contrato_renovar->vencimiento  =  Carbon::parse($contrato_renovar->vencimiento)->format('d/m/Y');

        $beneficiario2 = 0;
        $plan2 = 'Personalizado';

        if($contrato_renovar->plan_detalles_id  != '')
        {

            $plan_detalles2 = PlanDetalles::where('id',$contrato_renovar->plan_detalles_id )->first();
            $plan2          = Plan::where('id',$plan_detalles2->plan_id )->first();
            $beneficiario   = $plan_detalles->beneficiarios;
            $plan2          = $plan2->nombre;

        } else
        {
            $beneficiario2   =  Beneficiarios::where('contratos_id', $id )->where('estatus',0)->count(); 
        }

        $frecuencia = FrecuenciaPagos::where('id', $contrato_renovar->frecuencia_pagos_id)->first();
        
            $cuenta = "";
            $Bancos = "";

        if($contrato_renovar->personas_bancos_id != ''){
            $cuenta = PersonasBancos::find($contrato_renovar->personas_bancos_id);
            $Bancos = Bancos::where('id', $cuenta->bancos_id)->first();
        }
        

        $this->js=[
            //'popup-renovacion',
        ];

        
        return $this->view('ventas::renovacionesrechazdos', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'plan2'             => $plan2,
            'beneficiario2'     => $beneficiario2,
            'contrato_renovar'  => $contrato_renovar,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => '',
            'solicitud_id'      => '',
            'frecuencia22'      => $frecuencia,
            'per_bancos'        => $cuenta,
            'banco'             => $Bancos
        ]);
    }
            
        
}