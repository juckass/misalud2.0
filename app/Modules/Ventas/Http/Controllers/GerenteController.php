<?php

namespace App\Modules\Ventas\Http\Controllers;

use App\Modules\Ventas\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use DB;
use App\Modules\Base\Models\TipoPersona;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasBancos;
use App\Modules\Base\Models\PersonasDetalles;

use App\Modules\Contratos\Models\Contratos;

use App\Modules\Contratos\Models\ContratosTemp;
use App\Modules\Contratos\Models\Beneficiarios;
use App\Modules\Contratos\Models\Parentesco;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosDetalles;

use App\Modules\Base\Models\Bancos;
use App\Modules\Base\Models\BancoTipoCuenta;
use App\Modules\Empresa\Models\Sucursal;
use App\Modules\Ventas\Models\VendedoresSucusal;
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\Plan;
use App\Modules\Ventas\Models\PlanDetalles;
use App\Modules\Ventas\Models\Solicitudes;
use App\Modules\Ventas\Models\SolicitudBeneficiariosTemp;
use App\Modules\Contratos\Models\BeneficiariosTemp;

use Auth;


class GerenteController extends Controller {


    protected $tipo;
	protected $titulo = 'Escritorio Gerente de Ventas';

	public $librerias = [
        'datatables'
    ];
	public $js = [
        'escritorio'
    ];
    
    public $css = [
        'escritorio'
    ];
	public function index() {
        $activos  = 0;
        $vencidos = 0;
        $anulados = 0;
        $activos = Contratos::where('contratos.estatus_contrato_id', 1)
        ->where('contratos.anulado', false)->count();

        $vencidos = Contratos::where('contratos.estatus_contrato_id', 5)
        ->where('contratos.anulado', false)->count();

        $anulados = Contratos::where('contratos.anulado', true)->count();
		return $this->view('ventas::EscritorioGerente',[
       
            'activos'  =>$activos,
            'vencidos' =>$vencidos,
            'anulados' =>$anulados,
        ]);
	}                   
}