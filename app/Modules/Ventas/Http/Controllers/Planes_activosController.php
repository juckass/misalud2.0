<?php

namespace App\Modules\Ventas\Http\Controllers;

use App\Modules\Ventas\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use DB;
use App\Modules\Base\Models\TipoPersona;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasBancos;
use App\Modules\Base\Models\PersonasDetalles;

use App\Modules\Contratos\Models\Contratos;

use App\Modules\Contratos\Models\ContratosTemp;
use App\Modules\Contratos\Models\Beneficiarios;
use App\Modules\Contratos\Models\Parentesco;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosDetalles;

use App\Modules\Base\Models\Bancos;
use App\Modules\Base\Models\BancoTipoCuenta;
use App\Modules\Empresa\Models\Sucursal;
use App\Modules\Ventas\Models\VendedoresSucusal;
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\Plan;
use App\Modules\Ventas\Models\PlanDetalles;
use App\Modules\Ventas\Models\Solicitudes;
use App\Modules\Ventas\Models\SolicitudBeneficiariosTemp;
use App\Modules\Contratos\Models\BeneficiariosTemp;

use Auth;
use PDF;


class Planes_activosController extends Controller {


    protected $tipo;
	protected $titulo = 'Planes Activos';

	public $librerias = [
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
    ];
	public $js = [
        'planesactivos'
    ];
    
    public $css = [
        'planesactivos'
    ];
	public function index() {
        return $this->view('ventas::Plan_activos');
	}

    public function imprimir(request $request){

      


        $fecha_desde= Carbon::createFromFormat('d/m/Y',$request->fecha_desde);
        $fecha_hasta= Carbon::createFromFormat('d/m/Y',$request->fecha_hasta);
        
        $plan = Plan::with('detalle')
            ->whereBetween('plan.desde', [$fecha_desde, $fecha_hasta]);
            //->whereBetween('plan.hasta', [$fecha_desde, $fecha_hasta]);
        
           // dd($plan->get()->toArray());
            if ($request->has('html')) {
                     $Plan = [];
                     $i = 0;
                    foreach ($plan->get() as $pla) {
                         $Plan[] = [
                             $pla->id,
                             $pla->meses_pagar
                        ];
                    }
                return ['Plan' => $Plan];
            }
          
            $html = view('ventas::pdf.reporte', [
                     'i'            => 0,
                     'query'        => $plan->get()->toArray(),
                     'total'        => 0
            ])->render();

            $pdf = PDF::loadHTML($html);

            return $pdf->download('reporte.pdf');
    }    
}