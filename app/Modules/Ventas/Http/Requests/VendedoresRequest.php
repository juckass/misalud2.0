<?php

namespace App\Modules\Ventas\Http\Requests;

use App\Http\Requests\Request;

class VendedoresRequest extends Request {
    protected $reglasArr = [
		'codigo' => ['required', 'unique:vendedores,codigo']
	];
}