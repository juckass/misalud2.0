<?php

namespace App\Modules\Ventas\Http\Requests;

use App\Http\Requests\Request;

class VendedoresSucusalRequest extends Request {
    protected $reglasArr = [
		'vendedor_id' => ['required', 'integer'], 
		'sucursal_id' => ['required', 'integer']
	];
}