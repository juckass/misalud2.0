<?php

namespace App\Modules\Ventas\Http\Requests;

use App\Http\Requests\Request;

class SolicitudesRequest extends Request {
    protected $reglasArr = [
		'tipo_solicitud' => ['required', 'integer'], 
		'solicitante' => ['required', 'integer'], 
		'aquien' => ['required', 'integer'], 
		'indece' => ['required', 'integer']
	];
}