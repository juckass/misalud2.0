<?php

Route::group(['middleware' => 'web', 'prefix' => 'consulta', 'namespace' => 'App\\Modules\Consulta\Http\Controllers'], function()
{
    Route::get('/', 	    		            'ConsultaContratoController@index');
    Route::get('/buscar',   		            'ConsultaContratoController@buscar');
    Route::get('consulta/{id}', 	            'ConsultaContratoController@consulta');
    Route::get('cobrostable/{id}', 	            'ConsultaContratoController@cobrostable');
    Route::get('renovacion/{id}/{adonde}', 	    'ConsultaContratoController@renovacion');
    Route::post('planes', 		                'ConsultaContratoController@planes');
    Route::post('infoplan', 		            'ConsultaContratoController@infoplan');
    Route::post('renovacion/guardar', 		    'ConsultaContratoController@renovacionguardar');
    Route::get('beneficiarios/{id}/{adonde}', 	'ConsultaContratoController@beneficiarios');
    Route::get('beneficiario', 	                'ConsultaContratoController@beneficiario');
    Route::post('asisplan', 	                 'ConsultaContratoController@asisplan');
    Route::post('beneficiariosguardar', 	     'ConsultaContratoController@beneficiariosguardar');
});
