<?php

namespace App\Modules\Consulta\Http\Controllers;

//Controlador Padre
use App\Modules\Consulta\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
use Session;
//Modelos

use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasDetalles;
use App\Modules\Base\Models\PersonasDireccion;
use App\Modules\Base\Models\PersonasBancos;
use App\Modules\Base\Models\PersonasTelefono;
use App\Modules\Base\Models\PersonasCorreo;
use App\Modules\Base\Models\Bancos;
use App\Modules\Base\Models\BancoTipoCuenta;
use App\Modules\Base\Models\TipoTelefono;

use App\Modules\Base\Models\Estados;
use App\Modules\Base\Models\Municipio;
use App\Modules\Base\Models\Parroquia;
use App\Modules\Base\Models\Ciudades;
use App\Modules\Base\Models\Sector;
use App\Modules\Contratos\Models\ContratosTemp;
use App\Modules\Contratos\Models\Contratos;
use App\Modules\Contratos\Models\Beneficiarios;
use App\Modules\Contratos\Models\Parentesco;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosDetalles;
use App\Modules\Ventas\Models\SolicitudBeneficiariosTemp;
use App\Modules\Contratos\Models\BeneficiariosTemp;;
use App\Modules\Empresa\Models\Sucursal;
use App\Modules\Ventas\Models\Solicitudes;
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\Plan;
use App\Modules\Ventas\Models\PlanDetalles;
use App\Modules\Cobranza\Models\Cobros;
use App\Modules\Facturacion\Models\ContratosFacturar;
use App\Modules\Ventas\Models\VendedoresSucusal;

class ConsultaContratoController extends Controller
{
    protected $titulo = 'Consulta Cliente';

    public $js = [
        'consulta',
        'moment.min.js'   
    ];
    
    public $css = [
        'consulta'

    ];

    public $librerias = [
        'datatables',
        'bootstrap-sweetalert'
    ];

    public function index()
    {
        $perfil = \Auth()->user()->perfil->nombre;
        return $this->view('consulta::Consulta',[
            'Perfil' => $perfil
        ]);
    } 

    public function validar(Request $request){

        return $this->validar2($request);
    }

    public function cobrostable(Request $request, $id = 0)
    {
       
        $sql = Cobros::select(
            'id',
            'total_cobrar',
            'created_at',
            'fecha_pagado',
            'total_pagado',
            'tipo_pago',
            'num_recibo',
            'concepto'
        )->where('contratos_id', $id);

      
       

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    protected function validar2($id){
        
        $consulta= Personas::select('dni')
            ->where('dni',$id->id)
            ->where('dni','not like', $id->id.'-%');


        if(count($consulta->get()) == 0){
                
            return array_merge([
                'consul' => count($consulta->get()),
                's' => 'n',
                'msj' => trans('controller.nobuscar'),
            ]);
        }

        return array_merge([
            'consul' => count($consulta->get()),
            's' => 's',
            'msj' => trans('controller.buscar'),
        ]);
    }
    
    public function contratoactual($id){

        $Contratos = Contratos::select(
            'contratos.id',
            'contratos.planilla',
            'contratos.titular',
            'contratos.vencimiento',
            'contratos.inicio',
            'contratos.cargado',
            'contratos.primer_cobro',
            'contratos.fecha_incial',
            'contratos.inicial',
            'contratos.renovaciones',
            'contratos.tipo_pago',
            'contratos.cuotas',
            'contratos.cuotas_pagadas',
            'contratos.cuota_valor',
            'contratos.plan_detalles_id',
            'contratos.giros',
            'contratos.giros_pagados',
            'contratos.giro_valor',
            'contratos.bookie',
            'frecuencia_pagos.frecuencia',
            'frecuencia_pagos.dias',
            'contratos.total_pagado',
            'contratos.total_contrato',
            'contratos.anulado',
            'personas_bancos.cuenta',
            'bancos.nombre as banco',
            'estatus_contrato.nombre as estatus_contrato',
            'sucursal.nombre as sucursal',
            'personas.nombres as vendedor' 

        )
        ->leftJoin('vendedores', 'vendedores.id','=', 'contratos.vendedor_id')
        ->leftJoin('personas', 'personas.id','=','vendedores.personas_id')
        ->leftJoin('personas_bancos', 'personas_bancos.id','=', 'contratos.personas_bancos_id')
        ->leftJoin('bancos', 'bancos.id','=', 'personas_bancos.bancos_id')
        ->leftJoin('frecuencia_pagos', 'frecuencia_pagos.id','=', 'contratos.frecuencia_pagos_id')
        ->leftJoin('estatus_contrato','estatus_contrato.id', '=', 'contratos.estatus_contrato_id')      
        ->leftJoin('sucursal','sucursal.id', '=', 'contratos.sucursal_id')        
        ->where('contratos.id', $id)->first();

        $Contratos->vencimiento  =  Carbon::parse($Contratos->vencimiento)->format('d/m/Y');
        
        if($Contratos->anulado){
            $Contratos->estatus_contrato = 'Contrato Anulado';
        }


        return $Contratos;
    }

    public function buscar(Request $request){

        $validar = $this->validar2($request);

        if($validar['s'] == 'n'){

            return $validar;
        }
        $datos = [];

       $persona = Personas::select('*')->where('dni',$request->id)->where('dni','not like', $request->id.'-%')->first();
       $datos['id']      = $persona->id;
       $datos['tipo']    = $persona->tipo_persona->nombre;
       $datos['dni']     = $persona->tipo_persona->nombre.'-'.$persona->dni;
       $datos['nombres'] = $persona->nombres;

        if($persona->tipo_persona->nombre == 'V' or $persona->tipo_persona->nombre == 'E'){
            $persona_detalles = PersonasDetalles::where('personas_id', $persona->id)->first();
            
            $datos['sexo'] = '';
            $datos['profesion'] = '';
            $datos['edad'] = '';

            if(count($persona_detalles) == 1){
                $datos['fecha_nacimiento'] = $persona_detalles->fecha_nacimiento;
                    $sexo  = 'Femenino';
                if($persona_detalles->sexo == 'm'){
                    $sexo = 'Masculino';
                }
                $datos['sexo'] = $sexo;
                if($persona_detalles->profesion_id != ''){
                    $datos['profesion'] = $persona_detalles->profesion->nombre;
                }
               
                
                $fecha_n =Carbon::createFromFormat('d/m/Y', $persona_detalles->fecha_nacimiento);
                $date = Carbon::parse($fecha_n)->age;
                $datos['edad'] = $date;
            }
 
        }
        $persona_direccion = PersonasDireccion::where('personas_id', $persona->id)->first();
        
            $datos['estado']     = '';
            $datos['municipio']  = '';
            $datos['ciudad']     = '';
            $datos['sector']     = '';
            $datos['parroquia'] = '';
            $datos['direccion'] = '';

        if(count($persona_direccion) == 1){
            $datos['estado']     = $persona_direccion->estado->nombre;
            $datos['municipio']  = $persona_direccion->municipio->nombre;
            $datos['ciudad']     = $persona_direccion->ciudad->nombre;
            $datos['sector']     = $persona_direccion->sector->nombre;
            $datos['parroquia'] = $persona_direccion->parroquia->nombre;
            $datos['direccion'] = $persona_direccion->direccion;
        }

        $persona_telefono = PersonasTelefono::where('personas_id', $persona->id)
        ->where('tipo_telefono_id', 1)
        ->where('principal', 1)
        ->first();
   
        $datos['tlf_movil'] = '';
        if(count($persona_telefono) == 1){
            $datos['tlf_movil'] = $persona_telefono->numero;
        }

        $persona_telefono2 = PersonasTelefono::where('personas_id', $persona->id)
        ->where('tipo_telefono_id', 2)
        ->first();

        $datos['tlf_casa'] = '';
        if(count($persona_telefono2) == 1){
            $datos['tlf_casa'] = $persona_telefono2->numero;
        }

        $correo =  PersonasCorreo::where('personas_id', $persona->id)
        ->where('principal', 1)
        ->first();

        $datos['correo'] =  '';
        if(!is_null($correo)){
            $datos['correo'] = $correo->correo;

        }

        $bancos = PersonasBancos::select('banco_tipo_cuenta.nombre', 'personas_bancos.cuenta', 'bancos.nombre as banco')
        ->join('banco_tipo_cuenta','banco_tipo_cuenta.id', '=', 'personas_bancos.tipo_cuenta_id')
        ->join('bancos','bancos.id', '=', 'personas_bancos.bancos_id')
        ->where('personas_id', $persona->id)->get();

        if(count($bancos) == 0){
             $bancos = '';
        }
    
    
        $contratos = Contratos::select(
            'contratos.id',
            'contratos.planilla',
            'contratos.vencimiento',
            'contratos.inicio', 
            'contratos.vencimiento',
            'contratos.total_pagado',
            'contratos.total_contrato',
            'contratos.cobrando',
            'contratos.anulado',

            'estatus_contrato.nombre as estatus_contrato',
            'sucursal.nombre as sucursal',
            'personas.nombres as vendedor' 

        )
        ->join('vendedores', 'vendedores.id','=', 'contratos.vendedor_id')
        ->join('personas', 'personas.id','=','vendedores.personas_id')
        ->join('estatus_contrato','estatus_contrato.id', '=', 'contratos.estatus_contrato_id')      
        ->join('sucursal','sucursal.id', '=', 'contratos.sucursal_id')        
        ->where('titular', $persona->id)->get()->toArray();




        return array_merge([
            'datos_personales' => $datos,
            'bancos'           => $bancos, 
            'contratos'        => $contratos,    
            's'                => 's',
            'msj'              => 'Datos Encontrados',
        ]);
    }

    public function consulta(Request $request, $id = 0)
    {      

        $this->librerias = [
            'bootstrap-sweetalert',
            'datatables',
            'maskMoney'
        ];
       

        $this->css=[
            ''
        ];


        //informacion del contrato actual
        $Contratos = $this->contratoactual($id);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $id )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $id )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id', $id )->get();


    
        $this->js=[
            'popup-consulta'
             
        ];

        return $this->view('consulta::Contratos-consulta', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => '',
            'solicitud_id'      => ''
        ]);
    }
    public function renovacion(Request $request, $id=0, $adonde = 0)
    {
        
        //adonde: 0 = analista, 1= vendedor, dice de adonde esta accediendo
        $this->librerias = [

            'jquery-ui',
            'jquery-ui-timepicker',
            'bootstrap-wizard',
            'maskMoney'
        ];

        $this->js=[
            'renovacion',
            'jquery.validate.min.js',
            'additional-methods.min.js',
            //'bootstrap-wizard/jquery.bootstrap.wizard.min.js'
            'select2.full.min.js',
            'moment.min.js'   
        ];

        $this->css=[
            'select2.min.css',
            'select2-bootstrap.min.css'
        ];


        $num_beneficiarios = Contratos::select('beneficiarios','titular')->where('id', $id)->first();

        $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
            ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
            ->where('personas_bancos.personas_id', $num_beneficiarios->titular)->get();


        return $this->view('consulta::popup-renovacion', [
            'layouts'       => 'base::layouts.popup-consulta',
            'contratos_id'  => $id,
            'beneficiarios' => $num_beneficiarios->beneficiarios,
            'cuentas'       => $cuentas,
            'adonde'        => $adonde // es de adonde viene el formularo 
        ]);
    } 
    
    public function planes(Request $request){
        
        $fecha = Carbon::createFromFormat('d/m/Y',$request->dato)->format("Y-m-d");
        $planes = Plan::where('desde','<=',  $fecha)
            ->where('hasta','>=',  $fecha)->get();
        $salida = ['s' => 'n' , 'msj'=> 'NO Hay planes activos']; 
        if(count($planes) > 0 ){
            $salida = ['s' => 's', 'planes_id'=> $planes];
        }               
        return $salida;
    }
    public function infoplan(Request $request){
        
        
        $plan = Plan::where('id',$request->plan_id )->where('empresa_id',Session::get('empresa'))->get();

        $plan_detalles = PlanDetalles::where('plan_id',$request->plan_id )->where('beneficiarios', $request->beneficiarios)->first();

        $plan_detalles->contado2 = number_format($plan_detalles->contado, 2, ',', '.');
        $plan_detalles->inicial2 = number_format($plan_detalles->inicial, 2, ',', '.');
        $plan_detalles->total2 = number_format($plan_detalles->total, 2, ',', '.');

        $frecuencia = FrecuenciaPagos::where('frecuencia', $request->frecuencia)->get();

        return['plan' => $plan, 'plan_detalles'=> $plan_detalles, 'frecuencia' => $frecuencia]; 

    }

    public function renovacionGuardar(Request $request, $id = 0)
    {   
        DB::beginTransaction();
        try{
    
             $datos= $request->all();
             $contrato = Contratos::find($request->id);
        
            //Solicitud
            $datos['contrato_id'] = $request['id'];
            $date =Carbon::createFromFormat('d/m/Y', $datos['inicio'])->addYear();
            $datos['vencimiento'] = $date;
            $plan_detalles = PlanDetalles::where('plan_id',$datos['planes_id'])->where('beneficiarios', $request['Num_benefe'])->first();

            $datos['plan_detalles_id']  = $plan_detalles->id;
            $datos['total_contrato']    = $plan_detalles->total;
            $datos['inicial']           = $plan_detalles->inicial; 
            $datos['beneficiarios']     = $request['Num_benefe']; 

            $pago = 0;

            if($request['tipo_pago'] == 1 ){
                $pago = $plan_detalles->contado;
                $datos['total_contrato'] = $plan_detalles->contado; 
                $datos['cuotas'] = 1;
                $datos['inicial'] = '0';
                $datos['personas_bancos_id'] = null;
                $datos['frecuencia_pagos_id'] = null;
                $datos['primer_cobro'] = $request['fecha_decontado'];
                $datos['fecha_incial'] = null;
                //registrar cobro

                    $fecha_facturar = date('Y-m-d');
                $_tipo_pago = [
                    0 => 'Por Tarjeta',
                        'Transferencia',
                        'efectivo'
                ];

                Cobros::create([
                    "contratos_id"  => $contrato->id,
                    "sucursal_id"   => $contrato->sucursal_id,
                    "personas_id"   => $contrato->titular,
                    "total_cobrar"  => $pago,
                    "concepto"      => 'Pago de Contrato de Contado por: '.$_tipo_pago[$datos['pago']],
                    "cuota"         => 1,
                    "fecha_pagado"  => $request->fecha_decontado,
                    "total_pagado"  => $pago,
                    "num_recibo"    => $datos['n_recibo'],
                    "tipo_pago"     => 'caja',
                    "completo"      => true
                ]);

            }else{
                $datos['fecha_decontado']= null;
                $plan = Plan::where('id',$request['planes_id'] )->first();
                
                if($request['frecuencia'] == 'm'){
                    $cuotas = 1 * $plan->meses_pagar;
                }else{
                    $cuotas = 2 * $plan->meses_pagar; 
                }
                
                $datos['cuotas'] = $cuotas;
                $datos['primer_cobro'] = null;
                    
                $date =  Carbon::createFromFormat('d/m/Y', $datos['inicio']);
                
                $fecha_facturar = $date->subDay(1)->format('Y-m-d');
            }

            $datos['total_pagado']  = $pago; 
            unset($datos['n_recibo']);
            unset($datos['pago']);
            unset($datos['fecha_decontado']);
            $datos['cuotas_pagadas']  = null; 
            /*
                Tipo solicitud 
                    1 = contrato nuevo
                    2 = renovacion contrato
                    3 = agregar beneficiarios
                    4 = modificacion de informacion personal

                Solicitante 
                    id_persona quien solicita.
                aquien 
                    1-contrato
                    2-persona
                
                indice 
                    persona_id o contrato_id
            */
            ContratosFacturar::create([
                "fecha_facturar" =>  $fecha_facturar,
                "contratos_id"   => $contrato->id
            ]);

            
            $solicitud = Solicitudes::create([
                "tipo_solicitud" => 2,
                "solicitante"    => auth()->user()->personas->id,
                "aquien"         => 1,
                "indece"         => $datos['contrato_id']
            ]); 

            $datos['solicitud_id'] = $solicitud['id']; 

            ContratosTemp::create($datos);

            ContratosDetalles::create([
                "contratos_id" => $datos['contrato_id'],
                "personas_id"  => auth()->user()->personas_id,
                "operacion"    => 'Solicitud de Renovacion de Contrato',
                "planilla"     => $datos['planilla']
            ]); 
        
        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function beneficiarios(Request $request, $id=0, $adonde = 0)
    {
        
        //adonde: 0 = analista, 1= vendedor, dice de adonde esta accediendo
        $this->librerias = [
            'jquery-ui',
            'jquery-ui-timepicker',
            'bootstrap-wizard',
            'template',
            'maskMoney',
            'bootstrap-sweetalert',
        ];

        $this->js=[
            'popup-beneficiarios',
            'jquery.validate.min.js',
            'additional-methods.min.js',
            //'bootstrap-wizard/jquery.bootstrap.wizard.min.js'
            'select2.full.min.js',
            'moment.min.js'   
        ];

        $this->css=[
            'select2.min.css',
            'select2-bootstrap.min.css',
            
        ];

        $num_beneficiarios = Contratos::select('beneficiarios','bookie')->where('id', $id)->first();

        return $this->view('consulta::popup-beneficiarios', [
            'layouts'       => 'base::layouts.popup-consulta',
            'contratos_id'  => $id,
            'beneficiarios' => $num_beneficiarios->beneficiarios,
            'id'       => $id,
            'adonde'   => $adonde, // es de adonde viene el formularo 
            'bookie'   =>  $num_beneficiarios->bookie // es de adonde viene el formularo 
        ]);
    } 

    public function parentescos(){
        return Parentesco::all();
    }
    public function beneficiario(Request $request){
        
        $Beneficiarios = Beneficiarios::select(
            'personas.nombres','personas.dni',
            'beneficiarios.parentesco_id',
            'personas_detalles.fecha_nacimiento as fecha_nacimiento',
            'personas_detalles.sexo'
        )
        ->leftjoin('personas','personas.id','=','beneficiarios.personas_id')
        ->leftjoin('personas_detalles','personas_detalles.personas_id','=','beneficiarios.personas_id')
        ->where('beneficiarios.contratos_id',$request->id)->get();
    
        $datos = [];

        foreach($Beneficiarios as $Beneficiario){

            $datos[] = [
                'nombres' => $Beneficiario->nombres,
                'dni'=>$Beneficiario->dni,
                'parentesco_id'=>$Beneficiario->parentesco_id,
                'fecha_nacimiento'=> Carbon::parse($Beneficiario->fecha_nacimiento)->format('d/m/Y'),
                'sexo'=>$Beneficiario->sexo
            ];

        }
    
        return ['datos' =>$datos];
    }

    public function asisplan(Request $request){
        //op == cambio = 0. reduccion = 1  o inclusion = 2 
        
         $op = $request->op;  
         $bookie = $request->bookie;  
         $cuentas =  "";
        
         if($bookie){
            $Contratos = Contratos::where('contratos.id', $request->id_contrato)->first();
            $planes = '';
            if($op == 2){
               
                $fecha = Carbon::createFromFormat('d/m/Y',$request->dato)->format("Y-m-d");
                
                $planes = Plan::where('desde','<=',  $fecha)
                    ->where('hasta','>=',  $fecha)
                    ->get();
    
                $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
                    ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
                    ->where('personas_bancos.personas_id', $Contratos->titular)
                    ->get();
            }
           
           
            $salida = [
                's'                 => 's', 
                'planes'            => $planes,
                'contrato'          => $Contratos,
                'plan_detalles'     => '',
                'cuentas'           => '',
                'frecuencia'        => '',
                'plan_old'          => '',
                'plan_detalles_new' =>  ''
            ];
         }else{
            $Contratos = Contratos::select([
                "contratos.frecuencia_pagos_id",
                "contratos.tipo_pago",
                "contratos.cobrando",
                "contratos.titular",
                "contratos.cobrando",
                "contratos.total_contrato",
                "contratos.cuotas",
                "contratos.total_pagado",
                "contratos.inicial",
                "contratos.anulado",
                "contratos.plan_detalles_id",
                "plan.id as plan_id"
            ])
            ->leftjoin('plan_detalles', 'plan_detalles.id',"=", "contratos.plan_detalles_id")
            ->leftjoin('plan', 'plan.id',"=", "plan_detalles.plan_id")
            ->where('contratos.id', $request->id_contrato)
            ->first();
    
          
            
            $Contratos->total_pagado2 = number_format($Contratos->total_pagado, 2, ',', '.');
            $Contratos->inicial2 = number_format($Contratos->inicial, 2, ',', '.');
            $Contratos->total_contrato2 = number_format($Contratos->total_contrato, 2, ',', '.');
    
            $plan_detalles = PlanDetalles::where('id', $Contratos->plan_detalles_id)->first();
    
            $plan_detalles->contado2 = number_format($plan_detalles->contado, 2, ',', '.');
            $plan_detalles->inicial2 = number_format($plan_detalles->inicial, 2, ',', '.');
            $plan_detalles->total2 = number_format($plan_detalles->total, 2, ',', '.');
    
            $frecuencia = FrecuenciaPagos::where('id', $Contratos->frecuencia_pagos_id)->first();
    
            $plan_old ='';
            $plan_detalles_new ="";
            $planes="Personalizado";
    
            if(!is_null($Contratos->plan_id)){
                $planes = Plan::where('id', $Contratos->plan_id)->first();
            }   
                
            if($op == 2){
                $plan_old  = $planes;
                $fecha = Carbon::createFromFormat('d/m/Y',$request->dato)->format("Y-m-d");
                
                $planes = Plan::where('desde','<=',  $fecha)
                    ->where('hasta','>=',  $fecha)
                    ->get();
    
                $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
                    ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
                    ->where('personas_bancos.personas_id', $Contratos->titular)
                    ->get();
            }
    
            if($op == 1){
                $plan_old  = $planes;
                $plan_detalles_new = PlanDetalles::where('plan_id', $plan_old->id)
                    ->where('beneficiarios', $request->beneficiarios)
                    ->first();
                $plan_detalles_new->contado2 = number_format($plan_detalles_new->contado, 2, ',', '.');
                $plan_detalles_new->inicial2 = number_format($plan_detalles_new->inicial, 2, ',', '.');
                $plan_detalles_new->total2 = number_format($plan_detalles_new->total, 2, ',', '.');
            }  
    
            $salida = [
                's'                 => 's', 
                'planes'            => $planes,
                'contrato'          => $Contratos,
                'plan_detalles'     => $plan_detalles,
                'cuentas'           => $cuentas,
                'frecuencia'        => $frecuencia,
                'plan_old'          => $plan_old,
                'plan_detalles_new' => $plan_detalles_new
            ];
         }
         
       
         
         return $salida;
    }

    public function beneficiariosguardar(Request $request){
        
        DB::beginTransaction();
        try{
            $datos = [];
            $contrato = Contratos::find($request->id);

            switch ($request->opera) {
                case 0:
                    //cambio
                    $operacion = 'Solicitud de Cambio de Beneficiario';
                    $planes =  null;


                    break;
                case 1:
                    //rediccion
                    $operacion = 'Solicitud de Reduccion de Beneficiarios';
                    $planes =  null;

                    break;
                case 2:
                    $operacion = 'Solicitud de  Inclusion de Beneficiarios';
                    $planes =  $request->planes_id;

                    
                    break; 
                default:
                    # code...
                    break;
            }

            /*
            Tipo solicitud 
                1 = contrato nuevo
                2 = renovacion contrato
                3 = agregar beneficiarios
                4 = modificacion de informacion personal
            Solicitante 
                id_persona quien solicita.
            aquien 
                1-contrato
                2-persona
            indice 
                persona_id o contrato_id
            */

            $solicitud = Solicitudes::create([
                "tipo_solicitud" => 3,
                "solicitante"    => auth()->user()->personas->id,
                "aquien"         => 1,
                "indece"         => $request->id
            ]);

            $solicitud_beneficiario = SolicitudBeneficiariosTemp::create([
                "planilla"      => $request->planilla,
                "contratos_id"  => $request->id,
                "solicitud_id"  => $solicitud['id'],
                "fecha"         => $request->inicio,
                "operacion"     => $request->opera,
                "planes_id"     => $planes
            ]);
            
            $dni = Personas::select('dni')->where('id', $contrato->titular)->first();

            $datos = [];
            $req = $request->all();
            foreach ($req['dni_beneficiario'] as $_id => $beneficiario) {
                
                if($req['dni_beneficiario'][$_id] == ""){
                    $_contrato = Contratos::select('personas.dni')->where('contratos.id', $request->id)
                    ->leftjoin('personas', 'personas.id', '=', 'contratos.titular')->first();
                    
                    $persona = Personas::where('dni', 'LIKE', $_contrato->dni.'%')->count();
                    $result = $persona +  1;
                    $dni = $_contrato->dni . "-" . $result;

                }else{

                    $dni = $req['dni_beneficiario'][$_id];
                }

                $datos= [
                    "dni"                                => $dni,
                    "nombre"                             => $req['nombres_beneficiario'][$_id],
                    "sexo"                               => $req['sexo_beneficiario'][$_id],
                    "solicitud_beneficiarios_temp_id"    => $solicitud_beneficiario->id,
                    "parentesco_id"                      => $req['parentescos_beneficioario'][$_id],
                    "fecha_nacimiento"                   => $req['nacimiento_beneficiario'][$_id]
                ];
                
                BeneficiariosTemp::create($datos);   
            }
                
            ContratosDetalles::create([
                "contratos_id" => $request->id,
                "personas_id"  => auth()->user()->personas_id,
                "operacion"    => $operacion,
                "planilla"     => $request->planilla
            ]); 


        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir'),
            "donde" => $request->adonde
        ];

    }
}