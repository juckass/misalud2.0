var aplicacion, $form, tabla;
$(function() {

    $("#subir").on('click', function(e) {
        e.preventDefault();
        $("#upload:hidden").trigger('click');
    });
});

$("#upload").on('change', function() {
    var l = Ladda.create($("#subir").get(0));
    l.start();

    var options2 = {
        url: $url + 'carga',
        type: 'POST',
        data: $("#carga").serialize(),
        success: function(r) {
            aviso(r);
        },
        complete: function(x, e, o) {
            ajaxComplete(x, e, o);
            l.stop();
        }
    };

    $('#carga').ajaxSubmit(options2);
});