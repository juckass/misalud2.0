<?php
 $menu['facturacion'] = [
	[
		'nombre' 	=> 'Facturacion',
		'direccion' => '#Facturacion',
		'icono' 	=> 'fa fa-clipboard',
		'menu' 		=> [
			[
				'nombre' 	=> 'Inicio',
				'direccion' => 'facturacion',
				'icono' 	=> 'fa fa-building'
			],
			[
				'nombre' 	=> 'Afacturar',
				'direccion' => 'facturacion/afacturar',
				'icono' 	=> 'fa fa-cogs'
			],
			[
				'nombre' 	=> 'Libro',
				'direccion' => 'facturacion/libro',
				'icono' 	=> 'fa fa-book'
			],
			[
				'nombre' 	=> 'Cierre',
				'direccion' => 'facturacion/cierre',
				'icono' 	=> 'fa fa-times-circle'
			]
		]
	]
]; 