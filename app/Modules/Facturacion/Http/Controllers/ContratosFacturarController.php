<?php

namespace App\Modules\Facturacion\Http\Controllers;

//Controlador Padre
use App\Modules\Facturacion\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Facturacion\Http\Requests\ContratosFacturarRequest;

//Modelos
use App\Modules\Facturacion\Models\ContratosFacturar;

class ContratosFacturarController extends Controller
{
    protected $titulo = 'Contratos Facturar';

    public $js = [
        'ContratosFacturar'
    ];
    
    public $css = [
        'ContratosFacturar'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('facturacion::ContratosFacturar', [
            'ContratosFacturar' => new ContratosFacturar()
        ]);
    }

    public function nuevo()
    {
        $ContratosFacturar = new ContratosFacturar();
        return $this->view('facturacion::ContratosFacturar', [
            'layouts' => 'base::layouts.popup',
            'ContratosFacturar' => $ContratosFacturar
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $ContratosFacturar = ContratosFacturar::find($id);
        return $this->view('facturacion::ContratosFacturar', [
            'layouts' => 'base::layouts.popup',
            'ContratosFacturar' => $ContratosFacturar
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $ContratosFacturar = ContratosFacturar::withTrashed()->find($id);
        } else {
            $ContratosFacturar = ContratosFacturar::find($id);
        }

        if ($ContratosFacturar) {
            return array_merge($ContratosFacturar->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ContratosFacturarRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $ContratosFacturar = $id == 0 ? new ContratosFacturar() : ContratosFacturar::find($id);

            $ContratosFacturar->fill($request->all());
            $ContratosFacturar->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $ContratosFacturar->id,
            'texto' => $ContratosFacturar->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            ContratosFacturar::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            ContratosFacturar::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            ContratosFacturar::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = ContratosFacturar::select([
            'id', 'fecha_facturar', 'contratos_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}