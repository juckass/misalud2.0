<?php namespace App\Modules\Facturacion\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'base';
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'App/Modules/Facturacion/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'App/Modules/Facturacion/Assets/css',
	];
}