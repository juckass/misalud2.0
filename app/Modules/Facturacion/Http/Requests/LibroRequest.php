<?php

namespace Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;
 
class LibroRequest extends Request {
	protected $reglasArr = [
		'fecha' => ['required', 'date_format:"d/m/Y"'], 
		'desde' => ['required', 'integer'], 
		'hasta' => ['required', 'integer'], 
		'total' => ['required'], 
		'sucursal_id' => ['required', 'integer'], 
		'controlfacturacion_id' => ['required', 'integer']
	];
}