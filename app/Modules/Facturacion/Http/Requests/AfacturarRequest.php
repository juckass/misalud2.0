<?php

namespace Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;
 
class AfacturarRequest extends Request {
	protected $reglasArr = [
		'ci' => ['required', 'integer'], 
		'fecha' => ['required', 'date_format:"d/m/Y"'], 
		'total' => ['required'], 
		'correlativo' => ['integer'], 
		'sucursal_id' => ['required', 'integer'], 
		'controlfacturacion_id' => ['required', 'integer']
	];
}