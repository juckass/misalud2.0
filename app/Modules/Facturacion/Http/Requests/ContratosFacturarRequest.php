<?php

namespace App\Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;

class ContratosFacturarRequest extends Request {
    protected $reglasArr = [
		'fecha_facturar' => ['required', 'date_format:"d/m/Y"'], 
		'contratos_id' => ['required', 'integer']
	];
}