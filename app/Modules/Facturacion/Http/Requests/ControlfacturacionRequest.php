<?php

namespace Modules\Facturacion\Http\Requests;

use App\Http\Requests\Request;
 
class ControlfacturacionRequest extends Request {
	protected $reglasArr = [	
		'mes' => ['required', 'integer', 'unique:controlfacturacion,mes'], 
		'ano' => ['required', 'integer', 'unique:controlfacturacion,ano'], 
	];
}