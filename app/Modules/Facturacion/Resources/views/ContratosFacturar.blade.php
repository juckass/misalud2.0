@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos Facturar']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar ContratosFacturar.',
        'columnas' => [
            'Fecha Facturar' => '50',
		'Contratos' => '50'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $ContratosFacturar->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection