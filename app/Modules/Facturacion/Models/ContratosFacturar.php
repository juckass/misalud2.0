<?php

namespace App\Modules\Facturacion\Models;

use App\Modules\Base\Models\Modelo;

use Illuminate\Database\Eloquent\Model;

class ContratosFacturar extends Model
{
    protected $table = 'contratos_facturar';
    protected $fillable = ["fecha_facturar","contratos_id"];
    protected $campos = [
    'fecha_facturar' => [
        'type' => 'date',
        'label' => 'Fecha Facturar',
        'placeholder' => 'Fecha Facturar del Contratos Facturar'
    ],
    'contratos_id' => [
        'type' => 'number',
        'label' => 'Contratos',
        'placeholder' => 'Contratos del Contratos Facturar'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}