<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Afacturar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('afacturar', function (Blueprint $table) {

			$table->increments('id');
			$table->string('ci');
			$table->date('fecha');
			$table->decimal('total', 10, 2);
		
			$table->integer('correlativo')->unsigned()->nullable();

			$table->integer('sucursal_id')->unsigned();
			$table->integer('controlfacturacion_id')->unsigned();
			
			$table->foreign('controlfacturacion_id')
				->references('id')->on('controlfacturacion')
				->onDelete('cascade')->onUpdate('cascade');

			$table->foreign('sucursal_id')
				->references('id')->on('sucursal')
				->onDelete('cascade')->onUpdate('cascade');	

			$table->timestamps();
	
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afacturar');
    }
}
