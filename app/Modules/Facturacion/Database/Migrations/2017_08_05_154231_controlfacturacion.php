<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Controlfacturacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controlfacturacion', function (Blueprint $table) {
			$table->increments('id');
			$table->date('fecha_inicio');
			$table->date('fecha_final')->nullable();
		
			$table->integer('total_registros')->unsigned()->nullable();
			$table->decimal('total_bolivares', 10, 2)->unsigned()->nullable();
		
			$table->decimal('total_facturado', 10, 2)->unsigned()->nullable();
			$table->integer('mes')->unsigned();
			$table->integer('ano')->unsigned();
			$table->integer('estatus')->unsigned();
            
			$table->unique(['mes','ano'],'controlfacturacion_index_unique');
			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controlfacturacion');
    }
}
