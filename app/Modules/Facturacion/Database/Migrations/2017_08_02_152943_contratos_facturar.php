<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContratosFacturar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('contratos_facturar', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_facturar');
            $table->integer('contratos_id')->unsigned();

            $table->timestamps();

            $table->foreign('contratos_id')
                ->references('id')->on('contratos')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('contratos_facturar');
    }
}
