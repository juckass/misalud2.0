<?php

namespace App\Modules\Cobranza\Clases;

use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Exception;

//use App\Modules\Cobranza\Clases\Drivers\Banesco;
use App\Modules\Cobranza\Clases\Drivers\Bod;
use App\Modules\Cobranza\Clases\Drivers\Caroni;
use App\Modules\Cobranza\Clases\Drivers\DelSur;
use App\Modules\Cobranza\Clases\Drivers\Mercantil;
use App\Modules\Cobranza\Clases\Drivers\Provincial;
use App\Modules\Cobranza\Clases\Drivers\Venezuela;

class Txt 
{
    public $driver;
    public $drivers = [
        'Bod',
        'Caroni',
        'DelSur',
        'Mercantil',
        'Provincial',
        'Venezuela',
    ];

    public $archivo = '';

    public function __construct($driver, $archivo = '')
    {
        $_driver = $driver;
        $driver = strtolower($driver);
        $driver = ucwords($driver);
        $driver = str_replace(' ', '', $driver);
        $_driver = $driver;
        $driver = 'App\\Modules\\Cobranza\\Clases\\Drivers\\' . $driver;

        if (!in_array($_driver, $this->drivers)) {
            throw new Exception('No existe el driver ' . $_driver . '.');
        }
        
        if ($archivo == '') {
            $archivo = date('Y') . '/' . date('m') . '/' . date('YmdHis') . '_' . $_driver . '.txt';
        }
        
        $this->archivo = $archivo;

        $this->driver = new $driver($archivo);
    }

    public function cabecera($linea)
    {
        $this->driver->cabecera($linea);
    }

    public function detalle($linea)
    {
        $this->driver->detalle($linea);
    }

    public function pies($linea)
    {
        $this->driver->pies($linea);
    }

    public function leer($archivo = '')
    {
        if ($archivo == '') {
            $archivo = $this->archivo;
        }
        return $this->driver->leer($archivo);
    }

    public function formato_fecha($linea)
    {
        $this->driver->pies($linea);
    }
}