<?php

namespace App\Modules\Cobranza\Clases\Drivers;

class DelSur extends Generico
{
    protected $cabecera = [];
    /*
    protected $cabecera = [
        'codigo_empresa' => [
            'nombre'      => 'Código de Empresa',
            'formato'     => 'n',
            'long'        => 4,
            'valor'       => 695,
            'observacion' => ''
        ],
        'codigo_convenio' => [
            'campo'       => 'codigo_convenio',
            'nombre'      => 'Código del Convenio',
            'formato'     => 'n',
            'long'        => 8,
        ],
        'identificador' => [
            'campo'       => 'identificador',
            'nombre'      => 'Identifiacador, Contrato, Cuenta o Factura',
            'formato'     => 'n',
            'long'        => 11,
            'observacion' => ''
        ],
        'codigo_cedula_identidad' => [
            'campo'       => 'codigo_cedula_identidad',
            'nombre'      => 'Cédula de Identidad',
            'formato'     => 'n',
            'long'        => 10,
            'observacion' => ''
        ],
        'tipo_instrumento_afiliar' => [
            'campo'       => 'tipo_instrumento_afiliar',
            'nombre'      => 'Tipo de Instrumento a Afiliar',
            'formato'     => 'n',
            'long'        => 1,
            'opciones'    => [
                1 => 'Cuenta',// Cuenta
                2 => 'Tarjeta_de_credito'// TDC
            ],
            'observacion' => ''
        ],
        'codigo_cuenta_cliente' => [
            'campo'       => 'codigo_cuenta_cliente',
            'nombre'      => 'Código de Cuenta Cliente',
            'formato'     => 'n',
            'long'        => 20,
            'observacion' => ''
        ],
        'fecha_afiliacion' => [
            'campo'       => 'fecha_afiliacion',
            'nombre'      => 'Fecha de Afiliación',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'tipo_transacción' => [
            'campo'       => 'tipo_transacción',
            'nombre'      => 'Tipo de Transacción',
            'formato'     => 'n',
            'long'        => 1,
            'valor'       => 1,
            'opciones'    => [
                0 => 'Desafiliación',// Desafiliación
                1 => 'Afiliación'// Afiliación
            ],
            'observacion' => ''
        ],
    ];
    */

    protected $detalle = [
        'codigo_ordenante' => [
            'nombre'      => 'Código de Empresa',
            'formato'     => 'a',
            'long'        => 4,
            'valor'       => '0067',
            'observacion' => ''
        ],
        'clave_ordenante' => [
            'nombre'      => 'Clave Ordenante',
            'formato'     => 'a',
            'long'        => 15,
            'valor'       => '000010016982085',
            'observacion' => ''
        ],
        'tipo_operacion' => [
            'nombre'      => 'Tipo Operación',
            'formato'     => 'a',
            'long'        => 3,
            'valor'       => '001',
            'observacion' => ''
        ],
        'sub_tipo_operacion' => [
            'nombre'      => 'Sub Tipo Operación',
            'formato'     => 'n',
            'long'        => 3,
            'valor'       => '020',
            'observacion' => ''
        ],
        'numero_cuenta' => [
            'campo'       => 'cuenta',
            'nombre'      => 'Número de Cuenta',
            'formato'     => 'a',
            'long'        => 20,
            'observacion' => ''
        ],
        'monto' => [
            'campo'       => 'monto',
            'nombre'      => 'Monto',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
        'tipo_pagador' => [
            'nombre'      => 'Tipo de pagador',
            'formato'     => 'a',
            'long'        => 1,
            'valor'       => 'N',
            'observacion' => ''
        ],
        'numero_cedula' => [
            'campo'       => 'dni',
            'nombre'      => 'Cédula de Identidad',
            'formato'     => 'a',
            'long'        => 12,
            'observacion' => ''
        ],
        'nombre_beneficiario' => [
            'campo'       => 'persona_nombre',
            'nombre'      => 'Nombre del Beneficiario',
            'formato'     => 'a',
            'long'        => 30,
        ],
        'referencia_contrato' => [
            'campo'       => 'cobros_id',
            'nombre'      => 'Referencia Contrato',
            'formato'     => 'a',
            'long'        => 30,
        ],
        'numero_factura' => [
            'campo'       => 'correlativo',
            'nombre'      => 'Numero de Factura a cancelar',
            'formato'     => 'n',
            'long'        => 30,
        ],
        'fecha' => [
            'campo'       => 'fecha',
            'nombre'      => 'fecha de la factura',
            'formato'     => 'd',
            'long'        => 8,
            'formato_fecha' => 'dmY'
        ],
        'fecha_vencimiento' => [
            'campo'       => 'fecha',
            'nombre'      => 'fecha en que vence la factura',
            'formato'     => 'd',
            'long'        => 8,
            'formato_fecha' => 'dmY'
        ]
        /*
        codigo_ordenante: 0067
        clave_ordenante: 000010016982085
        tipo_operacion: 020
        sub_tipo_operacion: 001
        numero_cuenta: 01570015763715030825
        monto: 000000000004800
        tipo_pagador: N
        numero_cedula: V00002644032
        nombre_beneficiario: AMARISTA  ESTILITA            
        referencia_contrato: 00-00015461                   
        numero_factura: 000000000000000000010016982085
        fecha: 28042017
        fecha_vencimiento: 28042017
        */
    ];

    public function cabecera($linea)
    {
        return true;
    }

    public function procesar_campo($campo, $valor)
    {
        if ($campo == 'numero_cedula') {
            $cedula = 'V' . str_pad($valor, 11, '0', STR_PAD_LEFT);
            return $cedula;
        }
        
        if ($campo == 'nombre_beneficiario') {
            $nombre = str_pad($valor, 30, ' ', STR_PAD_RIGHT);
            return $nombre;
        }

        if ($campo == 'monto') {
            $monto = number_format($valor, 2, '', '');
            $monto = str_pad($monto, 15, '0', STR_PAD_LEFT);
            return $monto;
        }

        if ($campo == 'referencia_contrato') {
            $valor = '00-' .  str_pad($valor, 8, '0', STR_PAD_LEFT);
            $valor = str_pad($valor, 30, ' ', STR_PAD_RIGHT);
            
            return $valor;
        }

        if ($campo == 'numero_instrumento_cliente') {
            return '0' . $valor;
        }

        if ($campo == 'fecha_proceso') {
            return '00000000';
        }

        return $valor;
    }

    public function leerCabecera($linea)
    {
        return [];
    }

    public function leerDetalle($linea)
    {
        $data = parent::leerDetalle($linea);

        return $data;
    }
}