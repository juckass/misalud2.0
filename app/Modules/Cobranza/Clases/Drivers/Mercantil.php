<?php
namespace App\Modules\Cobranza\Clases\Drivers;
use Carbon\Carbon;

class Mercantil extends Generico
{
    protected $formato_fecha = 'Ymd';
    protected $lote_id;
    protected $cabecera = [
        'tipo_registro' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 1,
            'valor'       => '1',
            'observacion' => ''
        ],
        'identificacion_banco' => [
            'nombre'      => 'identificacion del banco',
            'formato'     => 'a',
            'long'        => 12,
            'valor'       => 'BAMRVECA',
            'relleno'     => 'd',
            'observacion' => ''
        ],
        'tipo_instrumento' => [
            'nombre'      => 'Tipo Instrumento',
            'formato'     => 'a',
            'long'        => 1,
            'valor'       => 'C',
            'observacion' => ''
        ],
        'tipo_gestion' => [
            'nombre'      => 'Tipo Gestion',
            'formato'     => 'n',
            'long'        => 1,
            'valor'       => 1,
            'opciones'    => [
                1 => 1, // Cobranza
                2 => 2, // Reintegro
                3 => 3, // Anulacion
            ],
            'observacion' => ''
        ],
        'numero_lote' => [
            'campo'       => 'lote_id',
            'nombre'      => 'Numero de Lote',
            'formato'     => 'a',
            'long'        => 15,
            'valor'       => '',
            'observacion' => ''
        ],
        'area_reservada' => [
            'nombre'      => 'Area Reservada',
            'formato'     => 'n',
            'long'        => 6,
            'valor'       => 0,
            'observacion' => ''
        ],
        'identificacion_producto' => [
            'nombre'      => 'Identificacion del Producto',
            'formato'     => 'a',
            'long'        => 5,
            'valor'       => 'DOMIC',
            'observacion' => ''
        ],
        'numero_empresa' => [
            'campo'       => 'empresa_rif',
            'nombre'      => 'Numero Empresa',
            'formato'     => 'a',
            'long'        => 11,
            'observacion' => ''
        ],
        'cantidad_total_registros' => [
            'campo'       => 'cantidad_total_registros',
            'nombre'      => 'Cantidad Total de Registros Detalle',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'monto_total_debito' => [
            'campo'       => 'monto_total_debito',
            'nombre'      => 'Monto Total de Registros Detalle',
            'formato'     => 'n',
            'long'        => 17,
            'observacion' => ''
        ],
        'fecha_valor' => [
            'campo'       => 'fecha_valor',
            'nombre'      => 'Fecha Valor',
            'formato'     => 'd',
            'long'        => 8,
            'observacion' => ''
        ],
        'numero_instrumento_empresa' => [
            'campo'       => 'empresa_cuenta',
            'nombre'      => 'Numero Instrumento Empresa',
            'formato'     => 'n',
            'long'        => 20,
            'observacion' => ''
        ],
        'area_reservada1' => [
            'campo'       => 'area_reservada1',
            'nombre'      => 'Area Reservada1',
            'formato'     => 'n',
            'long'        => 7,
            'valor'       => 0,
            'observacion' => ''
        ],
        'numero_serial_nota_empresa' => [
            'campo'       => 'numero_serial_nota_empresa',
            'nombre'      => 'Numero Serial Nota Empresa',
            'formato'     => 'n',
            'long'        => 8,
            'valor'       => 0,
            'observacion' => ''
        ],
        'codigo_respuesta' => [
            'campo'       => 'codigo_respuesta',
            'nombre'      => 'Codigo Respuesta Lote',
            'formato'     => 'n',
            'long'        => 4,
            'observacion' => ''
        ],
        'cantidad_total_registros_efectivos' => [
            'campo'       => 'cantidad_total_registros_efectivos',
            'nombre'      => 'Cantidad Total de Registros Efectivos',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'monto_total_registros_efectivos' => [
            'campo'       => 'monto_total_registros_efectivos',
            'nombre'      => 'Monto Total Registros Efectivos',
            'formato'     => 'n',
            'long'        => 17,
            'observacion' => ''
        ],
        'fecha_proceso' => [
            'campo'       => 'fecha_proceso',
            'nombre'      => 'Fecha Proceso',
            'formato'     => 'd',
            'long'        => 8,
            'observacion' => ''
        ],
        'area_reservada2' => [
            'campo'       => 'area_reservada2',
            'nombre'      => 'Area Reservada2',
            'formato'     => 'n',
            'long'        => 17,
            'valor'       => '',
            'observacion' => ''
        ],
        'area_reservada3' => [
            'campo'       => 'area_reservada3',
            'nombre'      => 'Area Reservada3',
            'formato'     => 'n',
            'long'        => 17,
            'valor'       => '',
            'observacion' => ''
        ],
        'area_reservada4' => [
            'campo'       => 'area_reservada4',
            'nombre'      => 'Area Reservada4',
            'formato'     => 'n',
            'long'        => 17,
            'valor'       => '',
            'observacion' => ''
        ],
        'area_libre' => [
            'campo'       => 'area_libre',
            'nombre'      => 'Area Libre',
            'formato'     => 'a',
            'long'        => 48,
            'valor'       => '',
            'observacion' => ''
        ],
    ];

    protected $detalle = [
        'tipo_registro_detalle' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 1,
            'valor'       => 2,
            'observacion' => ''
        ],
        'tipo_cliente' => [
            'campo'       => 'nacionalidad',
            'nombre'      => 'Tipo de registro',
            'formato'     => 'a',
            'long'        => 1,
            'opciones'    => [
                'V' => 'V', //Venezolano
                'E' => 'E', //Extranjero
                'J' => 'J', //Juridico
                'P' => 'P', //Pasaporte
                'G' => 'G', //Gobierno
                'R' => 'R', //Firma Personal
            ],
            'observacion' => ''
        ],
        'numero_cliente' => [
            'campo'       => 'dni',
            'nombre'      => 'Numero del Cliente',
            'formato'     => 'n',
            'long'        => 10,
            'observacion' => ''
        ],
        'numero_instrumento_cliente' => [
            'campo'       => 'cuenta',
            'nombre'      => 'Numero del instrumento',
            'formato'     => 'n',
            'long'        => 21
        ],
        'area_reservada' => [
            'campo'       => 'area_reservada',
            'nombre'      => 'Area Reservada',
            'formato'     => 'a',
            'long'        => 6,
            'valor'       => '',
            'observacion' => ''
        ],
        'area_reservada2' => [
            'campo'       => 'area_reservada2',
            'nombre'      => 'Area Reservada2',
            'formato'     => 'a',
            'long'        => 4,
            'valor'       => '',
            'observacion' => ''
        ],
        'identificacion_cliente_empresa' => [
            'campo'       => 'dni',
            'nombre'      => 'Identificación Cliente Empresa',
            'formato'     => 'n',
            'long'        => 16,
            'observacion' => ''
        ],
        'area_reservada3' => [
            'campo'       => 'area_reservada3',
            'nombre'      => 'Area Reservada3',
            'formato'     => 'a',
            'long'        => 9,
            'valor'       => '',
            'observacion' => ''
        ],
        'monto_operacion' => [
            'campo'       => 'monto',
            'nombre'      => 'Monto Operación',
            'formato'     => 'n',
            'long'        => 17,
            'observacion' => ''
        ],
        'area_reservada4' => [
            'campo'       => 'area_reservada4',
            'nombre'      => 'Area Reservada4',
            'formato'     => 'a',
            'long'        => 30,
            'valor'       => '',
            'observacion' => ''
        ],
        'identificacion_servicio' => [
            'campo'       => 'dni',
            'nombre'      => 'Identificación Servicio',
            'formato'     => 'a',
            'relleno'     => 'd',
            //'valor'       => 'Contrato',
            'long'        => 16,
            'observacion' => ''
        ],
        'area_reservada5' => [
            'campo'       => 'area_reservada5',
            'nombre'      => 'Area Reservada5',
            'formato'     => 'a',
            'long'        => 1,
            'valor'       => '',
            'observacion' => ''
        ],
        'numero_documento' => [
            'campo'       => 'correlativo',
            'nombre'      => 'Numero Documento',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
        'indicador_financiero' => [
            'campo'       => 'indicador_financiero',
            'nombre'      => 'Indicador Financiero',
            'formato'     => 'n',
            'long'        => 1,
            'valor'       => 0,
            'observacion' => ''
        ],
        'fecha_documento' => [
            'campo'       => 'fecha',
            'nombre'      => 'Fecha Documento',
            'formato'     => 'd',
            'long'        => 8,
            'observacion' => ''
        ],
        'codigo_respuesta' => [
            'campo'       => 'codigo_respuesta',
            'nombre'      => 'Codigo Respuesta',
            'formato'     => 'n',
            'long'        => 4,
            'observacion' => ''
        ],
        'mensaje_respuesta' => [
            'campo'       => 'mensaje_respuesta',
            'nombre'      => 'Mensaje Respuesta',
            'formato'     => 'a',
            'long'        => 30,
            'observacion' => ''
        ],
        'area_reservada6' => [
            'campo'       => 'area_reservada6',
            'nombre'      => 'Area Reservada6',
            'formato'     => 'n',
            'long'        => 7,
            'valor'       => '',
            'observacion' => ''
        ],
        'numero_serial_nota_cliente' => [
            'campo'       => 'correlativo',
            'nombre'      => 'Numero Serial Nota Cliente',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'fecha_periodo_desde' => [
            'campo'       => 'fecha_periodo_desde',
            'nombre'      => 'Fecha Periodo Desde',
            'formato'     => 'd',
            'long'        => 8,
            'observacion' => ''
        ],
        'fecha_periodo_hasta' => [
            'campo'       => 'fecha_periodo_hasta',
            'nombre'      => 'Fecha Periodo Hasta',
            'formato'     => 'd',
            'long'        => 8,
            'observacion' => ''
        ],
        'area_libre' => [
            'campo'       => 'area_libre',
            'nombre'      => 'Area Libre',
            'formato'     => 'a',
            'long'        => 35,
            'valor'       => 'M',
            'relleno'     => 'd',
            'observacion' => ''
        ],
    ];

    public function procesar_campo($campo, $valor) {
        if ($campo == 'numero_empresa') {
            $rif = substr($valor, 0, 1) . '0' . str_replace('-', '', substr($valor, 2));
            return $rif;
        }

        if ($campo == 'numero_instrumento_cliente') {
            return '0' . $valor;
        }

        if ($campo == 'fecha_proceso') {
            return '00000000';
        }
       
        return $valor;
    }

    public function leerCabecera($linea)
    {
        $data = parent::leerCabecera($linea);
       
        /* $_data = [
            'lote_id'                  => $lote->id,
            'empresa_rif'              => $empresa->rif,
            'empresa_nombre'           => $empresa->nombre,
            'empresa_cuenta'           => $numero_cuenta_empresa,
            'cantidad_total_registros' => $lote->total_registros,
            'monto_total_debito'       => $lote->total_cobrar,
            'fecha_valor'              => Carbon::now(),
            'fecha_proceso'            => Carbon::now(),
        ]; */
        
        $this->lote_id = $data['lote_id'];
        
        $_data = [
            'lote_id'                  => $data['lote_id'],// $lote->id,
            'empresa_rif'              => $data['empresa_rif'], // $empresa->rif,
            'empresa_nombre'           =>$data['empresa_nombre'],// $empresa->nombre,
            'empresa_cuenta'           =>$data['empresa_cuenta'],// $numero_cuenta_empresa,
            'cantidad_total_registros' =>$data['cantidad_total_registros_efectivos'],// $lote->total_registros,
            'monto_total_debito'       =>$data['monto_total_registros_efectivos'],// $lote->total_cobrar,
            'fecha_valor'              =>Carbon::now(),
            'fecha_proceso'            =>Carbon::now(),
        ];

        return $_data;
    }

    public function leerDetalle($linea)
    {
        $data = parent::leerDetalle($linea);
    
       /*  $_data = [
            'lote_id' =>  $this->lote_id //$data['numero_lote']
        ]; */
         
        $data['correlativo'] = intval(substr($linea, 132, 15));

        $data['lote_id'] = $this->lote_id;
        if($data['codigo_respuesta'] != '0074'){
            $data['anular'] =  true;
        }
        
        $valor = intval($data['monto']);
        $data['monto'] = substr($valor, 0 ,-2).'.'.substr($valor, -2);
        
        //return $_data;
        return $data;
    }
}
