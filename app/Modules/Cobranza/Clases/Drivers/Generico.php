<?php

namespace App\Modules\Cobranza\Clases\Drivers;

use Carbon\Carbon;

use Illuminate\Support\Facades\Storage;

abstract class Generico
{
    protected $relleno = STR_PAD_LEFT;
    protected $formato_fecha = 'Ymd';
    protected $errores = [];

    /*
    [
        'campo'       => 'Nombre del campo de la base de datos',
        'nombre'      => 'Nombre descripcion del campo txt',
        'formato'     => 'formato, puede ser a = alfanumerico, ó n = numerico',
        'long'        => 'Longitud del campo, debe ser entero',
        'opciones'    => 'lista de posibles valores, debe ser un array asociativo',
        'requerido'   => 'define si el campo es requerido',
        'valor'       => 'Valor por defecto del campo',
        'relleno'     => 'direccion del relleno, por defecto es "i" de izquierda, puede ser "d" de derecha o "a" de ambos',
        'observacion' => 'Observaciones que se quieran agregar al campo'
    ]
    */

    protected $cabecera = [];
    protected $detalle = [];
    protected $pies = [];
    protected $archivo;

    public function __construct($archivo)
    {
        $this->archivo = $archivo;
        $this->disk = 'lotes';
    }

    public function cabecera($linea)
    {
        $this->linea_data($this->cabecera, $linea);
    }

    public function detalle($linea)
    {
        $this->linea_data($this->detalle, $linea);
    }

    public function pies($linea)
    {
        $this->linea_data($this->pies, $linea);
    }

    public function linea_data($estructura, $linea)
    {
        if (empty($estructura)) {
            return '';
        }

        $data = [];
        foreach ($estructura as $index => $campo) {
            if (isset($campo['campo']) && isset($linea[$campo['campo']])) {
                $campo['valor'] = $linea[$campo['campo']];
            }

            $data[] = $this->formato($index, $campo);
        }

        $linea = implode('', $data);
       // dd($linea);
        $this->escribir($linea);
    }

    public function escribir($linea)
    {
        Storage::disk($this->disk)->append($this->archivo, $linea);
    }

    public function formato($index, $campo)
    {
        $salida = '';
        $valor_relleno = ' ';
        
        if ($campo['formato'] == 'n') {
            $valor_relleno = '0';
        } elseif ($campo['formato'] == 'a') {
            $valor_relleno = ' ';
        } elseif ($campo['formato'] == 'd') {
            $valor_relleno = '0';
            
            if (!isset($campo['valor'])) {
                $campo['valor'] = Carbon::now();
            }

            $formato = $this->formato_fecha;
            if (isset($campo['formato_fecha'])) {
                $formato = $campo['formato_fecha'];
            }

            $campo['valor'] = $campo['valor']->format($formato);
        } else {
            $this->errores[] = 'El campo "' . $campo['nombre'] . '" posee un formato no aceptado: ' . $campo['formato'];
            return '';
        }
        

        if (isset($campo['valor_relleno'])) {
            $valor_relleno = $campo['valor_relleno'];
        }

        if (!isset($campo['valor'])) {
            if ($campo['formato'] == 'n') {
                $campo['valor'] = 0;
            } elseif ($campo['formato'] == 'a') {
                $campo['valor'] = '';
            } elseif ($campo['formato'] == 'd') {
                $formato = $this->formato_fecha;
                if (isset($detalle['formato_fecha'])) {
                    $formato = $detalle['formato_fecha'];
                }
                $campo['valor'] = Carbon::now()->format($formato);
            }
            
            //$this->errores[] = 'El campo "' . $campo['nombre'] . '" no posee valor';
            //return '';
        }

        $campo['valor'] = $this->procesar_campo($index, $campo['valor']);

        if (!isset($campo['long'])) {
            $this->errores[] = 'El campo "' . $campo['nombre'] . '" no posee longitud';
            return '';
        }

        if (isset($campo['opciones']) && !isset($campo['opciones'][$campo['valor']])) {
            $this->errores[] = 'El campo "' . $campo['nombre'] . '" tiene un valor no permitido; 
            valor: ' . $campo['valor'] . ' 
            lista de valores: [' . implode(', ', array_keys($campo['opciones'])) . ']';
            return '';
        }
        
        if ($index == '') {
            dd(
                $salida, 
                $campo['valor'], 
                $campo['long'], 
                $valor_relleno,
                str_pad($campo['valor'], $campo['long'], $valor_relleno, STR_PAD_LEFT)
            );
        }

        if ($campo['formato'] == 'n') {
            //$campo['valor'] = $campo['valor'] + '';
            $campo['valor'] = str_replace(['.', ','], '', $campo['valor']);
        }
        
        $relleno = $this->relleno;
        if (isset($campo['relleno'])) {
            if ($campo['relleno'] == 'd') {
                $relleno = STR_PAD_RIGHT;
            } elseif ($campo['relleno'] == 'a') {
                $relleno = STR_PAD_BOTH;
            }
        }
        
        $salida = str_pad($campo['valor'], $campo['long'], $valor_relleno, $relleno);

        return $salida;
    }

    public function procesar_campo($campo, $valor)
    {
        return $valor;
    }

    public function leer($archivo)
    {
        /* $storage = Storage::disk('respuestas');
        if ($archivo == '') {
            $archivo = $this->archivo;
        }

        $exists = $storage->exists($archivo);
        if (!$exists) {
            return false;
        }
        */
        $cabecera = [];
        $detalle  = [];

        $handle = fopen(public_path('respuestas/' . $archivo), "r");
        if ($handle) {
            $i = 0;
            
            while (($line = fgets($handle)) !== false) {
                list($tipo, $data) = $this->leerLinea($i++, $line);
                if ($tipo == 0) {
                    $cabecera = $data;
                } else {
                    $detalle[] = $data;
                }
            }

            fclose($handle);
        } else {
            // error opening the file.
        } 

        //$contenido = $storage->get($archivo);
        //var_dump($contenido);
        //dd($cabecera, $detalle);
        return [$cabecera, $detalle];
    
    }

    public function leerLinea($i, $linea)
    {
        if ($i == 0) {
            return [0, $this->leerCabecera($linea)];
        }

        return [1, $this->leerDetalle($linea)];
    }

    public function leerCabecera($linea)
    {
        $apuntador = 0;
        $data = [];
        foreach ($this->cabecera as $nombreData => $cabecera) {
            $campo = $nombreData;
            if (isset($cabecera['campo'])) {
                $campo = $cabecera['campo'];
            }
            
            $data[$campo] = substr($linea, $apuntador, $cabecera['long']);
            $data[$campo] = trim($data[$campo]);
            if ($cabecera['formato'] == 'n') {
                $data[$campo] = $data[$campo];
            } elseif ($cabecera['formato'] == 'a') {
                //$data[$campo] = trim($data[$campo]);
            } elseif ($cabecera['formato'] == 'd') {
                $formato = $this->formato_fecha;
                if (isset($detalle['formato_fecha'])) {
                    $formato = $detalle['formato_fecha'];
                }
                $data[$campo] = Carbon::createFromFormat($formato, $data[$campo]);
            }
            
            $apuntador += $cabecera['long'];
        }

        $campos_obligatorios = [
            'lote_id',
            'empresa_rif',
            'empresa_nombre',
            'empresa_cuenta',
            'cantidad_total_registros',
            'monto_total_debito',
            'codigo_respuesta',
            'mensaje_respuesta',
            'fecha_valor',
            'fecha_proceso',
        ];

        foreach ($campos_obligatorios as $campo) {
            if (!isset($data[$campo])) {
                $data[$campo] = '';
            }
        }
       
        $data = $this->codigo_respuesta_cabecera($data);

        return $data;
    }

    public function leerDetalle($linea)
    {
        $apuntador = 0;
        $data = [];
        foreach ($this->detalle as $nombreData => $detalle) {
            $campo = $nombreData;
            
            if (isset($detalle['campo'])) {
                $campo = $detalle['campo'];
            }

            $data[$campo] = substr($linea, $apuntador, $detalle['long']);
            $data[$campo] = trim($data[$campo]);

            if ($detalle['formato'] == 'n') {
                $data[$campo] = $data[$campo];
            } elseif ($detalle['formato'] == 'a') {
                //$data[$campo] = trim($data[$campo]);
            } elseif ($detalle['formato'] == 'd') {
                $formato = $this->formato_fecha;
                if (isset($detalle['formato_fecha'])) {
                    $formato = $detalle['formato_fecha'];
                }
                $data[$campo] = Carbon::createFromFormat($this->formato_fecha, $data[$campo]);
            }
            
            $apuntador += $detalle['long'];
        }

        
        $campos_obligatorios = [
            'lote_id',
            'cobros_id',
            'persona_id',
            'persona_nombre',
            'nacionalidad',
            'dni',
            'cuenta',
            'monto',
            'correlativo',
            'codigo_respuesta',
            'mensaje_respuesta',
            'fecha',
        ];

        foreach ($campos_obligatorios as $campo) {
            if (!isset($data[$campo])) {
                $data[$campo] = '';
            }
        }
        
        $data = $this->codigo_respuesta_detalle($data);
        
        return $data;
    }

    public function codigo_respuesta_cabecera($data)
    {
        if ($data['codigo_respuesta'] == '') {
            $data['codigo_respuesta'] = null;
        }

        return $data;
    }

    public function codigo_respuesta_detalle($data)
    {
        if ($data['codigo_respuesta'] == '') {
            $data['codigo_respuesta'] = null;
        }

        /*     $data['anular'] = false;
            if ($data['codigo_respuesta'] == '12') {
                $data['anular'] = true;
            }
        */
        return $data;
    }
}
