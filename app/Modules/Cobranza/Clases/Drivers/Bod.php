<?php

namespace App\Modules\Cobranza\Clases\Drivers;

class Bod extends Generico
{
    protected $formato_fecha = 'dmY';
    protected $cabecera = [
        'tipo_registro' => [
            'nombre'      => 'tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => 1,
            'observacion' => ''
        ],
        'fecha_envio' => [
            'campo'       => 'fecha_proceso',
            'nombre'      => 'fecha de envio',
            'formato'     => 'd',
            'long'        => 8,
            'observacion' => ''
        ],
        'cantidad_registros' => [
            'campo'       => 'cantidad_total_registros',
            'nombre'      => 'cantidad de registros',
            'formato'     => 'n',
            'long'        => 5,
            'observacion' => ''
        ],
        'monto_total' => [
            'campo'       => 'monto_total_debito',
            'nombre'      => 'monto total de registros',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
        'disponible' => [
            'nombre'      => 'disponible',
            'formato'     => 'n',
            'long'        => 1,
            'observacion' => ''
        ],
        'numero_cuenta_empresa' => [
            'campo'       => 'empresa_cuenta',
            'nombre'      => 'Numero cuenta Empresa',
            'formato'     => 'n',
            'long'        => 12,
            'observacion' => ''
        ],
        'tipo_credito' => [
            'nombre'      => 'tipo de credito',
            'formato'     => 'a',
            'long'        => 12,
            'valor'       => 'CR',
            'observacion' => ''
        ],
        'disponible2' => [
            'nombre'      => 'disponible2',
            'formato'     => 'n',
            'long'        => 16,
            'valor'       => 0,
            'observacion' => ''
        ],
    ];

    protected $detalle = [
        'tipo_registro' => [
            'nombre'      => 'tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => 2,
            'observacion' => ''
        ],
        'tipo_debito' => [
            'nombre'      => 'tipo de debito',
            'formato'     => 'a',
            'long'        => 2,
            'valor'       => 'DB',
            'observacion' => ''
        ],
        'disponible' => [
            'nombre'      => 'disponible',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => 0,
            'observacion' => ''
        ],
        'codigo_banco' => [
            'nombre'      => 'codigo de banco',
            'formato'     => 'n',
            'long'        => 3,
            'valor'       => 116,
            'observacion' => ''
        ],
        'numero_cuenta' => [
            'campo'       => 'cuenta',
            'nombre'      => 'Numero cuenta',
            'formato'     => 'n',
            'long'        => 16,
            'observacion' => ''
        ],
        'numero_cuenta' => [
            'campo'       => 'cuenta',
            'nombre'      => 'Numero cuenta',
            'formato'     => 'n',
            'long'        => 16,
            'observacion' => ''
        ],
        'numero_documento' => [
            'campo'       => 'cobros_id',
            'nombre'      => 'Numero Documento',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
        'monto_operacion' => [
            'campo'       => 'monto',
            'nombre'      => 'Monto Operación',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
        'fecha_cobro' => [
            'campo'       => 'fecha',
            'nombre'      => 'Fecha cobro',
            'formato'     => 'd',
            'formato_fecha' => 'dmy',
            'long'        => 6,
            'observacion' => ''
        ],
    ];
}