<?php

namespace App\Modules\Cobranza\Clases\Drivers;

class Caroni extends Generico
{
    protected $cabecera = [
        'tipo' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => '01',
            'observacion' => ''
        ],
        'empresa' => [
            'campo'       => 'empresa_nombre',
            'nombre'      => 'Empresa de la Empresa',
            'formato'     => 'a',
            'long'        => 40,
            'observacion' => ''
        ],
        'cuenta_matriz' => [
            'campo'       => 'empresa_cuenta',
            'nombre'      => 'Tipo Instrumento',
            'formato'     => 'n',
            'long'        => 20,
            'observacion' => ''
        ],
        'nro_registros' => [
            'campo'       => 'cantidad_total_registros',
            'nombre'      => 'Nro de Registros',
            'formato'     => 'n',
            'long'        => 5,
            'observacion' => ''
        ],
        'monto_total' => [
            'campo'       => 'monto_total_debito',
            'nombre'      => 'Monto Total',
            'formato'     => 'n',
            'long'        => 13,
            'observacion' => ''
        ],
        'filler' => [
            'campo'       => 'filler',
            'nombre'      => 'Filler',
            'formato'     => 'a',
            'valor'        => 'a',
            'long'        => 20,
            'observacion' => ''
        ],
    ];

    protected $detalle = [
        'tipo' => [
            'campo'       => 'tipo',
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => '02',
            'observacion' => ''
        ],
        'cuenta' => [
            'nombre'      => 'Cuenta',
            'formato'     => 'n',
            'long'        => 20,
            'observacion' => ''
        ],
        'monto' => [
            'campo'       => 'monto',
            'nombre'      => 'Monto',
            'formato'     => 'n',
            'long'        => 12,
            'observacion' => ''
        ],
        'nombre_titular' => [
            'campo'       => 'persona_nombre',
            'nombre'      => 'Nombre del Titular',
            'formato'     => 'a',
            'long'        => 40
        ],
        'cedula' => [
            'campo'       => 'dni',
            'nombre'      => 'Cedula',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'filler' => [
            'campo'       => 'filler',
            'nombre'      => 'Filler',
            'formato'     => 'a',
            'valor'       => '',
            'long'        => 18,
            'observacion' => ''
        ],
    ];
}