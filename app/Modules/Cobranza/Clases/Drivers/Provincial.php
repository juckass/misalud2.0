<?php

namespace App\Modules\Cobranza\Clases\Drivers;

class Provincial extends Generico
{
    protected $cabecera = [
        'tipo_registro' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'a',
            'long'        => 2,
            'valor'       => '01',
            'observacion' => ''
        ],
        'tipo_operacion' => [
            'nombre'      => 'Tipo Operación',
            'formato'     => 'a',
            'long'        => 2,
            'valor'       => 2,
            'opciones'    => [
                1 => 1, // Nomina
                2 => 2, // Cobranza
                3 => 3, // Pago a Proveedores
                4 => 4, // Transferencias
            ]
        ],
        'codigo_banco' => [
            'nombre'      => 'Código Banco',
            'formato'     => 'a',
            'long'        => 4,
            'valor'       => '108-',
            'observacion' => ''
        ],
        'codigo_oficina' => [
            'campo'       => 'codigo_oficina',
            'nombre'      => 'Código de oficina',
            'formato'     => 'a',
            'long'        => 4,
            'observacion' => ''
        ],
        'digitos_cuenta' => [
            'campo'       => 'digitos_cuenta',
            'nombre'      => 'Digitos Cuenta',
            'formato'     => 'a',
            'long'        => 2,
            'valor'       => 10,
            'observacion' => ''
        ],
        'numero_cuenta' => [
            'campo'       => 'empresa_cuenta',
            'nombre'      => 'Numero Cuenta',
            'formato'     => 'a',
            'long'        => 10,
            'observacion' => ''
        ],
        'total_registros' => [
            'campo'       => 'cantidad_total_registros',
            'nombre'      => 'Total Registros',
            'formato'     => 'n',
            'long'        => 7,
            'observacion' => ''
        ],
        'total_importe' => [
            'campo'       => 'monto_total_debito',
            'nombre'      => 'Total Importe',
            'formato'     => 'n',
            'long'        => 17,
            'observacion' => ''
        ],
        'codigo_moneda' => [
            'campo'       => 'codigo_moneda',
            'nombre'      => 'Código Moneda',
            'formato'     => 'a',
            'long'        => 3,
            'valor'       => 'VEB',
            'observacion' => ''
        ],
        'fecha_sorpote' => [
            'campo'       => 'fecha_valor',
            'nombre'      => 'Fecha Soporte',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'rif' => [
            'campo'       => 'empresa_rif',
            'nombre'      => 'Rif',
            'formato'     => 'a',
            'long'        => 10,
            'observacion' => ''
        ],
        'referencia_debito' => [
            'campo'       => 'referencia_debito',
            'nombre'      => 'Referencia',
            'formato'     => 'a',
            'long'        => 8,
            'observacion' => ''
        ],
        'nombre_titular' => [
            'campo'       => 'empresa_nombre',
            'nombre'      => 'Nombre del Titular',
            'formato'     => 'a',
            'long'        => 27,
            'observacion' => ''
        ],
        'asoc-comercial' => [
            'campo'       => 'asoc-comercial',
            'nombre'      => 'Asociación comercial',
            'formato'     => 'a',
            'long'        => 15,
            'observacion' => ''
        ],
        'disponible' => [
            'campo'       => 'disponible',
            'nombre'      => 'Disponible',
            'formato'     => 'a',
            'long'        => 31,
            'observacion' => ''
        ],
    ];

    protected $detalle = [
        'tipo_registro' => [
            'campo'       => 'tipo_registro',
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => '02',
            'observacion' => ''
        ],
        'codigo_banco' => [
            'nombre'      => 'codigo_banco',
            'formato'     => 'a',
            'long'        => 4,
            'observacion' => ''
        ],
        'codigo_oficina' => [
            'campo'       => 'codigo_oficina',
            'nombre'      => 'Código de Oficina',
            'formato'     => 'n',
            'long'        => 4,
            'observacion' => ''
        ],
        'digitos_cuenta' => [
            'campo'       => 'digitos_cuenta',
            'nombre'      => 'Digitos Cuenta',
            'formato'     => 'a',
            'long'        => 2,
            'observacion' => ''
        ],
        'cedula' => [
            'campo'       => 'cedula',
            'nombre'      => 'Cedula',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'numero_cuenta' => [
            'campo'       => 'numero_cuenta',
            'nombre'      => 'Numero de Cuenta',
            'formato'     => 'a',
            'long'        => 10,
            'observacion' => ''
        ],
        'referencia' => [
            'campo'       => 'referencia',
            'nombre'      => 'Referencia',
            'formato'     => 'a',
            'long'        => 16,
            'observacion' => ''
        ],
        'monto_importe' => [
            'campo'       => 'monto_importe',
            'nombre'      => 'Monto Importe',
            'formato'     => 'n',
            'long'        => 17,
            'observacion' => ''
        ],
        'nombre_titular' => [
            'campo'       => 'nombre_titular',
            'nombre'      => 'Nombre del Titular',
            'formato'     => 'a',
            'long'        => 40,
            'observacion' => ''
        ],
        'tipo-movimiento' => [
            'campo'       => 'tipo-movimiento',
            'nombre'      => 'Tipo de Movimiento',
            'formato'     => 'a',
            'long'        => 1,
            'opciones'    => [
                1 => "A", // Afiliacion
                2 => "M", // modificacion
                3 => "B", // Baja
            ]
        ],
        'nro-identificacion' => [
            'campo'       => 'nro-identificacion',
            'nombre'      => 'Identifiación',
            'formato'     => 'a',
            'long'        => 11
        ],
        'otros-datos' => [
            'campo'       => 'otros-datos',
            'nombre'      => 'Otros Datos',
            'formato'     => 'a',
            'long'        => 18
        ],
        'resultado-oper' => [
            'campo'       => 'resultado-oper',
            'nombre'      => 'Resultado de la Operación',
            'formato'     => 'n',
            'long'        => 2
        ],
        'referencia_credito' => [
            'campo'       => 'referencia_credito',
            'nombre'      => 'Referencia',
            'formato'     => 'a',
            'long'        => 8,
        ],
        'disponible' => [
            'campo'       => 'disponible',
            'nombre'      => 'Disponible',
            'formato'     => 'a',
            'long'        => 15,
            'valor'       => ''
        ]
    ];
}