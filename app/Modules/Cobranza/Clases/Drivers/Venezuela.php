<?php

namespace App\Modules\Cobranza\Clases\Drivers;

class Venezuela extends Generico
{
    protected $cabecera = [
        'tipo_registro' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => '01',
            'observacion' => ''
        ],
        'rif' => [
            'campo'       => 'rif',
            'nombre'      => 'Rif',
            'formato'     => 'a',
            'long'        => 9,
            'opciones'    => ''
        ],
        'codigo_verificador_rif' => [
            'campo'       => 'codigo_verificador_rif',
            'nombre'      => 'Código Verificador Rif',
            'formato'     => 'n',
            'long'        => 1,
            'observacion' => ''
        ],
        'cantidad_transacciones' => [
            'campo'       => 'cantidad_transacciones',
            'nombre'      => 'Cantidad de Transacciones',
            'formato'     => 'n',
            'long'        => 10,
            'observacion' => ''
        ],
        'monto_total' => [
            'campo'       => 'monto_total',
            'nombre'      => 'Monto Total',
            'formato'     => 'n',
            'long'        => 13,
            'observacion' => ''
        ],
        'fecha_envio' => [
            'campo'       => 'fecha_envio',
            'nombre'      => 'Fecha de Envio',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'nro_lote' => [
            'campo'       => 'nro_lote',
            'nombre'      => 'Numero de Lote',
            'formato'     => 'n',
            'long'        => 10,
            'observacion' => ''
        ],
        'referencia_cuentas' => [
            'campo'       => 'referencia_cuentas',
            'nombre'      => 'Referencia Credito Cuentas',
            'formato'     => 'a',
            'long'        => 20,
            'observacion' => ''
        ],
        'referencia_tdc_bdv' => [
            'campo'       => 'referencia_tdc_bdv',
            'nombre'      => 'Referencia del Recaudador',
            'formato'     => 'a',
            'long'        => 20,
            'observacion' => ''
        ],
        'referencia_tdc_otros_bancos' => [
            'campo'       => 'referencia_tdc_otros_bancos',
            'nombre'      => 'Referencia TDC Otros Bancos',
            'formato'     => 'a',
            'long'        => 20,
            'observacion' => ''
        ],
        'fecha_valor_lote' => [
            'campo'       => 'fecha_valor_lote',
            'nombre'      => 'Fecha del Valor del Lote',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'fecha_culminacion' => [
            'campo'       => 'fecha_culminacion',
            'nombre'      => 'Fecha de Culminación',
            'formato'     => 'a',
            'long'        => 8,
            'observacion' => ''
        ],
    ];
    
        protected $detalle = [
        'tipo_registro' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => '02',
            'observacion' => ''
        ],
        'ci_rif' => [
            'nombre'      => 'ci_rif',
            'formato'     => 'n',
            'long'        => 9,
            'observacion' => ''
        ],
        'digito_verificador_rif' => [
            'nombre'      => 'ci_rif',
            'formato'     => 'n',
            'long'        => 1,
            'observacion' => ''
        ],
        'codigo_cliente' => [
            'campo'       => 'codigo_cliente',
            'nombre'      => 'Código Cliente',
            'formato'     => 'a',
            'long'        => 20,
            'observacion' => ''
        ],

        //Opciones de cuenta para el debito opcion 1
        'opcion 1' => [
            'tipo_pago' => [
                'nombre'      => 'Tipo Pago',
                'formato'     => 'a',
                'long'        => 1,
                'valor'       => 'C',
                'observacion' => [
                    1 => "T", // TDC
                    2 => "D", // TDD
                    3 => "C", // Cuenta
                ],
            ],
            'tipo_cuenta' => [
                'nombre'      => 'Tipo de Cuenta',
                'formato'     => 'n',
                'long'        => 2,
                'observacion' => [
                    1 => 00, // Cuenta corriente
                    2 => 01, // Cuenta de ahorro
                    3 => 02, // TDC
                ]
            ],
            'nro_cuenta' => [
                'campo'       => 'nro_cuenta',
                'nombre'      => 'Cuenta Cliente',
                'formato'     => 'n',
                'long'        => 20,
                'observacion' => ''
            ],
            'nro_tarjeta_TDC' => [
                'campo'       => 'nro_tarjeta_TDC',
                'nombre'      => 'Tarjeta del Cliente',
                'formato'     => 'n',
                'long'        => 20,
                'observacion' => ''
            ],
            'cvv2' => [
                'campo'       => 'cvv2',
                'nombre'      => 'CVV2',
                'formato'     => 'n',
                'long'        => 3,
                'observacion' => ''
            ],
            'fecha_expiracion' => [
                'campo'       => 'fecha_expiracion',
                'nombre'      => 'Fecha de Expiración',
                'formato'     => 'a',
                'long'        => 4,
                'observacion' => ''
            ],
            'monto_cobrado' => [
                'campo'       => 'monto_cobrado',
                'nombre'      => 'Monto Cobrado',
                'formato'     => 'n',
                'long'        => 13,
                'observacion' => ''
            ],        

        ], //end opciones

        //Opciones de cuenta para el debito opcion 2
        'opcion 2' => [
            'tipo_pago' => [
                'nombre'      => 'Tipo Pago',
                'formato'     => 'a',
                'long'        => 1,
                'observacion' => [
                    1 => "T", // TDC
                    2 => "D", // TDD
                    3 => "C", // Cuenta
                ]
            ],
            'tipo_cuenta' => [
                'nombre'      => 'Tipo de Cuenta',
                'formato'     => 'n',
                'long'        => 2,
                'observacion' => [
                    1 => 00, // Cuenta corriente
                    2 => 01, // Cuenta de ahorro
                    3 => 02, // TDC
                ]
            ],
            'nro_cuenta' => [
                'campo'       => 'nro_cuenta',
                'nombre'      => 'Cuenta Cliente',
                'formato'     => 'n',
                'long'        => 20,
                'observacion' => ''
            ],
            'nro_tarjeta_TDC_TDD' => [
                'campo'       => 'nro_tarjeta_TDC_TDD',
                'nombre'      => 'Tarjeta del Cliente',
                'formato'     => 'n',
                'long'        => 20,
                'observacion' => ''
            ],
            'cvv2' => [
                'campo'       => 'cvv2',
                'nombre'      => 'CVV2',
                'formato'     => 'n',
                'long'        => 3,
                'observacion' => ''
            ],
            'fecha_expiracion' => [
                'campo'       => 'fecha_expiracion',
                'nombre'      => 'Fecha de Expiración',
                'formato'     => 'a',
                'long'        => 4,
                'observacion' => ''
            ],
            'monto_cobrado' => [
                'campo'       => 'monto_cobrado',
                'nombre'      => 'Monto Cobrado',
                'formato'     => 'n',
                'long'        => 13,
                'observacion' => ''
            ],

        ], //end opciones

        'fecha_valor_debito' => [
            'campo'       => 'fecha_valor_debito',
            'nombre'      => 'Fecha Valor Debito',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'monto_deuda_original' => [
            'campo'       => 'monto_deuda_original',
            'nombre'      => 'Monto Deuda Original',
            'formato'     => 'n',
            'long'        => 13,
            'observacion' => ''
        ],
        'referencia_debito' => [
            'campo'       => 'referencia_debito',
            'nombre'      => 'Referencia del Debito',
            'formato'     => 'n',
            'long'        => 20,
            'observacion' => ''
        ],
        'email' => [
            'campo'       => 'email',
            'nombre'      => 'Email',
            'formato'     => 'a',
            'long'        => 50,
            'observacion' => ''
        ],
        'status' => [
            'campo'       => 'status',
            'nombre'      => 'Status',
            'formato'     => 'a',
            'long'        => 5,
            'observacion' => ''
        ],
        'descripcion_status' => [
            'campo'       => 'descripcion_status',
            'nombre'      => 'Descripción Status',
            'formato'     => 'a',
            'long'        => 120,
            'observacion' => ''
        ],
        'fecha_efectivo_cobro' => [
            'campo'       => 'fecha_efectivo_cobro',
            'nombre'      => 'Fecha Efectivo Cobro',
            'formato'     => 'n',
            'long'        => 8,
            'observacion' => ''
        ],
        'monto_total_cobrado' => [
            'campo'       => 'monto_total_cobrado',
            'nombre'      => 'Monto Total Cobrado',
            'formato'     => 'n',
            'long'        => 13,
            'observacion' => ''
        ],
        'cantidad_intentos' => [
            'campo'       => 'cantidad_intentos',
            'nombre'      => 'Cantidad de Intentos',
            'formato'     => 'n',
            'long'        => 2,
            'observacion' => ''
        ],
        'nuevo_nro_tdc' => [
            'campo'       => 'nuevo_nro_tdc',
            'nombre'      => 'Nuevo Nro. TDC',
            'formato'     => 'n',
            'long'        => 20,
            'observacion' => ''
        ],
        'nuevo_cvv2' => [
            'campo'       => 'nuevo_cvv2',
            'nombre'      => 'Nuevo CVV2',
            'formato'     => 'n',
            'long'        => 3,
            'observacion' => ''
        ],
        'fecha_vcmto_nueva_tdc' => [
            'campo'       => 'fecha_vcmto_nueva_tdc',
            'nombre'      => 'Fecha de vencimeinto de la nueva TDC',
            'formato'     => 'n',
            'long'        => 4,
            'observacion' => ''
        ],
    ];
}