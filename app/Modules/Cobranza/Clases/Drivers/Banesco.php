<?php

namespace App\Modules\Cobranza\Clases\Drivers;

class Banesco extends Generico
{
    protected $relleno = STR_PAD_RIGHT;
    public $cabeza = '';

    protected $cabecera = [
        'tipo_registro_c' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'a',
            'long'        => 3,
            'valor'       => 'HDR',
            'observacion' => ''
        ],
        'asociado_comercial' => [
            'nombre'      => 'Asociado Comercial',
            'formato'     => 'a',
            'long'        => 15,
            'valor'       => 'BANESCO',
            'relleno'     => 'd',
            'observacion' => ''
        ],
        'estandar' => [
            'nombre'      => 'Estándar EDIFACT',
            'formato'     => 'a',
            'long'        => 1,
            'valor'       => 'E',
            'observacion' => ''
        ],
        'version_estandar' => [
            'nombre'      => 'Versión del Estándar EDIFACT',
            'formato'     => 'a',
            'long'        => 6,
            'valor'       => 'D  96A',
            'observacion' => ''
        ],
        'tipo_documento' => [
            'nombre'      => 'Tipo documento',
            'formato'     => 'a',
            'long'        => 6,
            'valor'       => 'DIRDEB',
            'observacion' => ''
        ],
        'produccion' => [
            'nombre'      => 'Producción',
            'formato'     => 'a',
            'long'        => 1,
            'valor'       => 'P',
            'observacion' => ''
        ],
        'tipo_registro' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'a',
            'long'        => 2,
            'valor'       => '01',
            'observacion' => ''
        ],
        'tipo_transaccion' => [
            'nombre'      => 'Tipo de Transacción',
            'formato'     => 'a',
            'long'        => 35,
            'valor'       => 'SUB',
            'relleno'     => 'd',
            'observacion' => ''
        ],
        'condicion_orden_pago' => [
            'nombre'      => 'Condición de la Orden de Pago',
            'formato'     => 'a',
            'long'        => 3,
            'valor'       => '9',
            'relleno'     => 'd',
            'observacion' => ''
        ],
        'numero_lote' => [
            'campo'       => 'lote_id',
            'nombre'      => 'Número de la Orden de Pago',
            'formato'     => 'a',
            'long'        => 35,
            'relleno'     => 'd',
            'observacion' => ''
        ],
        'fecha' => [
            'campo'       => 'fecha',
            'nombre'      => 'Fecha creación Orden de Pago',
            'formato'     => 'd',
            'long'        => 14,
            'observacion' => ''
        ],
        'tipo_registro_cliente' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'a',
            'long'        => 2,
            'valor'       => '02',
            'observacion' => ''
        ],
        'nro_referencia_credito' => [
            'nombre'      => 'Nro. De Referencia del Crédito',
            'formato'     => 'a',
            'long'        => 30,
            'valor'       => '00015586                      ',
            'observacion' => ''
        ],
        'rif_ordenanteo' => [
            'campo'       => 'empresa_rif',
            'nombre'      => 'R.I.F. o C.I. del Ordenanteo',
            'formato'     => 'a',
            'long'        => 17,
            'valor'       => 'J306873660       ',
            'observacion' => ''
        ],
        'nombre-del-ordenante' => [
            'campo'       => 'empresa_nombre',
            'nombre'      => 'Nombre del Ordenante',
            'formato'     => 'a',
            'long'        => 35,
            //'valor'       => 'Corporacion Salud Vital c a        ',
            'observacion' => ''
        ],
        'monto-total-a-abonar' => [
            'campo'       => 'monto_total_debito',
            'nombre'      => 'Monto Total a Abonar',
            'formato'     => 'a',
            'long'        => 15,
            'observacion' => ''
        ],
        'moneda' => [
            'nombre'      => 'Moneda',
            'formato'     => 'a',
            'long'        => 3,
            'valor'       => 'VEF',
            'observacion' => ''
        ],
        'instruccion-del-ordenante' => [
            'nombre'      => 'Instrucción del Ordenante',
            'formato'     => 'a',
            'long'        => 1,
            'valor'       => ' ',
            'observacion' => ''
        ],
        'numero-de-cuenta-a-acreditar' => [
            'nombre'      => 'Número de Cuenta a acreditar',
            'formato'     => 'a',
            'long'        => 35,
            'valor'       => '01340186101863018125               ',
            'observacion' => ''
        ],
        'codigo-del-banco-ordenando' => [
            'nombre'      => 'Código del Banco Ordenando',
            'formato'     => 'a',
            'long'        => 11,
            'valor'       => 'BANESCO    ',
            'observacion' => ''
        ],
        'fecha-valor-de-la-orden' => [
            'nombre'      => 'Fecha valor de la orden',
            'formato'     => 'd',
            'long'        => 8,
            'observacion' => ''
        ],
        'forma-de-pago' => [
            'nombre'      => 'Forma de pago',
            'formato'     => 'a',
            'long'        => 3,
            'valor'       => 'CB ',
            'observacion' => ''
        ],
    ];

    protected $detalle = [
        'tipo_registro' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => '03',
            'observacion' => ''
        ],
        'numero-de-recibo-ref-del-debito' => [
            'nombre'      => 'Número de Recibo (Ref. del débito)',
            'formato'     => 'n',
            'long'        => 30,
            'observacion' => ''
        ],
        'monto_operacion' => [
            'campo'       => 'monto',
            'nombre'      => 'Monto Operación',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
        'moneda' => [
            'nombre'      => 'Moneda',
            'formato'     => 'a',
            'long'        => 3,
            'valor'       => 'VEF',
            'observacion' => ''
        ],
        'numero-de-cta-cliente' => [
            'campo'       => 'cuenta',
            'nombre'      => 'Número de Cta. Cliente',
            'formato'     => 'a',
            'long'        => 30
        ],
        'banco-codigo-swift' => [
            'campo'       => 'dni',
            'nombre'      => 'Banco (Código Swift)',
            'formato'     => 'a',
            'long'        => 11,
            'observacion' => ''
        ],
        'rif-o-ci-del-cliente' => [
            'campo'       => 'dni',
            'nombre'      => 'R.I.F. o C.I. del Cliente',
            'formato'     => 'a',
            'long'        => 17,
            'observacion' => ''
        ],
        'nombre_beneficiario' => [
            'campo'       => 'persona_nombre',
            'nombre'      => 'Nombre del Beneficiario',
            'formato'     => 'a',
            'long'        => 35,
        ],
        'area_reservada' => [
            'campo'       => 'area_reservada',
            'nombre'      => 'Area Reservada',
            'formato'     => 'a',
            'long'        => 3,
            'valor'       => '',
            'observacion' => ''
        ],
        'numero-de-contrato' => [
            'campo'       => 'contrato_id',
            'nombre'      => 'Número de Contrato',
            'formato'     => 'a',
            'long'        => 35,
            'observacion' => ''
        ],
        'fecha-de-vencimiento' => [
            'nombre'      => 'Fecha de Vencimiento',
            'formato'     => 'a',
            'long'        => 6,
            'valor'       => '      ',
            'observacion' => 'sólo aplica para tarjetas de crédito'
        ],
    ];

    protected $pies = [
        'tipo_registro' => [
            'nombre'      => 'Tipo de registro',
            'formato'     => 'n',
            'long'        => 2,
            'valor'       => '04',
            'observacion' => ''
        ],
        'total-de-creditos' => [
            'campo'       => 'monto_total_credito',
            'nombre'      => 'Total de Créditos',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
        'total-de-debitos' => [
            'campo'       => 'monto_total_debito',
            'nombre'      => 'Total de Débitos',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
        'monto-total-a-cobrar' => [
            'campo'       => 'total',
            'nombre'      => 'Monto Total a Cobrar',
            'formato'     => 'n',
            'long'        => 15,
            'observacion' => ''
        ],
    ];

    public function cabecera($linea)
    {
        $this->linea_data($this->cabecera, $linea);
    }

    public function leerLinea($i, $linea)
    {
        if ($i == 0) {
            $this->cabeza = $this->leerCabecera($line);
            return [0, $this->cabeza];
        } elseif ($i == 1) {
            $this->cabeza = array_merge($this->cabeza, $this->leerCabecera($line));
            return [0, $this->cabeza];
        }

        return [1, $this->leerDetalle($line)];
    }

    public function pies($linea)
    {
        $this->linea_data($this->pies, $linea);
    }

    public function leerDetalle($linea)
    {
        $data = parent::leerDetalle($linea);
        
        if ($data['tipo_registro'] == '04') {
            return [];
        }
        
        return $data;
    }
}