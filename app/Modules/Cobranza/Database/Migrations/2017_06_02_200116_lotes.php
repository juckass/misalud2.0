<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lotes extends Migration
{
    /**
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->date('fecha')->nullable();
            $table->integer('banco_empresa_id')->unsigned()->nullable();
            $table->integer('total_registros')->unsigned()->nullable();
            $table->boolean('recobro')->default(false);
            $table->decimal('total_cobrar', 15, 2)->nullable(); 
            $table->string('archivo', 100)->nullable(); 

            $table->boolean('cargado')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotes');
    }
}
