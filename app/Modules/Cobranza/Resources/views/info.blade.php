@extends('base::layouts.default')
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="dashboard-stat  green-jungle ">
                <div class="visual">
                    <i class="fa fa-check"></i>
                </div>
                <div class="details">
                    <div class="number">{{$cobrados_conut}}</div>
                    <div class="desc"> Cobrados </div>
                </div>
                <a class="more" href="javascript:;" data-tipo="1"> Total Registros cobrados
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dashboard-stat  yellow ">
                <div class="visual">
                    <i class="fa fa-calendar-times-o"></i>
                </div>
                <div class="details">
                    <div class="number">{{$cobradosno_conut}}</div>
                    <div class="desc"> No cobrados </div>
                </div>
                <a class="more" href="javascript:;" data-tipo="2"> Total Registros no cobrados
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dashboard-stat  red-thunderbird ">
                <div class="visual">
                    <i class="fa fa-times-circle-o"></i>
                </div>
                <div class="details">
                    <div class="number">{{$no_encontrados_count}}</div>
                    <div class="desc"> No encontrados </div>
                </div>
                <a class="more" href="javascript:;" data-tipo="3"> Total registros no encontrados
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="tabbable-line bg-white boxless tabbable-reversed col-md-12">
        <ul class="nav nav-tabs" style="margin-top: 10px;">
            <li class="active">
                <a href="#tab_0" data-toggle="tab">
                     <i class="fa fa-check"></i> Registros cobrados
                </a>
            </li>
            <li>
                <a href="#tab_1" data-toggle="tab">
                   <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Registros no cobrados
                </a>
            </li>
            <li>
                <a href="#tab_2" data-toggle="tab">
                  <i class="fa fa-times-circle" aria-hidden="true"></i> registros no encontrados
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                <div class="row">
                    <div class="col-md-12" style="overflow: auto;max-height: 400px;">
                         <table class="table table-hover table-striped" whidth="100%" >
                            <thead>
                                <th>ID CONTRATO</th>
                                <th>MONTO</th>
                                
                            </thead>
                            <tbody>
                                @foreach ($cobrados as $key => $value) 
                                <tr>
                                    <td>{{$value['contrato_id']}}</td>
                                    <td>{{$value['monto']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab_1">
                 <div class="row">
                    <div class="col-md-12" style="overflow: auto;max-height: 400px;">
                         <table class="table table-hover table-striped" whidth="100%" >
                            <thead>
                                <th>ID CONTRATO</th>
                                <th>MONTO</th>
                                <th>Respuesta</th>
                            </thead>
                            <tbody>
                           

                                @foreach ($no_cobrados as $key => $value) 
                                <tr>
                                    <td>{{$value['contrato_id']}}</td>
                                    <td>{{$value['monto']}}</td>
                                    <td>{{$value['msj']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab_2">
                 <div class="row">
                    <div class="col-md-12" style="overflow: auto;max-height: 400px;">
                        <table class="table table-hover table-striped" whidth="100%" >
                            <thead>
                                <th>Cedula</th>
                                <th>Numero de Cuenta</th>
                                <th>Correlativo</th>
                                <th>Monto</th>
                                <th>Respuesta</th>
                                
                            </thead>
                            <tbody>
                                @foreach ($no_encontrados as $key => $value) 
                                <tr>
                                    <td>{{$value['dni']}}</td>
                                    <td>{{$value['cuenta']}}</td>
                                    <td>{{$value['correlativo']}}</td>
                                    <td>{{$value['monto']}}</td>
                                    <td>{{$value['mensaje_respuesta']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
@endpush