@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos detalles']])
    
@endsection
@section('content')

<div class="row">
    

    <center>
        <table id="tabla4" class="table table-striped table-hover table-bordered tables-text">
            <thead>
                <tr>
                    <th style=" text-align: center;">Planilla</th>
                    <th style=" text-align: center;">C.I Titular</th>              
                    <th style=" text-align: center;">Titular</th>
                    <th style=" text-align: center;">Fecha Inicio</th>
                    <th style=" text-align: center;">Fecha vencimiento</th>
                    <th style=" text-align: center;">Sucursal</th>
                    <th style=" text-align: center;">Codigo Vendedor</th>

                </tr>
            </thead>
        </table>
    </center>        
    
</div>
	
@endsection
@push('js')
<script type="text/javascript">
    var datatableEspanol = {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
   $('#tabla4').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: dire + "/cobranza/analista/datainformacion",
            data: function (d) {
                d.id = "{{$tipo}}";
            }
        },
        language: datatableEspanol,
        columns: [
            { data: 'planilla', name: 'contratos.planilla' },
            { data: 'dni', name: 'personas.dni' },
            { data: 'nombres', name: 'personas.nombres' },
            { data: 'inicio', name: 'contratos.inicio' },
            { data: 'vencimiento', name: 'contratos.vencimiento' },
            { data: 'nombre', name: 'sucursal.nombre' },
            { data: 'codigo', name:'vendedores.codigo' }
        ]
    });

</script>
@endpush