@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Cobros']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Cobros.',
        'columnas' => [
            'Contratos' => '8.3333333333333',
		'Sucursal' => '8.3333333333333',
		'Personas' => '8.3333333333333',
		'Personas Bancos' => '8.3333333333333',
		'Bancos' => '8.3333333333333',
		'Total Cobrar' => '8.3333333333333',
		'Concepto' => '8.3333333333333',
		'Fecha Pagado' => '8.3333333333333',
		'Total Pagado' => '8.3333333333333',
		'Num Recibo' => '8.3333333333333',
		'Tipo Pago' => '8.3333333333333',
		'Completo' => '8.3333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Cobros->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection