@extends('base::layouts.default')
@section('content')
	@include('base::partials.ubicacion', ['ubicacion' => ['Exportar Lotes']])
	
	<div class="row">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"></h3>
            </div>
            <div class="panel-body">
                {{ Form::bsSelect('banco',$controller->bancos(), '', [
                    'label' => 'Bancos',
                    'required' => 'required'
                ]) }}

                <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <label for="min">Fecha del lote</label>
                    <input id="fecha" name="inicio" class="form-control" type="text" placeholder=""  required="date" />
                </div>

        
                {{ Form::bsSelect('tipo', [
                    0 => 'Normal',
                    1 => 'Giros',
                ], 0,
                [
                    'label' => 'Tipo de cobros',
                    'required' => 'required'
                ]) }}

                <center>
                    <div class="">
                        <br/>
                        <button id="generar" type="button" class="btn btn-info col-md-12 mt-ladda-btn ladda-button" data-style="expand-right">
                            <span class="ladda-label">
                                 Generar Lote
                            </span>
                        </button>
                    </div>
                </center>
            
            </div>
        </div>
	</div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Informacion de Lote</h3>
                </div>
                <div class="panel-body">
                    ID: <span id="lote-id"></span><hr>
                    Fecha de Lote : <span id="lote-fecha"></span><hr>
                  
                    N° de Registros : <span id="lote-registros"></span><hr>
                    Total Bolivares : <span id="lote-bolivares"></span><hr>
                    Archivo : <a href="" id="lote-url" download =""><span id="lote-archivo"></span></a>
                </div>
            </div>
        
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row"  style="display: none;"  id="cargar">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'url' => route('cobranza.export.carga'), 'method' => 'POST', 'files' => true]) !!}
                        <input id="lote2-id" name="lote_id" type="hidden" value="" />
                        <input id="upload" name="archivo"  class="pull-left btn-file" type="file" /> 
                        <br>
                        <br>
                        <center>
                            <button type="submit" class="btn blue">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                <span class="visible-lg-inline visible-md-inline">Cargar</span>
                            </button>
                        </center>
                    {!! Form::close() !!}
                </div>
            </div>
        
        </div>
        <div class="col-md-4"></div>
    </div>

@endsection
@push('js')
<script>
    var url = "{{url('public/lotes/')}}";
</script>
@endpush