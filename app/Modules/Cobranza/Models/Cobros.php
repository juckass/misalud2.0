<?php

namespace App\Modules\Cobranza\Models;

use Illuminate\Database\Eloquent\Model;

class Cobros extends Model
{
    protected $table = 'cobros';
    protected $fillable = [
        'contratos_id',
        'sucursal_id',
        'personas_id',
        'personas_bancos_id',
        'bancos_id',
        'total_cobrar',
        'concepto',
        'fecha_pagado',
        'total_pagado',
        'num_recibo',
        'tipo_pago',
        'tipo_cobro',
        'completo',
        'giro',
        'correlativo',
        'lotes_id',
    ];

    public function contrato()
    {
        return $this->belongsTo('App\Modules\Contratos\Models\Contratos', 'contratos_id');
    }

    public function persona()
    {
        return $this->belongsTo('App\Modules\Base\Models\Personas', 'personas_id');
    }

    public function banco()
    {
        return $this->belongsTo('App\Modules\Base\Models\PersonasBancos', 'personas_bancos_id');
    }
}