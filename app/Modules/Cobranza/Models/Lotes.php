<?php

namespace App\Modules\Cobranza\Models;

use App\Modules\Base\Models\Modelo;

use Illuminate\Database\Eloquent\Model;

class Lotes extends Model
{
    protected $table = 'lotes';
    protected $fillable = [
        "fecha",
        "banco_empresa_id",
        "total_registros",
        "recobro",
        "total_cobrar",
        "cargado",
        'archivo'
    ];
    
    protected $campos = [
        'fecha' => [
            'type' => 'date',
            'label' => 'Fecha',
            'placeholder' => 'Fecha del Lotes'
        ],
        'banco_empresa_id' => [
            'type' => 'number',
            'label' => 'Banco Empresa',
            'placeholder' => 'Banco Empresa del Lotes'
        ],
        'total_registros' => [
            'type' => 'number',
            'label' => 'Total Registros',
            'placeholder' => 'Total Registros del Lotes'
        ],
        'recobro' => [
            'type' => 'checkbox',
            'label' => 'Recobro',
            'placeholder' => 'Recobro del Lotes'
        ],
        'total_cobrar' => [
            'type' => 'text',
            'label' => 'Total Cobrar',
            'placeholder' => 'Total Cobrar del Lotes'
        ],
        'cargado' => [
            'type' => 'checkbox',
            'label' => 'Cargado',
            'placeholder' => 'Cargado del Lotes'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    public function cobros()
    {
        return $this->hasMany('App\Modules\Cobranza\Models\Cobros');
    }
}