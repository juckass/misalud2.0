<?php

namespace App\Modules\Cobranza\Http\Requests;

use App\Http\Requests\Request;

class LotesRequest extends Request {
    protected $reglasArr = [
		'fecha' => ['date_format:"d/m/Y"'], 
		'banco_empresa_id' => ['required', 'integer'], 
		'total_registros' => ['integer'], 
		'recobro' => ['required'], 
		'cargado' => ['required']
	];
}