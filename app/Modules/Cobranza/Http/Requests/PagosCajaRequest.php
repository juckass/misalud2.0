<?php

namespace App\Modules\Cobranza\Http\Requests;

use App\Http\Requests\Request;

class PagosCajaRequest extends Request {
    protected $reglasArr = [
        'id'          => ['required', 'integer'], 
		'sucursal_id' => ['required', 'integer'], 
		'cuotas'      => ['required', 'integer'], 
		'inicial'     => ['min:1', 'max:1'], 
		'recibo'      => ['min:3', 'max:20'], 
		'tipo_pago'   => ['min:3', 'max:20']
	];
}