<?php

namespace App\Modules\Cobranza\Http\Requests;

use App\Http\Requests\Request;

class CobrosRequest extends Request {
    protected $reglasArr = [
		'contratos_id'       => ['required', 'integer'], 
		'sucursal_id'        => ['required', 'integer'], 
		'personas_id'        => ['required', 'integer'], 
		'personas_bancos_id' => ['integer'], 
		'bancos_id'          => ['integer'], 
		'total_cobrar'       => ['required'], 
		'concepto'           => ['required'], 
		'fecha_pagado'       => ['date_format:"d/m/Y"'], 
		'num_recibo'         => ['min:3', 'max:20'], 
		'tipo_pago'          => ['min:3', 'max:20'], 
		'completo'           => ['required']
	];
}