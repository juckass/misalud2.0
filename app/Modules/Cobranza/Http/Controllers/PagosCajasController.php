<?php

namespace App\Modules\Cobranza\Http\Controllers;

//Controlador Padre
use App\Modules\Cobranza\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
//Request
use App\Modules\Cobranza\Http\Requests\ContratoTipoRequest;
use App\Modules\Cobranza\Http\Requests\PagosCajaRequest;
use Session;
//Modelos

use App\Modules\Empresa\Models\Sucursal;
use App\Modules\Contratos\Models\Contratos;
use App\Modules\Cobranza\Models\Cobros;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosFacturar;

class PagosCajasController extends Controller
{
    protected $titulo = 'Pago Caja';
    
    public $autenticar = false;

    public $js = [
        'Pagos'
    ];

    public $css = [
        ''
    ];

    public $librerias = [
       'bootstrap-select',
       'touchspin',
       'bootstrap-switch' 
         
    ];
    
    public function index(Request $resquet, $id = 0)
    {
        $contrato = Contratos::find($id);
        
        $inicial = 'n';
        $cuotas_pendientes =  $contrato->cuotas - $contrato->cuotas_pagadas; 
        $debe = Cobros::where('contratos_id', $resquet->id)
            ->where('completo', false)
            ->orderBy('created_at')
            ->get();
        
        $falta_pagar = 0;
        if ($contrato->fecha_incial === $contrato->fecha_pago ) {
            $inicial = 's';
            $falta_pagar += $contrato->inicial;
        }
        
        foreach($debe as $linea) {
            $cuotas_pendientes--;
            $falta_pagar += $linea->total_cobrar - $linea->total_pagado;
        }
        $falta_pagar += $cuotas_pendientes * round(($contrato->total_contrato - $contrato->inicial)  / $contrato->cuotas, 2);
        //dd($cuotas_pendientes);


        return $this->view('cobranza::popup-pagocaja', [
            'layouts'           => 'base::layouts.popup',
            'contrato'          => $contrato,
            'debe'              => $debe,
            'falta_pagar'       => $falta_pagar,
            'cuotas_pendientes' => intval($cuotas_pendientes),
            'inicial'           => $inicial   
        ]);
    }

    public function sucursales()
    {
        return Sucursal::where('empresa_id', Session::get('empresa'))
            //->where('id','!=',5)
            ->pluck('nombre', 'id');
    }

    public function guardar(PagosCajaRequest $resquet)
    {
        DB::beginTransaction();
        try{
            $num = 0;
            $total = 0;
            $pagos_pendientes = explode(',', $resquet->pagos_pendientes);
            $contrato = Contratos::find($resquet->id);
            $date = Carbon::parse($contrato->fecha_pago);
            
            //pagar la inicial tipo_cobro = 1 
            if ($resquet->inicial == "s") {
                $concepto = 'Pago de Inicial  del Contrato ID:' . $contrato->id . ' Tipo de pago: ' .$resquet->tipo_pago;
        
                Cobros::create([
                    'contratos_id' => $resquet->id,
                    'sucursal_id'  => $resquet->sucursal_id,
                    'personas_id'  => $contrato->titular,
                    'total_cobrar' => $contrato->inicial,
                    'concepto'     => $concepto,
                    'tipo_cobro'   => 1,//inicial
                    'fecha_pagado' => date('Y-m-d'),
                    'total_pagado' => $contrato->inicial,
                    'num_recibo'   => $resquet->recibo,
                    'completo'     => 1,
                    'tipo_pago'    => $resquet->tipo_pago,
                ]);
                
                $frecuencia = FrecuenciaPagos::find($contrato->frecuencia_pagos_id);
                
                if ($frecuencia->frecuencia == 'm' ) {
                    $date->day = $frecuencia->dias;
                } else {
                    $_dias = explode(',',  $frecuencia->dias);
                    $date->day = $_dias[0];
                }

                $date->addMonth();
                
                /* No se hace un actualizar masivo para poder mantener las variable de $contrato */
                $contrato->primer_cobro = Carbon::now();
                $contrato->fecha_pago = $date->format('Y-m-d');
                $contrato->total_pagado = $contrato->inicial;
                $contrato->save();
            }
            
            foreach ($pagos_pendientes as $pendiente) {
                if ($pendiente == '') {
                    continue;
                }

                $pendiente = Cobros::find($pendiente);
            
                $contrato->total_pagado += $pendiente->total_cobrar - $pendiente->total_pagado;
                $pendiente->update([
                    'fecha_pagado' => date('Y-m-d'),
                    'tipo_pago'    => $resquet->tipo_pago,
                    'completo'     => 1,
                    'num_recibo'   => $resquet->recibo,
                    'total_pagado' => $pendiente->total_cobrar
                ]);
                $contrato->cuotas_pagadas++;
                $contrato->save();
            }

            if ($resquet->cuotas > 0) {
                $cuota =  $contrato->total_contrato - $contrato->inicial;
                $cuota =  $cuota / $contrato->cuotas;

                $concepto = 'Pago de cuota  del Contrato ID:' . $contrato->id . ' Tipo de pago: ' .$resquet->tipo_pago;  
                for ($i=1; $i <= intval($resquet->cuotas); $i++) { 
                    Cobros::create([
                        'contratos_id' => $resquet->id,
                        'sucursal_id'  => $resquet->sucursal_id,
                        'personas_id'  => $contrato->titular,
                        'total_cobrar' => round($cuota,2),
                        'concepto'     => $concepto,
                         'tipo_cobro'  => 2,//cuota
                        'fecha_pagado' => date('Y-m-d'),
                        'total_pagado' => round($cuota,2),
                        'num_recibo'   => $resquet->recibo,
                        'completo'     => 1,
                    ]);

                    $total = $total +  $cuota;
                }

                $total_pagado = $contrato->total_pagado + $total;
                $total_cuotas = $contrato->cuotas_pagadas + $resquet->cuotas;

                $frecuencia = FrecuenciaPagos::find($contrato->frecuencia_pagos_id);
                
                if ($total_cuotas == $contrato->cuotas) {
                    $fecha = null;
                    
                    $contrato->fecha_pago = $fecha;
                    $contrato->total_pagado = $contrato->total_contrato;
                    $contrato->cuotas_pagadas = $contrato->cuotas;
                    $contrato->cobrando = true;
                    $contrato->save();

                    ContratosFacturar::create([
                        "fecha_facturar" => date('Y-m-d'),
                        "contratos_id"   => $resquet->id
                    ]);
                } else {
                    if ($frecuencia->frecuencia == 'm') {
                        $date->day = $frecuencia->dias;
                        $date->addMonths($resquet->cuotas);
                    } else {
                        //proceso por quincena
                        $date = Carbon::parse($contrato->fecha_pago);
                        
                        $_dias = explode(',',  $frecuencia->dias);
                        if ($resquet->cuotas == 1) {
                            if ($date->day < $_dias[0]) {
                                $date->day = $_dias[0];
                            } elseif ($date->day >= $_dias[1]) {
                                $date->day = $_dias[0];
                                $date->addMonth();
                            } else {
                                $date->day = $_dias[1];
                            }
                        } else {
                            $date->day = $_dias[$resquet->cuota % 2 == 0 && $date->day > $_dias[0] ? 1 : 0];
                            $date->addMonths(floor($resquet->cuota / 2) + 1);
                        }
                    }
                    
                    $contrato->fecha_pago = $date->format('Y-m-d');
                    $contrato->total_pagado = $total_pagado;
                    $contrato->cuotas_pagadas = $total_cuotas;
                    $contrato->cobrando = true;
                    $contrato->save();
                }
            }
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        
        DB::commit();
        return ['s' => 's', 'msj' => "Pago registrado, haga click en aceptar para cerrar la ventana"];
    }
}