<?php

namespace App\Modules\Cobranza\Http\Controllers;

use App\Modules\Cobranza\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;

use App\Modules\Base\Models\TipoPersona;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasBancos;
use App\Modules\Base\Models\PersonasDetalles;

use DB;
use App\Modules\Contratos\Models\Contratos;
use App\Modules\Contratos\Models\Beneficiarios;
use App\Modules\Contratos\Models\Parentesco;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosDetalles;
use App\Modules\Empresa\Models\Sucursal;
use App\Modules\Ventas\Models\VendedoresSucusal;
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\Plan;
use App\Modules\Ventas\Models\PlanDetalles;
use App\Modules\Ventas\Models\Solicitudes;
use App\Modules\Ventas\Models\SolicitudBeneficiariosTemp;
use App\Modules\Contratos\Models\BeneficiariosTemp;
use Auth;
use Carbon\Carbon;

use App\Modules\Contratos\Models\ContratosTemp;
use App\Modules\Base\Models\Bancos;
use App\Modules\Base\Models\BancoTipoCuenta;

class EscritorioanalistaController extends Controller {
    public $autenticar = false;
	protected $titulo = 'Escritorio Analista';

	public $librerias = [
        'datatables',
        'bootstrap-sweetalert'
    ];

	public $js = [
        'escritorioanalista'
    ];
    
    public $css = [
         'escritorioanalista'
    ];

    public function __construct() 
	{
        $this->init();
        $this->middleware('auth');
	}

	public function index(){
        $activos = 0;
        $vencidos = 0;
        $anulados = 0;
        $activos = Contratos::where('contratos.estatus_contrato_id', 1)
        ->where('contratos.anulado', false)->count();

        $vencidos = Contratos::where('contratos.estatus_contrato_id', 5)
        ->where('contratos.anulado', false)->count();

        $anulados = Contratos::where('contratos.anulado', true)->count();
		return $this->view('cobranza::EscritorioAnalista',[
       
            'activos'  =>$activos,
            'vencidos' =>$vencidos,
            'anulados' =>$anulados,
        ]);
	}
    public function contratos(Request $request, $id= 0){
        $this->librerias = [  
            'datatables'
        ];  
        $this->js = [];
       
        switch ( $id) {
            case 1:
                $text =  "Activos";
                break;
            case 2:
                $text =   "Vencidos";
                break;
            case 3:
                $text =   "Anulados";
                break;
            default :
                $text =   "";
                break;
        }
        $this->titulo = "infomarcion de tus contratos " . $text;

        return $this->view('cobranza::popup-info-estatus', [
            'layouts' => 'base::layouts.popup-consulta',
            'tipo' => $id
        ]);
    }
    public function datainformacion(Request $request){
        
        $sql = Contratos::select([
            'contratos.id', 
            'contratos.planilla', 
            'contratos.inicio', 
            'contratos.vencimiento', 
            'personas.nombres', 
            'personas.dni', 
            'sucursal.nombre',
            'sucursal.nombre',
            'vendedores.codigo'
            
        ])
        ->join('vendedores', 'vendedores.id', '=', 'contratos.vendedor_id')
        ->join('personas', 'personas.id', '=', 'contratos.titular')
        ->join('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id');
        
        switch ($request->id) {
            case 1:
                $sql->where('contratos.anulado', false)
                ->where('contratos.estatus_contrato_id', 1);
                break;
            case 2:
                $sql->where('contratos.anulado', false)
                ->where('contratos.estatus_contrato_id',5);
                break;
            case 3:
                $sql->where('contratos.anulado', true);
                break;
            default :
                break;
        }
    
     
        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            })
            ->editColumn('inicio', function($sql) {  
                return Carbon::parse($sql->inicio)->format('d/m/Y');
            }) 
            ->editColumn('vencimiento', function($sql) {  
                return Carbon::parse($sql->vencimiento)->format('d/m/Y');
            }) 
            ->make(true);
    } 
	public function solicitudes()
    {
    	$tipo_solicitud=[
    		1=>'contrato nuevo',
    		'renovacion contrato',
    		'agregar  o eliminnar beneficiarios',
    		'modificacion de informacion personal'
    	];
        
	    /*
		    Tipo solicitud 
		        1 = contrato nuevo
		        2 = renovacion contrato
		        3 = agregar beneficiarios
		        4 = modificacion de informacion personal

		    Solicitante 
		        id_persona quien solicita.
		    aquien 
		        1-contrato
		        2- perona
		    
		    indice 
		        persona_id o contrato_id
	    */
        $_solicitudes = Solicitudes::all();
    	$solicitudes = [];

        foreach ($_solicitudes as $key => $solicitud) {

        	$persona = Personas::where('id',$solicitud->solicitante )->first();

        	$solicitudes[] = [
        		'id'                => $solicitud->id,
        		'Tipo_solicitud'	=> $tipo_solicitud[$solicitud->tipo_solicitud],
        		'solicitante'		=> $persona->nombres,
        		'tipo' 				=> $solicitud->aquien,
        		'indice'			=> $solicitud->indece,
                'fecha'             => Carbon::parse($solicitud->created_at)->format('d/m/Y'),
                'tipo22'            => $solicitud->tipo_solicitud
        	];
        }
        
    	return $solicitudes;
    }
    

	public function confirmacion(Request $request)
    {
    
		/* Fecha
		Tipo de Solicitud
		Responsable
		sucursal */
        $sql = Solicitudes::select([
            'solicitudes.id', 
            'solicitudes.created_at', 
			'solicitudes.tipo_solicitud', 
            'personas.nombres', 
            'sucursal.nombre'      
        ])->join('contratos', 'solicitudes.indece', '=', 'contratos.id')
        ->join('vendedores', 'vendedores.id', '=', 'contratos.vendedor_id')
        ->join('personas', 'personas.id', '=', 'vendedores.personas_id')
        ->join('sucursal', 'sucursal.id', '=', 'contratos.sucursal_id');

		

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return  'apuntador';
            })
            ->editColumn('tipo_solicitud', function($sql) {
                
                switch ($sql->tipo_solicitud) {
                    case 1:
                        return "contrato nuevo";
                        break;
                    case 2:
                        return  "renovacion contrato";
                        break;
                    case 3:
                        return  "agregar beneficiarios";
                        break;
                    case 4:
                        return  "modificacion de informacion personal";
                        break;
                }

            }) 
            ->make(true);
    }

    public function validar(Request $request){
        return  Solicitudes::find($request->id);
      
    }
    
    public function consulta(Request $request, $id = 0, $opera = 0, $solicitud_id = 0)
    {      

        $this->librerias = [
            'bootstrap-sweetalert',
            'datatables',
            'maskMoney'
        ];
       
       

        $this->css=[
            ''
        ];

      
        $solicitud = Solicitudes::where('id',intval($id))->first();

        //informacion del contrato actual
        $Contratos = $this->contratoactual($solicitud->indece);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $solicitud->indece )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $solicitud->indece )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id', $solicitud->indece)->get();

        // Solicitud de renovacion de contrato 
        
        switch ($solicitud->tipo_solicitud) {
        case '2':
            $contrato_renovar = ContratosTemp::where('solicitud_id', $solicitud->id)->first();

            $contrato_renovar->vencimiento  =  Carbon::parse($contrato_renovar->vencimiento)->format('d/m/Y');

            $beneficiario2 = 0;
            $plan2 = 'Personalizado';

            if($contrato_renovar->plan_detalles_id  != '')
            {

                $plan_detalles2 = PlanDetalles::where('id',$contrato_renovar->plan_detalles_id )->first();
                $plan2          = Plan::where('id',$plan_detalles2->plan_id )->first();
                $beneficiario   = $plan_detalles->beneficiarios;
                $plan2          = $plan2->nombre;

            } else
            {
                $beneficiario2   =  Beneficiarios::where('contratos_id', $id )->where('estatus',0)->count(); 
            }

            $frecuencia = FrecuenciaPagos::where('id', $contrato_renovar->frecuencia_pagos_id)->first();
            
                $cuenta = "";
                $Bancos = "";

            if($contrato_renovar->personas_bancos_id != ''){
                $cuenta = PersonasBancos::find($contrato_renovar->personas_bancos_id);
                $Bancos = Bancos::where('id', $cuenta->bancos_id)->first();
            }
            

            $this->js=[
                'popup-renovacion',
            ];

            
            return $this->view('cobranza::Contratos-renovacion', [
                'layouts'           => 'base::layouts.popup-consulta',
                'Contrato'          => $Contratos,
                'Persona'           => $persona,
                'plan'              => $plan,
                'beneficiario'      => $beneficiario,
                'plan2'             => $plan2,
                'beneficiario2'     => $beneficiario2,
                'contrato_renovar'  => $contrato_renovar,
                'contratos_detalles'=> $contratos_detalles,
                'tipo'              => $opera,
                'solicitud_id'      => intval($id),
                'frecuencia22'      => $frecuencia,
                'per_bancos'        => $cuenta,
                'banco'             => $Bancos
                ]);
            break;
            
        }
        
    
        $this->js=[
            'popup-consulta'
             
        ];

        return $this->view('cobranza::Contratos-consulta', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => $opera,
            'solicitud_id'      => intval($id)
        ]);
    }
    
    public function contratoactual($id){
        
        $Contratos = Contratos::select(
            'contratos.id',
            'contratos.planilla',
            'contratos.titular',
            'contratos.vencimiento',
            'contratos.inicio',
            'contratos.cargado',
            'contratos.primer_cobro',
            'contratos.fecha_incial',
            'contratos.inicial',
            'contratos.renovaciones',
            'contratos.tipo_pago',
            'contratos.cuotas',
            'contratos.cuotas_pagadas',
            'contratos.cuota_valor',
            'contratos.plan_detalles_id',
            'contratos.giros',
            'contratos.giros_pagados',
            'contratos.giro_valor',
            'contratos.bookie',
            'frecuencia_pagos.frecuencia',
            'frecuencia_pagos.dias',
            'contratos.total_pagado',
            'contratos.total_contrato',
            'contratos.anulado',
            'personas_bancos.cuenta',
            'bancos.nombre as banco',
            'estatus_contrato.nombre as estatus_contrato',
            'sucursal.nombre as sucursal',
            'personas.nombres as vendedor' 

        )
        ->leftJoin('vendedores', 'vendedores.id','=', 'contratos.vendedor_id')
        ->leftJoin('personas', 'personas.id','=','vendedores.personas_id')
        ->leftJoin('personas_bancos', 'personas_bancos.id','=', 'contratos.personas_bancos_id')
        ->leftJoin('bancos', 'bancos.id','=', 'personas_bancos.bancos_id')
        ->leftJoin('frecuencia_pagos', 'frecuencia_pagos.id','=', 'contratos.frecuencia_pagos_id')
        ->leftJoin('estatus_contrato','estatus_contrato.id', '=', 'contratos.estatus_contrato_id')      
        ->leftJoin('sucursal','sucursal.id', '=', 'contratos.sucursal_id')        
        ->where('contratos.id', $id)->first();

        $Contratos->vencimiento  =  Carbon::parse($Contratos->vencimiento)->format('d/m/Y');
        
        if($Contratos->anulado){
            $Contratos->estatus_contrato = 'Contrato Anulado';
        }


        return $Contratos;
    }
    public function beneficiariosconfirmacion(Request $request, $id = 0)
    {
        
        //adonde: 0 = analista, 1= vendedor, dice de adonde esta accediendo
        
        $this->librerias = [
            'bootstrap-sweetalert'
        ];
        $this->js=[
            'beneficiariosconfirmacion'
             
        ];

        $this->css=[
            ''
        ];

   
        $solicitud = Solicitudes::find($id);

        //informacion del contrato actual
        $Contratos = $this->contratoactual($solicitud->indece);
           
        $persona = Personas::select('*')->where('id',$Contratos->titular)->first();

    
        $beneficiario = 0;
        $plan = 'Personalizado';

        if($Contratos->plan_detalles_id  != ''){

            $plan_detalles = PlanDetalles::where('id',$Contratos->plan_detalles_id )->first();
            $plan          = Plan::where('id',$plan_detalles->plan_id )->first();
            $beneficiario  = $plan_detalles->beneficiarios;
            $plan          = $plan->nombre;
        }else{
           $beneficiario =  Beneficiarios::where('contratos_id', $solicitud->indece )->where('estatus',0)->count(); 
        }

        $_beneficiarios = Beneficiarios::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'parentesco.nombre as parentesco',
            'personas_detalles.sexo',
            'personas_detalles.fecha_nacimiento'
        )
        ->leftJoin('personas', 'personas.id','=','beneficiarios.personas_id')
        ->leftJoin('personas_detalles', 'personas_detalles.personas_id','=','personas.id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios.parentesco_id')
        ->where('contratos_id', $solicitud->indece )->where('estatus',0)->get();
    
        $contratos_detalles = ContratosDetalles::select(
            'personas.dni',
            'tipo_persona.nombre',
            'personas.nombres',
            'contratos_detalles.operacion',
            'contratos_detalles.planilla',
            'contratos_detalles.created_at'

        )
        ->leftJoin('personas', 'personas.id','=','contratos_detalles.personas_id')
        ->leftJoin('tipo_persona', 'tipo_persona.id','=','personas.tipo_persona_id')
        ->orderBy( 'contratos_detalles.id', 'desc')
        ->where('contratos_id',$solicitud->indece )->get();

        
        $solicitud_temp = SolicitudBeneficiariosTemp::where('solicitud_id', $solicitud->id)->first();


        $beneficiarios_temp = BeneficiariosTemp::select([
            "beneficiarios_temp.dni",
            "beneficiarios_temp.nombre",
            'parentesco.nombre as parentesco',
            "beneficiarios_temp.sexo",
            "beneficiarios_temp.solicitud_beneficiarios_temp_id",
            "beneficiarios_temp.parentesco_id",
            "beneficiarios_temp.fecha_nacimiento"
            ])
            ->leftJoin('parentesco', 'parentesco.id','=','beneficiarios_temp.parentesco_id')
            ->where('solicitud_beneficiarios_temp_id', $solicitud_temp->id)->get();

        $operacion='';

        $inf_plan= $plan;

        if($Contratos->tipo_pago == 1){
            $t = "De contado";
        }else{
            $t = "Credito";

        }
        $inf_tipo= $t;
        
        $inf_frecuencia     = $Contratos->frecuencia == 'm' ? 'Mensual'.' : '. $Contratos->dias : 'Quincenal' .' : '. $Contratos->dias;
        $inf_cuotas2        = $Contratos->total_contrato / $Contratos->cuotas;
        $inf_cuotas         = $Contratos->cuotas;
        $inf_total          = $Contratos->total_contrato;
        $inf_total_pagado   = $Contratos->total_pagado;
        
        $num_b = $beneficiarios_temp->count();
        
        switch ($solicitud_temp->operacion) {
            case 0:
                //cambio
                $operacion = 'Cambio de Beneficiario';
              
                break;
            case 1:
                //reduccion
                $operacion = 'Reduccion de Beneficiarios';
                $_plan_detalles2 = PlanDetalles::where('id', $Contratos->plan_detalles_id)->first();

                $_plan_detalles = PlanDetalles::where('plan_id',$_plan_detalles2->plan_id)
                    ->where('beneficiarios', $num_b)
                    ->first();

                $inf_cuotas2 = $_plan_detalles->total / $Contratos->cuotas;

                $inf_total   = $_plan_detalles->total;

                break;
            case 2:
                $operacion = 'Inclusion de Beneficiarios';
                $_plan = Plan::where('id',$solicitud_temp->planes_id )->first();
                $inf_plan = $_plan->nombre;

                $_plan_detalles = PlanDetalles::where('plan_id',$solicitud_temp->planes_id)
                ->where('beneficiarios', $num_b)
                ->first();

                $inf_cuotas2 = $_plan_detalles->total / $Contratos->cuotas;

                $inf_total   = $_plan_detalles->total;

                break; 
            default:
                # code...
                break;
        }


        return $this->view('cobranza::beneficiariosconfirmacion', [
            'layouts'           => 'base::layouts.popup-consulta',
            'Contrato'          => $Contratos,
            'Persona'           => $persona,
            'plan'              => $plan,
            'beneficiario'      => $beneficiario,
            'info_beneficiarios'=> $_beneficiarios,
            'beneficiarios_temp'=> $beneficiarios_temp,
            'contratos_detalles'=> $contratos_detalles,
            'tipo'              => $operacion,
            'solicitud_id'      => $solicitud->id,
            'inf_plan'          => $inf_plan,
            'inf_tipo'          => $inf_tipo,
            'inf_frecuencia'    => $inf_frecuencia,
            'inf_cuotas2'       => $inf_cuotas2,
            'inf_cuotas'        => $inf_cuotas,
            'inf_total'         => $inf_total,
            'inf_total_pagado'  => $inf_total_pagado
        ]);
    }

    public function confirmacioncontrato(Request $request){
        DB::beginTransaction();
        try{

      
            $contrato =  Contratos::find($request->id);

            
            $planilla = $contrato->planilla;
            $confi = $request->confirmacion;
            $m = false;
            switch ($confi) {

                case '1':
                    $descripcion  = 'Contrato Aceptado';
                    $confirmacion = 1;
                    $m = true;

                    break;

                case '2':
                    $descripcion = 'Contrato Rechazado Por: '.$request->descripcion;
                    $confirmacion = 4;
                    $m = true;
                    break;  


                case '3': //renovacion     
                    //Crear decripcion para el contrato detalle
                    $descripcion = "Resumen Contrato anterior, planilla: ". $contrato->planilla . ", fecha de contrato: ".$contrato->inicio. ", fecha vencimiento: ".  $contrato->vencimiento. ", total del contrato: ".$contrato->total_contrato.", total pagado: ". $contrato->total_pagado;

                    ContratosDetalles::create([
                       "contratos_id" => $request['id'],
                       "personas_id"  => auth()->user()->personas_id,
                       "operacion"    => $descripcion,
                       "planilla"     => 's/n'
                    ]);

                    $descripcion  = 'Renovacion de Contrato Aceptado';
                    $confirmacion = 1;

                    $datos = ContratosTemp::where('solicitud_id', $request->solicitud_id)->first()->toArray();
                    

                    $datos['renovaciones'] = $contrato->renovaciones + 1;
                    $datos['bookie'] = false;
                    if($datos['tipo_pago'] == 1 ){
                       
                        $datos['inicial'] = '0';
                        $datos['personas_bancos_id'] = null;
                        $datos['frecuencia_pagos_id'] = null;
                        $datos['fecha_incial'] = null;

                    }else{
                        $datos['primer_cobro'] = null;
                    }

                    $datos['estatus_contrato_id']  = 1; 

                    $datos['renovaciones']  = $contrato->renovaciones + 1;
                      
                    $datos['cuotas_pagadas']  = null;  
                    $datos['anulado']  = 0;  
                    $planilla = $contrato->planilla;
                    $contrato->fill($datos);
                    $contrato->save();

                 
                    
                    ContratosTemp::where('solicitud_id', $request->solicitud_id)->delete();
                    break;
                
                case '4':
            
                    $descripcion = 'Renovacion de Contrato Rechazada Por: '.$request->descripcion;
                    $confirmacion = 4;

                    $datos = ContratosTemp::where('solicitud_id', $request->solicitud_id)->first();

                    $datos->update([
                        'rechazado' => true
                    ]);
                    $planilla =  $datos->planilla;
                    break;

                default:
                    # code...
                    break;
            }
            
           if($m){
                $contrato->update([
                    'estatus_contrato_id' => $confirmacion
                ]);
            }
        
        
            ContratosDetalles::create([
               "contratos_id" => $contrato->id,
               "personas_id"  => Auth()->user()->personas->id,
               "operacion"    => $descripcion,
               "planilla"     => $planilla
            ]); 

            Solicitudes::where('id', $request->solicitud_id)->delete();
            

        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return ['s' => 's'];

    
    }

    public function confirmacionbene(Request $request){
        
        DB::beginTransaction();
        try{
            $datos = [];

            $solicitud = Solicitudes::find($request->id);


            $contrato  = Contratos::find($solicitud->indece);

            $solicitud_temp = SolicitudBeneficiariosTemp::where('solicitud_id', $solicitud->id)->first();


            $beneficiarios_temp = BeneficiariosTemp::select([
                "beneficiarios_temp.dni",
                "beneficiarios_temp.nombre",
                
                "beneficiarios_temp.sexo",
                "beneficiarios_temp.solicitud_beneficiarios_temp_id",
                "beneficiarios_temp.parentesco_id",
                "beneficiarios_temp.fecha_nacimiento"
                ])
                ->where('solicitud_beneficiarios_temp_id', $solicitud_temp->id)->get();

            $this->guardarbeneficiarios2($beneficiarios_temp, $contrato->id);
            $num_b = $beneficiarios_temp->count();

            switch ($solicitud_temp->operacion) {
                case 0:
                    //cambio
                    $operacion = 'Confirmacion de Cambio de Beneficiario';
                    
                    break;
                case 1:
                    //reduccion
                    $operacion = 'Confirmacion deReduccion de Beneficiarios';
                    $_plan_detalles2 = PlanDetalles::where('id', $contrato->plan_detalles_id)->first();

                    $_plan_detalles = PlanDetalles::where('plan_id',$_plan_detalles2->plan_id)
                        ->where('beneficiarios', $num_b)
                        ->first();
                
                        $datos['plan_detalles_id']  = $_plan_detalles->id;
                        $datos['total_contrato']    = $_plan_detalles->total;
                        $datos['beneficiarios']     = $contrato->beneficiarios - 1 ;
                
                    break;
                case 2:
                    $operacion = 'Confirmacion de Inclusion de Beneficiarios';
                    $_plan = Plan::where('id',$solicitud_temp->planes_id )->first();
                    
                    $_plan_detalles = PlanDetalles::where('plan_id',$solicitud_temp->planes_id)
                    ->where('beneficiarios', $num_b)
                    ->first();

                    $datos['plan_detalles_id']  = $_plan_detalles->id;
                    $datos['total_contrato']    = $_plan_detalles->total;
                        $datos['beneficiarios']     = $contrato->beneficiarios + 1 ;
                    break; 
                default:
                    # code...
                    break;
            }

            $datos['planilla'] = $solicitud_temp->planilla;

            $contrato->update($datos);


            ContratosDetalles::create([
                "contratos_id" => $contrato->id,
                "personas_id"  => auth()->user()->personas_id,
                "operacion"    => $operacion,
                "planilla"     => $solicitud_temp->planilla
            ]); 


            Solicitudes::where('id', $request->id)->delete();


            SolicitudBeneficiariosTemp::where('solicitud_id', $solicitud->id)->delete();


            BeneficiariosTemp::where('solicitud_beneficiarios_temp_id', $solicitud_temp->id)->delete();


        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    
    }

    public function guardarbeneficiarios2($request, $id){
        DB::beginTransaction();
        try { 

            Beneficiarios::where('contratos_id', $id)->delete();
            $datos = [];

            foreach ($request as $beneficiario) {
               
                $persona = Personas::where('dni', $beneficiario->dni)->first();

                if(count($persona) == 0){
                   $persona = Personas::create([
                       "tipo_persona_id" => 1,
                       "dni"             => $beneficiario->dni,
                       "nombres"         => $beneficiario->nombre
                    ]);

                    PersonasDetalles::create([
                       "personas_id"      => $persona->id,
                       "sexo"             => $beneficiario->sexo,
                       "fecha_nacimiento" => $beneficiario->fecha_nacimiento
                    ]);
                }


                $datos= [
                    "contratos_id"  => $id,
                    "personas_id"   => $persona->id,
                    "parentesco_id" => $beneficiario->parentesco_id,
                    "estatus"       => 0
                ];
                
                Beneficiarios::create($datos);   
            }
   

        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();

        return ['s' => 's'];
    }
}