<?php

namespace App\Modules\Cobranza\Http\Controllers;

use App\Modules\Cobranza\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Session;
use DB;
use Auth;

use App\Modules\Cobranza\Clases\Txt;

use App\Modules\Base\Models\Bancos;
use App\Modules\Empresa\Models\Empresa;
use App\Modules\Empresa\Models\Sucursal;
use App\Modules\Empresa\Models\BancosEmpresa;
use App\Modules\Contratos\Models\Contratos;
use App\Modules\Cobranza\Models\Cobros;
use App\Modules\Cobranza\Models\Lotes;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosFacturar;
use App\Modules\Contratos\Models\ContratosDetalles;


class ExporlotesController extends Controller {
    protected $titulo = 'Exportar lotes';

    public $librerias = [
        'jquery-ui',
        'jquery-ui-timepicker',
        'ladda'
    ];

    public $js = [
        'exportlores'
    ];
  
    public function index()
    {   
        return $this->view('cobranza::exportlotes');
    }

    public function prueba()
    {
       /*  $archivo = '2017/11/20171108211018_Mercantil.txt';
        $txt = new txt('mercantil');
        $salida = $txt->leer($archivo);
        return $salida; */
    }

    public function carga(Request $request)
    {
        
        $this->titulo = "";
       
        $_ruta = date('Y') . '/' . date('m') . '/';
        $ruta = public_path('respuestas/' . $_ruta);
        $archivo = $request->archivo;

        if (!is_dir($ruta)) {
            mkdir($ruta, 777, true);
        }

        do {
            $nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
        } while (is_file($ruta . $nombre_archivo));
            
        $archivo->move($ruta, $nombre_archivo);
        chmod($ruta . $nombre_archivo, 0777);

        //$archivo = '2017/11/20171108211018_Mercantil.txt';
       

       // dd($cabecera, $detalle, $archivo);
        DB::beginTransaction();
        try{ 
            $archivo = $ruta . $nombre_archivo;
            $lotes = Lotes::find($request->lote_id);
            $lotes->banco_empresa_id;
            $BancosEmpresa = BancosEmpresa::find($lotes->banco_empresa_id);
            $txt = new txt(strtolower($BancosEmpresa->nombre));
            //list($cabecera, $detalle) = $txt->leer($_ruta . $nombre_archivo);
            list($cabecera, $detalle) = $txt->leer($_ruta . $nombre_archivo);
            
            /**d
             * TODO: aqui tienes que actualizar los datos de los cobros de los clientes
             * todo la informacion están en las variables $cabecera, $detalle
             * 1. Ver cuando se cobro. 
             * 2. A quien se le cobro "si tenemos N° de correlativo del banco en la tabla de cobro serie perfecto".
             * 3. Estatus a ver si se cobro y si no porque no se cobro.
             * 
             *   si se cobró
             *       codigo
             *       msj del codigo
             *   error de numero de cuenta
             *       se anula el contrato
             *       o si tiene error los datos del cliente
             *       o si no se pudo afiliar
             *   relacionar el numero 
            */

            $lote = null;
            if ($cabecera) {
                // TODO: si existe cabecera se puede lote_id
                //$cabecera['lote_id']
                $lote = Lotes::findOrFail($cabecera['lote_id']);
            }

            $cobrados = [];
            $no_cobrados =[];
            $no_encontrados = [];
            $num  = 0;
            foreach ($detalle as $det) {
                $num++;
               
                $cobro = null;
                
                if (!$lote && $det['lote_id']) {
                    $lote = Lotes::findOrFail($cabecera['lote_id']);
                }
            
                $correlativo = 0;
                if ($det['correlativo']) {
                    $correlativo = $det['correlativo'];
                    //$fecha = Carbon::createFromFormat($txt->formato_fecha, $det['fecha']);

                    $cobro = Cobros::where('correlativo', $correlativo)
                    // ->where('fecha', $fecha)
                        ->first();
                } elseif ($det['cobros_id']) {
                    $cobro = Cobros::findOrFail($det['cobros_id']);
                }

                
                if (!$cobro) {
                    // TODO: hay que generar una advertencia de que el cobro no se encuentra
                    $no_encontrados[] = $det;
                    continue;
                }

                

                if ($det['anular']) {

                    $texto = "Respuesta cobro del sistema: ".$det['mensaje_respuesta'];
                    $this->detalles_contrato( $cobro->contratos_id, $texto);
                    $no_cobrados[] =[
                        'monto'       => $cobro->monto,
                        'contrato_id' => $cobro->contrato_id,
                        'msj' => $det['mensaje_respuesta'],
                    ]; 

                    continue;

                } /* elseif ($det['codigo_respuesta']) {
                    // TODO: si el codigo es diferente a los codigos de correcto no sucede nada
                    dd('error');
                    continue;
                } */
                $pagado = $cobro->total_pagado + floatval($det['monto']);

                $cobro->total_pagado = $pagado;

                $_cobrado = floatval($cobro->total_cobrar) - floatval($pagado);
                
                if($_cobrado == 0){
                    $cobro->completo = true;
                }
                
                $cobro->fecha_pagado = Carbon::now();
                //$cobro->num_recibo = '';
                $cobro->tipo_pago = 'Pago automatico por domiciliación';
                $cobro->save();
                $contrato = $cobro->contrato;
                $cobro->tipo_cobro;
                
                if($cobro->tipo_cobro == 1){ //si es inicial
                    if($_cobrado == 0){
                        $contrato->primer_cobro = $det['fecha'];
                        $contrato->total_pagado = $det['monto'];
                        $texto = "Respuesta cobro del sistema: Inicial Cobrada con exito";
                        $this->detalles_contrato( $cobro->contratos_id, $texto);
                    }else{
                        dd('reguntar si anulo el contrato');
                    }
                }else{
                    if( $_cobrado == 0){
                        $cuotas_pagas =  $contrato->cuotas_pagadas + 1;
                        $contrato->cuotas_pagadas = $cuotas_pagas;
                        
                        $texto = "Respuesta cobro del sistema: Cuota Cobrada con exito";
                        if($cuotas_pagas == $contrato->cuotas){
                            $contrato->cobrando = true;
                            $texto = "Respuesta cobro del sistema: Contrato Cobrato en su totalidad";
                        }

                        $this->detalles_contrato($cobro->contratos_id, $texto);
                    }
                    $contrato->total_pagado = $contrato->total_pagado + $det['monto'];
                }
                $cobrados[] =[
                    'monto'     => $cobro->monto,
                    'contrato_id' => $cobro->contratos_id
                ]; 
                
                $contrato->save();
            }
            $lote ->update([
                'cargado' => true
            ]);
            $lote->archivo = $txt->archivo;
            $lote->save();
         } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        DB::commit();
        /* $this->view('cobranza::info',[
            'cobrados'       => $cobrados,
            'no_cobrados'    => $no_cobrados,
            'no_encontrados' => $no_encontrados
        ]);  */

          
        return $this->view('cobranza::info',[
            'cobrados'              => $cobrados,
            'cobrados_conut'        => count($cobrados),
            'cobradosno_conut'      => count($no_cobrados),
            'no_cobrados'           => $no_cobrados,
            'no_encontrados'        => $no_encontrados, 
            'no_encontrados_count'  => count($no_encontrados) 
        ]); 
    }
    public function bancos()
    {
        return BancosEmpresa::pluck('nombre', 'id');
    }
    
    public function generar(Request $request)
    {
        DB::beginTransaction();
        try{
            //datos del banco a generar

            $fecha = Carbon::createFromFormat('d/m/Y', $request->fecha); // fecha de generacion de lotes
            $BancosEmpresa = BancosEmpresa::find($request->banco); //se busca la informacion del banco  
            $numero_cuenta_empresa = $BancosEmpresa->cuenta; //numero de cuenta de la empresa
            $banco_id = $BancosEmpresa->bancos_id; //id banco a generar 

            $banco = Bancos::find($BancosEmpresa->bancos_id);
            
            $empresa = Empresa::find(Session::get('empresa'));
            $sucursal = Sucursal::where('empresa_id', Session::get('empresa'))->first();
            
            $txt = new txt($BancosEmpresa->nombre);
            // $txt = '';

            //buscar contratos fecha de corte.
           
            $contratos = Contratos::select([
               'contratos.id',
               'contratos.titular',
               'contratos.plan_detalles_id',
               'contratos.frecuencia_pagos_id',
               'contratos.total_contrato',
               'contratos.total_pagado',
               'contratos.cuota_valor',
               'contratos.cuotas',
               'contratos.cuotas_pagadas',
               'contratos.giros_pagados',
               'contratos.giros',
               'contratos.giro_valor',
               'contratos.personas_bancos_id',
               'contratos.inicial',
               'contratos.fecha_incial',
               'contratos.fecha_pago',
               'contratos.bookie',
               'personas_bancos.cuenta'
            ])
            ->leftjoin('personas_bancos', 'personas_bancos.id','=','contratos.personas_bancos_id')
          
            ->where('personas_bancos.bancos_id', $banco_id)//tipo pago credito
            ->whereIn('tipo_contrato', [0,1]) //tipos de contratos 0 = normales, 1 = corporativos a credito
            ->where('anulado', false)
            ->where('cobrando', false); //contrato que no este pagado;
            
            $lote = Lotes::create([
                'fecha'            => $fecha,
                'banco_empresa_id' => $request->banco, //$banco_id,
                'total_registros'  => 0,
                'recobro'          => 0,
                'total_cobrar'     => 0,
                'cargado'          => false,
                'archivo'          => ''//aqui tiene que poner el nombre del archivo o la ruta
            ]);

            if($request->tipo == 1){//giros
                $contratos
                    ->where('contratos.giros','!=', null)
                    ->where('bookie', true);
            }else{
                $contratos->where('fecha_pago','<=', $fecha->format('Y-m-d')); 
            }
        
            //generar cobros
            $cobros = [];
            $date = Carbon::now();
            //giros
            if($request->tipo == 1) {//giros
                foreach ( $contratos->get() as $key =>  $contrato) {

                    $cobro = Cobros::create([
                        'contratos_id'           => $contrato->id,
                        'sucursal_id'            => 1,
                        'personas_id'            => $contrato->titular,
                        'personas_bancos_id'     => $contrato->personas_bancos_id,
                        'bancos_id'              => $banco_id,
                        'total_cobrar'           => $contrato->giro_valor,
                        'concepto'               => "Cobro de giro por sistema",
                        'giro'                   => true,
                    ]);

                    $cobros[] = $cobro->id;

                    ContratosDetalles::create([
                        "contratos_id" => $contrato->id,
                        "personas_id"  => auth()->user()->personas->id,
                        "operacion"    => "Generacion de Cobro de giro por sistema",
                        "planilla"     => 's/n'
                    ]);
                }
            }else{
                //cobros normales
                foreach ( $contratos->get() as $key =>  $contrato) {
                    //pagar inicial
                    if($contrato->fecha_incial == $contrato->fecha_pago){

                        $cobro = Cobros::create([
                            'contratos_id'           => $contrato->id,
                            'sucursal_id'            => 1,
                            'personas_id'            => $contrato->titular,
                            'personas_bancos_id'     => $contrato->personas_bancos_id,
                            'bancos_id'              => $banco_id,
                            'tipo_cobro'            => 1,//inicial
                            'total_cobrar'           => $contrato->inicial,
                            'lotes_id'               => $lote->id,
                            'concepto'               => "Cobro de inicial por sistema",
                        ]);

                        $cobros[] = $cobro->id;

                        ContratosDetalles::create([
                            "contratos_id" => $contrato->id,
                            "personas_id"  => auth()->user()->personas->id,
                            "operacion"    => "Generacion de Cobro de inicial por sistema",
                            "planilla"     => 's/n'
                        ]);

                        $frecuencia = FrecuenciaPagos::find($contrato->frecuencia_pagos_id);
                        //$date = Carbon::now();
                        $date = Carbon::parse($contrato->fecha_pago);
                        if($frecuencia->frecuencia == 'm' ){
                            $date->day = $frecuencia->dias;
                        }else{
                            $_dias = explode(',',  $frecuencia->dias);
                            $date->day = $_dias[0];
                        }
        
                        $date->addMonth();

                    }else{
                        //cuota normal

                        if($contrato->bookie){
                            $cuota = $contrato->cuota_valor;
                        }else{
                            $cuota = $contrato->total_contrato - $contrato->inicial;
                            $cuota = $cuota / $contrato->cuotas;
                        }  

                        $cobro = Cobros::create([
                            'contratos_id'      => $contrato->id,
                            'sucursal_id'       => 1,
                            'personas_id'       => $contrato->titular,
                            'personas_bancos_id'=> $contrato->personas_bancos_id,
                            'bancos_id'         => $banco_id,
                            'tipo_cobro'        => 2,//inicial
                            'total_cobrar'      => round($cuota,2),
                            'lotes_id'          => $lote->id,
                            'concepto'          => "Cobro de couta por sistema",
                        ]);

                        $cobros[] = $cobro->id;

                        ContratosDetalles::create([
                            "contratos_id" => $contrato->id,
                            "personas_id"  => auth()->user()->personas->id,
                            "operacion"    => "Generacion de Cobro de couta por sistema",
                            "planilla"     => 's/n'
                        ]);
                        
                        $frecuencia = FrecuenciaPagos::find($contrato->frecuencia_pagos_id);
                        //$date = Carbon::now();
                        $date = Carbon::parse($contrato->fecha_pago);

                        if($frecuencia->frecuencia == 'm' ){
                            $date->day = $frecuencia->dias;
                            $date->addMonth();
                        }else{
                            //proceso por quincena
                            $date = Carbon::createFromFormat('Y-m-d', $contrato->fecha_pago);
                            
                            $_dias = explode(',',  $frecuencia->dias);

                            if($date->day == $_dias[0]){
                                $date->day = $_dias[1];
                            }else{
                                $date->day = $_dias[0];
                                $date->addMonth();
                            }
                        }
                    }

                    Contratos::where('id', $contrato->id)->update([
                        'fecha_pago'   => $date->format('Y-m-d'),
                    ]); 
                }
            }

            //generacion de txt


            if($request->tipo == 1){//giros
                $_cobros = Cobros::select([
                        DB::raw('(SUM(total_cobrar) - SUM(total_pagado)) as total'),
                        DB::raw('COUNT(id) as registros')
                    ])
                    ->where('completo', false)
                    ->where('bancos_id', $banco_id)
                    ->where('giro', true)
                    ->first();

                Cobros::where('completo', false)
                    ->where('giro', true)
                    ->where('bancos_id', $banco_id)
                    ->update([
                        'lotes_id' => $lote->id
                    ]);
                    
                $cobros = Cobros::where('completo', false)
                    ->where('giro', true)
                    ->where('bancos_id', $banco_id)
                    ->get();
            }else{
                $recobros =Cobros::select(['*'])
                    ->where('completo', false)
                    ->where('bancos_id', $banco_id)
                    ->count();

                $_cobros = Cobros::select([
                        DB::raw('(SUM(total_cobrar) - SUM(total_pagado)) as total'),
                        DB::raw('COUNT(id) as registros')
                    ])
                    ->where('bancos_id', $banco_id)
                    ->where('completo', false)
                    ->first();

                Cobros::where('completo', false)
                    ->where('bancos_id', $banco_id)
                    ->update([
                        'lotes_id' => $lote->id
                    ]);

                $cobros = Cobros::where('completo', false)
                    ->where('bancos_id', $banco_id)
                    ->get();
            }
            
            $lote->total_cobrar = $_cobros->total;
            $lote->total_registros = $_cobros->registros;
            $lote->save();

            $monto_total_debito  = $lote->total_cobrar;
            $monto_total_credito = 0;
            
            $total = $monto_total_debito + $monto_total_credito;

            $data = [
                'lote_id'                  => $lote->id,
                'empresa_rif'              => $empresa->rif,
                'empresa_nombre'           => $empresa->nombre,
                'empresa_cuenta'           => $numero_cuenta_empresa,
                'cantidad_total_registros' => $lote->total_registros,
                'monto_total_debito'       => $monto_total_debito,
                'monto_total_credito'      => $monto_total_credito,
                'total'                    => $total,
                'fecha_valor'              => Carbon::now(),
                'fecha_proceso'            => Carbon::now(),
            ];
            $txt->cabecera($data);
            foreach ($cobros as $cobro) {
                $persona      = $cobro->persona;

                $persona_id   = $persona->id;
                $nacionalidad = $persona->tipo_persona->nombre;
                $cuenta       = $cobro->banco->cuenta;

                $cobro->correlativo = $banco->correlativo;
                $cobro->save();

                $data = [
                    'contrato_id'    => $cobro->contrato->id,
                    'lote_id'        => $lote->id,
                    'cobros_id'      => $cobro->id,
                    'persona_id'     => $persona->id,
                    'persona_nombre' => $persona->nombres,
                    'nacionalidad'   => $nacionalidad,
                    'dni'            => $persona->dni,
                    'cuenta'         => trim($cuenta),
                    'monto'          => $cobro->total_cobrar - $cobro->total_pagado,
                    'correlativo'    => $banco->correlativo++,
                    'fecha'          => Carbon::now(),
                ];
                
                $txt->detalle($data);
            }
            
            $data = [
                'lote_id'                  => $lote->id,
                'empresa_rif'              => $empresa->rif,
                'empresa_nombre'           => $empresa->nombre,
                'empresa_cuenta'           => $numero_cuenta_empresa,
                'cantidad_total_registros' => $lote->total_registros,
                'monto_total_debito'       => $monto_total_debito,
                'monto_total_credito'      => $monto_total_credito,
                'total'                    => $total,
                'fecha_valor'              => Carbon::now(),
                'fecha_proceso'            => Carbon::now(),
            ];
            $txt->pies($data);

            $banco->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        
        $lote->archivo = $txt->archivo;
        $lote->save();

        // $txt->archivo;
        DB::commit();
        return ['s' => 's', 'msj' => "Pago registrado", 'lotes' => $lote ];
    }
    
    public function lotes(Request $request){
        $fecha = Carbon::createFromFormat('d/m/Y', $request->fecha);
    
        $BancosEmpresa = BancosEmpresa::where('id', $request->banco)->first();
        $lotes = Lotes::select(['*'])
            ->where('fecha','=', $fecha)
            ->where('banco_empresa_id','=',   $request->banco)
            ->where('cargado','=',  false)
            ->first();
        
        if($lotes){
            return ['s' => 's', 'msj' => "lote encontrato",'lotes'=> $lotes ];
        }

        return ['s' => 'n', 'msj' => "",'lotes'=>$lotes ];
    }

    protected function detalles_contrato($contrato_id, $msj){
        ContratosDetalles::create([
            "contratos_id" => $contrato_id,
            "operacion"    =>  $msj, //"Respuesta cobro del sistema: ".$det['mensaje_respuesta'],
            "planilla"     => 's/n'
        ]);
    }

}