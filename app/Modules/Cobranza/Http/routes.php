<?php

Route::group(['middleware' => 'web', 'prefix' => 'cobranza', 'namespace' => 'App\\Modules\Cobranza\Http\Controllers'], function()
{
    Route::group(['prefix' => 'pagos'], function() {
        Route::get('/consulta/pagos/{id}', 		'PagosCajasController@index');
		Route::post('/debe', 					'PagosCajasController@debe');
		Route::post('/guardar',					'PagosCajasController@guardar');
    });

    Route::group(['prefix' => 'export'], function() {
        Route::get('/',  'ExporlotesController@index');
        Route::post('/generar',  'ExporlotesController@generar');
        Route::post('/lotes',  'ExporlotesController@lotes');
        Route::get('/prueba',  'ExporlotesController@prueba');
        Route::post('/carga',  'ExporlotesController@carga')->name('cobranza.export.carga');
    });

    
    Route::group(['prefix' => 'analista'], function() {
        Route::get('/escritorio', 	                             'EscritorioanalistaController@index');
        Route::get('/escritorio/confirmacion', 	                 'EscritorioanalistaController@confirmacion');
		Route::post('/escritorio/validar',                       'EscritorioanalistaController@validar');
		Route::post('/confirmacion',                             'EscritorioanalistaController@confirmacioncontrato');
        Route::get('/consulta/{id}/{opera}/{solicitud_id}',      'EscritorioanalistaController@consulta');
        Route::get('/beneficiariosconfirmacion/{id}',            'EscritorioanalistaController@beneficiariosconfirmacion');
        Route::post('/confirmacionbene',                         'EscritorioanalistaController@confirmacionbene');
        Route::get('/contratos/{id}',                            'EscritorioanalistaController@contratos');
        Route::get('/datainformacion',                           'EscritorioanalistaController@datainformacion');
    });
    
});