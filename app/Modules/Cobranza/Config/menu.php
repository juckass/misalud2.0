<?php

$menu['Analista'] = [
	[
		'nombre' 	=> 'Analista',
		'direccion' => 'cobranza/analista/escritorio',
		'icono' 	=> 'fa fa-home',
	]
];

$menu['cobranza'] = [
	[
		'nombre' 	=> 'Lotes',
		'direccion' => '#lotes',
		'icono' 	=> 'fa fa-clipboard',
		'menu' 		=> [
			[
				'nombre' 	=> 'Generar',
				'direccion' => 'cobranza/export',
				'icono' 	=> 'fa fa-download'
			]
		]
	]
];