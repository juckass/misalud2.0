$(function() {
    var Ladda;
    $("#fecha").datepicker();
    console.log(url);
    $('#generar').on('click', function() {
        if ($('#fecha').val() == '') {
            aviso('Introduzca fecha de lotes');
            return false;
        }
        if ($('#banco').val() == '') {
            aviso('Seleccione Banco');
            return false;
        }

        generar();
    });

    $('#fecha').on('change', function() {
        $.ajax({
            url: $url + 'lotes',
            type: 'POST',
            data: {
                'banco': $('#banco').val(),
                'fecha': $('#fecha').val(),
                'tipo': $('#tipo').val(),
            },
            success: function(r) {
                aviso(r);
                if (r.s == 's') {
                    $('#generar').prop('disabled', true);
                    $("#lote-id").html('').append(r.lotes.id);
                    $("#lote-fecha").html('').append(r.lotes.fecha);
                    $("#lote-registros").html('').append(r.lotes.total_registros);
                    $("#lote-bolivares").html('').append(r.lotes.total_cobrar);
                    $("#lote-recobro").html('').append(r.lotes.recobro);
                    $("#lote-archivo").html('').append(r.lotes.archivo);
                    $("#lote2-id").val(r.lotes.id);

                    $('#cargar').css('display', 'block');
                    $('#lote-url').attr({
                        'href': url + '/' + r.lotes.archivo,
                        'style': 'color: blue'
                    });

                } else {
                    $("#lote-id").html('');
                    $("#lote-fecha").html('');
                    $("#lote-registros").html('');
                    $("#lote-bolivares").html('');
                    $("#lote-recobro").html('');
                    $("#lote-archivo").html('');
                    $("#lote2-id").val('');
                    $('#lote-url').attr({
                        'href': '',
                        'style': 'color: blue'
                    });
                    $('#cargar').css('display', 'none');

                    $('#generar').prop('disabled', false);
                }
            },
            complete: function() {

                $('#generar').prop('disabled', false);

            }
        });
    });

});

function generar() {
    var l = Ladda.create($("#generar").get(0));
    $('#generar').prop('disabled', true);
    l.start();
    $.ajax({
        url: $url + 'generar',
        type: 'POST',
        data: {
            'banco': $('#banco').val(),
            'fecha': $('#fecha').val(),
            'tipo': $('#tipo').val(),
        },
        success: function(r) {
            aviso(r);

            $("#lote-id").html('').append(r.lotes.id);
            $("#lote-fecha").html('').append(r.lotes.fecha.date);
            $("#lote-registros").html('').append(r.lotes.total_registros);
            $("#lote-bolivares").html('').append(r.lotes.total_cobrar);
            $("#lote-recobro").html('').append(r.lotes.recobro);

            $("#lote-archivo").html('').append('');

            $("#lote-archivo").html('').append(r.lotes.archivo);

            $('#lote-url').attr({
                'href': url + '/' + r.lotes.archivo,
                'style': 'color: blue'
            });
        },
        complete: function() {
            l.stop();
            $('#generar').prop('disabled', false);

        }
    });
}