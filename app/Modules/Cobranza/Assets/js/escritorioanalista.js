var aplicacion, $form, tabla, $tablaPlan;
$(function() {

    confimacion();
    $('.more').on('click', function() {
        var win = window.open(dire + '/cobranza/analista/contratos/' + this.getAttribute('data-tipo'), 'estatus', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });

});


function confimacion() {
    solicitud_asignadas = $('#tabla1')
        .on("click", "tbody tr", function() {
            //procesarsolicitud(this.id);
            contrato(this.id);
        })
        .dataTable({
            processing: true,
            serverSide: true,
            ajax: $url + "confirmacion",
            oLanguage: datatableEspanol,
            columns: [
                { data: 'created_at', name: 'solicitudes.created_at' },
                { data: 'tipo_solicitud', name: 'solicitudes.tipo_solicitud' },
                { data: 'nombres', name: 'personas.nombres' },
                { data: 'nombre', name: 'sucursal.nombre' },
            ]
        });
}

function contrato(id) {

    $.ajax({
        url: $url + 'validar',
        type: 'POST',
        data: {
            'id': id,
        },
        success: function(r) {
            if (r.s == 'n') {

                return false
            }
            if (r.tipo_solicitud == 3) {
                var win = window.open(dire + '/cobranza/analista/beneficiariosconfirmacion/' + id, 'beneficiarios', 'height=500,width=1000,resizable=yes,scrollbars=yes');
                win.focus();
                return false
            }
            var win = window.open(dire + '/cobranza/analista/consulta/' + id + '/' + r.tipo_solicitud + '/0', 'consulta', 'height=500,width=1000,resizable=yes,scrollbars=yes');
            win.focus();
            return false
        }
    });


}