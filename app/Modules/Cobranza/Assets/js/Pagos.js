var $num = 1,
    $total = 0,
    $cantidad_cuotas = 0,
    $minimo = 0;

$(function() {
    //debe();

    $("#cuotas").TouchSpin({
        'min': 0,
        'max': Math.abs($pendientes - $cuotas_vencidas)
    }).val(0);

    $('#cuotas').on('change', function() {
        var cuota = $('#cuotas').val();
        
        if (cuota <= $pendientes) {
            cargos('cuota', $cantidad_cuotas > cuota ? 'menos' : 'mas');

            $cantidad_cuotas = cuota;
        }
    });

    $('#inicial').bootstrapSwitch('state', false, true);
    $('#inicial').on('switchChange.bootstrapSwitch', function(event, state) {
        $opera = 'menos';
        $('#_inicial').val('n');
        if (state) {
            $opera = 'mas';
            $('#_inicial').val('s');
        }
        cargos('inicial', $opera);
    });

    $('#guardar').on('click', function() {
        if ($tiene_inicial && $('#_inicial').val() == 'n') {
            aviso('Debe pagar la inicial antes de pagar una cuota', false);
            return;
        }

        $('#submit_form').ajaxSubmit({
            'url': dire + '/cobranza/pagos/guardar',
            'type': 'POST',
            'success': function(r) {
                alert(r.msj);
                if (r.s == 's') {
                    window.close();
                }
            }
        });

        return false;
    });

    $('tbody', '#cargos_pendiente').on('click', 'tr', function(e){
        var t = $(this),
            id         = t.data('id'), 
            fecha      = $('td:eq(1)', t).text(),
            monto      = $('td:eq(2)', t).text(),
            fecha_pago = $('td:eq(3)', t).text(),
            pago       = $('td:eq(4)', t).text(),
            pendiente  = $('td:eq(5)', t).text(),
            estatus    = $('td:eq(6)', t).text();
            
        monto      = to_number(monto);
        pago       = to_number(pago);
        pendiente  = to_number(pendiente);
        check = $('input', t).prop('checked');

        if (check) {
            pendiente *= -1;
        }

        if (!$(e.target).is('input')) {
            $('input', t).prop('checked',  !check);
        }

        fpagos_pendientes();
        
        $total += pendiente;

        $('#total').html(number_format($total, 2));
    });
});

function cargos($tipo, $operacion) {
    $concepto = '';
    $pagar = 0;

    switch ($tipo) {
        case 'inicial':
            $concepto = 'Pago de Inicial';
            $pagar = $inicial;
            $clase = 'inicial';
            break;
        case 'cuota':
            $concepto = 'Pago de Cuota';
            $pagar = $cuota;
            $clase = 'cuota';
            break;
        default:
            break;
    }

    if ($operacion == 'mas') {
        $num++;
        $total += $pagar;

        $('#cargos').append(
            '<tr class="' + $clase + '">' + 
                '<td>&nbsp;</td>' +
                '<td>' + $concepto + '</td>' +
                '<td>' + number_format($pagar, 2) + '</td>' +
            '</tr>'
        );
    } else if ($operacion == 'menos') {
        $num--;
        $monto = $clase == 'inicial' ? $inicial : $cuota;
        $total -= $monto;

        $('.' + $clase + ':last').remove();
    }

    fpagos_pendientes();
    
    /**/
    if ($('#cuotas').val() == $pendientes) {
        $total = $falta_pagar;
    }

    if ($tiene_inicial && $('#_inicial').val() == 's') { 
        $total += $inicial;
    }

    if ($('#cuotas').val() == 0) {
        $total = 0;
    }
    
    $('#total').html(number_format($total, 2));
}

function to_number($var){
    $var = $var || '0';
    return +($var.replace(/\./g,'').replace(/,/g,'.'));;
}

function fpagos_pendientes() {
    var ids = [],
        pagos_pendientes = $("#pagos_pendientes");
    $('input:checked', '#cargos_pendiente').each(function(){
        var tr = $($(this).parents('tr')),
            id = tr.data('id');
        
        ids.push(id);
    });
    pagos_pendientes.val(ids.join(','));
}