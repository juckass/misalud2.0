var op = 0;
$(function() {

    $('.confirm').live('click', function() {
        cerrar_windows(op)
    });


    $('#aceptar').on('click', function() {

        swal({
                title: "",
                text: "Esta Seguro que Desea Aceptar y Activar EL Contrato?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                cancelButtonClass: "btn-danger",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    confirmacion($('#contrato_id').val(), 3, '', $('#solicitud_id').val());

                } else {
                    swal("Cancelado", "", "error");
                }
            });

    });

    $('#rechazar').on('click', function() {

        swal({
                title: "",
                text: "Esta Seguro que Desea Rechazar el Contrato?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                cancelButtonClass: "btn-danger",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal({
                        title: "",
                        text: "Motivo de Rechazo",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        inputPlaceholder: "Write something"
                    }, function(inputValue) {
                        if (inputValue === false) return false;
                        if (inputValue === "") {
                            swal.showInputError("Debe Introducir motivo de Rechazo");
                            return false
                        }
                        swal("", " Contrato Rechazado", "success");
                        confirmacion($('#contrato_id').val(), 4, inputValue, $('#solicitud_id').val());
                    });
                } else {
                    swal("Cancelado", "", "error");
                }
            });
    });

});

function confirmacion($id, $confirmacion, result, solicitud_id) {

    $.ajax({
        url: dire + '/cobranza/analista/confirmacion',
        data: {
            'id': $id,
            'confirmacion': $confirmacion,
            'descripcion': result,
            'solicitud_id': solicitud_id
        },
        type: 'POST',
        success: function(r) {

            if (r.s == 's') {
                window.opener.location.reload();
                window.close();
            }
        }
    });
}

function cerrar_windows(op) {
    if (op == 1) {

    }

}