var aplicacion, $form, $numero = 0,
    datos_persona = [],
    p = false,
    id_contrato = '',
    a = false,
    id = '',
    c = false;
$(function() {
    aplicacion = new app('submit_form', {
        'limpiar': function() {
            $('#meses').val('');
            $('#porsentaje').val('');
            $('#contado').val('');
            $('#inicial').val('');
            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
            $('#contol').css('display', 'none');
            $('#goza').val('');

            $('#guardar', '#botonera').prop('disabled', false);
            $('#dni').prop('readonly', false);
            $('#fecha2').prop('readonly', false);
            $('#anular_contrato').prop('disabled', false);
            $('#bene').prop('disabled', false);

            $('#fecha_decontado').prop('readonly', false);
            $('#n_recibo').prop('readonly', false);
            $('#pago').prop('readonly', false);

            $("#beneficiarios").html('');
            $("#id_contrato").html('');
            $("#estatus_contrato").html('');
            $("#cobrado").html('');
            $("#renovaciones").html('');
            id_contrato = '';
            $numero = 0;
            p = false; // nose
            a = false; // anulodo
            c = false; // cobrado
            $id = '';
            tabla.fnDraw();
        },
        'buscar': function(r) {

            $("#beneficiarios").html('');
            $("#id_contrato").html('');
            $("#estatus_contrato").html('');
            $("#renovaciones").html('');
            $("#cobrado").html('');

            buscar_template(r.id);

            $('#goza').val(r.goza == 1 ? 0 : 1);

            $("#frecuencia_pagos_id").val(1);

            buscar_cliente(function() {
                $("#personas_bancos_id").val(r.personas_bancos_id);
            });

            $('#dni').prop('readonly', true);
            $('#fecha2').prop('readonly', true);

            id_contrato = '';
            id_contrato = r.id;

            $('#tipo_pago').change();

            if (r.tipo_pago == 1) {
                $('#fecha_decontado').val(r.cobros.fecha_pagado);
                $('#n_recibo').val(r.cobros.num_recibo);
                $('#pago').val();

                $('#fecha_decontado').prop('readonly', true);
                $('#n_recibo').prop('readonly', true);
                $('#pago').prop('readonly', true);

            }

            $('#contol').css('display', 'block');

            info_plan($('#planes_id').val(), $('#Num_benefe').val(), r.frecuencia_pagos_id);

            $('#id_contrato').append(r.id);
            $('#renovaciones').append(r.renovaciones);


            if (r.estatus != 'Activo') {
                $('#estatus_contrato').append(r.estatus + ' ');
            }
            if (r.anulado == 1) {
                $('#estatus_contrato').append('Contrato Anulado');

                $('#guardar', '#botonera').prop('disabled', true);

                $('#anular_contrato').prop('disabled', true);
                $('#bene').prop('disabled', true);

                a = true;
                c = true;
            } else {
                $('#estatus_contrato').append(r.estatus);
            }

            var msj_cobrando = "";
            if (r.cobrando == 1) {
                msj_cobrando = "Cobrado";
                $('#cobrado').append(msj_cobrando);

                c = true;
            } else {
                msj_cobrando = "Debitando";
                $('#cobrado').append(msj_cobrando);
            }


            p = true;
        },
    });

    $('#eliminar', '#botonera').remove();

    //fecha Inicio
    /* $("#fecha", $form).datepicker({
        maxDate: "+0d"
    }); */

    $('#fecha', $form).datepicker({
        defaultDate: "dd-mm-aaa",
        // defaultDate: "+1w",
        changeMonth: true,
        maxDate: '0',

        onClose: function(selectedDate) {
            var fecha_limite = moment(selectedDate, "DD/MM/YYYY").add(15, 'day').format("DD/MM/YYYY");
            console.log(fecha_limite);
            $("#fecha2").datepicker("option", "maxDate", fecha_limite);
        }
    });

    $('#fecha2', $form).datepicker({
        defaultDate: "DD/MM/YYYY",
        // defaultDate: "+1w",
        changeMonth: true,
        //maxDate: '0',
        onClose: function(selectedDate) {
            //$( "#fecha" ).datepicker( "option", "minDate", selectedDate );
        }
    });

    $('#ver_cobros').on('click', function() {
        var win = window.open(dire + '/contratos/analista/cobros/' + id_contrato, 'cobros', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });

    $('#historial').on('click', function() {
        var win = window.open(dire + '/contratos/analista/historial/' + id_contrato, 'historial', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });
    $('#renovacion').on('click', function() {
        var win = window.open(dire + '/contratos/opcontratos/renovacion/' + id_contrato + '/0', 'renovacion', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });

    $('#refinanciamiento').on('click', function() {
        var win = window.open($url + 'refinanciamiento/' + id_contrato, '', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });
    $('#asis_beneficiarios').on('click', function() {
        if (a == true) {
            alert('Aviso: el Contrato se encuenta Anulado');
            return false;
        }
        if (c == true) {
            alert('Aviso: el Contrato se encuenta cobrado');
            return false;
        }

        var win = window.open(dire + '/contratos/opcontratos/beneficiarios/' + id_contrato + '/0', 'beneficiarios', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });

    $('#fecha').change(function() {
        validar($(this).val());
    });

    //sucursal ---- vendedor
    $('#sucursal_id').change(function() {

        aplicacion.rellenar({
            vendedor_id: {}
        });

        aplicacion.selectCascada($(this).val(), 'vendedor_id', 'vendedores');
    });

    //persona --titular
    $("#btn-buscar-persona").on('click', function() {
        if (p == true) {
            return false;
        }
        $('#dni').blur();
    });

    $('#nombres').prop('disabled', true);

    $(".dni_beneficiario", $form).numeric({ min: 0 });

    var selectFormGroup = function(event) {
        event.preventDefault();

        var $selectGroup = $(this).closest('.input-group-select'),
            param = $(this).attr("href").replace("#", ""),
            concept = $(this).text();

        $selectGroup.find('.concept').text(concept);
        $selectGroup.find('.input-group-select-val').val(param);
    }

    var countFormGroup = function($form) {
        return $form.find('.form-group').length;
    };

    $('.cont-persona').on('click', '.dropdown-menu a', selectFormGroup);

    $('#dni', $form).numeric({ min: 0 })
        .blur(function() {
            buscar_cliente();
        });

    $('#goza').on('change', function() {
        if ($('#dni').val() == '') {
            return false;
        }

        if (p == true) {
            return false;
        }

        if ($('#goza').val() == 0) {
            $numero = 1;
            $('#Num_benefe').val($numero);
            $("#beneficiarios").append(tmpl("tmpl-demo2"));
            $('#dni_beneficiario').val(datos_persona.dni);
            $('#nombres_beneficiario').val(datos_persona.nombre);
            $('#sexo_beneficioario').val(datos_persona.sexo);
            $('#nacimiento_beneficiario').val(datos_persona.fecha_nacimiento);
            $('#edad').val(datos_persona.edad);
            $('#parentescos_beneficioario').val(datos_persona.parentescos);
        }

    });

    $('#tabla-beneficiarios').on('click', '.eliminar', function() {
        if (p == true) {
            return false;
        }
        $(this).parents('tr').remove();
        $numero = $numero - 1;
        $('#Num_benefe').val($numero);
    });

    $('#calcular').click(function() {

        info_plan($('#planes_id').val(), $('#Num_benefe').val(), '');
    });

    $('#tipo_pago').change(function() {

        $('#credito').css('display', 'none');
        $('#decontado').css('display', 'none');

        if ($(this).val() == 1) {
            $('#decontado').css('display', 'block');
        } else if ($(this).val() == 2) {
            $('#credito').css('display', 'block');
        } else {

            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
        }

    });

    $(".nacimiento_beneficiario").live().datepicker({
        maxDate: "+0d"
    });

    $("#fecha_decontado", $form).datepicker({
        maxDate: "+0d"
    });

    $('#agregar').on("click", function() {

        if (p == true) {
            return false;
        }

        if ($numero <= 9) {
            $numero++;
            $("#beneficiarios").append(tmpl("tmpl-demo2"));
            $('#Num_benefe').val($numero);
        } else {
            alert('no puedes Tener mas Beneficiarios');
        }

    });

    $('#tabla').on("click", "tbody tr", function() {
        aplicacion.buscar(this.id);
    });

    $form = aplicacion.form;

    tabla = datatable('#tabla', {
        ajax: $url + "datatable",
        columns: [
            { "data": "id", "name": "contratos.id" },
            { "data": "planilla", "name": "contratos.planilla" },
            { "data": "nombres", "name": "personas.nombres" },
            { "data": "dni", "name": "personas.dni" }
        ]
    });

    $('#anular_contrato').on('click', function() {

        if (id_contrato == '') {
            return false;
        }

        if (a == true) {
            return false;
        }

        swal({
                title: "",
                text: "Esta Seguro que Desea Anular el Contrato?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                cancelButtonClass: "btn-danger",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal({
                        title: "",
                        text: "Motivo de Anulacion",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        inputPlaceholder: ""
                    }, function(inputValue) {
                        if (inputValue === false) return false;
                        if (inputValue === "") {
                            swal.showInputError("Debe Introducir motivo de Anulacion");
                            return false
                        }
                        swal("", " Contrato Anulado", "success");
                        confirmacion(id_contrato, inputValue);


                    });
                } else {
                    swal("Cancelado", "", "error");
                }
            });

    });
});


function validar($dato) {

    $.ajax({
        url: $url + 'planes',
        type: 'POST',
        data: {
            'dato': $dato,
        },
        success: function(r) {

            aplicacion.rellenar({
                planes_id: {}
            });


            if (r.s == 's') {
                for (var i in r.planes_id) {
                    $('#planes_id').append('<option value="' + r.planes_id[i].id + '">' + r.planes_id[i].nombre + '</option>');
                }
            }
        }

    });
}

function buscar_cliente($callback) {
    $callback = $callback || function() {};

    var tipo_persona_id = $('#tipo_persona').val(),
        dni = $('#dni', $form).val();

    if (dni == '') {
        $('#tipo_persona_id').prop('disabled', false);
        $('#tipo_telefono_id').prop('disabled', false);
        $('#nombres').prop('disabled', false);
        $('#numero').prop('disabled', false);
        $('#cuenta').prop('disabled', false);
        aplicacion.limpiar();
        return false;
    }

    $.post($url + 'validar', {
        'tipo_persona_id': tipo_persona_id,
        'dni': dni,
    }, function(r) {
        if (r.s == 'n') {
            $('#nombres').val('');
            var win = window.open(dire + '/personas/persona/nuevo?tipo_persona_id=' + tipo_persona_id + '&dni=' + dni, 'nuevo', 'height=500,width=1000,resizable=yes,scrollbars=yes');
            win.focus();
            return false
        }

        aplicacion.rellenar({
            personas_bancos_id: {}
        });

        $('#nombres').val(r.persona.nombres).prop('disabled', true);

        for (var i in r.cuentas) {

            $('#personas_bancos_id').append('<option value="' + r.cuentas[i].id + '"> ' + r.cuentas[i].nombre + ' => ' + r.cuentas[i].cuenta + '</option>');
        }

        datos_persona = r.detalles;


        $callback();

    });
}

function info_plan($plan_id, $beneficiarios, $valor_f) {

    $.ajax({
        url: $url + 'infoplan',
        type: 'POST',
        data: {
            'plan_id': $plan_id,
            'beneficiarios': $beneficiarios,
            'frecuencia': $('#frecuencia').val()
        },
        success: function(r) {
            $('#meses').val(r.plan[0].meses_pagar);

            $('#porsentaje').val(r.plan_detalles.porsertaje_descuento + ' %');

            $('#contado').val(r.plan_detalles.contado2);

            $('#inicial').val(r.plan_detalles.inicial2);

            var cuotas = 0;

            switch ($('#frecuencia').val()) {
                case 'm':
                    // mensual
                    cuotas = 1 * r.plan[0].meses_pagar;

                    break;

                case 'q':
                    // mensaul
                    cuotas = 2 * r.plan[0].meses_pagar;
                    break;

                default:
                    // statements_def
                    cuotas = 0
                    break;
            }

            var $result = r.plan_detalles.total - r.plan_detalles.inicial;

            $('#n_giros').val(cuotas);

            $('#giro_bs').val(number_format($result / cuotas, 2)).maskMoney();

            $('#total').val(r.plan_detalles.total2);


            $('#cobros').html('').append('<option value="">- Seleccione</option>');

            aplicacion.rellenar({
                frecuencia_pagos_id: {}
            });

            for (var i in r.frecuencia) {
                $('#frecuencia_pagos_id').append('<option value="' + r.frecuencia[i].id + '">' + r.frecuencia[i].dias_str + '</option>');
            }
            if ($valor_f != '') {
                $('#frecuencia_pagos_id').val($valor_f);
            }

        }
    });
}

function buscar_template($contrato_id) {
    //funcion que busca los los datos de los formularios con template
    //---------------------------------------------------------------------
    //busca informacion de los bancos

    $.ajax({
        'url': $url + 'beneficiario',
        'data': {
            'id': $contrato_id
        },
        'method': 'GET',
        'success': function(r) {

            $("#beneficiarios").html('');
            if (r.datos.length) {
                $("#beneficiarios").append(tmpl('tmpl-demo5', r));
            } else {
                //$("#"+$button).click();
            }
        }
    });
    //---------------------------------------------------------------------	
}

function confirmacion($id_contrato, $porque) {
    $.ajax({
        url: dire + '/contratos/opcontratos/opanular',
        data: {
            id: $id_contrato,
            porque: $porque
        },
        type: 'GET',
        success: function(r) {
            //buscar(result);
            cerrar_windows(1, $id_contrato);
        }
    });
}

/**/
function cerrar_windows(op, $id_contrato) {
    if (op == 1) {
        aplicacion.limpiar();
        aplicacion.buscar($id_contrato);
    }
}