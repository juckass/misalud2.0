@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos Historial']])
    
@endsection
@section('content')
	<div class="row">
		{!! Form::open(['id' => 'submit_form', 'name' => 'formulario', 'method' => 'POST' ]) !!}
			<div class="row">
                <center> <h3 class="">Beneficiarios</h3></center>
                <div class="col-md-12">
                    <table border='1' class="table table-striped table-hover" id="tabla-beneficiarios">
                        <thead>
                            <tr>
                                <th>N° C.I</th>
                                <th>Nombres</th>
                                <th>Sexo</th>
                                <th>Fecha de Nacimiento</th>
                                <th>Parentesco</th>
                                <th style="width: 60px">
                                    <button id="agregar" type="button" class="btn green tooltips circule" data-container="body" data-placement="top">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="beneficiarios"></tbody>
                    </table>
                </div>
                			<input type="hidden" name="id" value="{{$id}}">
                <center><button type="button" id="guardar" class="btn btn-info">Guardar</button></center>
            </div> 
				
		{!! Form::close() !!}   
	</div>
@endsection
@push('css')
<style type="text/css" media="screen">
	hr{
		border-color: #000;
	}

	#credito{
		display: none;
	}

	#decontado{
		display: none;
	}
</style>	
@endpush

@push('js')
<script s type="text/javascript" charset="utf-8" async defer>
    
    $id 	 = '{{$id}}';
    buscar_template($id);
    
    $('#agregar').on("click", function() {
        $("#beneficiarios").append(tmpl("tmpl-demo2"));
      
    });
    $('#tabla-beneficiarios').on('click', '.eliminar', function() {
        $(this).parents('tr').remove();
    });
    
    
    $('#guardar').on('click', function() {

        $('#submit_form').ajaxSubmit({
            'url': dire + '/contratos/corporativo/beneficiariosguardar',
            'type': 'POST',
            'success': function(r) {
                aviso(r);
                if (r.donde == 1) {
                    cerrar_windows(2);
                }
                cerrar_windows(1);
            }
        });
        return false;
    });

    function buscar_template($contrato_id) {
        //funcion que busca los los datos de los formularios con template
        //---------------------------------------------------------------------
        //busca informacion de los bancos

        $.ajax({
            'url': dire + '/contratos/corporativo/beneficiario',
            'data': {
                'id': $contrato_id
            },
            'method': 'GET',
            'success': function(r) {

                $("#beneficiarios").html('');
                if (r.datos.length) {
                    $("#beneficiarios").append(tmpl('tmpl-demo5', r));
                } else {
                    //$("#"+$button).click();
                }
            }
        });
        //---------------------------------------------------------------------	
    }

</script>
<script type="text/x-tmpl" id="tmpl-demo2">
        <tr>
            <td>
                <input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="Dni" value="">
            </td> 
            <td>
               <input type="text" value=""  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos"> 
            </td>
            <td>
                <select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
                    <option value="m">Masculino</option>
                    <option value="f">Femenino</option>
                </select>
            </td>
            <td>
                <input  value="" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
            </td>
            <td>
                <select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
                    <option value="">seleccione</option>
                    @foreach($controller->parentescos() as $parentesco)
                        <option value="{{$parentesco->id}}">{{$parentesco->nombre}}</option>
                    @endforeach
                </select>
            </td>
            <td>
               <button type="button" class="btn btn-danger eliminar"><i class="fa fa-minus-circle" aria-hidden="true"></i></button> 
            </td> 
        </tr>
</script>
<script type="text/x-tmpl" id="tmpl-demo5">
    {% for (var i=0, file; file=o.datos[i]; i++) { %}
       <tr>
            <td>
                <input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="Dni" value="{%=file.dni%}">
            </td> 
            <td>
               <input type="text" value="{%=file.nombres%}"  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos"> 
            </td>
            <td>
                <select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
                    <option value="m" {% if (file.sexo == 'm') { %} selected="selected" {% } %}>Masculino</option>
                    <option value="f" {% if (file.sexo == 'f') { %} selected="selected" {% } %}>Femenino</option>
                </select>
            </td>
            <td>
                <input  value="{%=file.fecha_nacimiento%}" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
            </td>
            <td>
                <select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
                    <option value="">seleccione</option>
                    @foreach($controller->parentescos() as $parentesco)
                        <option value="{{$parentesco->id }}" {% if (file.parentesco_id == {{$parentesco->id}}) { %} selected="selected" {% } %}>
                                {{$parentesco->nombre}}
                            </option>
                         
                    @endforeach
                </select>
            </td>
            <td>
               <button type="button" class="btn btn-danger eliminar">
                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                </button> 
            </td>  
        </tr>
    {% } %}
</script>
@endpush
