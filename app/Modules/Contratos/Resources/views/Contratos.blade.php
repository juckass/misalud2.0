@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Contratos.',
        'columnas' => [
            'id' => '25',
            'Planilla' => '25',
            'Nombres' => '25',
            'Cendula' => '25'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
           {{-- panel de control --}}
			<div id="contol" style="display: none;"> 
				<div class="panel-group accordion" id="accordion3">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"> <h3 class="panel-title"><i class="fa fa-cogs" aria-hidden="true"></i>Panel de Control </h3> </a>
							</h4>
						</div>
						<div id="collapse_3_1" class="panel-collapse in">
							<div class="panel-body">
								<div id="">
									<center><h3>ID Contrato: <span id="id_contrato"></span></h3></center>
									<center><h3>Estatus Contrato: <span id="estatus_contrato"></span></h3></center>
									<center><h3>Estatus Cobrado: <span id="cobrado"></span></h3></center>
									<center><h3>Renovaciones: <span id="renovaciones"></span></h3></center>
									<br>
									<div class="btn-group btn-group-justified">

										<a id="renovacion" class="btn btn-default">Renovación</a>

										<a id="refinanciamiento" class="btn btn-primary">Refinanciamiento</a>

										
										<a id="asis_beneficiarios" class="btn btn-info">Gestion de Beneficiarios</a>

										<a id="anular_contrato" class="btn btn-danger"><span id="text-spam">Anular Contrato</span></a>
					
									</div>
									<br>
									
									<div class="btn-group btn-group-justified">
									
									<a id="ver_cobros" class="btn green">Ver Cobros del Contratos</a>

									<a id="historial" class="btn dark">Ver Historal del Contrato</a>

									</div>

								</div>   
							</div>
						</div>
					</div>
				</div>
			</div>
			{{--  fin de panel de control  --}}
			{{--  datos Generales  --}}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Datos Generales</h3>
				</div>
				<div class="panel-body">
					<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<label for="min">Fecha del Contrato</label>
						<input id="fecha" name="inicio" class="form-control" type="text" placeholder=""  required="date" />
					</div>
					{{ Form::bsSelect('sucursal_id', $controller->sucursales(), '', [
						'label'    => 'Sucursal',
						'required' => 'required'
					]) }}
					{{ Form::bsSelect('vendedor_id', [], '', [
						'label'    => 'Vendedor',
						'required' => 'required'
					]) }}							
					{{ Form::bsNumber('planilla', '',[
						'label'    => 'N° de Planilla',
						'required' => 'required'
					]) }}    
				</div>
			</div>
			{{--  fin datos generales  --}}
			{{--  titular  --}}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Titular</h3>
				</div>
				<div class="panel-body">
					<div class="form-group col-md-3 cont-persona">
					<label for="nombres">Cedula:</label>
					<div class="form-group multiple-form-group input-group">
					
						{{-- <div class="input-group-btn input-group-select">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="concept">-</span><span class="caret"></span></button>
							<ul class="dropdown-menu" role="menu">
								@foreach ($controller->tipo_persona() as $id => $tipo)
									<li><a href="{{ $id }}">{{ $tipo }}</a></li>
								@endforeach
							</ul>

							<input id="tipo_persona" name="tipo_persona" class="input-group-select-val" type="hidden" />
						</div> --}}

							{{ Form::text('dni', '', [
								'id'          => 'dni',
								'class'       => 'form-control',
								'placeholder' => '',
								'required' => 'required'

							]) }}
							<span class="input-group-btn">
								<button id="btn-buscar-persona" type="button" class="btn btn-primary btn-add"><i class="fa fa-search"></i></button>
							</span>
					</div>
				</div>
				{{ Form::bsText('nombres', '', [
					'class'       => 'form-control',
					'label'       => 'Nombres y Apellidos:',
					'class_cont'  => 'col-md-6',
					'placeholder' => '',
					'required' => 'required'
				]) }}
				{{ Form::bsSelect('goza', [
					'0'=>'Si',
					'1'=>'No'
					], '', [
						'label'    => 'Goza de los beneficios',
						'required' => 'required'
				]) }}   
            	</div>
			</div>
			{{--  fin titular  --}}
			{{--  beneficiarios  --}}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Beneficiarios</h3>
				</div>
				<div class="panel-body">
					<div class="col-md-12">
						<table border='1' class="table table-striped table-hover" id="tabla-beneficiarios">
							<thead>
								<tr>
									<th>DNI</th>
									<th>Nombres</th>
									<th>Sexo</th>
									<th>Fecha de Nacimiento</th>
									<th>Parentesco</th>
									<th style="width: 60px">
										<button id="agregar" type="button" class="btn green tooltips circule" data-container="body" data-placement="top">
											<i class="fa fa-plus" aria-hidden="true"></i>
										</button>
									</th>
								</tr>
							</thead>
							<tbody id="beneficiarios"></tbody>
						</table>
					</div>
				</div>
			</div>
			{{--  fin beneficiarios  --}}
			{{--  Calculos de Planes  --}}
			<div class="panel panel-primary" id="info-bookie" style="display: none;">
				<div class="panel-heading">
					<h3 class="panel-title">Informacion del Plan</h3>
				</div>
				<div class="panel-body">
					<div class="alert alert-dismissible alert-danger">
						<strong>Aviso!</strong>
						<P>La informacion Suministrada de este contrato viene del ultimo respaldo de bookie</P>
					</div>
					<center><table id="tabla" class="table table-striped table-hover table-bordered tables-text">
						<thead>
							<tr>
								<th style="width: 14.28%; text-align: center;">Tipo de Pago</th>
								<th style="width: 14.28%; text-align: center;">Frecuencia de Pago</th>
								<th style="width: 14.28%; text-align: center;">Cantidad Cuotas</th>
								<th style="width: 14.28%; text-align: center;">Cuotas(Bs)</th>
								<th style="width: 14.28%; text-align: center;">Inicial</th>
								<th style="width: 14.28%; text-align: center;">Cantidad giros</th>
								<th style="width: 14.28%; text-align: center;">giros(Bs)</th>
								<th style="width: 14.28%; text-align: center;">Total Contrato(Bs)</th>
								<th style="width: 14.28%; text-align: center;">Total Cancelado(Bs)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><span id="inf-tipo"></span></td>
								<td><span id="inf-frecuencia"></span></td>
								<td><span id="inf-cuotas"></span></td>
								<td><span id="inf-cuotas2"></span></td>
								<td><span id="inf-inicial"></span></td>
								<td><span id="can-giros"></span></td>
								<td><span id="giros"></span></td>
								<td><span id="inf-total"></span></td>
								<td><span id="inf-total-pagado"></span></td>
							</tr>
						</tbody>
					</table></center>

				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Calculos de Planes</h3>
				</div>
				<div class="panel-body">
					{{ Form::bsSelect('planes_id', [], '', [
							'label'    => 'Planes',
							'required' => 'required'
					]) }}

					{{ Form::bsText('Num_benefe','', [
							'label'    => 'N° de Beneficiarios',
							'required' => 'required',
							'readonly' => 'true'
					]) }}

					{{ Form::bsSelect('frecuencia', [
							'm'=>'Mensual',
							'q'=>'Quincenal'
							], '', [
							'label'    => 'Frecuencia de Pago',
							'required' => 'required'
					]) }}
					{{ Form::bsText('meses', '', [
						'class'       => 'form-control',
						'label'       => 'Meses para Pagar',
						'disabled' => 'disabled'
					]) }}  
					<button type="button" id="calcular"  class="btn btn-info col-md-12"> Calcular</button>
					<div class="col-md-12"></div>
					<div class="col-md-12">
												
						<center>                    
							<span class="caption-subject font-red bold uppercase"> De Contado</span>
						</center>  
					</div>
																	
					<div class="table-scrollable">
						<table id="tabla-plan" class="table table-striped table-hover table-bordered">
								<thead>
									<tr>
										<th>(-) %</th>
										<th>Bs</th>
									</tr>
								</thead>
								<tbody>
								<tr>
									<td><input name="porsentaje" id="porsentaje" class="form-control" type="text" disabled="" /></td>
									<td><input name="contado" id="contado" class="form-control" type="text" disabled=""/></td>
							</tr>
													
								</tbody>
						</table>
					</div>
					<center>
						<span class="caption-subject font-red bold uppercase"> Credito</span>
					</center>
					<div class="table-scrollable">
						<table id="tabla-plan" class="table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th>Inicial Credito</th> 
									<th>N. de Cuotas</th>
									<th>Monto por Cuota (Bs)</th>
									<th>Total 1 A&ntilde;o (Bs)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input name="" id="inicial" class="form-control" type="text" disabled=""/></td>
									<td><input name="" id="n_giros" class="form-control" type="text" disabled=""/></td>
									<td><input name="" id="giro_bs" class="form-control" type="text" disabled=""/></td>
									<td><input name="" id="total" class="form-control" type="text" disabled=""/></td>
								</tr>
													
							</tbody>
						</table>
					</div>   
				</div>
			</div>
			{{--  fin Calculos de Planes  --}}
			{{--  Metodos de Pagos  --}}
			
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Metodos de Pagos</h3>
				</div>
				<div class="panel-body">
				
					{{ Form::bsSelect('tipo_pago', [
						'1'=>'De Contado',
						'2'=>'Credito'                  
					], '', [
					'label'    => 'Tipo de Pago',
					'required' => 'required'
					]) }}

					<div class="col-md-12"></div>

					<div class="col-md-12" id="credito" style="display: none;">

						{{ Form::bsSelect('personas_bancos_id', [], '', [
							'label'    => 'Número de Cuenta',
							'required' => 'required',
							'class_cont' => 'col-md-4 col-sm-6 col-xs-12'
						]) }}

						<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<label for="min">Fecha de Cobro de Inicial</label>
							<input id="fecha2" name="fecha_incial" class="form-control" type="text" placeholder="" required="" />
						</div>

						{{ Form::bsSelect('frecuencia_pagos_id', [], '', [
								'label'    => 'Dias de Cobros',
								'required' => 'required',
								'class_cont' => 'col-md-4 col-sm-6 col-xs-12'
						]) }}
					</div>

					<div class="col-md-12" id="decontado" style="display: none;" >
						{{ Form::bsText('fecha_decontado', '', [
							'label'         => 'Fecha',
							'placeholder'   => 'Fecha',
							'required'      => 'required',
							'class_cont'    => 'col-md-4 col-sm-6 col-xs-12'
						]) }}
						
						{{ Form::bsNumber('n_recibo', '',[
							'label'    => 'N° de Recibo',
							'required' => 'required'
						]) }} 

						{{ Form::bsSelect('pago', [
							'0'=>'Por Tarjeta',
							'1'=>'Transferencia',
							'2'=>'Efectivo'                 
							], '', [
							'label'    => 'Tipo de Pago',
							'required' => 'required'
						]) }}
					
					</div>
				</div>
			</div>
			{{--  fin Metodos de Pagos  --}}
        {!! Form::close() !!}
    </div>
@endsection
@push('js')
<script s type="text/javascript" charset="utf-8" async defer>
    
    $super = '{{auth()->user()->super}}';

</script>
<script type="text/x-tmpl" id="tmpl-demo2">
        <tr>
            <td>
                <input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="Dni" value="">
            </td> 
            <td>
               <input type="text" value=""  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos"> 
            </td>
            <td>
                <select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
                    <option value="m">Masculino</option>
                    <option value="f">Femenino</option>
                </select>
            </td>
            <td>
                <input  value="" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
            </td>
            <td>
                <select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
                    <option value="">seleccione</option>
                    @foreach($controller->parentescos() as $parentesco)
                        <option value="{{$parentesco->id}}">{{$parentesco->nombre}}</option>
                    @endforeach
                </select>
            </td>
            <td>
               <button type="button" class="btn btn-danger eliminar"><i class="fa fa-minus-circle" aria-hidden="true"></i></button> 
            </td> 
        </tr>
</script>
<script type="text/x-tmpl" id="tmpl-demo5">
    {% for (var i=0, file; file=o.datos[i]; i++) { %}
       <tr>
            <td>
                <input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="Dni" value="{%=file.dni%}">
            </td> 
            <td>
               <input type="text" value="{%=file.nombres%}"  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos"> 
            </td>
            <td>
                <select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
                    <option value="m" {% if (file.sexo == 'm') { %} selected="selected" {% } %}>Masculino</option>
                    <option value="f" {% if (file.sexo == 'f') { %} selected="selected" {% } %}>Femenino</option>
                </select>
            </td>
            <td>
                <input  value="{%=file.fecha_nacimiento%}" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
            </td>
            <td>
                <select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
                    <option value="">seleccione</option>
                    @foreach($controller->parentescos() as $parentesco)
                        <option value="{{$parentesco->id }}" {% if (file.parentesco_id == {{$parentesco->id}}) { %} selected="selected" {% } %}>
                                {{$parentesco->nombre}}
                            </option>
                         
                    @endforeach
                </select>
            </td>
            <td>
               <button type="button" class="btn btn-danger eliminar">
                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                </button> 
            </td>  
        </tr>
    {% } %}
</script>

@endpush
@push('css')
<style type="text/css">
#tabla-plan thead{
    background-color: white;
}
.form-body{
    overflow: hidden;
}
.tcentro{
    text-align: center;
    vertical-align: middle !important;
}

#tabla-plan input{
    width: 100%;
}
#ui-datepicker-div{
    z-index: 3 !important;
}
</style>
@endpush
