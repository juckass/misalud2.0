@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Parentesco']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Parentesco.',
        'columnas' => [
            'Nombre' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Parentesco->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection