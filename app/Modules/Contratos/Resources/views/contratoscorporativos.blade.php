@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Contratos.',
        'columnas' => [
            'id' => '25',
            'Planilla' => '25',
            'Nombres' => '25',
            'Cendula' => '25'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
           {{-- panel de control --}}
			<div id="contol" style="display: none;"> 
				<div class="panel-group accordion" id="accordion3">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"> <h3 class="panel-title"><i class="fa fa-cogs" aria-hidden="true"></i>Panel de Control </h3> </a>
							</h4>
						</div>
						<div id="collapse_3_1" class="panel-collapse in">
							<div class="panel-body">
								<div id="">
									<center><h3>ID Contrato: <span id="id_contrato"></span></h3></center>
									<center><h3>Estatus Contrato: <span id="estatus_contrato"></span></h3></center>
									<center><h3>Estatus Cobrado: <span id="cobrado"></span></h3></center>
									<center><h3>Renovaciones: <span id="renovaciones"></span></h3></center>
									<br>
									<div class="btn-group btn-group-justified">

										<a id="renovacion" class="btn btn-default">Renovación</a>

										<a id="refinanciamiento" class="btn btn-primary">Refinanciamiento</a>

										
										<a id="asis_beneficiarios" class="btn btn-info">Gestion de Beneficiarios</a>

										<a id="anular_contrato" class="btn btn-danger"><span id="text-spam">Anular Contrato</span></a>
					
									</div>
									<br>
									
									<div class="btn-group btn-group-justified">
									
									<a id="ver_cobros" class="btn green">Ver Cobros del Contratos</a>

									<a id="historial" class="btn dark">Ver Historal del Contrato</a>

									</div>

								</div>   
							</div>
						</div>
					</div>
				</div>
			</div>
			{{--  fin de panel de control  --}}
			{{--  datos Generales  --}}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Datos Generales</h3>
				</div>
				<div class="panel-body">
					<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<label for="min">Fecha del Contrato</label>
						<input id="fecha" name="inicio" class="form-control" type="text" placeholder=""  required="date" />
					</div>
					<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
						<label for="min">Fecha de Vencimiento</label>
						<input id="vencimiento" name="vencimiento" class="form-control" type="text" placeholder=""  required="date" />
					</div>
					{{ Form::bsSelect('sucursal_id', $controller->sucursales(), '', [
						'label'    => 'Sucursal',
						'required' => 'required'
					]) }}
					{{ Form::bsSelect('vendedor_id', [], '', [
						'label'    => 'Vendedor',
						'required' => 'required'
					]) }}							
					{{ Form::bsNumber('planilla', '',[
						'label'    => 'N° de Planilla',
						'required' => 'required'
					]) }}    
				</div>
			</div>
			{{--  fin datos generales  --}}
			{{--  titular  --}}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Titular</h3>
				</div>
				<div class="panel-body">
					<div class="form-group col-md-3 cont-persona">
					<label for="nombres">Cedula:</label>
					<div class="form-group multiple-form-group input-group">
					
						{{-- <div class="input-group-btn input-group-select">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="concept">-</span><span class="caret"></span></button>
							<ul class="dropdown-menu" role="menu">
								@foreach ($controller->tipo_persona() as $id => $tipo)
									<li><a href="{{ $id }}">{{ $tipo }}</a></li>
								@endforeach
							</ul>

							<input id="tipo_persona" name="tipo_persona" class="input-group-select-val" type="hidden" />
						</div> --}}

							{{ Form::text('dni', '', [
								'id'          => 'dni',
								'class'       => 'form-control',
								'placeholder' => '',
								'required' => 'required'

							]) }}
							<span class="input-group-btn">
								<button id="btn-buscar-persona" type="button" class="btn btn-primary btn-add"><i class="fa fa-search"></i></button>
							</span>
					</div>
				</div>
				{{ Form::bsText('nombres', '', [
					'class'       => 'form-control',
					'label'       => 'Nombres y Apellidos:',
					'class_cont'  => 'col-md-6',
					'placeholder' => '',
					'required' => 'required'
				]) }}
				{{ Form::bsSelect('tipo_contrato', [
					'1'=>'Corporativo',
					'2'=>'intercambio'
					], '', [
						'label'    => 'Tipo de Contrato',
						'required' => 'required'
				]) }}   
            	</div>
			</div>
			{{--  fin titular  --}}
			{{--  beneficiarios  --}}
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Beneficiarios</h3>
				</div>
				<div class="panel-body">
					<div class="col-md-12" id="_benes" style="display: none;">
					<button type="button" id="benes" class="btn btn-info col-md-12"  >Ver Beneficiarios</button>
					<br><br><br>
					</div>
					
					<div class="col-md-6">
					<a href="{{url('public/formatos/formato_caga_beneficiarios.xlsx')}}"><button type="button" id="calcular"  class="btn btn-info col-md-12"> Descargar formato</button></a>
					</div>
					<div class="col-md-6">
											
						<input id="upload" name="subir" type="file"/>
						<button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button  col-md-12" data-style="expand-right">
							<span class="ladda-label">
								<i class="icon-arrow-right"></i> Carga archivo Excel
							</span>
						</button>		
						
					</div>
				</div>
			</div>
			{{--  fin beneficiarios  --}}
			{{--  Calculos de Planes  --}}

			<div class="panel panel-primary" id="info-bookie" style="display: none;">
				<div class="panel-heading">
					<h3 class="panel-title">Informacion del Plan</h3>
				</div>
				<div class="panel-body">
					<div class="alert alert-dismissible alert-danger">
						<strong>Aviso!</strong>
						<P>La informacion Suministrada de este contrato viene del ultimo respaldo de bookie</P>
					</div>
					<center><table id="tabla" class="table table-striped table-hover table-bordered tables-text">
						<thead>
							<tr>
								<th style="width: 14.28%; text-align: center;">Tipo de Pago</th>
								<th style="width: 14.28%; text-align: center;">Frecuencia de Pago</th>
								<th style="width: 14.28%; text-align: center;">Cantidad Cuotas</th>
								<th style="width: 14.28%; text-align: center;">Cuotas(Bs)</th>
								<th style="width: 14.28%; text-align: center;">Inicial</th>
								<th style="width: 14.28%; text-align: center;">Cantidad giros</th>
								<th style="width: 14.28%; text-align: center;">giros(Bs)</th>
								<th style="width: 14.28%; text-align: center;">Total Contrato(Bs)</th>
								<th style="width: 14.28%; text-align: center;">Total Cancelado(Bs)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><span id="inf-tipo"></span></td>
								<td><span id="inf-frecuencia"></span></td>
								<td><span id="inf-cuotas"></span></td>
								<td><span id="inf-cuotas2"></span></td>
								<td><span id="inf-inicial"></span></td>
								<td><span id="can-giros"></span></td>
								<td><span id="giros"></span></td>
								<td><span id="inf-total"></span></td>
								<td><span id="inf-total-pagado"></span></td>
							</tr>
						</tbody>
					</table></center>

				</div>
			</div>

			<div class="" id="_planes" style="display: none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Metodos de Pagos</h3>
					</div>
					<div class="panel-body">
					
						{{ Form::bsSelect('tipo_pago', [
							'1'=>'De Contado',
							'2'=>'Credito'                  
						], '', [
						'label'    => 'Tipo de Pago',
						'required' => 'required'
						]) }}

						<div class="col-md-12"></div>

						<div class="col-md-12" id="credito" style="display: none;">

							{{ Form::bsNumber('inicial', '',[
								'label'    => 'Inical ',
								'required' => 'required'
							]) }}
							
							{{ Form::bsNumber('total_credito', '',[
								'label'    => 'total del Contrato ',
								'required' => 'required'
							]) }}

							{{ Form::bsSelect('frecuencia', [
								'm'=>'Mensual',
								'q'=>'Quincenal'
								], '', [
								'label'    => 'Frecuencia de Pago',
								'required' => 'required'
							]) }}

							{{ Form::bsNumber('cuotas', '',[
								'label'    => ' valor de Cuotas',
								'required' => 'required'
							]) }}
							{{ Form::bsNumber('meses', '',[
								'label'    => ' Meses para pagar',
								'required' => 'required'
							]) }}

							{{ Form::bsSelect('personas_bancos_id', [], '', [
								'label'    => 'Número de Cuenta',
								'required' => 'required',
								
							]) }}

							<div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
								<label for="min">Fecha de Cobro de Inicial</label>
								<input id="fecha2" name="fecha_incial" class="form-control" type="text" placeholder="" required="" />
							</div>

							{{ Form::bsSelect('frecuencia_pagos_id', [], '', [
									'label'    => 'Dias de Cobros',
									'required' => 'required',
									
							]) }}
						</div>

						<div class="col-md-12" id="decontado" style="display: none;" >
							
							{{ Form::bsNumber('total_contado', '',[
								'label'    => 'N° total a pagar ',
								'required' => 'required'
							]) }}

							{{ Form::bsText('fecha_decontado', '', [
								'label'         => 'Fecha',
								'placeholder'   => 'Fecha',
								'required'      => 'required',
							]) }}
							
							{{ Form::bsNumber('n_recibo', '',[
								'label'    => 'N° de Recibo',
								'required' => 'required'
							]) }} 

							{{ Form::bsSelect('pago', [
								'0'=>'Por Tarjeta',
								'1'=>'Transferencia',
								'2'=>'Efectivo'                 
								], '', [
								'label'    => 'Tipo de Pago',
								'required' => 'required'
							]) }}
						</div>
					</div>
				</div>
			</div>
			<div class="" id="combenio" style="display: none;">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Terminos del Intercambio</h3>
					</div>
					<div class="panel-body">
					<div class="form-group col-sm-12">
						
						<textarea placeholder="Terminos del Intercambio" cont_class="col-sm-12" id="observaciones" class="form-control" rows="6" name="observaciones" cols="50"></textarea>
					</div>
						
					</div>
				</div>
			</div>


        {!! Form::close() !!}
    </div>
@endsection
@push('js')
	<script s type="text/javascript" charset="utf-8" async defer>
		
		$super = '{{auth()->user()->super}}';

	</script>
	<script type="text/x-tmpl" id="tmpl-demo2">
			<tr>
				<td>
					<input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="Dni" value="">
				</td> 
				<td>
				<input type="text" value=""  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos"> 
				</td>
				<td>
					<select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
						<option value="m">Masculino</option>
						<option value="f">Femenino</option>
					</select>
				</td>
				<td>
					<input  value="" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
				</td>
				<td>
					<select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
						<option value="">seleccione</option>
						@foreach($controller->parentescos() as $parentesco)
							<option value="{{$parentesco->id}}">{{$parentesco->nombre}}</option>
						@endforeach
					</select>
				</td>
				<td>
				<button type="button" class="btn btn-danger eliminar"><i class="fa fa-minus-circle" aria-hidden="true"></i></button> 
				</td> 
			</tr>
	</script>
	<script type="text/x-tmpl" id="tmpl-demo5">
		{% for (var i=0, file; file=o.datos[i]; i++) { %}
		<tr>
				<td>
					<input type="text"  id="dni_beneficiario" class="form-control" name="dni_beneficiario[]" placeholder="Dni" value="{%=file.dni%}">
				</td> 
				<td>
				<input type="text" value="{%=file.nombres%}"  id="nombres_beneficiario" class="form-control" name="nombres_beneficiario[]" placeholder="Nombres y Apellidos"> 
				</td>
				<td>
					<select name="sexo_beneficiario[]"  id="sexo_beneficiario" class="form-control">
						<option value="m" {% if (file.sexo == 'm') { %} selected="selected" {% } %}>Masculino</option>
						<option value="f" {% if (file.sexo == 'f') { %} selected="selected" {% } %}>Femenino</option>
					</select>
				</td>
				<td>
					<input  value="{%=file.fecha_nacimiento%}" class="form-control nacimiento_beneficiario" id="nacimiento_beneficiario" type="text" name='nacimiento_beneficiario[]' placeholder="" />
				</td>
				<td>
					<select name="parentescos_beneficioario[]" id="parentescos_beneficioario" class="form-control">
						<option value="">seleccione</option>
						@foreach($controller->parentescos() as $parentesco)
							<option value="{{$parentesco->id }}" {% if (file.parentesco_id == {{$parentesco->id}}) { %} selected="selected" {% } %}>
									{{$parentesco->nombre}}
								</option>
							
						@endforeach
					</select>
				</td>
				<td>
				<button type="button" class="btn btn-danger eliminar">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</button> 
				</td>  
			</tr>
		{% } %}
	</script>
@endpush
@push('css')
<style type="text/css">
#tabla-plan thead{
    background-color: white;
}
.form-body{
    overflow: hidden;
}
.tcentro{
    text-align: center;
    vertical-align: middle !important;
}

#tabla-plan input{
    width: 100%;
}
#ui-datepicker-div{
    z-index: 3 !important;
}
</style>
@endpush
