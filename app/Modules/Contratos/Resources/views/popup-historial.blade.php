@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Contratos Historial']])
    
@endsection
@section('content')
	<div class="row"> 
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Historial del Contrato</h3>
			</div>
			<div class="panel-body">
				<table class="table table-striped table-hover ">	
					<thead>
						<tr>
							<th>#</th>
							<th>fecha</th>
							<th>Operacion</th>
							<th>Responsable</th>
							<th>Planilla</th>
						</tr>
					</thead>
					<tbody>
						<?php $num2 = 1;?>
						@foreach($contratos_detalles as $contrato_detalle )
							<tr>
								<td>{{$num2++}}</td>
								
								<?php
							        $fecha_n = \Carbon\Carbon::parse($contrato_detalle->created_at)->format('d/m/Y');

								?>
								
								<td>{{$fecha_n}}</td>
								<td>{{$contrato_detalle->operacion}}</td>
								<td>{{$contrato_detalle->nombre}}-{{$contrato_detalle->dni}} {{$contrato_detalle->nombres}}</td>
								<td>{{$contrato_detalle->planilla}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
