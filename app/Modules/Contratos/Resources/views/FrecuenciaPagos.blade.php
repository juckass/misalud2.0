@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Frecuencia Pagos']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar FrecuenciaPagos.',
        'columnas' => [
            'Frecuencia' => '33.333333333333',
		'Dias' => '33.333333333333',
		'Dias Str' => '33.333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $FrecuenciaPagos->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection