var aplicacion,
$form,
$numero = 0, //numero de beneficiarios
datos_persona = [],
p = false,
id_contrato = '',
a = false,
id = '',
c = false;
$(function() {
    aplicacion = new app('formulario', {
        'limpiar': function() {
            $('#info-bookie').css('display', 'none');
            $('#meses').val('');
            $('#porsentaje').val('');
            $('#contado').val('');
            $('#inicial').val('');
            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
            $('#contol').css('display', 'none');
            $('#goza').val('');

            $('#guardar', '#botonera').prop('disabled', false);
            $('#dni').prop('readonly', false);
            $('#fecha2').prop('readonly', false);
            $('#anular_contrato').prop('disabled', false);
            $('#bene').prop('disabled', false);

            $('#fecha_decontado').prop('readonly', false);
            $('#n_recibo').prop('readonly', false);
            $('#pago').prop('readonly', false);

            $("#beneficiarios").html('');
            $("#id_contrato").html('');
            $("#estatus_contrato").html('');
            $("#cobrado").html('');
            $("#renovaciones").html('');
            id_contrato = '';
            $numero = 0;
            p = false; // nose
            a = false; // anulodo
            c = false; // cobrado
            $id = '';
            
            $('#_benes').css('display', 'none');
            tabla.ajax.reload();
        },
        'buscar': function(r) {
            $('#info-bookie').css('display', 'none');
            id_contrato = r.id;
            $("#id_contrato").html('');
            $("#estatus_contrato").html('');
            $("#renovaciones").html('');
            $("#cobrado").html('');
            
            //buscar_template(r.id);
           
        
            if ($('#tipo_contrato').val() == 1) {
                $('#_planes').css('display', 'block');
                $('#combenio').css('display', 'none');
            }else{
                $('#_planes').css('display', 'none');
                $('#combenio').css('display', 'block');
            }
            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
    
            $('#_benes').css('display', 'block');
            if ($('#tipo_pagos').val() == 1) {
                $('#decontado').css('display', 'block');
                $('#total_contado').val(r.total_contrato);
            } else if ($('#tipo_pago').val() == 2) {
                $('#credito').css('display', 'block');
                $('#total_credito').val(r.total_contrato);
            } else {
                $('#credito').css('display', 'none');
                $('#decontado').css('display', 'none');
            }

            buscar_cliente(function() {
                $("#personas_bancos_id").val(r.personas_bancos_id);
            });

            frecuencia($('#frecuencia').val(), r.frecuencia_pagos_id);
        
            p = true;
        }
    });

   $form = aplicacion.form;

    tabla = datatable('#tabla', {
        ajax: $url + "datatable",
        columns: [
            { "data": "id", "name": "contratos.id" },
            { "data": "planilla", "name": "contratos.planilla" },
            { "data": "nombres", "name": "personas.nombres" },
            { "data": "dni", "name": "personas.dni" }
        ]
    });

    $('#tabla').on("click", "tbody tr", function() {
        aplicacion.buscar(this.id);
    });

    $("#subir").on('click', function(e){
	    e.preventDefault();
	    $("#upload:hidden").trigger('click');
    });
    
    $('#eliminar', '#botonera').remove();

    $('#fecha', $form).datepicker({
        defaultDate: "dd-mm-aaa",
        // defaultDate: "+1w",
        changeMonth: true,
        maxDate: '0',

        onClose: function(selectedDate) {
            var fecha_limite = moment(selectedDate, "DD/MM/YYYY").add(1, 'year').format("DD/MM/YYYY");
            $('#vencimiento', $form).val(fecha_limite);
        }
    });

    
    $('#fecha2', $form).datepicker({
        defaultDate: "DD/MM/YYYY",
        // defaultDate: "+1w",
        changeMonth: true,
        //maxDate: '0',
        onClose: function(selectedDate) {
            //$( "#fecha" ).datepicker( "option", "minDate", selectedDate );
        }
    });
    

    $('#vencimiento', $form).datepicker({
        defaultDate: "DD/MM/YYYY",
        // defaultDate: "+1w",
        changeMonth: true,
        //maxDate: '0',
        onClose: function(selectedDate) {
            //$( "#fecha" ).datepicker( "option", "minDate", selectedDate );
        }
    });
    $('#benes').on('click',function(){
        var win = window.open(dire + '/contratos/corporativo/bene/' + id_contrato, 'renovacion', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        win.focus();
        return false
    });

    $('#sucursal_id').change(function() {

        aplicacion.rellenar({
            vendedor_id: {}
        });

        aplicacion.selectCascada($(this).val(), 'vendedor_id', 'vendedores');
    });
    $('#frecuencia').change(function() {
        frecuencia($(this).val(), '');
      
    });

    $("#btn-buscar-persona").on('click', function() {
        if (p == true) {
            return false;
        }
        $('#dni').blur();
    });

    $('#tipo_pago').change(function() {
        
        $('#credito').css('display', 'none');
        $('#decontado').css('display', 'none');

        if ($(this).val() == 1) {
            $('#decontado').css('display', 'block');
        } else if ($(this).val() == 2) {
            $('#credito').css('display', 'block');
        } else {
            $('#credito').css('display', 'none');
            $('#decontado').css('display', 'none');
        }

    });

    $('#nombres').prop('disabled', true);
    var selectFormGroup = function(event) {
        event.preventDefault();

        var $selectGroup = $(this).closest('.input-group-select'),
            param = $(this).attr("href").replace("#", ""),
            concept = $(this).text();

        $selectGroup.find('.concept').text(concept);
        $selectGroup.find('.input-group-select-val').val(param);
    }

    var countFormGroup = function($form) {
        return $form.find('.form-group').length;
    };

    $('.cont-persona').on('click', '.dropdown-menu a', selectFormGroup);

    $('#dni', $form).numeric({ min: 0 })
    .blur(function() {
        buscar_cliente();
    });

    $('#tipo_contrato').on('change', function() {

        if ($('#tipo_contrato').val() == 1) {
            $('#_planes').css('display', 'block');
            $('#combenio').css('display', 'none');
        }else{
            $('#_planes').css('display', 'none');
            $('#combenio').css('display', 'block');

        }
       
    });


    $("#fecha_decontado", $form).datepicker({
        maxDate: "+0d"
    });
});


function buscar_cliente($callback) {
    $callback = $callback || function() {};

    var tipo_persona_id = $('#tipo_persona').val(),
        dni = $('#dni', $form).val();

    if (dni == '') {
        $('#tipo_persona_id').prop('disabled', false);
        $('#tipo_telefono_id').prop('disabled', false);
        $('#nombres').prop('disabled', false);
        $('#numero').prop('disabled', false);
        $('#cuenta').prop('disabled', false);
        aplicacion.limpiar();
        return false;
    }

    $.post($url + 'buscarpersona', {
        'tipo_persona_id': tipo_persona_id,
        'dni': dni,
    }, function(r) {
        if (r.s == 'n') {
            $('#nombres').val('');
            var win = window.open(dire + '/personas/persona/nuevo?tipo_persona_id=' + tipo_persona_id + '&dni=' + dni, 'nuevo', 'height=500,width=1000,resizable=yes,scrollbars=yes');
            win.focus();
            return false
        }

        aplicacion.rellenar({
            personas_bancos_id: {}
        });

        $('#nombres').val(r.persona.nombres).prop('disabled', true);

        for (var i in r.cuentas) {

            $('#personas_bancos_id').append('<option value="' + r.cuentas[i].id + '"> ' + r.cuentas[i].nombre + ' => ' + r.cuentas[i].cuenta + '</option>');
        }

        datos_persona = r.detalles;

        $callback();

    });
}
function frecuencia(dia, $valor_f){

    $.ajax({
        url: $url + 'frecuencia',
        data: {
            id: dia,
        },
        type: 'POST',
        success: function(r) {
            aplicacion.rellenar({
                frecuencia_pagos_id: {}
            });
            
             for (var i in r.frecuencia) {
                $('#frecuencia_pagos_id').append('<option value="' + r.frecuencia[i].id + '">' + r.frecuencia[i].dias_str + '</option>');
            }
            if ($valor_f != '') {
                $('#frecuencia_pagos_id').val($valor_f);
            }  
        }
    });
   
}

function confirmacion($id_contrato, $porque, $op) {
    $.ajax({
        url: $url + $op,
        data: {
            id: $id_contrato,
            porque: $porque
        },
        type: 'GET',
        success: function(r) {
            //buscar(result);
            cerrar_windows(1, $id_contrato);
        }
    });
}

