<?php

namespace App\Modules\Contratos\Models;



use Illuminate\Database\Eloquent\Model;


class Beneficiarios extends Model
{
    protected $table = 'beneficiarios';
    protected $fillable = ["contratos_id","personas_id","parentesco_id","estatus"];    
}