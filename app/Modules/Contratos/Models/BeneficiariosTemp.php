<?php

namespace App\Modules\Contratos\Models;

use App\Modules\Base\Models\Modelo;

use Illuminate\Database\Eloquent\Model;

class BeneficiariosTemp extends Model
{
    protected $table = 'beneficiarios_temp';
    protected $fillable = ["dni","nombre","sexo","solicitud_beneficiarios_temp_id","parentesco_id","fecha_nacimiento"];
    protected $campos = [
    'dni' => [
        'type' => 'text',
        'label' => 'Dni',
        'placeholder' => 'Dni del Beneficiarios Temp'
    ],
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Beneficiarios Temp'
    ],
    'sexo' => [
        'type' => 'text',
        'label' => 'Sexo',
        'placeholder' => 'Sexo del Beneficiarios Temp'
    ],
    'solicitud_beneficiarios_temp_id' => [
        'type' => 'number',
        'label' => 'Solicitud Beneficiarios Temp',
        'placeholder' => 'Solicitud Beneficiarios Temp del Beneficiarios Temp'
    ],
    'parentesco_id' => [
        'type' => 'number',
        'label' => 'Parentesco',
        'placeholder' => 'Parentesco del Beneficiarios Temp'
    ],
    'fecha_nacimiento' => [
        'type' => 'date',
        'label' => 'Fecha Nacimiento',
        'placeholder' => 'Fecha Nacimiento del Beneficiarios Temp'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}