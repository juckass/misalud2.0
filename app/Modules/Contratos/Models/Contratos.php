<?php

namespace App\Modules\Contratos\Models;

use App\Modules\Base\Models\Modelo;



class Contratos extends Modelo
{
    protected $table = 'contratos';
    protected $fillable = ["planilla","titular","meses","observaciones","tipo_contrato","vendedor_id","sucursal_id","empresa_id","cargado","primer_cobro","vencimiento","inicio","plan_detalles_id","frecuencia_pagos_id","tipo_pago","total_contrato","total_pagado","cuota_valor","cuotas","cuotas_pagadas","giros_pagados","giros","giro_valor","personas_bancos_id","beneficiarios","inicial","fecha_incial","fecha_pago","anulado","bookie","cobrando","renovaciones","estatus_contrato_id"];
    protected $campos = [
    'planilla' => [
        'type' => 'text',
        'label' => 'Planilla',
        'placeholder' => 'Planilla del Contratos'
    ],
    'titular' => [
        'type' => 'number',
        'label' => 'Titular',
        'placeholder' => 'Titular del Contratos'
    ],
    'vendedor_id' => [
        'type' => 'number',
        'label' => 'Vendedor',
        'placeholder' => 'Vendedor del Contratos'
    ],
    'sucursal_id' => [
        'type' => 'number',
        'label' => 'Sucursal',
        'placeholder' => 'Sucursal del Contratos'
    ],
    'empresa_id' => [
        'type' => 'number',
        'label' => 'Empresa',
        'placeholder' => 'Empresa del Contratos'
    ],
    'cargado' => [
        'type' => 'date',
        'label' => 'Cargado',
        'placeholder' => 'Cargado del Contratos'
    ],
    'primer_cobro' => [
        'type' => 'date',
        'label' => 'Primer Cobro',
        'placeholder' => 'Primer Cobro del Contratos'
    ],
    'vencimiento' => [
        'type' => 'date',
        'label' => 'Vencimiento',
        'placeholder' => 'Vencimiento del Contratos'
    ],
    'inicio' => [
        'type' => 'date',
        'label' => 'Inicio',
        'placeholder' => 'Inicio del Contratos'
    ],
    'plan_detalles_id' => [
        'type' => 'number',
        'label' => 'Plan Detalles',
        'placeholder' => 'Plan Detalles del Contratos'
    ],
    'frecuencia_pagos_id' => [
        'type' => 'number',
        'label' => 'Frecuencia Pagos',
        'placeholder' => 'Frecuencia Pagos del Contratos'
    ],
    'tipo_pago' => [
        'type' => 'number',
        'label' => 'Tipo Pago',
        'placeholder' => 'Tipo Pago del Contratos'
    ],
    'total_contrato' => [
        'type' => 'text',
        'label' => 'Total Contrato',
        'placeholder' => 'Total Contrato del Contratos'
    ],
    'total_pagado' => [
        'type' => 'text',
        'label' => 'Total Pagado',
        'placeholder' => 'Total Pagado del Contratos'
    ],
    'cuotas' => [
        'type' => 'number',
        'label' => 'Cuotas',
        'placeholder' => 'Cuotas del Contratos'
    ],
    'cuotas_pagadas' => [
        'type' => 'number',
        'label' => 'Cuotas Pagadas',
        'placeholder' => 'Cuotas Pagadas del Contratos'
    ],
    'giros_pagados' => [
        'type' => 'number',
        'label' => 'Giros Pagados',
        'placeholder' => 'Giros Pagados del Contratos'
    ],
    'giro_valor' => [
        'type' => 'text',
        'label' => 'Giro Valor',
        'placeholder' => 'Giro Valor del Contratos'
    ],
    'personas_bancos_id' => [
        'type' => 'number',
        'label' => 'Personas Bancos',
        'placeholder' => 'Personas Bancos del Contratos'
    ],
    'beneficiarios' => [
        'type' => 'number',
        'label' => 'Beneficiarios',
        'placeholder' => 'Beneficiarios del Contratos'
    ],
    'inicial' => [
        'type' => 'text',
        'label' => 'Inicial',
        'placeholder' => 'Inicial del Contratos'
    ],
    'fecha_incial' => [
        'type' => 'date',
        'label' => 'Fecha Incial',
        'placeholder' => 'Fecha Incial del Contratos'
    ],
    'fecha_pago' => [
        'type' => 'date',
        'label' => 'Fecha Pago',
        'placeholder' => 'Fecha Pago del Contratos'
    ],
    'anulado' => [
        'type' => 'checkbox',
        'label' => 'Anulado',
        'placeholder' => 'Anulado del Contratos'
    ],
    'bookie' => [
        'type' => 'checkbox',
        'label' => 'Bookie',
        'placeholder' => 'Bookie del Contratos'
    ],
    'cobrando' => [
        'type' => 'checkbox',
        'label' => 'Cobrando',
        'placeholder' => 'Cobrando del Contratos'
    ],
    'renovaciones' => [
        'type' => 'number',
        'label' => 'Renovaciones',
        'placeholder' => 'Renovaciones del Contratos'
    ],
    'estatus_contrato_id' => [
        'type' => 'number',
        'label' => 'Estatus Contrato',
        'placeholder' => 'Estatus Contrato del Contratos'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}