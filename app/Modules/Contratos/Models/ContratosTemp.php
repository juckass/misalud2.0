<?php

namespace App\Modules\Contratos\Models;

use App\Modules\Base\Models\Modelo;

use Illuminate\Database\Eloquent\Model;

class ContratosTemp extends Model
{
    protected $table = 'contratos_temp';
    protected $fillable = ["planilla","contrato_id","solicitud_id","primer_cobro","vencimiento","inicio","plan_detalles_id","frecuencia_pagos_id","tipo_pago","total_contrato","total_pagado","cuotas","cuotas_pagadas","personas_bancos_id","beneficiarios","rechazado","inicial","fecha_incial"];
    protected $campos = [
    'planilla' => [
        'type' => 'text',
        'label' => 'Planilla',
        'placeholder' => 'Planilla del Contratos Temp'
    ],
    'contrato_id' => [
        'type' => 'number',
        'label' => 'Contrato',
        'placeholder' => 'Contrato del Contratos Temp'
    ],
    'solicitud_id' => [
        'type' => 'number',
        'label' => 'Solicitud',
        'placeholder' => 'Solicitud del Contratos Temp'
    ],
    'primer_cobro' => [
        'type' => 'date',
        'label' => 'Primer Cobro',
        'placeholder' => 'Primer Cobro del Contratos Temp'
    ],
    'vencimiento' => [
        'type' => 'date',
        'label' => 'Vencimiento',
        'placeholder' => 'Vencimiento del Contratos Temp'
    ],
    'inicio' => [
        'type' => 'date',
        'label' => 'Inicio',
        'placeholder' => 'Inicio del Contratos Temp'
    ],
    'plan_detalles_id' => [
        'type' => 'number',
        'label' => 'Plan Detalles',
        'placeholder' => 'Plan Detalles del Contratos Temp'
    ],
    'frecuencia_pagos_id' => [
        'type' => 'number',
        'label' => 'Frecuencia Pagos',
        'placeholder' => 'Frecuencia Pagos del Contratos Temp'
    ],
    'tipo_pago' => [
        'type' => 'number',
        'label' => 'Tipo Pago',
        'placeholder' => 'Tipo Pago del Contratos Temp'
    ],
    'total_contrato' => [
        'type' => 'text',
        'label' => 'Total Contrato',
        'placeholder' => 'Total Contrato del Contratos Temp'
    ],
    'total_pagado' => [
        'type' => 'text',
        'label' => 'Total Pagado',
        'placeholder' => 'Total Pagado del Contratos Temp'
    ],
    'cuotas' => [
        'type' => 'number',
        'label' => 'Cuotas',
        'placeholder' => 'Cuotas del Contratos Temp'
    ],
    'cuotas_pagadas' => [
        'type' => 'number',
        'label' => 'Cuotas Pagadas',
        'placeholder' => 'Cuotas Pagadas del Contratos Temp'
    ],
    'personas_bancos_id' => [
        'type' => 'number',
        'label' => 'Personas Bancos',
        'placeholder' => 'Personas Bancos del Contratos Temp'
    ],
    'beneficiarios' => [
        'type' => 'number',
        'label' => 'Beneficiarios',
        'placeholder' => 'Beneficiarios del Contratos Temp'
    ],
    'rechazado' => [
        'type' => 'checkbox',
        'label' => 'Rechazado',
        'placeholder' => 'Rechazado del Contratos Temp'
    ],
    'inicial' => [
        'type' => 'text',
        'label' => 'Inicial',
        'placeholder' => 'Inicial del Contratos Temp'
    ],
    'fecha_incial' => [
        'type' => 'date',
        'label' => 'Fecha Incial',
        'placeholder' => 'Fecha Incial del Contratos Temp'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}