<?php

namespace App\Modules\Contratos\Models;
use App\Modules\Base\Models\Modelo;


class ContratosDetalles extends Modelo
{
    protected $table = 'contratos_detalles';
    protected $fillable = ["contratos_id","personas_id","operacion","planilla"];
    

    protected $hidden = [ 'updated_at'];

    
}