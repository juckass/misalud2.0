<?php

namespace App\Modules\Contratos\Models;

use App\Modules\Base\Models\Modelo;



class FrecuenciaPagos extends Modelo
{
    protected $table = 'frecuencia_pagos';
    protected $fillable = ["frecuencia","dias","dias_str"];
    protected $campos = [
    'frecuencia' => [
        'type' => 'text',
        'label' => 'Frecuencia',
        'placeholder' => 'Frecuencia del Frecuencia Pagos'
    ],
    'dias' => [
        'type' => 'text',
        'label' => 'Dias',
        'placeholder' => 'Dias del Frecuencia Pagos'
    ],
    'dias_str' => [
        'type' => 'text',
        'label' => 'Dias Str',
        'placeholder' => 'Dias Str del Frecuencia Pagos'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}