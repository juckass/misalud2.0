<?php

namespace App\Modules\Contratos\Models;

use App\Modules\Base\Models\Modelo;



class EstatusContrato extends Modelo
{
    protected $table = 'estatus_contrato';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Estatus Contrato'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}