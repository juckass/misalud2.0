<?php

namespace App\Modules\Contratos\Models;

use App\Modules\Base\Models\Modelo;



class ContratoTipo extends Modelo
{
    protected $table = 'contrato_tipo';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Contrato Tipo'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}