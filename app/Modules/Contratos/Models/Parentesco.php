<?php

namespace App\Modules\Contratos\Models;

use App\Modules\Base\Models\Modelo;



class Parentesco extends Modelo
{
    protected $table = 'parentesco';
    protected $fillable = ["nombre"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Parentesco'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}