<?php

namespace App\Modules\Contratos\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Modules\Contratos\Models\ContratoTipo;
class tipocontratoseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		$tipo_contratos = [ 	
			['Normal'],
			['Corporativo'],
			['Combenio'],
		];

		DB::beginTransaction();
		try{
			foreach ($tipo_contratos as $contrato) {
				ContratoTipo::create([
					'nombre'  => $contrato[0]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	
    }
}
