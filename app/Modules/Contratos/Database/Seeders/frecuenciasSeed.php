<?php

namespace App\Modules\Contratos\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Modules\Contratos\Models\FrecuenciaPagos;

class frecuenciasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
        $frecuencias = [
        	['m', '05','Mensual los 05'], 	
        	['m', '10','Mensual los 10'], 	
        	['m', '15','Mensual los 15'], 	
        	['m', '17','MENSUAL LOS 17'], 	
        	['m', '20','Mensual los 20'], 	
        	['m', '25','Mensual los 25'], 	
			['m', '30','Mensual los 30'], 
			['m', '11','Mensual los 11'], 
				
        	['q', '01,16', 'QUINCENAL 16 y 01'], 	
        	['q', '15,30', 'DOCENTE 15 Y 30'], 	
			['q', '05,20', 'FERROMINERA 05 Y 20'], 	
			['q', '07,22', 'QUINCENALES 07 Y 22'], 	
			['q', '10,25', 'OBREROS DE EDUCACION 10 Y 25'],
			 	
		];

		DB::beginTransaction();
		try{
			foreach ($frecuencias as $parentesco) {
				FrecuenciaPagos::create([
					'frecuencia'  => $parentesco[0],
					'dias'  	  => $parentesco[1],
					'dias_str'   => $parentesco[2]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
    }
}
