<?php

namespace App\Modules\Contratos\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ContratosDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(parentescoseed::class);
		$this->call(tipocontratoseed::class);
		$this->call(estatuscontrotatoseed::class);
		$this->call(frecuenciasSeed::class);
    }
}
