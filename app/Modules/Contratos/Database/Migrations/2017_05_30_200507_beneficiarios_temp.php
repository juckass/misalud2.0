<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BeneficiariosTemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*

        beneficiarios_temp
        solicitud_beneficiarios_temp_id
        dni
        nombre
        fecha_nacimiento
        parentesco_id
        sexo

        */
        
        Schema::create('beneficiarios_temp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dni',20);
            $table->string('nombre', 60)->nullable();
            $table->string('sexo', 10)->nullable();
            $table->integer('solicitud_beneficiarios_temp_id')->unsigned();
            $table->integer('parentesco_id')->unsigned();
            $table->date('fecha_nacimiento');
        
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('beneficiarios_temp');
    }
}
