<?php

Route::group(['middleware' => 'web', 'prefix' => 'contratos', 'namespace' => 'App\Modules\Contratos\Http\Controllers'], function()
{

    Route::group(['prefix' => 'parentesco'], function() {
        Route::get('/',                 'ParentescoController@index');
        Route::get('nuevo',             'ParentescoController@nuevo');
        Route::get('cambiar/{id}',      'ParentescoController@cambiar');
        
        Route::get('buscar/{id}',       'ParentescoController@buscar');

        Route::post('guardar',          'ParentescoController@guardar');
        Route::put('guardar/{id}',      'ParentescoController@guardar');

        Route::delete('eliminar/{id}',  'ParentescoController@eliminar');
        Route::post('frecuencias',   'ParentescoController@restaurar');
        Route::delete('destruir/{id}',  'ParentescoController@destruir');

        Route::get('datatable',         'ParentescoController@datatable');
    });


    Route::group(['prefix' => 'frecuenciapagos'], function() {
        Route::get('/',                 'FrecuenciaPagosController@index');
        Route::get('nuevo',             'FrecuenciaPagosController@nuevo');
        Route::get('cambiar/{id}',      'FrecuenciaPagosController@cambiar');
        
        Route::get('buscar/{id}',       'FrecuenciaPagosController@buscar');

        Route::post('guardar',          'FrecuenciaPagosController@guardar');
        Route::put('guardar/{id}',      'FrecuenciaPagosController@guardar');

        Route::delete('eliminar/{id}',  'FrecuenciaPagosController@eliminar');
        Route::post('restaurar/{id}',   'FrecuenciaPagosController@restaurar');
        Route::delete('destruir/{id}',  'FrecuenciaPagosController@destruir');

        Route::get('datatable',         'FrecuenciaPagosController@datatable');
    });


    Route::group(['prefix' => 'contratotipo'], function() {
        Route::get('/',                 'ContratoTipoController@index');
        Route::get('nuevo',             'ContratoTipoController@nuevo');
        Route::get('cambiar/{id}',      'ContratoTipoController@cambiar');
        
        Route::get('buscar/{id}',       'ContratoTipoController@buscar');

        Route::post('guardar',          'ContratoTipoController@guardar');
        Route::put('guardar/{id}',      'ContratoTipoController@guardar');

        Route::delete('eliminar/{id}',  'ContratoTipoController@eliminar');
        Route::post('restaurar/{id}',   'ContratoTipoController@restaurar');
        Route::delete('destruir/{id}',  'ContratoTipoController@destruir');

        Route::get('datatable',         'ContratoTipoController@datatable');
    });


    Route::group(['prefix' => 'estatuscontrato'], function() {
        Route::get('/',                 'EstatusContratoController@index');
        Route::get('nuevo',             'EstatusContratoController@nuevo');
        Route::get('cambiar/{id}',      'EstatusContratoController@cambiar');
        
        Route::get('buscar/{id}',       'EstatusContratoController@buscar');

        Route::post('guardar',          'EstatusContratoController@guardar');
        Route::put('guardar/{id}',      'EstatusContratoController@guardar');

        Route::delete('eliminar/{id}',  'EstatusContratoController@eliminar');
        Route::post('restaurar/{id}',   'EstatusContratoController@restaurar');
        Route::delete('destruir/{id}',  'EstatusContratoController@destruir');

        Route::get('datatable',         'EstatusContratoController@datatable');
    });
    
    
    Route::group(['prefix' => 'contratos'], function() {
        Route::get('/',                 'ContratosController@index');
        Route::get('nuevo',             'ContratosController@nuevo');
        Route::get('cambiar/{id}',      'ContratosController@cambiar');
        
        Route::get('buscar/{id}',       'ContratosController@buscar');
        
        Route::post('guardar',          'ContratosController@guardar');
        Route::put('guardar/{id}',      'ContratosController@guardar');
        Route::post('infoplan',			'ContratosController@infoplan');
        Route::post('asisplan',			'ContratosController@asisplan');
        Route::post('planes',           'ContratosController@planes');
        Route::get('vendedores/{id}',   'ContratosController@vendedores');
        Route::post('buscarpersona',    'ContratosController@buscarpersona');
        Route::delete('eliminar/{id}',  'ContratosController@eliminar');
        Route::post('restaurar/{id}',   'ContratosController@restaurar');
        Route::post('frecuencias',      'ContratosController@frecuencias');
        Route::delete('destruir/{id}',  'ContratosController@destruir');
        Route::get('beneficiario',   	'ContratosController@beneficiario');
        Route::get('historial/{id}', 	'ContratosController@historial');
        Route::get('cobros/{id}', 		'ContratosController@cobros');
        Route::get('cobrostable/{id}', 	'ContratosController@cobrostable');
        Route::get('datatable',         'ContratosController@datatable');
        Route::get('anular',            'ContratosController@opanular');
        Route::get('activar',            'ContratosController@opactivar');
        Route::get('beneficiarios/{id}/{adonde}', 	'ContratosController@asisbeneficiarios');
        Route::post('guardar/beneficiarios', 	'ContratosController@beneficiariosguardar');
        
        Route::get('refinanciamiento/{id}',     'ContratosController@refinaciamiento');
        Route::post('refinaciamientoguardar',   'ContratosController@refinaciamientoguardar');
        
        Route::get('renovacion/{id}/{adonde}', 	'ContratosController@renovacion');
        Route::post('renovacion/guardar',		'ContratosController@renovacionGuardar');
        Route::put('renovacion/guardar/{id}',	'ContratosController@renovacionGuardar');
    });
    
    Route::group(['prefix' => 'corporativo'], function() {
        Route::get('/',                     'ContratosCorporativoController@index');
        Route::get('buscar/{id}',           'ContratosCorporativoController@buscar');
        Route::get('vendedores/{id}',       'ContratosCorporativoController@vendedores');
        Route::post('buscarpersona',        'ContratosCorporativoController@buscarpersona');
        Route::post('frecuencia',           'ContratosCorporativoController@frecuencia');
        Route::post('guardar',              'ContratosCorporativoController@guardar');
        Route::put('guardar/{id}',          'ContratosCorporativoController@guardar');
        Route::get('datatable',             'ContratosCorporativoController@datatable');
        Route::get('bene/{id}', 	        'ContratosCorporativoController@bene');
        Route::get('beneficiario',   	    'ContratosCorporativoController@beneficiario');
        Route::post('beneficiariosguardar', 'ContratosCorporativoController@beneficiariosguardar');
    });
        
    //{{route}}
});