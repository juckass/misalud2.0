<?php

namespace App\Modules\Contratos\Http\Requests;

use App\Http\Requests\Request;

class FrecuenciaPagosRequest extends Request {
    protected $reglasArr = [
		'frecuencia' => ['required', 'min:3', 'max:1'], 
		'dias' => ['required', 'min:3', 'max:10'], 
		'dias_str' => ['required', 'min:3', 'max:100']
	];
}