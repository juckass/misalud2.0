<?php

namespace App\Modules\Contratos\Http\Requests;

use App\Http\Requests\Request;

class BeneficiariosRequest extends Request {
    protected $reglasArr = [
		'contratos_id' => ['required', 'integer'], 
		'personas_id' => ['required', 'integer'], 
		'parentesco_id' => ['required', 'integer'], 
		'estatus' => ['required']
	];
}