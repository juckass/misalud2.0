<?php

namespace App\Modules\Contratos\Http\Requests;

use App\Http\Requests\Request;

class BeneficiariosTempRequest extends Request {
    protected $reglasArr = [
		'dni' => ['required', 'min:3', 'max:20'], 
		'nombre' => ['min:3', 'max:60'], 
		'sexo' => ['min:3', 'max:10'], 
		'solicitud_beneficiarios_temp_id' => ['required', 'integer'], 
		'parentesco_id' => ['required', 'integer'], 
		'fecha_nacimiento' => ['required', 'date_format:"d/m/Y"']
	];
}