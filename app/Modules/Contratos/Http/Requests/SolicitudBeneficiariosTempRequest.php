<?php

namespace App\Modules\Contratos\Http\Requests;

use App\Http\Requests\Request;

class SolicitudBeneficiariosTempRequest extends Request {
    protected $reglasArr = [
		'planilla' => ['min:3', 'max:10'], 
		'contratos_id' => ['required', 'integer'], 
		'solicitud_id' => ['required', 'integer'], 
		'planes_id' => ['integer'], 
		'fecha' => ['required', 'date_format:"d/m/Y"'], 
		'operacion' => ['required', 'integer']
	];
}