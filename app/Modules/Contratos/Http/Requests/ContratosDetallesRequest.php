<?php

namespace App\Modules\Contratos\Http\Requests;

use App\Http\Requests\Request;

class ContratosDetallesRequest extends Request {
    protected $reglasArr = [
		'contratos_id' => ['required', 'integer'], 
		'personas_id' => ['required', 'integer'], 
		'operacion' => ['required'], 
		'planilla' => ['min:3', 'max:10']
	];
}