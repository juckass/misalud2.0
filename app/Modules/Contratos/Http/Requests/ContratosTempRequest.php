<?php

namespace App\Modules\Contratos\Http\Requests;

use App\Http\Requests\Request;

class ContratosTempRequest extends Request {
    protected $reglasArr = [
		'planilla' => ['min:3', 'max:10'], 
		'contrato_id' => ['required', 'integer'], 
		'solicitud_id' => ['required', 'integer'], 
		'primer_cobro' => ['date_format:"d/m/Y"'], 
		'vencimiento' => ['required', 'date_format:"d/m/Y"'], 
		'inicio' => ['required', 'date_format:"d/m/Y"'], 
		'plan_detalles_id' => ['integer'], 
		'frecuencia_pagos_id' => ['integer'], 
		'tipo_pago' => ['required', 'integer'], 
		'total_contrato' => ['required'], 
		'total_pagado' => ['required'], 
		'cuotas' => ['required', 'integer'], 
		'cuotas_pagadas' => ['integer'], 
		'personas_bancos_id' => ['integer'], 
		'beneficiarios' => ['required', 'integer'], 
		'rechazado' => ['required'], 
		'fecha_incial' => ['date_format:"d/m/Y"']
	];
}