<?php
namespace App\Modules\Contratos\Http\Controllers;

//Controlador Padre
use App\Modules\Contratos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;
use Session;
use Carbon\Carbon;
use Excel; 
//Request
use App\Modules\Ventas\Http\Requests\ContratosRequest;

//Modelos
use App\Modules\Contratos\Models\Contratos;
use App\Modules\Contratos\Models\Parentesco;
use App\Modules\Contratos\Models\Beneficiarios;
use App\Modules\Contratos\Models\FrecuenciaPagos;
use App\Modules\Contratos\Models\ContratosDetalles;
use App\Modules\Cobranza\Models\Cobros;
use App\Modules\Facturacion\Models\ContratosFacturar;
use App\Modules\Ventas\Models\Plan;
use App\Modules\Ventas\Models\PlanDetalles;
use App\Modules\Ventas\Models\VendedoresSucusal;
use App\Modules\Ventas\Models\Vendedores;
use App\Modules\Ventas\Models\Solicitudes;
use App\Modules\base\Models\Personas;
use App\Modules\base\Models\PersonasDetalles;
use App\Modules\base\Models\PersonasBancos;
use App\Modules\Empresa\Models\sucursal;
    

class ContratosCorporativoController extends Controller
{
    protected $titulo = "Contratos Corporativos";

    public $js = [
       'ContratosCorporativo',
       'moment.min'
    ];
    
    public $css = [
       'contratoscorporativos'
    ];
    
    public $_tipo_pago = [
        'Por Tarjeta', // el indice es cero inicialmente
        'Transferencia',
        'efectivo'
    ];

    public $librerias = [
        'datatables',
        'maskedinput',
        'alphanum',
        'template',
        'jquery-ui',
        'jquery-ui-timepicker',
        'bootstrap-sweetalert',
        'maskMoney'
    ];

    public function index()
    {
        return $this->view('contratos::contratoscorporativos',[
            'Personas' => new Personas(),
        ]);
    }

    public function parentescos()
    {
        return Parentesco::all();
    }

    public function buscar(Request $request, $id = 0)
    {
        $Contratos = Contratos::select([
            'contratos.id',
            'contratos.planilla',
            'contratos.titular',
            'contratos.tipo_contrato',
            'contratos.renovaciones',
            'contratos.vencimiento',
            'contratos.beneficiarios as Num_benefe',
            'personas.dni',
            'personas.nombres',
            'personas.tipo_persona_id',
            "contratos.vendedor_id",
            "contratos.total_pagado",
            "contratos.giros_pagados",
            "contratos.giro_valor",
            "contratos.inicial",
            "contratos.giros",
            "contratos.cuota_valor",
            "contratos.sucursal_id",
            //DB::raw('DATE_FORMAT(contratos.inicio, "%d/%m/%Y") as fecha'),

            "contratos.inicio as fecha",
            "contratos.plan_detalles_id",
            "frecuencia_pagos.frecuencia",
            "contratos.frecuencia_pagos_id",
            "contratos.observaciones",
            "contratos.tipo_pago",
            "contratos.cobrando",
            "contratos.total_contrato",
            "contratos.cuotas",
            "contratos.meses",
            "contratos.personas_bancos_id",
            
            "contratos.fecha_incial as fecha2",
            "contratos.anulado",
            "contratos.bookie",
            "estatus_contrato.nombre as estatus"
        ])
        ->leftjoin('frecuencia_pagos', 'frecuencia_pagos.id','=','contratos.frecuencia_pagos_id')
        ->leftjoin('estatus_contrato', 'estatus_contrato.id','=','contratos.estatus_contrato_id')
        ->leftjoin('personas', 'personas.id','=','contratos.titular')
        ->where('contratos.id', $id)
        ->first();

        $cobros = '';

        if($Contratos->tipo_pago ==  1){
            $cobros = Cobros::where('contratos_id',  $id)->first();
        }
        
        $Contratos->fecha = Carbon::parse($Contratos->fecha)->format('d/m/Y');
        $Contratos->vencimiento = Carbon::parse($Contratos->vencimiento)->format('d/m/Y');

        if($Contratos->fecha2 != ''){
            $Contratos->fecha2 = Carbon::parse($Contratos->fecha2)->format('d/m/Y');
        }

        $vendedor_id = VendedoresSucusal::select([
            'vendedores_sucusal.vendedor_id as id',
            'personas.nombres as nombre'
        ])
        ->leftjoin('vendedores', 'vendedores.id', '=', 'vendedores_sucusal.vendedor_id')
        ->leftjoin('personas', 'personas.id', '=', 'vendedores.personas_id')
        ->where(
            'vendedores_sucusal.sucursal_id', $Contratos->sucursal_id
        )
       ->pluck('nombre','id')
       ->put('_', $Contratos->vendedor_id);
       

        $Num_benefe =  Beneficiarios::where('contratos_id',$id)->where('personas_id', $Contratos->titular);

        if($Contratos->plan_detalles_id == ''){
            $PlanDetalles = '';
            $plan = '';
        }else{
            $PlanDetalles = PlanDetalles::select(
                'plan_detalles.plan_id',
                'plan_detalles.beneficiarios',
                'plan_detalles.porsertaje_descuento',
                'plan_detalles.contado',
                'plan_detalles.inicial',
                'plan_detalles.total',
                'plan.meses_pagar'
            )
            ->leftjoin('plan','plan.id','plan_detalles.plan_id')
            ->where('plan_detalles.id',$Contratos->plan_detalles_id)->first();
            
            $plan = Plan::where('id', $PlanDetalles->plan_id)
            ->pluck('nombre','id')
            ->put('_', $PlanDetalles->plan_id);  
        }
    
        if ($Contratos) {
            return array_merge($Contratos->toArray(), [
                'vendedor_id' => $vendedor_id,
                'goza'        => $Num_benefe->count(),
                'planDetalles'=> $PlanDetalles,
                'planes_id'   => $plan,
                'cobros'      => $cobros,
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function vendedores(Request $request)
    {
        $sql = VendedoresSucusal::join('vendedores', 'vendedores.id','=', 'vendedores_sucusal.vendedor_id')
            ->join('personas', 'personas.id','=','vendedores.personas_id')
            ->where('vendedores_sucusal.sucursal_id', $request->id)
            ->where('vendedores.estatus', 1)//estatus 1 = activo 
            ->pluck('vendedores.codigo','vendedores.id')
            ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el sucursal no Contiene Vendedores Activos'];
        
        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Vendedores encontrados', 'vendedor_id'=> $sql];
        }
        
        return $salida;
    }

    public function guardar(Request $request, $id=0)
    {
        DB::beginTransaction();
        try{
            $Contratos = $id == 0 ? new Contratos() : Contratos::find($id);
           
            $datos = $request->all();

            $persona = Personas::where('dni', $datos['dni'])->first();
          
            $datos['titular'] = $persona->id;

            $datos['empresa_id']    = Session::get('empresa');
            $datos['cargado']       = date('Y-m-d');
            $datos['total_pagado']  = 0;
            $pago = 0;

            if($request->tipo_contrato==1){
                $pago = $request->total_contado; 
                if($request->tipo_pago == 1 ){
                    //pago contado
                    $datos['cuotas']              = 1;
                    $datos['inicial']             = '0';
                    $datos['personas_bancos_id']  = null;
                    $datos['frecuencia_pagos_id'] = null;
                    $datos['fecha_pago']          = null;
                    $datos['cobrando']            = 1;
                    $datos['total_pagado']        = $request->total_contado; 
                    $datos['total_contrato']      = $request->total_contado; 
                }else{
                    if($request->frecuencia == 'm'){
                        $cuotas = 1 * $datos['meses'];
                    }else{
                        $cuotas = 2 * $datos['meses']; 
                    }
                    
                    $datos['cuotas']         = $cuotas;
                    $datos['cuota_valor']    = $request->cuotas;
                    $datos['fecha_pago']     = $datos['fecha_incial'];
                    $datos['total_contrato'] = $request->total_credito; 
                }

                $datos['beneficiarios'] = 0;
                $datos['estatus_contrato_id']  = 1;  
            }else{
                $datos['estatus_contrato_id'] = 1;  
                $datos['beneficiarios']       = 0;
                $datos['total_contrato']      = 0; 
                $datos['total_pagado']        = 0;            
                $datos['frecuencia_pagos_id'] = null;
                $datos['cuotas']              = 0;
                $datos['inicial']             = '0';
                $datos['personas_bancos_id']  = null;
                $datos['frecuencia_pagos_id'] = null;
                $datos['fecha_pago']          = null;
                $datos['cobrando']            = 1; 
                $datos['tipo_pago']           = 3; 
            }

            $Contratos->fill($datos);
            $Contratos->save();

            $_tipo_pago = [
                'Por Tarjeta',
                'Transferencia',
                'efectivo'
            ];
             
            if($id == 0 ){
                if($request->tipo_pago == 1){
                    ContratosFacturar::create([
                        "fecha_facturar" => date('Y-m-d'),
                        "contratos_id"   => $Contratos->id
                    ]);

                    Cobros::create([
                        "contratos_id"  => $Contratos->id,
                        "sucursal_id"   => $datos['sucursal_id'],
                        "personas_id"   => $persona->id,
                        "total_cobrar"  => $pago,
                        "concepto"      => 'Pago de Contrato de Contado por: '.$_tipo_pago[$datos['pago']],
                        "cuota"         => 1,
                        "fecha_pagado"  => $request->fecha_decontado,
                        "total_pagado"  => $pago,
                        "num_recibo"    => $datos['n_recibo'],
                        "tipo_pago"     => 'caja',
                        "completo"      => true
                    ]);
                }
            }
             
            $_nombres = auth()->user()->personas->nombres;

            ContratosDetalles::create([
                "contratos_id" => $Contratos->id,
                "personas_id"  => auth()->user()->personas->id,
                "operacion"    => $id == 0 ? "Carga de Nuevo contrato Por Analista: " .$_nombres : "Contratos Modificado Por Analista: ".$_nombres,
                "planilla"     => $request->planilla
            ]);
             

            $mimes = [
                'text/csv',
                'application/vnd.ms-excel',
                'application/vnd.oasis.opendocument.spreadsheet',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ];
    
            $ruta = public_path('archivos/');
            $archivo = $request->file('subir');
    
            $mime = $archivo->getClientMimeType();
    
            if (!in_array($mime, $mimes)) {
                return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
            }
            
            do {
                $nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
            } while (is_file($ruta . $nombre_archivo));
                
            $archivo->move($ruta, $nombre_archivo);
            chmod($ruta . $nombre_archivo, 0777);
            
            $excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();
                 
            
            $paremtesco = [];
            foreach ( Parentesco::all() as $key => $value) {
                $paremtesco[str_slug($value->nombre, '-')] =$value->id;
            }
            $num = 0;

            Beneficiarios::where('contratos_id', $Contratos->id)->delete();

            foreach ($excel as $key => $value){
                if($value->cedula  ==""){
                    continue;
                }
                $persona = Personas::where('dni', $value->cedula)->first();
                
                if(count($persona) == 0){
                  
                    $persona = Personas::create([
                        "tipo_persona_id" => 1,
                        "dni"             => strval($value->cedula),
                        "nombres"         => $value->nombres_y_apelidos
                    ]);
                
                    PersonasDetalles::create([
                        "personas_id"      => $persona->id,
                        "sexo"             => $value->sexo,
                        "fecha_nacimiento" => $value->fecha_de_nacimiento
                    ]);
                }

                if (array_key_exists(str_slug($value->parentesco,'-'), $paremtesco)) {
                    $_paren = $paremtesco[str_slug($value->parentesco, '-')];    
                }else{
                    $paren = Parentesco::create([
                        'nombre' => $value->parentesco,
                    ]);
                    $_paren = $paren->id;
                }

                $_datos= [
                    "contratos_id"  => $Contratos->id,
                    "personas_id"   => $persona->id,
                    "parentesco_id" => $_paren,
                    "estatus"       => 0
                ];
                $num++;
                Beneficiarios::create($_datos);   
            }

            
            Contratos::where('id', $Contratos->id)->update([
                'beneficiarios' => $num
            ]);

        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            //'id'    => $ContratoTipo->id,
            //'texto' => $ContratoTipo->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function buscarpersona(Request $request){
        
        $persona = Personas::where('dni',$request->dni)->first();
       
        
        if(count($persona) == 0){
            return [
            's' => 'n'
            ];
        }   
        
        $_persona = $persona->toArray();
        $detalles ='';
        if($persona->tipo_persona_id == 1 ||$persona->tipo_persona_id == 2){
            $detalles = PersonasDetalles::where('personas_id',$persona->id)->first();
            if(!$detalles){
                return [
                    's'   => 'n'
                ];
            }
            $detalles->nombre = $_persona[0]['nombres'];
            $detalles->dni = $_persona[0]['dni'];
            $detalles->edad = $date;
            $detalles->parentescos = 33;
            $fecha_n =Carbon::createFromFormat('d/m/Y', $detalles->fecha_nacimiento);
            $date = Carbon::parse($fecha_n)->age;
        }
        
        $cuentas = PersonasBancos::select('personas_bancos.id', 'personas_bancos.cuenta','bancos.nombre' )
            ->leftJoin('bancos', 'bancos.id','=','personas_bancos.bancos_id')
            ->where('personas_bancos.personas_id', $persona->id)->get()->toArray();
            
        return [
            'persona'   => $persona,
            'cuentas'   => $cuentas,
            'detalles'  => $detalles,
            's'         => 's', 
            'msj' => trans('controller.incluir')
        ];
    }

    public function frecuencia(Request $request)
    {
        $frecuencia = FrecuenciaPagos::where('frecuencia', $request->id)->get();

        return [
            'frecuencia'=>  $frecuencia,
            's'         => 's', 
            'msj'       => trans('controller.incluir')
        ];
    }
    
    public function datatable(Request $request)
    {
        $sql = Contratos::select(
            'contratos.id as id',
            'contratos.planilla as planilla',
            'personas.nombres', 
            'personas.dni' 
        )
        ->leftJoin('personas', 'personas.id','=','contratos.titular')
        ->where('contratos.tipo_contrato','!=',0); 

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }

    public function bene(Request $request, $id=0){
         //adonde: 0 = analista, 1= vendedor, dice de adonde esta accediendo
     
        return $this->view('contratos::popup-bene', [
            'layouts'       => 'base::layouts.popup-consulta',
            'id'  => $id,
        ]);
    }
    public function beneficiario(Request $request){
        
        $Beneficiarios = Beneficiarios::select(
            'personas.nombres','personas.dni',
            'beneficiarios.parentesco_id',
            'personas_detalles.fecha_nacimiento as fecha_nacimiento',
            'personas_detalles.sexo'
        )
        ->leftjoin('personas','personas.id','=','beneficiarios.personas_id')
        ->leftjoin('personas_detalles','personas_detalles.personas_id','=','beneficiarios.personas_id')
        ->where('beneficiarios.contratos_id',$request->id)->get();
    
        $datos = [];

        foreach($Beneficiarios as $Beneficiario){

            $datos[] = [
                'nombres' => $Beneficiario->nombres,
                'dni'=>$Beneficiario->dni,
                'parentesco_id'=>$Beneficiario->parentesco_id,
                'fecha_nacimiento'=> Carbon::parse($Beneficiario->fecha_nacimiento)->format('d/m/Y'),
                'sexo'=>$Beneficiario->sexo
            ];

        }
    
        return ['datos' =>$datos];
    }
    public function beneficiariosguardar(Request $request){
        
        DB::beginTransaction();
        try{

            $datos = [];
            $contrato = Contratos::find($request->id);

            $num = 0;
            Beneficiarios::where('contratos_id', $request->id)->delete();
            $datos = [];
            $bene = $request->all();
            foreach ($bene['dni_beneficiario'] as $_id => $beneficiario) {
               $num++;
                if($bene['dni_beneficiario'][$_id] == ""){
                  
                    $_contrato = Contratos::select('personas.dni')->where('contratos.id', $request->id)
                    ->leftjoin('personas', 'personas.id', '=', 'contratos.titular')->first();
                    
                    $persona = Personas::where('dni', 'LIKE', $_contrato->dni.'%')->count();
                    $result = $persona +  1;
                    $dni = $_contrato->dni . "-" . $result;
                }else{
                    $dni = $bene['dni_beneficiario'][$_id];
                }

                $persona = Personas::where('dni', $dni)->first();

                if(count($persona) == 0){
                   $persona = Personas::create([
                       "tipo_persona_id" => 1,
                       "dni"             => $dni,
                       "nombres"         => $bene['nombres_beneficiario'][$_id]
                    ]);

                    PersonasDetalles::create([
                       "personas_id"      => $persona->id,
                       "sexo"             => $bene['sexo_beneficiario'][$_id],
                       "fecha_nacimiento" => $bene['nacimiento_beneficiario'][$_id]
                    ]);
                }


                $_datos= [
                    "contratos_id"  => $request->id,
                    "personas_id"   => $persona->id,
                    "parentesco_id" => $bene['parentescos_beneficioario'][$_id],
                    "estatus"       => 0
                ];
                
                Beneficiarios::create($_datos);   
            }
            
           $datos['beneficiarios'] = $num;
            $contrato->update($datos);

        } catch(QueryException $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            's'     => 's',
            'msj'   => trans('controller.incluir'),
            "donde" => $request->adonde
        ];

    }
}
