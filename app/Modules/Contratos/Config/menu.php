<?php

$menu['contratos'] = [
	[
		'nombre' 	=> 'Contratos',
		'direccion' => '#contratos',
		'icono' 	=> 'fa fa-clipboard',
		'menu' 		=> [
			[
				'nombre' 	=> 'Definiciones',
				'direccion' => '#Definiciones',
				'icono' 	=> 'fa fa-gear',
				'menu' 		=> [
					[
						'nombre' 	=> 'Parentescos',
						'direccion' => 'contratos/parentesco',
						'icono' 	=> 'fa fa-users'
					],
					[
						'nombre' 	=> 'Tipos de Contratos',
						'direccion' => 'contratos/contratotipo',
						'icono' 	=> 'fa fa-list-ul'
					],
					[
						'nombre' 	=> 'Frecuencias de pago',
						'direccion' => 'contratos/frecuenciapagos',
						'icono' 	=> 'fa fa-list-ul'
					],
					[
						'nombre' 	=> 'Estatus Contratos',
						'direccion' => 'contratos/estatuscontrato',
						'icono' 	=> 'fa fa-list-ul'
					]
				]
			],
			[
				'nombre' 	=> 'Contratos',
				'direccion' => 'contratos/contratos',
				'icono' 	=> 'fa fa-plus-circle'
			],
			[
				'nombre' 	=> 'Contratos Corporativo',
				'direccion' => 'contratos/corporativo',
				'icono' 	=> 'fa fa-plus-circle'
			]
		]
	]
];