<?php

Route::group(['middleware' => 'web', 'prefix' => 'tareas', 'namespace' => 'App\\Modules\Tareas\Http\Controllers'], function()
{
    Route::get('/', 'TareasController@index');
});
