<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;

class EscritorioController extends Controller {
	public $autenticar = false;

	protected $titulo = 'Escritorio';
	protected $prefijo = '';
	public function __construct() {
		parent::__construct();

		$this->middleware('auth');
	}

	public function getIndex() {
		
		$permisos = [
			'ventas/planes',
			'/consulta',
			'ventas/escritorio',
			'cobranza/analista/escritorio',
		];
		$pase = 0;
		$ultimoPermiso = '';
		foreach ($permisos as $permiso) {
			
			if ($this->permisologia($permiso)) {
				$pase++;
				$ultimoPermiso = $permiso;
			}
		}
		if(\Auth::user()->super == 's'){
			
			return $this->view('base::Escritorio');
		}
		
		if ($pase >= 1){
			if(\Auth::user()->perfil->nombre == "Vendedor") {
				return redirect($this->prefijo . 'ventas/escritorio');
			}
			if(\Auth::user()->perfil->nombre == "Consulta") {
				return redirect($this->prefijo . '/consulta');
			}
			if(\Auth::user()->perfil->nombre == "Analista") {
				return redirect($this->prefijo . 'cobranza/analista/escritorio');
			}
			return $this->view('base::Inicio');
		} else {
			
			return $this->view('base::Escritorio');
		}
		//return $this->view('base::Escritorio');
	}
}