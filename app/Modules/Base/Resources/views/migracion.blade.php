@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')


	@include('base::partials.ubicacion', ['ubicacion' => ['migracion']])

@endsection

@section('content')
	{!! Form::open(['id' => 'carga', 'name' => 'carga', 'method' => 'POST']) !!}
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h4>Cargar Archivo <small>Excel (.csv, .xls, .xlsx, .ods)</small></h4>
			<hr>
			{{ Form::bsSelect('tipo', [
				0 =>'Clientes Y Contratos',
				1 =>'Vendedores',
			], 0,
			[
				'label' => 'Tipo de Carga',
				'required' => 'required'
			]) }}
			<br>
			<input id="upload" name="subir" type="file"/>
			<button id="subir" type="button" class="btn btn-primary mt-ladda-btn ladda-button" data-style="expand-right">
				<span class="ladda-label">
					<i class="icon-arrow-right"></i> Carga archivo Excel
				</span>
			</button>
		</div>
	{!! Form::close() !!}
@endsection

@push('js')
	<script>
	var Ladda;
		$(function() {
			$("#subir").on('click', function(e){
				e.preventDefault();
				$("#upload:hidden").trigger('click');
			});

			$("#upload").on('change', function(){
				var l = Ladda.create($("#subir").get(0));
				l.start();
					var direccion ="contratos";
				if($('#tipo').val() == 1){
					direccion = 'vendedores';
				}
				var options2 = { 
					url : $url + direccion,
					type : 'POST',
					success: function(r){
						
						aviso(r);
					},
					complete : function(){
						l.stop();
					}
				}; 
				
				$('#carga').ajaxSubmit(options2); 
			});
		});
	</script>
@endpush