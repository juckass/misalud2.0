<?php

namespace App\Modules\Empresa\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

use App\Modules\Empresa\Models\Empresa;
use App\Modules\Empresa\Models\Sucursal;
class empresaseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

		DB::beginTransaction();
		try{
			$empresa = Empresa::create([
				"rif" 			=>'J-30687366-0' ,
				"nombre" 		=> 'MI SALUD MEDICINA PREPAGADA C.A.',
				"abreviatura" 	=> 'Mi salud',
				"tlf" 			=> '0',
				"direccion" 	=> 'direccion'
			]);


			Sucursal::create([
				"empresa_id"    => $empresa->id,
				"nombre"   		=> 'Ciudad Bolivar' ,
				"abreviatura"   => 'CBO',
				"cod_sucursal"  => '1',
				"correlativo"   => '0',
				"estados_id"    => '6',
				"ciudades_id"   => '88',
				"municipios_id" => '17',
				"parroquias_id" => '14' ,
				
				"direccion"		=> 'direccion'
			]);
			Sucursal::create([
				"empresa_id"    => $empresa->id,
				"nombre"   		=> 'Puerto Ordaz' ,
				"abreviatura"   => 'POZ',
				"cod_sucursal"  => '2',
				"correlativo"   => '0',
				"estados_id"    => '6',
				"ciudades_id"   => '88',
				"municipios_id" => '17',
				"parroquias_id" => '14' ,
				
				"direccion"		=> 'direccion'
			]);
			Sucursal::create([
				"empresa_id"    => $empresa->id,
				"nombre"   		=> 'El tigre' ,
				"abreviatura"   => 'TGR',
				"cod_sucursal"  => '3',
				"correlativo"   => '0',
				"estados_id"    => '6',
				"ciudades_id"   => '88',
				"municipios_id" => '17',
				"parroquias_id" => '14' ,
				
				"direccion"		=> 'direccion'
			]);
			Sucursal::create([
				"empresa_id"    => $empresa->id,
				"nombre"   		=> 'Puerto la Cruz' ,
				"abreviatura"   => 'PLC',
				"cod_sucursal"  => '4',
				"correlativo"   => '0',
				"estados_id"    => '6',
				"ciudades_id"   => '88',
				"municipios_id" => '17',
				"parroquias_id" => '14' ,
				
				"direccion"		=> 'direccion'
			]);
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	
    }
}
