<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sucursal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursal', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('empresa_id')->unsigned();
            
            $table->string('nombre', 80)->unique();
            $table->string('abreviatura', 80)->unique();
            $table->integer('cod_sucursal')->unsigned();
            $table->integer('correlativo')->unsigned();

            $table->integer('estados_id')->unsigned();
            $table->integer('ciudades_id')->unsigned();
            $table->integer('municipios_id')->unsigned();
            $table->integer('parroquias_id')->unsigned();
            $table->integer('sectores_id')->unsigned()->nullable();
            $table->string('direccion', 200);
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('empresa_id')
                ->references('id')->on('empresa')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('estados_id')
                ->references('id')->on('estados')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('ciudades_id')
                ->references('id')->on('ciudades')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('municipios_id')
                ->references('id')->on('municipios')
                ->onDelete('cascade')->onUpdate('cascade'); 

            $table->foreign('parroquias_id')
                ->references('id')->on('parroquias')
                ->onDelete('cascade')->onUpdate('cascade'); 

            $table->foreign('sectores_id')
                ->references('id')->on('sectores')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('sucursal');
    }
}
