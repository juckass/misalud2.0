@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Empresa']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Empresa.',
        'columnas' => [
            'Rif' => '16.666666666667',
		'Nombre' => '16.666666666667',
		'Abreviatura' => '16.666666666667',
		'Tlf' => '16.666666666667',
		'Direccion' => '16.666666666667'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Empresa->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection